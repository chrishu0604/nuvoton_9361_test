/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright (c) Nuvoton Technology Corp. All rights reserved.                                             */
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include "ISD93xx.h"
#include "Driver\DrvSYS.h"
#include "Driver\DrvQSPI.h"
#include "Driver\DrvGPIO.h"
#include "Driver\DrvPDMA.h"

#include "NVTTypes.h"

/*---------------------*/
/* Constant Definition */
/*---------------------*/
#define SPI_MASTER

/*--------------------*/
/* Function Prototype */
/*--------------------*/


void InitialSPIPortMaster(void)
{
	/* Enable SPI0 function */
	//DrvGPIO_InitFunction(FUNC_SPI0);
	DrvGPIO_SPI_PA0_PA1_PA2_PA3();
	//DrvGPIO_SPI_MOSI1_PB6();
	//DrvGPIO_SPI_MISO1_PB5();
	
	/* Configure SPI0 as a master, 32-bit transaction */
	DrvSPI_Open(eDRVSPI_PORT0, eDRVSPI_MASTER, eDRVSPI_TYPE3, 8);
	//DrvSPI_SetSuspendCycle(eDRVSPI_PORT0, 0xf);   //Set syspend cycle
	//DrvSPI_Open(eDRVSPI_PORT0, eDRVSPI_MASTER, eDRVSPI_TYPE1, 8);
	
	/* Enable the automatic slave select function of SS0. */
	//DrvSPI_EnableAutoCS(eDRVSPI_PORT0, eDRVSPI_SS0);
	//DrvSPI_DisableAutoCS(eDRVSPI_PORT0);
	//DrvSPI_DisableAutoCS(eDRVSPI_PORT0);	  //Slave Select non-auto
	DrvSPI_IoMode(eDRVSPI_PORT0, eDRVSPI_STANDARD); 	 //Mode Selection

	//DrvSPI_Set2BitSerialDataIOMode(eDRVSPI_PORT0,1); //seperate SPI0 and SPI1

	/* Set the active level of slave select. */
	DrvSPI_SetSlaveSelectActiveLevel(eDRVSPI_PORT0, eDRVSPI_ACTIVE_LOW_FALLING);

	/* SPI clock rate 10MHz */
	DrvSPI_SetClock(eDRVSPI_PORT0, 10240000); 

	/* Enable the SPI0 interrupt and install the callback function. */
	//DrvSPI_EnableInt(eDRVSPI_PORT0, SPI0_Callback, 0);		

	DrvSPI_EnableSPITransfer(eDRVSPI_PORT0);
}

void InitialSPIPortSlave(void)
{
	/* Enable SPI0 function */
	//DrvGPIO_InitFunction(FUNC_SPI0);

	/* Configure SPI0 as a master, 32-bit transaction */
	DrvSPI_Open(eDRVSPI_PORT0, eDRVSPI_SLAVE, eDRVSPI_TYPE1, 32);

	
	/* Enable the automatic slave select function of SS0. */
	DrvSPI_EnableAutoCS(eDRVSPI_PORT0, eDRVSPI_SS0);

	/* Set the active level of slave select. */
	DrvSPI_SetSlaveSelectActiveLevel(eDRVSPI_PORT0, eDRVSPI_ACTIVE_LOW_FALLING);

	/* SPI clock rate 3MHz */
	DrvSPI_SetClock(eDRVSPI_PORT0, 1000000);

	/* Enable the SPI0 interrupt and install the callback function. */
	//DrvSPI_EnableInt(eDRVSPI_PORT0, SPI0_Callback, 0);
}
