#include "bsp.h"
#include "radio.h"
#include "ISD93xx.h"
#include "Driver\DrvADC.h"
#include "Ram.h"
#include "qdt_utils.h"

/**************************************************************/
/* extern function                                            */
/**************************************************************/
#ifdef USE_AUDIO_CODEC_G711
extern void playAudio(unsigned char *AudioRXBuffer);
#elif (defined USE_AUDIO_CODEC_G722)
extern void playAudio(signed short *AudioRXBuffer);
#else
extern void playAudio(signed short *AudioRXBuffer);
#endif
extern qdt_bool transmit_finish;

RETURN_TYPE QDT_Init(void)
{
	g_Data.ucStopToRx = qdt_false;
	g_Data.ucParentCalling = qdt_false;
	return QDT_SYS_NOERROR;
}



RETURN_TYPE QDT_ParseRFPackage(qdt_uint8 * ucRXBuffer)
{
  qdt_uint16 ucIndex = 0;
	qdt_uint16 ucRFPackageIndex = 0;
  qdt_uint8 ucChannelType;
  qdt_uint8 ucCommand;  	
#ifdef USE_AUDIO_CODEC_G711
  unsigned char AudioRXBuffer[720];   
#elif (defined USE_AUDIO_CODEC_G722)
	signed short AudioRXBuffer[40];	
#else
	signed short AudioRXBuffer[360];	
#endif	
	ucChannelType = ucRXBuffer[0];
	ucCommand = ucRXBuffer[1];
	
	if (ucChannelType == QDT_RFPACKAGE_AUDIO)
	{
		ucRFPackageIndex = 0;
#ifdef USE_AUDIO_CODEC_G711
		for(ucIndex = 1; ucIndex < 721; ucIndex++)	//handle audio package(package length is 42 bytes), combine 2 CHAR	into 1 SHORT SIGNED type. 
		{	
			AudioRXBuffer[ucRFPackageIndex] = ucRXBuffer[ucIndex+1];
			ucRFPackageIndex++;
		}
#elif (defined USE_AUDIO_CODEC_G722)
		for(ucIndex = 1; ucIndex < 21; ucIndex++)  //handle audio package(package length is 42 bytes), combine 2 CHAR  into 1 SHORT SIGNED type. 
		{	
			AudioRXBuffer[ucRFPackageIndex] = ucRXBuffer[2*ucIndex] | (ucRXBuffer[2*ucIndex+1]<<8) /*| (ucRXBuffer[4*ucIndex+2]<<16) | (ucRXBuffer[4*ucIndex+3]<<24)*/;
			ucRFPackageIndex++;
		}

#else
		for(ucIndex = 1; ucIndex < 360; ucIndex++)  //handle audio package(package length is 42 bytes), combine 2 CHAR  into 1 SHORT SIGNED type. 
		{	
			AudioRXBuffer[ucRFPackageIndex] = ucRXBuffer[2*ucIndex] | (ucRXBuffer[2*ucIndex+1]<<8) /*| (ucRXBuffer[4*ucIndex+2]<<16) | (ucRXBuffer[4*ucIndex+3]<<24)*/;
			ucRFPackageIndex++;
		}	
#endif
		playAudio(AudioRXBuffer);
	}
	else if (ucChannelType == QDT_RFPACKAGE_COMMAND)
	{
		//TO DOING: handle Parents Uint command.
		if (ucCommand == QDT_PARENTCOMMAND_CALLING)
			QDT_Parents_Calling();
		else if (ucCommand == QDT_PARENTCOMMAND_HANDUP)
			QDT_Parents_HandUp();
		
		 //g_Data.ucStopToRx = qdt_true;
	}
	else
	{
		return QDT_SYS_FAILED;
	}
	
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Parents_Calling(void)
{
	QDT_TRACE("QDT_Parents_Calling....\n");
	g_Data.ucParentCalling = qdt_true;
	DrvADC_PGAMute(eDRVADC_MUTE_PGA); //mute MIC
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Parents_HandUp(void)
{
	QDT_TRACE("QDT_Parents_HandUp....\n");
	g_Data.ucParentCalling = qdt_false;	
	DrvADC_PGAUnMute(eDRVADC_MUTE_PGA); //un-mute MIC
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_SentOut_Command(QdtCommand_e e_Command)
{
	qdt_uint8 ucIndex = 0;

	switch (e_Command)
	{
	case QDT_PARENTCOMMAND_CALLING:
		pRadioConfiguration->Radio_Custom_Long_Payload[0] = QDT_RFPACKAGE_COMMAND;
		pRadioConfiguration->Radio_Custom_Long_Payload[1] = QDT_PARENTCOMMAND_CALLING;
		break;
	case QDT_PARENTCOMMAND_HANDUP:
		pRadioConfiguration->Radio_Custom_Long_Payload[0] = QDT_RFPACKAGE_COMMAND;
		pRadioConfiguration->Radio_Custom_Long_Payload[1] = QDT_PARENTCOMMAND_HANDUP;
		break;		
	default:
		pRadioConfiguration->Radio_Custom_Long_Payload[0] = QDT_RFPACKAGE_AUDIO;
		pRadioConfiguration->Radio_Custom_Long_Payload[1] = QDT_NON_COMMAND;
		break;			
	}
	vRadio_StartTx(pRadioConfiguration->Radio_ChannelNumber, (U8 *) &pRadioConfiguration->Radio_Custom_Long_Payload[0u]);
	transmit_finish = qdt_false;
	return TRUE;
	
}




