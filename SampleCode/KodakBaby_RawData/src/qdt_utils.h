#ifndef __QDT_UTILS_H__
#define __QDT_UTILS_H__ 1

#include <stdio.h>

typedef unsigned char qdt_uint8;
typedef unsigned int qdt_uint16;
typedef unsigned long qdt_uint32;

typedef unsigned char qdt_bool;
#define qdt_true 1
#define qdt_false 0

typedef signed char RETURN_TYPE;
#define	QDT_SYS_NOERROR				0
#define	QDT_SYS_FAILED				-1

typedef enum {
	QDT_LEVEL_NONE = 0,
	QDT_LEVEL_ERROR,
	QDT_LEVEL_WARNING,
	QDT_LEVEL_DEBUG,
	QDT_LEVEL_TRACE,
	QDT_LEVEL_INFO,
	QDT_LEVEL_COUNT
} QdtDebugLevel;

#define __qdt_debug_min  QDT_LEVEL_TRACE
#define QDT_LEVEL_LOG(level,format,args...)                          \
    do{                                                       \
		char *levelstring [QDT_LEVEL_COUNT] = {"QDT_LEVEL_NONE", "QDT_LEVEL_ERROR",   \
		                                       "QDT_LEVEL_WARNING", "QDT_LEVEL_DEBUG", \
		                                       "QDT_LEVEL_TRACE", "QDT_LEVEL_INFO"}; \
        if (level <= __qdt_debug_min)                         \
        {                                                     \
			printf("[%s][%s:%d] " format, levelstring[level], __FILE__, __LINE__, ## args);  \
        }                                                     \
    }while(0)

#define QDT_ERROR(format,args...)    QDT_LEVEL_LOG (QDT_LEVEL_ERROR,   format,##args)
#define QDT_WARNING(format,args...)  QDT_LEVEL_LOG (QDT_LEVEL_WARNING, format,##args)
#define QDT_DEBUG(format,args...)    QDT_LEVEL_LOG (QDT_LEVEL_DEBUG,   format,##args)
#define QDT_TRACE(format,args...)    QDT_LEVEL_LOG (QDT_LEVEL_TRACE,   format,##args)
#define QDT_INFO(format,args...)     QDT_LEVEL_LOG (QDT_LEVEL_INFO,    format,##args)


/***********************************************************************/
/* Module Type Define                                                                                         */
/***********************************************************************/
//#define USE_AUDIO_CODEC_G711
#define USE_AUDIO_CODEC_G722

#define RADIO_TX_MODE
//#define RADIO_RX_MODE
//#define RADIO_TXRX_MODE

/***********************************************************************/
/* Receive Package Type                                                                                             */
/***********************************************************************/
typedef enum {
	QDT_RFPACKAGE_AUDIO = 0x01,
	QDT_RFPACKAGE_COMMAND = 0x02,
} QdtRFPackageType_e;

typedef enum {
	QDT_NON_COMMAND = 0x00,
	QDT_PARENTCOMMAND_CALLING = 0x01,
	QDT_PARENTCOMMAND_HANDUP  = 0x02,
} QdtCommand_e;


/*****************************************************************************/
/*  Global Function Declarations                                                                                                */
/*****************************************************************************/
RETURN_TYPE QDT_Init(void);

RETURN_TYPE QDT_ParseRFPackage(qdt_uint8 * ucRXBuffer);
RETURN_TYPE QDT_SentOut_Command(QdtCommand_e e_Command);


/*Audio Docking Funtion*/
RETURN_TYPE QDT_Parents_Calling(void);
RETURN_TYPE QDT_Parents_HandUp(void);


#endif  // #ifdef __QDT_UTILS_H__
