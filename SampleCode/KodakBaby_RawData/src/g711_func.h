/****************************************************************
 *                                                              *
 * Copyright (c) Nuvoton Technology Corp. All rights reserved.  *
 *                                                              *
 ****************************************************************/
#ifndef _G711_FUNC_H
#define	_G711_FUNC_H

extern void g711_linear2alaw(short *speech, char *bitstream, unsigned short u16Len);	
extern void g711_linear2ulaw(short *speech, char *bitstream, unsigned short u16Len);
extern void g711_alaw2linear(char *bitstream,short *speech, unsigned short u16Len);
extern void g711_ulaw2linear(char *bitstream,short *speech, unsigned short u16Len);
extern void g711_alaw2ulaw(char *alaw_val, char *ulaw_val, unsigned short u16Len);
extern void g711_ulaw2alaw(char *ulaw_val, char *alaw_val, unsigned short u16Len);

#endif 














