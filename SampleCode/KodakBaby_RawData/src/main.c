/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright(c) 2009 Nuvoton Technology Corp. All rights reserved.                                         */
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "ISD93xx.h"
#include "Driver\DrvUART.h"
#include "Driver\DrvGPIO.h"
#include "Driver\DrvSYS.h"
#include "Driver\DrvQSPI.h"
#include "Driver\DrvPDMA.h"
#include "Driver\DrvADC.h"
#include "Driver\DrvI2C.h"
#include "NVTTypes.h"

#include "bsp.h"
#include "radio.h"
#include "Ram.h"
#include "qdt_utils.h"


//#include "si446x_api_lib.h"

/*---------------------------------------------------------------------------------------------------------*/
/* Extern Function Prototypes                                                                              */
/*---------------------------------------------------------------------------------------------------------*/
extern void InitialSPIPortMaster(void);
extern void PlayBufferSet(void);

/*---------------------------------------------------------------------------------------------------------*/
/* Define Function Prototypes                                                                              */
/*---------------------------------------------------------------------------------------------------------*/
void InitialUART(void);
void RecordStart(void);
void PlayStart(void);

void S7Init(void);
void S7EncDec(void);

void SysTimerDelay(uint32_t us);
void Delay(uint32_t delayCnt);


void RF446x_Pollhandler(void);
BIT vSampleCode_SendFixPacket(void);


/*------------------------------------------------------------------------*/
/*                            Local Macros                                */
/*------------------------------------------------------------------------*/
#define PACKET_SEND_INTERVAL  5000u

/*------------------------------------------------------------------------*/
/*                          Local variables                               */
/*------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------------------------------------*/
/* Define global variables                                                                                 */
/*---------------------------------------------------------------------------------------------------------*/
GlobalData g_Data;

extern volatile uint32_t CallBackCounter;	//in RecordPCM.c
extern qdt_bool	bMicBufferReady;				//in RecordPCM.c
extern qdt_bool bPdmaTxDone;
//extern qdt_bool bPdmaRxDone;
//extern qdt_bool bPdmaRxDone, bPdmaTxDone, bIsTestOver;

#ifdef USE_AUDIO_CODEC_G711
extern volatile __align(4) BYTE s8Out_bytes[720];
#elif (defined USE_AUDIO_CODEC_G722)
extern volatile __align(4) signed short s16Out_words[40];
#else
extern volatile __align(4) signed short s16Out_words[40];
#endif

uint8_t bMain_IT_Status;
uint16_t lPer_MsCnt;


static qdt_bool ucPressBTN = qdt_false;
qdt_bool transmit_finish = qdt_true;
qdt_bool bTransmitBufferIsempty = qdt_true;
uint16_t ucTransmitBufferIdx = 0;
#ifdef USE_AUDIO_CODEC_G711
unsigned char AudioRXBuffer[720];
#elif (defined USE_AUDIO_CODEC_G722)
signed short AudioRXBuffer[40];
#endif
//====================
// Functions & main

void interrupt_callback_function(uint32_t u32GpaStatus, uint32_t u32GpbStatus)
{

    //printf("GPIO Wake up from Standby-Power-Down Mode\n");
	GPIOB->ISRC = GPIOB->ISRC;
	GPIOA->ISRC = GPIOA->ISRC;

	//si446x_get_int_status(0u, 0u, 0u);
	//gRadio_CheckTransmitted();
	gRadio_CheckTxRxStatus();

}

void InitGPIOWakeupInSPD(void)
 {	
	GPIOA->ISRC = GPIOA->ISRC;
	GPIOB->ISRC = GPIOB->ISRC;
	////INT from GPA pin1 as an example wake up
    //DrvGPIO_SetIntCallback(interrupt_callback_CheckRF_function);
    DrvGPIO_SetIntCallback(interrupt_callback_function);
    DrvGPIO_SetDebounceTime(3, DBCLKSRC_HCLK);
    DrvGPIO_EnableDebounce(GPB, 7);
	DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
	//DrvGPIO_DisableInt(GPB, 7);  						    //disable RF-4461 interrupt Pin
    //DrvGPIO_EnableInt(GPA, 4, IO_FALLING, MODE_EDGE);      //enable power key interrupt Pin
    //DrvGPIO_EnableInt(GPB, 6, IO_FALLING, MODE_EDGE);      //enable PTT key interrupt Pin
}
void UartInit(void)
{
	STR_UART_T sParam;
	//DrvGPIO_InitFunction(FUNC_UART0);
    //DrvGPIO_InitFunction(FUNC_UART0_FLOW);
    DrvGPIO_UART_TXRX_PA8_PA9();
	//DrvGPIO_UART_RTSCTS_PA10_PA11();
	
    sParam.u32BaudRate 		= 230400;
    sParam.u8cDataBits 		= DRVUART_DATABITS_8;
    sParam.u8cStopBits 		= DRVUART_STOPBITS_1;
    sParam.u8cParity 		= DRVUART_PARITY_NONE;
    sParam.u8cRxTriggerLevel= DRVUART_FIFO_4BYTES;
	//sParam.u8TimeOut 			= 0;

	if(DrvUART_Open(UART_PORT0,&sParam) == 0) 
	{
		DrvUART_EnableInt(UART_PORT0, DRVUART_RDAINT,interrupt_callback_function);
		UART0->IER.AUTO_CTS_EN = 1;
     	//UART0->IER.AUTO_RTS_EN = 1;

	}
//while(1)
//	DrvUART_Write(UART_PORT0, ucTest, 1);
	printf("\n\n\n\n\nAUDIO DOCKING MCU BUILD: %s %s\n", __TIME__, __DATE__);
	printf("#################### UART_DEBUG_READY ####################\n");
}


void GpioInit(void)
{
#ifdef RADIO_TX_MODE
	DrvGPIO_Open(GPA,15, IO_OUTPUT); //RF-Si4463 shoutdown PIN
	DrvGPIO_Open(GPB,1, IO_OUTPUT); //RF-Si4463 SPI SSB PIN
	DrvGPIO_Open(GPB,7, IO_INPUT); //RF-Si4463 nIRQ PIN	
	DrvGPIO_Open(GPB,2, IO_OUTPUT); //PWR_CTRL PIN


	//DrvGPIO_Open(GPB,5, IO_INPUT); //for testing ONE SPI path
	//DrvGPIO_Open(GPB,6, IO_INPUT); //for testing ONE SPI path
	DrvGPIO_Open(GPB,5, IO_OUTPUT); //for testing ONE SPI path
	DrvGPIO_Open(GPB,6, IO_OUTPUT); //for testing ONE SPI path
	
	DrvGPIO_Open(GPA,13, IO_INPUT); //CM36686 INT PIN
	DrvGPIO_Open(GPB,4, IO_INPUT); //Power key
	DrvGPIO_Open(GPA,5, IO_INPUT); //volume up key
	DrvGPIO_Open(GPB,2, IO_INPUT); //volume down key
	DrvGPIO_Open(GPB,11, IO_INPUT); //Pairing/Page key
	DrvGPIO_Open(GPA,14, IO_OUTPUT); //Power LED
	DrvGPIO_Open(GPB,0, IO_OUTPUT);  //low battery LED
	
	DrvGPIO_Open(GPB,15, IO_OUTPUT); //VS100x-mp3 decoder  reset pin.
	DrvGPIO_Open(GPB,14, IO_OUTPUT); //VS100x-mp3 decoder  XDSC pin.
	DrvGPIO_Open(GPB,13, IO_OUTPUT); //VS100x-mp3 decoder  XDSC pin.

	DrvGPIO_Open(GPB,8, IO_INPUT); //DC_PlugIn_Det.
	DrvGPIO_Open(GPB,10, IO_OUTPUT); //RF PWR_CTRL
	

	SYS->GPA_ALT.GPA2              =0;  //Disable 9160 SPI CS funtion. it be controled at qdt_flash_hal.c
	DrvGPIO_Open(GPA,2, IO_OUTPUT); //SPI FLASH SSB0
	QDT_TRACE("GPIO Init successful\n");

	//AUDIODOCKING_LED_PWR_RED(qdt_ON);
	//AUDIODOCKING_LED_PWR_GREEN(qdt_OFF);
	//AUDIODOCKING_RF_446x_PER_CTRL(qdt_OFF);
	//AUDIODOCKING_RF_446x(qdt_OFF);

	//AUDIODOCKING_CAMERA_PWR(qdt_OFF);			//Power down A500-camera

	//QDT_AD_ResetMP3();
	DrvGPIO_ClrBit(GPB,15);      //tmp code. for mp3-vs1053
	//AUDIODOCKING_A500_PWR(qdt_ON);
	DrvGPIO_Open(GPB,5, IO_OUTPUT); //PTT(push to talk key) 
    DrvGPIO_Open(GPB,6, IO_OUTPUT); //Alert key(play alert sound)	
    
#ifdef USE_AKM
        /*Trigger AKM reset pin*/
	DrvGPIO_Open(GPB,12, IO_OUTPUT); //AKM reset ping
	DrvGPIO_ClrBit(GPB,12);
	QDT_Common_DelayTime(100);
	DrvGPIO_SetBit(GPB,12);

	/*QDT_Common_DelayTime(5);
	DrvGPIO_Open(GPB,12, IO_OUTPUT); //AKM reset ping
	DrvGPIO_SetBit(GPB,12);
	QDT_Common_DelayTime(10);
	DrvGPIO_ClrBit(GPB,12);
	QDT_Common_DelayTime(1);
	DrvGPIO_SetBit(GPB,12);
	QDT_Common_DelayTime(5);*/
#endif	

	DrvGPIO_Open(GPA,12, IO_OUTPUT);
	//AUDIODOCKING_LED_NIGHTLIGHT(qdt_OFF);

		DrvGPIO_ClrBit(GPB,5); 
    DrvGPIO_ClrBit(GPB,6); 

	
#ifdef USE_SWITCH
	AUDIODOCKING_SPK_SWITCH(qdt_OFF);
#endif
#endif	



#ifdef RADIO_RX_MODE
	DrvGPIO_Open(GPA,15, IO_OUTPUT); //RF-Si4463 shoutdown PIN
	DrvGPIO_Open(GPB,1, IO_OUTPUT); //RF-Si4463 SPI SSB PIN
	DrvGPIO_Open(GPB,7, IO_INPUT); //RF-Si4463 nIRQ PIN
	DrvGPIO_Open(GPB,2, IO_OUTPUT); //PWR_CTRL PIN

	DrvGPIO_Open(GPA,4, IO_INPUT); //power key 
	DrvGPIO_Open(GPA,5, IO_INPUT); //volume up 
	DrvGPIO_Open(GPA,6, IO_INPUT); //volume down 
	DrvGPIO_Open(GPA,7, IO_INPUT); //low battery 
	//DrvGPIO_Open(GPA,10, IO_INPUT); //PTT(push to talk key)  revB
    //DrvGPIO_Open(GPA,11, IO_INPUT); //Alert key(play alert sound) revB

	DrvGPIO_Open(GPB,5, IO_INPUT); //PTT(push to talk key) 
    DrvGPIO_Open(GPB,6, IO_INPUT); //Alert key(play alert sound)	
    DrvGPIO_Open(GPA,10, IO_INPUT); //DC_DET

	DrvGPIO_Open(GPA,12, IO_OUTPUT); // Intensity LED_G1
	DrvGPIO_Open(GPB,13, IO_OUTPUT); // Intensity LED_R1
	DrvGPIO_Open(GPB,14, IO_OUTPUT); // Intensity LED_G2
	DrvGPIO_Open(GPB,15, IO_OUTPUT); // Intensity LED_R2
	
	DrvGPIO_Open(GPA,13, IO_OUTPUT); //Alert LED
	DrvGPIO_Open(GPA,14, IO_OUTPUT); //Power LED

	DrvGPIO_Open(GPB,0, IO_OUTPUT); //low battery LED
	DrvGPIO_Open(GPB,3, IO_OUTPUT); //Vibrator

	//DrvGPIO_SetBit(GPA,12);//Intensity LED tune on for CES		
	//DrvGPIO_SetBit(GPA,13);//Alert LED tune on for CES		
	//DrvGPIO_ClrBit(GPA,14);//Power Light tune on for CES		
	//DrvGPIO_ClrBit(GPB,0);//ow battery LED tune on for CES	

	SYS->GPA_ALT.GPA2              =0;  //Disable 9160 SPI CS funtion. it be controled at qdt_flash_hal.c
	DrvGPIO_Open(GPA,2, IO_OUTPUT); //SPI FLASH SSB0
	QDT_TRACE("GPIO Init successful\n");

	DrvGPIO_ClrBit(GPB,3);

	DrvGPIO_ClrBit(GPB,0);
	DrvGPIO_ClrBit(GPA,15);
#endif

	QDT_TRACE("GPIO Init successful\n");
}

void LdoOn(void)
{
	SYSCLK->APBCLK.ANA_EN=1;
	ANA->LDOPD.PD=0;
	ANA->LDOSET=3;
}

#if 0
void InitialI2C(void)
{
	/*  GPIO initial and select operation mode for I2C*/
    DrvGPIO_InitFunction(FUNC_I2C0); //Set I2C I/O
	//DrvI2C_Open(I2C_PORT0, (DrvSYS_GetHCLK() * 1000), 12000);  //clock = 24Kbps
	DrvI2C_Open(I2C_PORT0, (DrvSYS_GetHCLK() * 1000), 48000);  //clock = 48Kbps
	DrvI2C_EnableInt(I2C_PORT0); //Enable I2C0 interrupt and set corresponding NVIC bit
}
#endif
/*---------------------------------------------------------------------------------------------------------*/
/* SysTimerDelay                                                                                           */
/*---------------------------------------------------------------------------------------------------------*/
void SysTimerDelay(uint32_t us)
{
#ifdef USE_SYSTEM_CLOCK_48MHZ	
    SysTick->LOAD = us * 48; /* Assume the internal 49MHz RC used */
#endif
#ifdef USE_SYSTEM_CLOCK_98MHZ	
    SysTick->LOAD = us * 98; /* Assume the internal 49MHz RC used */
#endif
    SysTick->VAL  =  (0x00);
    SysTick->CTRL = (1 << SYSTICK_CLKSOURCE) | (1<<SYSTICK_ENABLE);

    /* Waiting for down-count to zero */
    while((SysTick->CTRL & (1 << 16)) == 0);
}

void InitialSystemClock(void)
{
#if 0
    /* Unlock the protected registers */	
	UNLOCKREG();
	SYSCLK->PWRCON.OSC49M_EN = 1;
	SYSCLK->CLKSEL0.HCLK_S = 0; /* Select HCLK source as 48MHz */ 
	SYSCLK->CLKDIV.HCLK_N  = 0;	/* Select no division          */
	SYSCLK->CLKSEL0.OSCFSel = 0;	/* 1= 32MHz, 0=48MHz */
	//SYSCLK->CLKSEL0.STCLK_S = 3; /* Use internal HCLK */
	SYSCLK->PWRCON.XTL32K_EN = 1;
#endif
#if 1
	DrvSYS_UnlockKeyAddr();
    DrvSYS_SetOscCtrl(E_SYS_OSC49M, 1);
    DrvSYS_SetOscCtrl(E_SYS_OSC10K, 1);
    DrvSYS_SetOscCtrl(E_SYS_XTL32K, 1);
#ifdef USE_SYSTEM_CLOCK_48MHZ	
	DrvSYS_SetHCLK(E_DRVSYS_48M, 1);
#endif
#ifdef USE_SYSTEM_CLOCK_98MHZ	
	DrvSYS_SetHCLK(E_DRVSYS_96M, 1);
#endif

	DrvSYS_LockKeyAddr();	
#endif
}

/*---------------------------------------------------------------------------------------------------------*/
/*  Main Function									                                           			   */
/*---------------------------------------------------------------------------------------------------------*/
int32_t main (void)
{
	uint32_t aliveCounter = 0;
	uint32_t aliveCounter2 = 0;
	uint8_t  lPktSending = 0;

	
	
	InitialSystemClock();

	DrvADC_AnaOpen();
	LdoOn();  					//Enable GPA0~GPA7 power if no external power providing
	

	
	/* Set UART Configuration */
	//UartInit();
	//printf("\n\n\n\n\nMCU BUILD: %s %s\n", __TIME__, __DATE__);
	//printf("#################### UART_DEBUG_READY ####################\n");
	
	QDT_Init();
    /* Set GPIO Configuration */
	GpioInit();

	/* Set PDMI Configuration*/
//	DrvPDMA_Init();			//PDMA initialization

	/* Set SPI Configuration */
	InitialSPIPortMaster(); 	//Set SPI as master

	//InitialI2C();
	
	InitCalibrateOSC();
	IsCountingStop();
	
	//PDMA_SPI(); 	
	//bPdmaRxDone=FALSE;
	//bPdmaTxDone=FALSE;
	
#ifdef USE_AUDIO_CODEC_G722
	S7Init();
#endif	
	vRadio_Init();//init RF

//printf("\n=== 16K sampling PCM Recording to DataFlash  ===\n");
	RecordStart();
	while(CallBackCounter == 0)	;
	



	PlayBufferSet();
	S7EncDec();

	
	

	while(CallBackCounter == 1)	;
		
	//printf("\n=== Play PCM from DataFlash ===\n");
	PlayStart();

#if  defined(RADIO_RX_MODE) || defined(RADIO_TXRX_MODE)  
	vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);
#endif
    //DrvGPIO_ClrBit(GPA, 14);	
	//DrvGPIO_ClrBit(GPA, 5);
    InitGPIOWakeupInSPD();
	while(1)
	{
#if 0
if (bMicBufferReady==TRUE)
	S7EncDec();
#else
#if  defined(RADIO_TX_MODE) //|| defined(RADIO_TXRX_MODE)  
		if (bMicBufferReady==TRUE && transmit_finish)
	{
		DrvGPIO_SetBit(GPB,5); 
		S7EncDec();
		DrvGPIO_ClrBit(GPB,5); 
	}
	
	if (transmit_finish && !bTransmitBufferIsempty && g_Data.ucParentCalling == qdt_false)
	{   
	  //DrvGPIO_SetBit(GPA, 14);
		transmit_finish = qdt_false;
	    DrvGPIO_SetBit(GPB,6); 
		vSampleCode_SendFixPacket();
	}
#endif
#if  defined(RADIO_RX_MODE) //|| defined(RADIO_TXRX_MODE)  
		
				//if (bMicBufferReady==TRUE && transmit_finish)
				//{
				//	S7EncDec();
				//}
				
				//if (transmit_finish && g_Data.ucParentCalling == qdt_false)
				//{	
					//DrvGPIO_SetBit(GPA, 14);
				//	vSampleCode_SendFixPacket();
				//	transmit_finish = qdt_false;
				//}
			
		//if (transmit_finish)
		//		vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);
#if 0		
		if (!DrvGPIO_GetBit(GPA, 14)&& !ucPressBTN  && transmit_finish)
		{
			QDT_SentOut_Command(QDT_PARENTCOMMAND_CALLING);
			ucPressBTN = qdt_true;
		}
		else if (DrvGPIO_GetBit(GPA, 14) && ucPressBTN)
		{
			//QDT_SentOut_Command(QDT_PARENTCOMMAND_HANDUP);
			ucPressBTN = qdt_false;
		}		
#endif		


#endif	

 //       RF446x_Pollhandler();

#endif


	    if(IsCountingStop()==TRUE)            //Calibrate the OSC freq to 49.152MHz
		StartPclkCount();
	}

	/* Lock protected registers */
	
}


/*!
 * This function is used to send "ACK"back to the sender.
 *
 * @return  None.
 */
void vSampleCode_SendAcknowledge(void)
{
#if 0
  // Payload
  customRadioPacket[0u] = 'A';
  customRadioPacket[1u] = 'C';
  customRadioPacket[2u] = 'K';

  // 3 bytes sent to TX FIFO
  vRadio_StartTx_Variable_Packet(pRadioConfiguration->Radio_ChannelNumber, &customRadioPacket[0], 3u);
#endif  
}
/*!
 * This function is used to send the custom packet.
 *
 * @return  None.
 */
BIT gSampleCode_SendVariablePacket(void)
{
 int i = 0;
#if 0
  SEGMENT_VARIABLE(boPbPushTrack,  U8, SEG_DATA);
  SEGMENT_VARIABLE(lTemp,         U16, SEG_DATA);
  SEGMENT_VARIABLE(pos,            U8, SEG_DATA);

  //gHmi_PbIsPushed(&boPbPushTrack, &lTemp);
  
  //if( boPbPushTrack & eHmi_Pb1_c)
  { 
    //for (i=0;i<40;i++)
    {

  		for(pos = 0u; pos < 40; pos++)
  		{
  			//printf("s16Out_words[%d] = %x \n",pos, s16Out_words[pos]);
			//printf("s16Out_words[pos]>>24 = %x \n",s16Out_words[pos]>>24);
			//printf("s16Out_words[pos]>>16 = %x \n",s16Out_words[pos]>>16);
			//printf("s16Out_words[pos]>>8= %x \n",s16Out_words[pos]>>8);
			//printf("s16Out_words[pos] = %x \n",s16Out_words[pos]& 0xFF);
    		customRadioPacket[4*pos+3] = (s16Out_words[pos]>>24) & 0xFF;;
			customRadioPacket[4*pos+2] = (s16Out_words[pos]>>16) & 0xFF;
			customRadioPacket[4*pos+1] = (s16Out_words[pos]>>8) & 0xFF;;
			customRadioPacket[4*pos] = s16Out_words[pos] & 0xFF;;
			//printf("customRadioPacket[%d] = %x \n",4*pos+3, customRadioPacket[4*pos+3]);
			//printf("customRadioPacket[%d] = %x \n",4*pos+2, customRadioPacket[4*pos+2]);
			//printf("customRadioPacket[%d] = %x \n",4*pos+1, customRadioPacket[4*pos+1]);
			//printf("customRadioPacket[%d] = %x \n",4*pos, customRadioPacket[4*pos]);
			
			//Buffer[ucTransmitBufferIdx] = customRadioPacket[4*pos] | (customRadioPacket[4*pos+1]<<8) | (customRadioPacket[4*pos+2]<<16) | (customRadioPacket[4*pos+3]<<24);

			//ucTransmitBufferIdx++;
  		}

		//for(pos = 0; pos < 10; pos++)
		//{
			//printf("!customRadioPacket[%d] = %x \n",4*pos+3, customRadioPacket[4*pos+3]<<24);
			//printf("!customRadioPacket[%d] = %x \n",4*pos+2, customRadioPacket[4*pos+2]<<16);
			//printf("!customRadioPacket[%d] = %x \n",4*pos+1, customRadioPacket[4*pos+1]<<8);
			//Buffer[ucTransmitBufferIdx] = customRadioPacket[4*pos] | (customRadioPacket[4*pos+1]<<8) | (customRadioPacket[4*pos+2]<<16) | (customRadioPacket[4*pos+3]<<24);
			//printf("!customRadioPacket[%d] = %x \n",4*pos, customRadioPacket[4*pos]);		
			//printf("Buffer[%d] = %x \n",pos, Buffer[pos]);
		//}
		
    	//QDT_DEBUG("pRadioConfiguration->Radio_PacketLength = %d\n",pRadioConfiguration->Radio_PacketLength);
		//SysTimerDelay(1000);
		
		vRadio_StartTx_Variable_Packet(pRadioConfiguration->Radio_ChannelNumber, &customRadioPacket[0], 160);
		//if (ucTransmitBufferIdx >= 40) 
		//{
		//	ucTransmitBufferIdx = 0;
		//	bTransmitBufferIsempty = qdt_true;
			
			//playAudio(Buffer);		
		//}
    }
	
   	/* Packet sending initialized */
   	return TRUE;
   }

 
#endif
	   return FALSE;
}


/*!
 * This function is used to compare the content of the received packet to a string.
 *
 * @return  None.
 */
BIT gSampleCode_StringCompare(U8* pbiPacketContent, U8* pbiString, U8 lenght)
{
  while ((*pbiPacketContent++ == *pbiString++) && (lenght > 0u))
  {
    if( (--lenght) == 0u )
    {
      return TRUE;
    }
  }

  return FALSE;
}
BIT vSampleCode_SendFixPacket(void)
{
  qdt_uint16 ucPayloadIndex = 0;
  qdt_uint16 ucEncodeBufferIndex = 0;
  pRadioConfiguration->Radio_Custom_Long_Payload[0] = QDT_RFPACKAGE_AUDIO;
  pRadioConfiguration->Radio_Custom_Long_Payload[1] = QDT_NON_COMMAND;
  
#ifdef USE_AUDIO_CODEC_G711
	  for(ucPayloadIndex = 1; ucPayloadIndex < 721; ucPayloadIndex++)
	  {
		pRadioConfiguration->Radio_Custom_Long_Payload[ucPayloadIndex+1] = s8Out_bytes[ucEncodeBufferIndex];
		//pRadioConfiguration->Radio_Custom_Long_Payload[2*ucPayloadIndex+1] = ucEncodeBufferIndex>>8;
		//pRadioConfiguration->Radio_Custom_Long_Payload[2*ucPayloadIndex] = ucEncodeBufferIndex&0xFF;
		ucEncodeBufferIndex++;
	   }
#elif (defined USE_AUDIO_CODEC_G722)
  for(ucPayloadIndex = 1; ucPayloadIndex < 21; ucPayloadIndex++)
  {
	  //pRadioConfiguration->Radio_Custom_Long_Payload[4*ucIndex+3] = (s16Out_words[ucIndex]>>24) & 0xFF;;
	  //pRadioConfiguration->Radio_Custom_Long_Payload[4*ucIndex+2] = (s16Out_words[ucIndex]>>16) & 0xFF;
	  pRadioConfiguration->Radio_Custom_Long_Payload[2*ucPayloadIndex+1] = (s16Out_words[ucEncodeBufferIndex]>>8) & 0xFF;;
	  pRadioConfiguration->Radio_Custom_Long_Payload[2*ucPayloadIndex] = s16Out_words[ucEncodeBufferIndex] & 0xFF;;
	  ucEncodeBufferIndex++;
  }
#else
  for(ucPayloadIndex = 1; ucPayloadIndex < 360; ucPayloadIndex++)
  {
	  pRadioConfiguration->Radio_Custom_Long_Payload[2*ucPayloadIndex+1] = (s16Out_words[ucEncodeBufferIndex]>>8) & 0xFF;;
	  pRadioConfiguration->Radio_Custom_Long_Payload[2*ucPayloadIndex] = s16Out_words[ucEncodeBufferIndex] & 0xFF;;
	  ucEncodeBufferIndex++;
  }

#endif
	#if 0
#ifdef USE_AUDIO_CODEC_G711
		ucTransmitBufferIdx = 0;
		for(ucPayloadIndex = 1; ucPayloadIndex < 241; ucPayloadIndex++)
		{
			AudioRXBuffer[ucTransmitBufferIdx] = pRadioConfiguration->Radio_Custom_Long_Payload[ucPayloadIndex+1];
			ucTransmitBufferIdx++;
		}
		playAudio(AudioRXBuffer);
#else
	ucTransmitBufferIdx = 0;
	for(ucPayloadIndex = 1; ucPayloadIndex < 41; ucPayloadIndex++)
	{
  		AudioRXBuffer[ucTransmitBufferIdx] = pRadioConfiguration->Radio_Custom_Long_Payload[2*ucPayloadIndex] | (pRadioConfiguration->Radio_Custom_Long_Payload[2*ucPayloadIndex+1]<<8)/* | (pRadioConfiguration->Radio_Custom_Long_Payload[4*ucIndex+2]<<16) | (pRadioConfiguration->Radio_Custom_Long_Payload[4*ucIndex+3]<<24)*/;
		ucTransmitBufferIdx++;
	}
    playAudio(AudioRXBuffer);
#endif
	#endif
//  DrvGPIO_SetBit(GPB, 0);
  vRadio_StartTx(pRadioConfiguration->Radio_ChannelNumber, (U8 *) &pRadioConfiguration->Radio_Custom_Long_Payload[0u]);
  /* Packet sending initialized */
  return TRUE;
}


/**
 *  Demo Application Poll-Handler
 *
 *  @note This function must be called periodically.
 *
 */
U8 BufferIdx = 0;
signed short RxBuffer[40]; 
void RF446x_Pollhandler(void)
{

    //gRadio_CheckTxRxStatus();
#if 1  
#if  defined(RADIO_TX_MODE) || defined(RADIO_TXRX_MODE)  
  	// Check if the radio packet sent successfully
  	if (TRUE == gRadio_CheckTransmitted())
  	{
    	/* Blink once LED1 to show Packet Transmission has been done. */
    	/* Clear Packet Sending flag */
		transmit_finish = qdt_true;

		//DrvGPIO_ClrBit(GPA, 14);  //debug pin for measure RF transmit timing.
DrvGPIO_ClrBit(GPB, 0);
		
		#if 0
		{
  	qdt_uint8 ucIndex = 0;
	  qdt_uint8 ucRFPackageIndex = 0;			
		ucRFPackageIndex = 0;
		for(ucIndex = 1; ucIndex < 21; ucIndex++)
		{
  			AudioRXBuffer[ucRFPackageIndex] = pRadioConfiguration->Radio_Custom_Long_Payload[2*ucIndex] | (pRadioConfiguration->Radio_Custom_Long_Payload[2*ucIndex+1]<<8)/* | (pRadioConfiguration->Radio_Custom_Long_Payload[4*ucIndex+2]<<16) | (pRadioConfiguration->Radio_Custom_Long_Payload[4*ucIndex+3]<<24)*/;
			ucRFPackageIndex++;
		}
    	playAudio(AudioRXBuffer);
	  }
		#endif

	
  	}
#endif

#if  defined(RADIO_RX_MODE) || defined(RADIO_TXRX_MODE)  
  	// Check if radio packet received
		//  	if (TRUE == gRadio_CheckTransmitted())
		//			transmit_finish = qdt_true;
  	//if (!g_Data.ucStopToRx)
  	{
  	if (TRUE == gRadio_CheckReceived())
  	{	
  		QDT_ParseRFPackage(&fixRadioPacket[0]);
  	}
  		}
#endif  
#endif
}


