#ifdef USE_AKM
#include <stdio.h>
#include "ISD93xx.h"
#include "Driver\DrvI2C.h"
#include "Driver\DrvGPIO.h"
#include "AK7755Setup.h"
#include "qdt_utils.h"
#include "qdt_common_func.h"
#include "qdt_flash_hal.h"


//unsigned char AK7755_SlaveAddr = 0x18;	 			/* AK7755 Device ID */
unsigned char Tx_Data[2];
unsigned char Pram_Data[SIZE_OF_PRAM];
unsigned char Cram_Data[SIZE_OF_CRAM];
//uint8_t Rx_Data_High;
//uint8_t Rx_Data_Low;
uint8_t Rx_Data;
uint8_t CramDataBuf[50];
uint8_t PramDataBuf[200];
uint8_t CramDataBufLen;
uint8_t PramDataBufLen;
uint8_t SingleDataLen;
uint8_t PramDataLen;
uint8_t CramDataLen;
uint8_t PramDataLenTransfer;
uint8_t CramDataLenTransfer;
volatile uint8_t SingleEndFlag = 0;
volatile uint8_t PramEndFlag = 0;
volatile uint8_t CramEndFlag = 0;

#if 0
#if 0 //3words_segmented
unsigned char ak77dspPRAM[AK77XX_DSP_PRAM_BUF_SIZE] = {
	0xB8, 0x00, 0x00,		// command code and address
	0x0C, 0x94, 0xED, 0x1E, 0x74,
	0x04, 0xA8, 0x9C, 0x6D, 0x55,
	0x00, 0x44, 0x02, 0x11, 0x99,
	0x0C, 0x94, 0xED, 0x1E, 0x64,
	0x08, 0xA9, 0x1E, 0x6D, 0x56,
	0x0C, 0x44, 0x00, 0x11, 0x8E,
	0x00, 0x94, 0xED, 0x1E, 0x69,
	0x08, 0xA9, 0x1C, 0x6D, 0x5C,
	0x0E, 0x44, 0x06, 0x18, 0x04,
	0x00, 0x94, 0xED, 0x1E, 0x7C,
	0x08, 0xF9, 0x9C, 0x6D, 0x5C,
	0x00, 0x44, 0x02, 0x51, 0x8C,
	0x00, 0x94, 0xED, 0x1A, 0x4D,
	0x08, 0xA9, 0x1C, 0x6B, 0x56,
	0x0C, 0x44, 0x02, 0x97, 0xB4,
	0x08, 0x14, 0x49, 0x92, 0x7C,
	0x08, 0xA9, 0x18, 0xED, 0x5C,
	0x0C, 0x44, 0x06, 0x91, 0x84,
	0x00, 0xDC, 0x6A, 0x9C, 0xCD,
	0x08, 0xE0, 0x98, 0x6F, 0xEC,
	0x0E, 0x0D, 0x86, 0x17, 0xCE,
	0x02, 0xDD, 0x68, 0x12, 0x3C,
	0x0A, 0xE0, 0x98, 0xE1, 0x5C,
	0x0C, 0x8C, 0xA5, 0x93, 0x35,
	0x00, 0x5D, 0x49, 0x1C, 0xCC,
	0x0A, 0x60, 0xB8, 0x6B, 0x56,
	0x0E, 0x8D, 0xA7, 0x1D, 0xC4,
	0x02, 0x5D, 0x49, 0x92, 0x7C,
	0x08, 0xE1, 0x9B, 0xEF, 0xED,
	0x0C, 0x0D, 0x86, 0x13, 0x34,
	0x02, 0xDD, 0x69, 0x18, 0x36,
	0x08, 0xE0, 0x91, 0x6D, 0x5C,
	0x0C, 0x0D, 0x82, 0x11, 0x84,
	0x00, 0x94, 0xED, 0x1E, 0x7C,
	0x08, 0xA9, 0x1C, 0x69, 0x6D,
	0x0C, 0x44, 0x02, 0x17, 0x8E,
	0x00, 0x94, 0xED, 0x98, 0x4C,
	0x08, 0xA9, 0x1C, 0x69, 0x7C,
	0x03, 0x44, 0x02, 0x11, 0x84
};
#else
const unsigned char ak77dspPRAM[AK77XX_DSP_PRAM_BUF_SIZE] = {
	0xB8, 0x00, 0x00,		// command code and address
	0x0C, 0x94, 0xED, 0x1E, 0x74,
	0x04, 0xA9, 0x1C, 0x6D, 0x55,
	0x00, 0x44, 0x02, 0x11, 0x99,
	0x09, 0x58, 0x6F, 0x4D, 0xD0,
	0x01, 0x5A, 0x7C, 0xC9, 0x27,
	0x09, 0x35, 0x31, 0x01, 0x86,
	0x03, 0x4B, 0xFA, 0x2C, 0x60,
	0x06, 0x78, 0xFB, 0xA0, 0xD6,
	0x0F, 0x82, 0xD3, 0xDC, 0xC0,
	0x05, 0x79, 0x77, 0x03, 0x9D,
	0x07, 0xFE, 0x22, 0x43, 0x38,
	0x02, 0xD9, 0xA1, 0x9E, 0x17,
	0x00, 0x7B, 0xE2, 0xE2, 0x70,
	0x09, 0xDA, 0x3C, 0xFF, 0xBB,
	0x02, 0x38, 0xDA, 0x3E, 0xA8,
	0x00, 0x84, 0x87, 0x85, 0xCA,
	0x00, 0xDE, 0x9F, 0x11, 0x42,
	0x04, 0xFD, 0x96, 0xDB, 0xF2,
	0x0A, 0x2E, 0x84, 0x9B, 0xF7,
	0x07, 0xBD, 0xDC, 0xE9, 0x9C,
	0x03, 0xBE, 0xC5, 0xAB, 0xF1,
	0x07, 0xE6, 0x2F, 0x8D, 0x0A,
	0x00, 0xA7, 0x83, 0xB6, 0x4F,
	0x0D, 0x8C, 0x21, 0xF2, 0x0C,
	0x03, 0x8A, 0x18, 0x3D, 0x50,
	0x05, 0x0C, 0x68, 0x84, 0x59,
	0x06, 0xB0, 0x56, 0x6E, 0xE4,
	0x03, 0x7C, 0xD5, 0xBD, 0x10,
	0x08, 0x0E, 0x94, 0x17, 0xD0,
	0x0D, 0x7E, 0xA6, 0x87, 0xD9,
	0x09, 0x6D, 0x33, 0xE2, 0x9E,
	0x0C, 0x4F, 0xBF, 0x8D, 0x2F,
	0x08, 0xF8, 0x56, 0x59, 0xE3,
	0x0E, 0x83, 0x5B, 0x76, 0x0B,
	0x0F, 0x55, 0x02, 0x91, 0xD4,
	0x0C, 0x0E, 0x74, 0x98, 0xB0,
	0x09, 0x0C, 0xE1, 0xFD, 0x56,
	0x07, 0x78, 0x7F, 0xC5, 0xCF,
	0x04, 0x99, 0x04, 0xA7, 0x68
};
#endif
const unsigned char ak77dspCRAM[AK77XX_DSP_CRAM_BUF_SIZE] = {
	0xB4, 0x00, 0x00,		// command code and address
	0x20, 0x00, 0x00,
	0x3F, 0x8E, 0xB0,
	0x80, 0x72, 0xF0,
	0xC0, 0x71, 0x50,
	0x7F, 0x8D, 0x10,
	0x40, 0x00, 0x00,
	0x3F, 0x4B, 0x00,
	0x80, 0xB9, 0x00,
	0xC0, 0xB5, 0x00,
	0x7F, 0x47, 0x00,
	0x40, 0x00, 0x00,
	0x3E, 0xE3, 0xE0,
	0x81, 0x26, 0x10,
	0xC1, 0x1C, 0x20,
	0x7E, 0xD9, 0xF0,
	0x40, 0x00, 0x00
};
#endif
I2CAK7755RWMode_t I2CAK7755RWMode = kI2CAK7755Idle;

void I2C0_Callback_SingleTx(uint32_t status)
{
	if (status == 0x08)						/* START has been transmitted */
	{
		I2C0->DATA = 0;
		I2C0->DATA = (AK7755_SlaveAddr<<1);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x18)				/* SLA+W has been transmitted and ACK has been received */
	{
		I2C0->DATA = Tx_Data[SingleDataLen++];
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x20)				/* SLA+W has been transmitted and NACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 1, 1, 1, 0);
	}
	else if (status == 0x28)				/* DATA has been transmitted and ACK has been received */
	{
		if (SingleDataLen != 2)
		{
			DrvI2C_WriteData(I2C_PORT0, Tx_Data[SingleDataLen++]);
			DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
		}
		else
		{
			DrvI2C_Ctrl(I2C_PORT0, 0, 1, 1, 0);
			SingleEndFlag = 1;
		}
	}
	else
	{
		printf("Status 0x%x is NOT processed\n", status);
	}
}

void I2C0_Callback_CramRx(uint32_t status)
{
	if (status == 0x08)					   	/* START has been transmitted and prepare SLA+W */
	{
		I2C0->DATA = 0;
		I2C0->DATA = (AK7755_SlaveAddr<<1);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x18)				/* SLA+W has been transmitted and ACK has been received */
	{
		DrvI2C_WriteData(I2C_PORT0, Cram_Data[CramDataLen++]);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x20)				/* SLA+W has been transmitted and NACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 1, 1, 1, 0);
	}
	else if (status == 0x28)				/* DATA has been transmitted and ACK has been received */
	{
		if (CramDataLen == 3)
		{
			DrvI2C_Ctrl(I2C_PORT0, 1, 0, 1, 0); 		//repeat start
		}
		else
		{
			DrvI2C_WriteData(I2C_PORT0, Cram_Data[CramDataLen++]);
			DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
		}
	}
	else if (status == 0x10)				/* Repeat START has been transmitted and prepare SLA+R */
	{
		DrvI2C_WriteData(I2C_PORT0, AK7755_SlaveAddr<<1 | 0x01);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x40)				/* SLA+R has been transmitted and ACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 1);
	}
	else if (status == 0x48)				/* SLA+R has been transmitted and Not-ACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x50)				// DATA has been received and ACK has been returned 
	{
		CramDataBuf[CramDataBufLen++] = DrvI2C_ReadData(I2C_PORT0);
		if (CramDataBufLen == 47)
			DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1,0);
		else
			DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1,1);
			
	}
	else if (status == 0x58)			//DATA has been received and NACK has been returned
	{
		CramDataBuf[CramDataBufLen++] = DrvI2C_ReadData(I2C_PORT0);
		DrvI2C_Ctrl(I2C_PORT0, 0, 1, 1, 0);
		CramEndFlag = 1;		
	}
	else
	{
		printf("Status 0x%x is NOT processed\n", status);
	}

}

void I2C0_Callback_CramTx(uint32_t status)
{
	if (status == 0x08)						/* START has been transmitted */
	{
		I2C0->DATA = 0;
		I2C0->DATA = (AK7755_SlaveAddr<<1);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x18)				/* SLA+W has been transmitted and ACK has been received */
	{
		I2C0->DATA = Cram_Data[CramDataLen++];
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x20)				/* SLA+W has been transmitted and NACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 1, 1, 1, 0);
	}
	else if (status == 0x28)				/* DATA has been transmitted and ACK has been received */
	{
		if (CramDataLen != CramDataLenTransfer)
		{
			DrvI2C_WriteData(I2C_PORT0, Cram_Data[CramDataLen++]);
			DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
		}
		else
		{
			DrvI2C_Ctrl(I2C_PORT0, 0, 1, 1, 0);
			CramEndFlag = 1;
		}
	}
	else
	{
		printf("Status 0x%x is NOT processed\n", status);
	}
}


void I2C0_Callback_SingleRx(uint32_t status)
{
	if (status == 0x08)					   	/* START has been transmitted and prepare SLA+W */
	{
		I2C0->DATA = 0;
		I2C0->DATA = (AK7755_SlaveAddr<<1);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x18)				/* SLA+W has been transmitted and ACK has been received */
	{
		DrvI2C_WriteData(I2C_PORT0, Tx_Data[SingleDataLen++]);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x20)				/* SLA+W has been transmitted and NACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 1, 1, 1, 0);
	}
	else if (status == 0x28)				/* DATA has been transmitted and ACK has been received */
	{
		if (SingleDataLen != 1)
		{
			DrvI2C_WriteData(I2C_PORT0, Tx_Data[SingleDataLen++]);
			DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
		}
		else
		{
			DrvI2C_Ctrl(I2C_PORT0, 1, 0, 1, 0);			//repeat start
		}
	}
	else if (status == 0x10)				/* Repeat START has been transmitted and prepare SLA+R */
	{
		DrvI2C_WriteData(I2C_PORT0, AK7755_SlaveAddr<<1 | 0x01);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x40)				/* SLA+R has been transmitted and ACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 1);
	}
	else if (status == 0x48)				/* SLA+R has been transmitted and Not-ACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x50)				// DATA has been received and ACK has been returned 
	{
		Rx_Data = DrvI2C_ReadData(I2C_PORT0);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
		//SingleEndFlag = 1;
	}
	else if (status == 0x58)			//DATA has been received and NACK has been returned
	{
		//Rx_Data = DrvI2C_ReadData(I2C_PORT0);
		DrvI2C_Ctrl(I2C_PORT0, 0, 1, 1, 0);
		SingleEndFlag = 1;
	}
	else
	{
		printf("Status 0x%x is NOT processed\n", status);
	}

}
#if 0
void I2C0_Callback_PramTx(uint32_t status)
{
	if (status == 0x08)						/* START has been transmitted */
	{
		I2C0->DATA = 0;
		I2C0->DATA = (AK7755_SlaveAddr<<1);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x18)				/* SLA+W has been transmitted and ACK has been received */
	{
		I2C0->DATA = Pram_Data[PramDataLen++];
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x20)				/* SLA+W has been transmitted and NACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 1, 1, 1, 0);
	}
	else if (status == 0x28)				/* DATA has been transmitted and ACK has been received */
	{
		if (PramDataLen != PramDataLenTransfer)
		{
			DrvI2C_WriteData(I2C_PORT0, Pram_Data[PramDataLen++]);
			DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
		}
		else
		{
			DrvI2C_Ctrl(I2C_PORT0, 0, 1, 1, 0);
			PramEndFlag = 1;
		}
	}
	else
	{
		printf("Status 0x%x is NOT processed\n", status);
	}
}

void I2C0_Callback_PramRx(uint32_t status)
{
	if (status == 0x08)					   	/* START has been transmitted and prepare SLA+W */
	{
		I2C0->DATA = 0;
		I2C0->DATA = (AK7755_SlaveAddr<<1);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x18)				/* SLA+W has been transmitted and ACK has been received */
	{
		DrvI2C_WriteData(I2C_PORT0, Pram_Data[PramDataLen++]);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x20)				/* SLA+W has been transmitted and NACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 1, 1, 1, 0);
	}
	else if (status == 0x28)				/* DATA has been transmitted and ACK has been received */
	{
		if (PramDataLen == 3)
		{
			DrvI2C_Ctrl(I2C_PORT0, 1, 0, 1, 0); 		//repeat start
		}
		else
		{
			DrvI2C_WriteData(I2C_PORT0, Pram_Data[PramDataLen++]);
			DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
		}
	}
	else if (status == 0x10)				/* Repeat START has been transmitted and prepare SLA+R */
	{
		DrvI2C_WriteData(I2C_PORT0, AK7755_SlaveAddr<<1 | 0x01);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x40)				/* SLA+R has been transmitted and ACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 1);
	}
	else if (status == 0x48)				/* SLA+R has been transmitted and Not-ACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x50)				// DATA has been received and ACK has been returned 
	{
		PramDataBuf[PramDataBufLen++] = DrvI2C_ReadData(I2C_PORT0);
		if (PramDataBufLen == 194)
			DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1,0);
		else
			DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1,1);
	}
	else if (status == 0x58)			//DATA has been received and NACK has been returned
	{
		PramDataBuf[PramDataBufLen++] = DrvI2C_ReadData(I2C_PORT0);
		DrvI2C_Ctrl(I2C_PORT0, 0, 1, 1, 0);
		PramEndFlag = 1;
	}
	else
	{
		printf("Status 0x%x is NOT processed\n", status);
	}

}

#endif
void AK7755_Write_Single (uint8_t addr, uint8_t Data)
{
	if (I2CAK7755RWMode != kI2CWritingAK7755)
	{
		I2CAK7755RWMode = kI2CWritingAK7755;
		DrvI2C_UninstallCallBack(I2C_PORT0, I2CFUNC);
		DrvI2C_InstallCallback(I2C_PORT0, I2CFUNC, I2C0_Callback_SingleTx);
	}
	Tx_Data[0] = addr;
	Tx_Data[1] = Data;
	SingleDataLen = 0;
	SingleEndFlag = 0;
	DrvI2C_Ctrl(I2C_PORT0, 1, 0, 0, 0);
	while (SingleEndFlag == 0);
	SingleEndFlag = 0;
	I2CAK7755RWMode = kI2CAK7755Idle;
}

uint8_t AK7755_Read_Single(uint8_t addr)
{
	if (I2CAK7755RWMode != kI2CReadingAK7755)
	{
		I2CAK7755RWMode = kI2CReadingAK7755;
		DrvI2C_UninstallCallBack(I2C_PORT0, I2CFUNC);
		DrvI2C_InstallCallback(I2C_PORT0, I2CFUNC, I2C0_Callback_SingleRx);
	}
	Tx_Data[0] = addr;//addr-0x80;
	SingleDataLen = 0;
	SingleEndFlag = 0;
	DrvI2C_Ctrl(I2C_PORT0, 1, 0, 0, 0);
	while (SingleEndFlag == 0);
	SingleEndFlag = 0;
	I2CAK7755RWMode = kI2CAK7755Idle;
	return Rx_Data;
}
#if 0
void AK7755_Write_Pram(unsigned char *uPramData, unsigned short uDataLength)
{ 
	int i;
	
	PramDataLenTransfer = uDataLength;
	if (I2CAK7755RWMode != kI2CWritingAK7755)
	{
		I2CAK7755RWMode = kI2CWritingAK7755;
		DrvI2C_UninstallCallBack(I2C_PORT0, I2CFUNC);
		DrvI2C_InstallCallback(I2C_PORT0, I2CFUNC, I2C0_Callback_PramTx);
	}
	
    	for(i=0;i<(uDataLength);i++)
	{	
		Pram_Data[i] = *uPramData++;  
	}
	PramDataLen = 0;
	PramEndFlag = 0;
	DrvI2C_Ctrl(I2C_PORT0, 1, 0, 0, 0);
	while (PramEndFlag == 0);
	PramEndFlag = 0;	
	I2CAK7755RWMode = kI2CAK7755Idle;
}

void AK7755_Read_Pram(uint8_t CommandCode, uint16_t addr)
{ 
	int i;

	if (I2CAK7755RWMode != kI2CReadingAK7755)
	{
		I2CAK7755RWMode = kI2CReadingAK7755;
		DrvI2C_UninstallCallBack(I2C_PORT0, I2CFUNC);
		DrvI2C_InstallCallback(I2C_PORT0, I2CFUNC, I2C0_Callback_PramRx);
	}
	
	Pram_Data[0] = CommandCode;
	Pram_Data[1] = (uint8_t)((addr>>8 ) & 0xFF);
	Pram_Data[2] = (uint8_t)(addr & 0xFF);
	PramDataLen = 0;
	PramDataBufLen = 0;
	PramEndFlag = 0;
	DrvI2C_Ctrl(I2C_PORT0, 1, 0, 0, 0);
	while (PramEndFlag == 0);
	PramEndFlag = 0;
	I2CAK7755RWMode = kI2CAK7755Idle;
}
#endif
void AK7755_Write_Cram(unsigned char *uPramData, unsigned short uDataLength)
{ 
	int i;

	CramDataLenTransfer = uDataLength;
	if (I2CAK7755RWMode != kI2CWritingAK7755)
	{
		I2CAK7755RWMode = kI2CWritingAK7755;
		DrvI2C_UninstallCallBack(I2C_PORT0, I2CFUNC);
		DrvI2C_InstallCallback(I2C_PORT0, I2CFUNC, I2C0_Callback_CramTx);
	}
    	for(i=0;i<(uDataLength);i++)
	{	
		Cram_Data[i] = *uPramData++;  
	}
	CramDataLen = 0;
	CramEndFlag = 0;
	DrvI2C_Ctrl(I2C_PORT0, 1, 0, 0, 0);
	while (CramEndFlag == 0);
	CramEndFlag = 0;
	I2CAK7755RWMode = kI2CAK7755Idle;
}

void AK7755_Read_Cram(uint8_t CommandCode, uint16_t addr)
{ 
	int i;

	if (I2CAK7755RWMode != kI2CReadingAK7755)
	{
		I2CAK7755RWMode = kI2CReadingAK7755;
		DrvI2C_UninstallCallBack(I2C_PORT0, I2CFUNC);
		DrvI2C_InstallCallback(I2C_PORT0, I2CFUNC, I2C0_Callback_CramRx);
	}
	Cram_Data[0] = CommandCode;
	Cram_Data[1] = (uint8_t)((addr>>8 ) & 0xFF);
	Cram_Data[2] = (uint8_t)(addr & 0xFF);
	CramDataLen = 0;
	CramDataBufLen = 0;
	CramEndFlag = 0;
	DrvI2C_Ctrl(I2C_PORT0, 1, 0, 0, 0);
	while (CramEndFlag == 0);
	CramEndFlag = 0;
	I2CAK7755RWMode = kI2CAK7755Idle;
}

void AK7755_PTTMode_Init(void)
{
#ifdef USE_AUDIO_SAMPLEREATE_8K
		AK7755_Write_Single(0xC0, 0x58);
#endif	
#ifdef USE_AUDIO_SAMPLEREATE_16K
		AK7755_Write_Single(0xC0, 0x3A);
#endif	
	
		AK7755_Write_Single(0xC1, 0x20);
	AK7755_Write_Single(0xC2, 0x00);
	AK7755_Write_Single(0xC3, 0x05);
	AK7755_Write_Single(0xC4, 0x61);
	AK7755_Write_Single(0xC5, 0x00);
		AK7755_Write_Single(0xC6, 0x33);
		AK7755_Write_Single(0xC7, 0x03);
	AK7755_Write_Single(0xC8, 0xC0);
		AK7755_Write_Single(0xC9, 0x22);
	AK7755_Write_Single(0xCA, 0x01);
	AK7755_Write_Single(0xCC, 0x00);
	AK7755_Write_Single(0xCD, 0xC0);
	AK7755_Write_Single(0xCE, 0x71);
	AK7755_Write_Single(0xCF, 0x20);
	AK7755_Write_Single(0xD0, 0x00);
	AK7755_Write_Single(0xD1, 0x00);
		AK7755_Write_Single(0xD2, 0x00);
	AK7755_Write_Single(0xD3, 0x08);
	AK7755_Write_Single(0xD4, 0x00);
	AK7755_Write_Single(0xD5, 0x30);
	AK7755_Write_Single(0xD6, 0x30);
	AK7755_Write_Single(0xD7, 0x30);
		AK7755_Write_Single(0xD8, 0x40);
		AK7755_Write_Single(0xD9, 0x40);
	AK7755_Write_Single(0xDA, 0x10);//0x10
	AK7755_Write_Single(0xDB, 0x00);
	AK7755_Write_Single(0xDD, 0x30);
	AK7755_Write_Single(0xDE, 0x00);
	AK7755_Write_Single(0xE6, 0x01);
	AK7755_Write_Single(0xEA, 0x80);
}

#if 0
void AK7755_MusicMode_Init(void)
{
#ifdef USE_AUDIO_SAMPLEREATE_8K
	AK7755_Write_Single(0xC0, 0x58);//5A
#endif	
#ifdef USE_AUDIO_SAMPLEREATE_16K
	AK7755_Write_Single(0xC0, 0x3A);//5A
#endif	

	AK7755_Write_Single(0xC1, 0x20);
	AK7755_Write_Single(0xC2, 0x00);
	AK7755_Write_Single(0xC3, 0x05);
	AK7755_Write_Single(0xC4, 0x61);
	AK7755_Write_Single(0xC5, 0x00);
	AK7755_Write_Single(0xC6, 0x33);
	AK7755_Write_Single(0xC7, 0x03);
	AK7755_Write_Single(0xC8, 0xC0);
	AK7755_Write_Single(0xC9, 0x28);
	AK7755_Write_Single(0xCA, 0x01);
	AK7755_Write_Single(0xCC, 0x00);
	AK7755_Write_Single(0xCD, 0xC0);
	AK7755_Write_Single(0xCE, 0x71);
	AK7755_Write_Single(0xCF, 0x20);
	AK7755_Write_Single(0xD0, 0x00);
	AK7755_Write_Single(0xD1, 0x00);
	AK7755_Write_Single(0xD2, 0x00);//08//0F
	AK7755_Write_Single(0xD3, 0x0F);
	AK7755_Write_Single(0xD4, 0x00);
	AK7755_Write_Single(0xD5, 0x30);
	AK7755_Write_Single(0xD6, 0x30);
	AK7755_Write_Single(0xD7, 0x30);
	AK7755_Write_Single(0xD8, 0x18);
	AK7755_Write_Single(0xD9, 0x18);
	AK7755_Write_Single(0xDA, 0x10);
	AK7755_Write_Single(0xDB, 0x00);
	AK7755_Write_Single(0xDD, 0x30);
	AK7755_Write_Single(0xDE, 0x00);
	AK7755_Write_Single(0xE6, 0x01);
	AK7755_Write_Single(0xEA, 0x80);
#if 0	
	AK7755_Read_Single(0x40);
	AK7755_Read_Single(0x41);
	AK7755_Read_Single(0x42);
	AK7755_Read_Single(0x43);
	AK7755_Read_Single(0x44);
	AK7755_Read_Single(0x45);
	AK7755_Read_Single(0x46);
	AK7755_Read_Single(0x47);
	AK7755_Read_Single(0x48);
	AK7755_Read_Single(0x49);
	AK7755_Read_Single(0x4A);
	AK7755_Read_Single(0x4C);
	AK7755_Read_Single(0x4D);
	AK7755_Read_Single(0x4E);
	AK7755_Read_Single(0x4F);
	AK7755_Read_Single(0x50);
	AK7755_Read_Single(0x51);
	AK7755_Read_Single(0x52);
	AK7755_Read_Single(0x53);
	AK7755_Read_Single(0x54);
	AK7755_Read_Single(0x55);
	AK7755_Read_Single(0x56);
	AK7755_Read_Single(0x57);
	AK7755_Read_Single(0x58);
	AK7755_Read_Single(0x59);
	AK7755_Read_Single(0x5A);
	AK7755_Read_Single(0x5B);
	AK7755_Read_Single(0x5D);
	AK7755_Read_Single(0x5E);
	AK7755_Read_Single(0x66);
	AK7755_Read_Single(0x6A);
#endif		
}
#endif
#if 0
uint8_t I2cTransferBlock(uint8_t devAddr,
					uint8_t i2cCmd, 
					uint8_t OpCmd,
					uint16_t startAddr, 
					uint8_t *wBuf, 
					uint8_t bCount, 
					uint16_t total, 
					uint16_t i2cLimit)
{
	uint8_t arrBuf[20], regBuf[2];	
	uint8_t arrCmd[3];	
	uint8_t i;
	uint8_t i2cCmdRun = 0;
	uint8_t rCheck=1;
	uint16_t addr = startAddr; 
	uint16_t dataLength = i2cLimit;
	uint16_t arrOffset = 0;
	uint16_t crc16B, crcChecked=1;
	
	/* dataLength should be alignment */
	if( dataLength % bCount )	
		return 0;	
	
		if( OpCmd != 0 )
		{
			i2cCmdRun = i2cCmd ;
		}
		
	/* write PRAM or CRAM */
	while( arrOffset < total )
	{	
		if( arrOffset + dataLength > total )
			dataLength = total - arrOffset;
		
		/* Runtime - Preparation */
		if( OpCmd != 0 )
		{
			i2cCmd = i2cCmdRun + (dataLength/bCount-1);
		}
		arrBuf[0]=i2cCmd;					
		arrBuf[1]=(uint8_t)((addr>>8 ) & 0xFF);	
		arrBuf[2]=(uint8_t)(addr & 0xFF);	
		for(i=0; i<dataLength;i++)
			arrBuf[i+3]=wBuf[i + arrOffset];	
	
		if (i2cCmd == CMD_PRAM_DL)
			AK7755_Write_Pram(arrBuf, dataLength+3);
		else if (i2cCmd == CMD_CRAM_DL)
			AK7755_Write_Cram(arrBuf, dataLength+3);
		
		/* Runtime - Operation */
		if( OpCmd != 0 )
		{
			arrBuf[0]=OpCmd;
			arrBuf[1]=0x00;
			arrBuf[2]=0x00;	
			AK7755_Write_Pram(arrBuf, 3);	
		}					
		
		/* go to next data */
		if( crcChecked )
		{
			addr += dataLength/bCount;
			arrOffset = addr*bCount;
		}
		else
		{
			/* re-send */
		}
	}
	
	/* Runtime - Operation */
	if( OpCmd != 0 )
	{
		arrBuf[0]=OpCmd;
		arrBuf[1]=0x00;
		arrBuf[2]=0x00;	
		AK7755_Write_Pram(arrBuf, 3);	
	}

	printf("write finish.....\n");

}
#endif

void AK7755_CRAM_Init(void)
{
	uint32_t  u32CRAMPointer;
	uint16_t arrOffset = 0;
	uint16_t dataLength = 15;
	uint8_t i;
	uint16_t CramAddr = 0x0000;
	uint8_t arrBuf[20];
	uint8_t pTemp[SIZE_TRANSFER];

	while(arrOffset < AK77XX_DSP_CRAM_SIZE)
	{
		if( arrOffset + dataLength > AK77XX_DSP_CRAM_SIZE )
			dataLength = AK77XX_DSP_CRAM_SIZE - arrOffset;
		
		u32CRAMPointer = FLASH_AKM_CRAM_ADDR + arrOffset;
		QDT_flash_ReadBytes(u32CRAMPointer, dataLength, pTemp);
		
		arrBuf[0]=CMD_CRAM_DL;					
		arrBuf[1]=(uint8_t)((CramAddr>>8 ) & 0xFF); 
		arrBuf[2]=(uint8_t)(CramAddr & 0xFF);	
		for(i=0; i<dataLength;i++)
			arrBuf[i+3]=pTemp[i];	
		AK7755_Write_Cram((unsigned char*)arrBuf, dataLength+3);
		
		CramAddr += dataLength/3;
		arrOffset = CramAddr*3;
	}
}

void AK7755_PRAM_Init(void)
{
	uint32_t  u32PRAMPointer;
	uint16_t arrOffset = 0;
	uint16_t dataLength = 15;
	uint8_t i;
	uint16_t PramAddr = 0x0000;
	uint8_t arrBuf[20];
	uint8_t pTemp[SIZE_TRANSFER];

	while(arrOffset < AK77XX_DSP_PRAM_SIZE)
	{
		if( arrOffset + dataLength > AK77XX_DSP_PRAM_SIZE )
			dataLength = AK77XX_DSP_PRAM_SIZE - arrOffset;
		
		u32PRAMPointer = FLASH_AKM_PRAM_ADDR + arrOffset;
		QDT_flash_ReadBytes(u32PRAMPointer, dataLength, pTemp);
		
		arrBuf[0]=CMD_PRAM_DL;					
		arrBuf[1]=(uint8_t)((PramAddr>>8 ) & 0xFF); 
		arrBuf[2]=(uint8_t)(PramAddr & 0xFF);	
		for(i=0; i<dataLength;i++)
			arrBuf[i+3]=pTemp[i];	
		//AK7755_Write_Pram((unsigned char*)arrBuf, dataLength+3);
		AK7755_Write_Cram((unsigned char*)arrBuf, dataLength+3);
		
		PramAddr += dataLength/5;
		arrOffset = PramAddr*5;
	}
}

void AK7755_Init(void)
{
	QDT_Common_DelayTime(2);

	AK7755_PTTMode_Init();
	QDT_Common_DelayTime(1);
	
	AK7755_Write_Single(0xC1, 0x21);
	QDT_Common_DelayTime(10);

	//I2cTransferBlock(AK7755_SlaveAddr, CMD_CRAM_DL, 0, 0x0000, ak77dspCRAM, 3, SIZE_OF_CRAM, SIZE_TRANSFER); 
	//AK7755_Read_Cram(0x34, 0x0000);
	//I2cTransferBlock(AK7755_SlaveAddr, CMD_PRAM_DL, 0, 0x0000, ak77dspPRAM, 5, SIZE_OF_PRAM, SIZE_TRANSFER);
	//AK7755_Read_Pram(0x38, 0x0000);

	
	//AK7755_Write_Cram((unsigned char*)ak77dspCRAM, AK77XX_DSP_CRAM_BUF_SIZE);
	//AK7755_Read_Cram(0x34, 0x0000);
	//AK7755_Write_Pram((unsigned char*)ak77dspPRAM, AK77XX_DSP_PRAM_BUF_SIZE);
	//AK7755_Read_Pram(0x38, 0x0000);

	AK7755_CRAM_Init();
	AK7755_PRAM_Init();


	QDT_Common_DelayTime(1);
	AK7755_Write_Single(0xCF, 0x2C);//0x20
	
}

#endif
