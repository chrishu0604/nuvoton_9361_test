#ifdef BUILDTYPE_PARENTUINT_PROJECT
#include "bsp.h"
#include "radio.h"
#include "ISD93xx.h"
#include "Driver\DrvADC.h"
#include "Driver\DrvPDMA.h"
#include "Driver\DrvDPWM.h"
#include "Driver\DrvGPIO.h"
#include "Driver\DrvTIMER.h"
#include "Driver\DrvSYS.h"
#include "Driver\DrvFMC.h"
#include "FMC.h"
#include "PMU.h"

#include "qdt_ram.h"
#include "qdt_fifo.h"
#include "qdt_flash_hal.h"
#include "qdt_sysState.h"
#include "qdt_audiodocking.h"
#include "qdt_parentunit.h"
#include "CM36686Setup.h"

#include "qdt_utils.h"
#include "qdt_parentunit_func.h"
#include "qdt_common_func.h"


#include "si446x_api_lib.h"    //RF-664x header file
#include "qdt_human_voice.h"
#include "qdt_upgrade_func.h"
#include "qdt_factory_func.h"

qdt_uint8 g_ucDumpImageData[256];


extern qdt_uint8 HV_PowerOn[2];
extern qdt_uint8 HV_PowerOff[2];
extern qdt_uint8 HV_PairingMode[2];
extern qdt_uint8 HV_PairingSuccess[2];
extern qdt_uint8 HV_BabyCry[1];
extern qdt_uint8 HV_BEE[1];
extern qdt_uint8 HV_FactoryReset[2];
extern const qdt_uint8 g_ucFWVersion[13];

/**************************************************************/
/* extern function                                                                                       */
/**************************************************************/
extern BIT vSampleCode_SendFixPacket(void);
extern void S7EncDec(void);
/**************************************************************/
/* static  function                                                                                       */
/**************************************************************/
static qdt_bool __qdt_IsPressPush2TalkButton(void);


static qdt_bool __qdt_IsPressPush2TalkButton(void)
{
	static qdt_uint32 u32PressBTNCount = 0;
	static qdt_uint32 u32RleaseBTNCount = 5;
	static qdt_bool bPressPush2TalkButton = qdt_false;
	if(!DrvGPIO_GetBit(GPB, (GPIO_PARENTUNIT_KEY_PTT-10)) && !bPressPush2TalkButton)
	{
		u32PressBTNCount++;
		if (u32PressBTNCount > PRESS_BUTTON_THRESHOLD)
			bPressPush2TalkButton = qdt_true;
	}
	else if (DrvGPIO_GetBit(GPB, (GPIO_PARENTUNIT_KEY_PTT-10)) && bPressPush2TalkButton)
	{
		u32RleaseBTNCount--;
		if (u32RleaseBTNCount == 0)
		{
			bPressPush2TalkButton = qdt_false;
			u32RleaseBTNCount = 5;
		}
	}

	return bPressPush2TalkButton;
}

RETURN_TYPE QDT_PU_AudioDocking_OnLine(void)
{
	QDT_TRACE("QDT_AudioDucking_OnLine....\n");
	g_Data.bAudioDockingOnLine = qdt_true;
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_PU_AudioDocking_AnswerCalling(void)
{
	QDT_TRACE("QDT_PU_AudioDocking_AnswerCalling....\n");

	g_Data.bAudidDockingAnswerCalling = qdt_true;	
	g_Data.bAudidDockingHandUp = qdt_false;
	//g_Data.bTwinkleOrangeLED = qdt_true;
	QDT_Common_MicResume();
	QDT_Common_SPKPause();	

	g_Data.bAllow2UsingBoardCastChan = qdt_true;

	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_PU_AudioDocking_HandUp(void)
{
	QDT_TRACE("QDT_PU_AudioDocking_HandUp....\n");
	g_Data.bAudidDockingAnswerCalling = qdt_false;
	g_Data.bAudidDockingHandUp = qdt_true;	
	g_Data.ucWaitADHandupACKCount = 0;
	QDT_Common_MicPause();
	QDT_Common_SPKResume(); 	

	g_Data.bAllow2UsingBoardCastChan = qdt_false;
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_PU_HandleAlertKey(void)
{
	g_Data.bHandleAlertStatus = qdt_true;
	//PlayG722Stream(4);
	g_Data.bHandleAlertStatus = qdt_false;
	return TRUE;
}

RETURN_TYPE QDT_PU_EnableGpioPwm(void)
{
	  //init GPIO
        SYS->GPA_ALT.GPA12=1;                //PWM Ch0 function on GPA12
        SYS->GPB_ALT.GPB13=3;                  //PWM Ch0 function on GPB13
        SYS->GPB_ALT.GPB14=1;                //PWM Ch2 function on GPB14
	  SYS->GPB_ALT.GPB15=1;                //PWM Ch3 function on GPB15

        //Set and open clock source
	SYSCLK->PWRCON.OSC10K_EN=1;		//Enable 10K oscillation;
	SYSCLK->CLKSEL1.PWM01_S=2;		//Clock source, 0=>10K, 1=>32K, 2=>HCLK
        SYSCLK->CLKSEL1.PWM23_S=2;             //Clock source, 0=>10K, 1=>32K, 2=>HCLK
        SYSCLK->CLKSEL2.PWM45_S=2;
	SYSCLK->APBCLK.PWM01_EN = 1;	//Enable PWM clock source
        SYSCLK->APBCLK.PWM23_EN = 1; //Enable PWM clock source
		SYSCLK->APBCLK.PWM45_EN = 1;
        //===============

        //PWM0~2
        //set divider
	PWMA->PPR.CP01=255;	//Pre-scaler divider = CP01+1 =256
        PWMA->PPR.CP23=255; //Pre-scaler divider = CP23+1 =256
	  PWMB->PPR.CP01=255;


        //open PWM0 and set resolution
        PWMA->POE.PWM0=1;                  //GPA12 PWM output enable
	PWMA->PCR.CH0MOD=1;			//Auto-load mode
	PWMA->PCR.CH0INV=1;		//Signal inverted
	PWMA->CSR.CSR0=4;			//Set Timer1 clock divider = 1;
        PWMA->CNR0=DUTY_LED_INTENSITY;     //Duty (CMR0+1)/(CN0R+1)
        PWMA->CMR0=0;                           //Duty (CMR0+1)/(CNR0+1), duty is from 1 ~ (DUTY_RESOLUTION+1)/(DUTY_RESOLUTION+1) low 
	PWMA->PCR.CH0EN=1;			//Timer enable

        //open PWMB CH0 and set resolution
     PWMB->POE.PWM0=1;                  //GPB13 PWM output enable
     PWMB->PCR.CH0MOD=1;                      //Auto-load mode
	//PWMB->PCR.CH0INV=1;		//Signal inverted
     PWMB->CSR.CSR0=4;                     //Set Timer1 clock divider = 1;
     PWMB->CNR0=DUTY_LED_INTENSITY;     //Duty (CMR1+1)/(CNR1+1)
     PWMB->CMR0=0;                           //Duty (CMR0+1)/(CNR0+1), duty is from 1 ~ (DUTY_RESOLUTION+1)/(DUTY_RESOLUTION+1) low
     PWMB->PCR.CH0EN=1;                  //Timer enable

        //open PWM2 and set resolution
        PWMA->POE.PWM2=1;                  //GPB14 PWM output enable
        PWMA->PCR.CH2MOD=1;                      //Auto-load mode
        PWMA->PCR.CH2INV=1;              //Signal inverted
        PWMA->CSR.CSR2=4;                     //Set Timer1 clock divider = 1;
        PWMA->CNR2=DUTY_LED_INTENSITY;     //Duty (CMR1+1)/(CNR1+1)
        PWMA->CMR2=0;                           //Duty (CMR0+1)/(CNR0+1), duty is from 1 ~ (DUTY_RESOLUTION+1)/(DUTY_RESOLUTION+1) low
        PWMA->PCR.CH2EN=1;                  //Timer enable

        
        //open PWM3 and set resolution
        PWMA->POE.PWM3=1;                  //GPB15 PWM output enable
        PWMA->PCR.CH3MOD=1;                      //Auto-load mode
        PWMA->PCR.CH3INV=1;              //Signal inverted
        PWMA->CSR.CSR3=4;                     //Set Timer1 clock divider = 1;
        PWMA->CNR3=DUTY_LED_INTENSITY;     //Duty (CMR1+1)/(CNR1+1)
        PWMA->CMR3=0;                           //Duty (CMR0+1)/(CNR0+1), duty is from 1 ~ (DUTY_RESOLUTION+1)/(DUTY_RESOLUTION+1) low
        PWMA->PCR.CH3EN=1;                  //Timer enable

	return TRUE;
}

RETURN_TYPE QDT_PU_DisableGpioPwm(void)
{
	SYSCLK->APBCLK.PWM01_EN = 0;	//Disable PWM clock source
      	SYSCLK->APBCLK.PWM23_EN = 0; //Disable PWM clock source
	SYSCLK->APBCLK.PWM45_EN = 0;
      	PWMA->POE.PWM0=0;                  //GPA12 PWM output disable
	PWMB->POE.PWM0=0;                  //GPA13 PWM output disable
      	PWMA->POE.PWM2=0;                  //GPB14 PWM output disable
	PWMA->POE.PWM3=0;                  //GPB15 PWM output disable
	
	return TRUE;
}
RETURN_TYPE QDT_PU_SysPowerOn(void)
{
	QDT_TRACE("Parent Unit power on....\n");
	
	PARENTUNIT_LED_PWR_RED(qdt_OFF);
	PARENTUNIT_LED_PWR_GREEN(qdt_ON);
	QDT_PU_EnableGpioPwm();

	if (g_Data.ucSysState == SYSSTATE_STANDBY || g_Data.ucPreSysState == SYSSTATE_STANDBY)	
		QDT_Common_Init();										/* Initial QDT variable and function */
	
	PARENTUNIT_RF_446x(qdt_ON);
 	vRadio_Init();	                							/* Initial RF-446x */

	DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
		
	QDT_Common_DelayTime(100);
	QDT_Common_SPKPause();					//pause SPK ouput		
	QDT_Common_MicPause(); 					//pause MIC input	

	if (g_Data.ucSysState == SYSSTATE_STANDBY || g_Data.ucPreSysState == SYSSTATE_STANDBY)
		QDT_Play_HumanVoice(HV_PowerOn,2);		
		
	if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
	{
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_UNPAIRING;
		g_Data.bTwinkleRedLED = qdt_true;
		QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
		#ifdef USER_SELECT_FREQ		
		QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);     //change RF Freq
		#endif				
	}
	else
	{
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_RUNNING;
		g_Data.bTwinkleRedLED = qdt_false;		
		QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
		#ifdef USER_SELECT_FREQ		
		QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
		#endif
	}

	vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
	return QDT_SYS_NOERROR;
}
RETURN_TYPE QDT_PU_SysPowerOff(void)
{
	if (g_Data.bFactoryResetFlag != qdt_true && g_Data.ucSysState != SYSSTATE_SLEEP && g_Data.ucSysState != SYSSTATE_STANDBY)
		QDT_Play_HumanVoice(HV_PowerOff,2);		
	
	PARENTUNIT_LED_PWR_RED(qdt_ON);
	PARENTUNIT_LED_PWR_GREEN(qdt_OFF);	
	PARENTUNIT_LED_ALERT_ORANGE(qdt_OFF);
		
	QDT_PU_DisableGpioPwm();
	//PARENTUNIT_LED_INTENSITY_G1(qdt_OFF);
	//PARENTUNIT_LED_INTENSITY_G2(qdt_OFF);
	//PARENTUNIT_LED_INTENSITY_R1(qdt_OFF);
	//PARENTUNIT_LED_INTENSITY_R2(qdt_OFF);

	g_Data.ucPreSysState = g_Data.ucSysState;
	g_Data.ucSysState = SYSSTATE_STANDBY;	
	g_Data.bTwinkleRedLED = qdt_false;
	g_Data.bTwinkleGreenLED = qdt_false;
	g_Data.bTwinkleOrangeLED = qdt_false;

	DrvGPIO_DisableInt(GPB, 7);  		    //disable RF-4461 interrupt Pin
	PARENTUNIT_RF_446x(qdt_OFF);   			//turn off RF-4461
		
	if (g_UserSaveData.ucPairingState != _QDT_PAIRINGSTATE_FINISHED_)  //pairing is not finish, recover to  _QDT_PAIRINGSTATE_WAITFORSTAR_
		g_UserSaveData.ucPairingState =  _QDT_PAIRINGSTATE_WAITFORSTAR_;
		
	//QDT_flash_WriteUserData(&g_UserSaveData);
	QDT_Common_SaveUserData(qdt_true);
		
	QDT_ClearCmd();
	QDT_Common_SPKPause();					//pause SPK ouput		
	QDT_Common_MicPause(); 					//pause MIC input	

	QDT_TRACE("Parent Unit power off....\n");
		

	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_PU_SysPowerOnOff(void)
{
	if (g_Data.ucSysState == SYSSTATE_STANDBY)  // standby to poweron
	{		
		#ifdef SUPPORT_SLEEP_MODE
		//PMU_StandbyPowerDown();
		PMU_ExitSleepMode();
		#endif		
		
		QDT_PU_SysPowerOn();
	}
	else if (g_Data.ucSysState == SYSSTATE_WAKEUP)
	{
		g_Data.bEnableSleepLED = qdt_false;
		QDT_PU_SysPowerOn();
	}
	else										// poweron to standby
	{

		QDT_PU_SysPowerOff();
		//QDT_PU_SysEnterSleep();	
		#ifdef SUPPORT_SLEEP_MODE
		//PMU_StandbyPowerDown();
		//PMU_RTCInit();
		//PMU_SetCurrentTime(0x00180000); 		//18:00:00
		//PMU_SetAlarmTime(0x00180030);			//18:01:00	
		//SysTimerDelay(1000);		
		PMU_EnterSleepMode();
		#endif		

	}
	
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_PU_UnPaired(void)
{
	QDT_TRACE("Parent Unit UnPaired....\n");
	QDT_PU_SysPowerOnOff();
	
	QDT_ClearCmd();
	//g_Data.ucPreSysState = g_Data.ucSysState;
	//g_Data.ucSysState = SYSSTATE_UNPAIRING;
	g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_WAITFORSTAR_; 	
	g_UserSaveData.ucPairingSyncWord[0] = UNI_PAIRING_SYNCWORD_BYTE0;
	g_UserSaveData.ucPairingSyncWord[1] = UNI_PAIRING_SYNCWORD_BYTE1;
	g_UserSaveData.ucPairingSyncWord[2] = UNI_PAIRING_SYNCWORD_BYTE2;
	g_UserSaveData.ucPairingSyncWord[3] = UNI_PAIRING_SYNCWORD_BYTE3;
	g_UserSaveData.ucAllocRFFreqCH = RF_PARENTUNIT_CH0;

	//QDT_flash_WriteUserData(&g_UserSaveData);
	QDT_Common_SaveUserData(qdt_false);

	return QDT_SYS_NOERROR;
}

qdt_bool QDT_PU_IsGetLowPowerSignal(void)
{
	static qdt_uint8 u32LowPowerCount = 0;
	static qdt_uint8 u32RleaseLowPowerCount = 5;
	static qdt_bool bLowPowerDetected = qdt_false;
	if(!DrvGPIO_GetBit(GPA, 7) && !bLowPowerDetected)
	{
		u32LowPowerCount++;
		if (u32LowPowerCount > 10)
			bLowPowerDetected = qdt_true;
	}
	else if (DrvGPIO_GetBit(GPA, 7) && bLowPowerDetected)
	{
		u32RleaseLowPowerCount--;
		if (u32RleaseLowPowerCount == 0)
		{
			bLowPowerDetected = qdt_false;
			u32RleaseLowPowerCount = 5;
		}
	}

	return bLowPowerDetected;
}
qdt_bool QDT_PU_IsDCConnected(void)
{
	static qdt_uint8 ucDCConnectedCount = 0;
	static qdt_uint8 ucRleaseDCConnectedCount = 5;
	static qdt_bool bDCConnected = qdt_true;
	if(DrvGPIO_GetBit(GPA, 10))
	{
		ucDCConnectedCount++;
		if (ucDCConnectedCount > 10)
			bDCConnected = qdt_true;
	}
	else if (!DrvGPIO_GetBit(GPA, 10))
	{
		ucRleaseDCConnectedCount--;
		if (ucRleaseDCConnectedCount == 0)
		{
			bDCConnected = qdt_false;
			ucRleaseDCConnectedCount = 5;
		}
	}

	return bDCConnected;
}


RETURN_TYPE QDT_PU_PushToTalk(void)
{
	RFHEADER SentOutRFHeader;
	qdt_uint8 ucSentOutData[10];
	qdt_uint8 ucDestSerialNumber[5];
	
	if (!g_Data.bDisablePTTButton)  //when audio come from parentunit, it can't allow another parentunit to PTT.
	{
	
	if (g_Data.bMicBufferReady)
	{
		S7EncDec();
	}
	
	if (__qdt_IsPressPush2TalkButton() 
		&& g_Data.bRFTransmitFinished && !g_Data.bTransmitBufferIsempty 
		&& g_Data.bAudidDockingAnswerCalling == qdt_true && g_Data.bAudidDockingHandUp == qdt_false)
	{	
		vSampleCode_SendFixPacket();
	}
			
	if (__qdt_IsPressPush2TalkButton())
	{
		if (!g_Data.bAudidDockingAnswerCalling)
		{
			QDT_Common_SPKPause();		
			ucDestSerialNumber[0] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[0];
			ucDestSerialNumber[1] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[1];
			ucDestSerialNumber[2] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[2];
			ucDestSerialNumber[3] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[3];			
			ucDestSerialNumber[4] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[4];				
			QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFCMD_PARENTCOMMAND_CALLING, ucDestSerialNumber);
			QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData);
				//PARENTUNIT_LED_ALERT_ORANGE(qdt_ON);
				QDT_Common_DelayTime(100);
		}
			g_Data.bIsInPTTMode= qdt_true;
			g_Data.bDisableSleepMode = qdt_true;
			g_Data.bIsQuietMode = qdt_false;
	}
	else if (!__qdt_IsPressPush2TalkButton())
	{
		if (g_Data.bAudidDockingAnswerCalling)
		{
			ucDestSerialNumber[0] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[0];
			ucDestSerialNumber[1] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[1];
			ucDestSerialNumber[2] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[2];
			ucDestSerialNumber[3] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[3];		
			ucDestSerialNumber[4] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[4];	
			QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFCMD_PARENTCOMMAND_HANDUP, ucDestSerialNumber);
			QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData);			
				QDT_Common_DelayTime(100);
				g_Data.ucWaitADHandupACKCount++;
				if (g_Data.ucWaitADHandupACKCount > 50)
				{
					QDT_DEBUG("AudioDocking don't ack HANDUP RF cmd, force handup\n");
					QDT_PU_AudioDocking_HandUp();
				}
				//QDT_PU_PushToTalkLED(qdt_ON);
				//PARENTUNIT_LED_ALERT_ORANGE(qdt_OFF);
				//g_Data.bTwinkleOrangeLED = qdt_false;
		}
		else if (!g_Data.bAudidDockingAnswerCalling && !g_Data.bSPKOpened)	//user press push to talk button quickly,  recover SPK output.
		{
					;//QDT_Common_SPKResume(); 		   
		}

			g_Data.bIsInPTTMode= qdt_false;
	}				
	}
	else
		QDT_Common_SPKResume(); 
	
	if (g_Data.bAudidDockingAnswerCalling)	
		QDT_PU_PushToTalkLED(qdt_ON);
	else
		QDT_PU_CheckSoundActivity(); 
	
  return QDT_SYS_NOERROR;	
}

RETURN_TYPE QDT_PU_SoundActivity(signed short *pi16Src, qdt_uint16 u16PcmCount)
{
	static qdt_uint16 u16ToSleepCount = 0;
	qdt_uint16 u16temp;
	qdt_sint16 PcmValue;
	qdt_uint32 TotalPcmValue;

	TotalPcmValue = 0;
	for(u16temp=1; u16temp< (u16PcmCount + 1); u16temp++)
	{
		PcmValue= *pi16Src;
		if (PcmValue < 0)
			PcmValue = 0xFFFFFFFF - PcmValue + 1; //negative number transfer to positive number
		if (u16temp%16)
		{
			TotalPcmValue += PcmValue;
			//printf("1AudioDetectLevel u16temp: %d PcmValue: %d TotalPcmValue: %d\n",u16temp, PcmValue, TotalPcmValue);
		}
		else
		{
			TotalPcmValue += PcmValue;
			//printf("2AudioDetectLevel u16temp: %d PcmValue: %d TotalPcmValue: %d\n",u16temp, PcmValue, TotalPcmValue);
			if (TotalPcmValue > 70000)
				g_Data.ucSoundActivity = 6;
			else if(TotalPcmValue > 50000)
				g_Data.ucSoundActivity = 5;
			else if(TotalPcmValue > 25000)
				g_Data.ucSoundActivity = 4;
			else if(TotalPcmValue > 10000)
				g_Data.ucSoundActivity = 3;
			else if(TotalPcmValue > 5000)
				g_Data.ucSoundActivity = 2;
			else if(TotalPcmValue > 2000)
				g_Data.ucSoundActivity = 1;
			else
				g_Data.ucSoundActivity = 0;
			
			TotalPcmValue = 0;
		}
		pi16Src++;
	}

	if (g_Data.ucSoundActivity > 3)
	{
		//if (u16ToSleepCount == 0)
			g_Data.bDisableSleepMode = qdt_true;	
			g_Data.bIsQuietMode = qdt_false;
		//else
		//	u16ToSleepCount--;
		u16ToSleepCount = 0xFFFF;
	}
	else
	{
		if (u16ToSleepCount > 255)
		{
			g_Data.bDisableSleepMode = qdt_false;
		}
		else
			u16ToSleepCount++;
	}
	return QDT_SYS_NOERROR;	
}
RETURN_TYPE QDT_PU_PushToTalkLED(qdt_bool bOnOff)
{

	PWMA->CMR0=1000;
	PWMB->CMR0=0;
	PWMA->CMR2=0;
	PWMA->CMR3=1000;
	
	return QDT_SYS_NOERROR;	
}

RETURN_TYPE QDT_PU_CheckSoundActivity(void)
{
	static qdt_uint32 u32LoopCounter = 0;

	if (g_Data.bEnableSleepLED)
		return QDT_SYS_NOERROR;
	
	u32LoopCounter++;
	if (u32LoopCounter >= 6000)   //count [xxx] ms to detect NightLED
	{
		//g_Data.ucSoundActivity++;
		switch (g_Data.ucSoundActivity)
		{
			case 0:
				PWMA->CMR0=3;
				PWMB->CMR0=3;
				PWMA->CMR2=3;
				PWMA->CMR3=3;
				break;
			case 1:
				PWMA->CMR0=10;
				PWMB->CMR0=10;
				PWMA->CMR2=10;
				PWMA->CMR3=10;
				break;
			case 2:
				PWMA->CMR0=50;
				PWMB->CMR0=50;
				PWMA->CMR2=50;
				PWMA->CMR3=50;
				break;
			case 3:
				PWMA->CMR0=100;
				PWMB->CMR0=100;
				PWMA->CMR2=100;
				PWMA->CMR3=100;
				break;
			case 4:
				PWMA->CMR0=250;
				PWMB->CMR0=250;
				PWMA->CMR2=250;
				PWMA->CMR3=250;
				break;
			case 5:
				PWMA->CMR0=500;
				PWMB->CMR0=500;
				PWMA->CMR2=500;
				PWMA->CMR3=500;
				break;
			case 6:
				PWMA->CMR0=1000;
				PWMB->CMR0=1000;
				PWMA->CMR2=1000;
				PWMA->CMR3=1000;
				break;
			default:
				break;			
		}		
		u32LoopCounter = 0;		
//		if (g_Data.ucSoundActivity > 7)
//			g_Data.ucSoundActivity = 0;	
	}
	return QDT_SYS_NOERROR;	
}


RETURN_TYPE QDT_PU_AudioALC(signed short *pi16Src, qdt_uint16 u16PcmCount)
{
	qdt_uint16 u16temp;
	qdt_sint16 PcmValue;
	qdt_uint32 TotalPcmValue;
	static qdt_uint8 ALCCount = 0;
	static qdt_uint8 ALCFlag = 0;
	
	TotalPcmValue = 0;
	for(u16temp = 0; u16temp< u16PcmCount; u16temp++)
	{
		PcmValue= *pi16Src;
		if (PcmValue < 0)
			PcmValue = 0xFFFFFFFF - PcmValue + 1; //negative number transfer to positive number
		if (u16temp < (u16PcmCount - 1))
		{
			TotalPcmValue += PcmValue;
		}
		else
		{
			TotalPcmValue += PcmValue;

			#if 1//ALC function
			if (ALCFlag == 0)
			{				
				if (TotalPcmValue >= ALC_TH)
				{
					ALCCount = ALCCount + 1;
					if (ALCCount == 100)//count 2 sec and  larger than ALCThreshold every time
					{
						DrvADC_SetPGAGaindB(2000);
						ALCCount = 0;
						ALCFlag = 1;
					}
				}
				else
					ALCCount = 0;
			}
			else
			{				
				if (TotalPcmValue < ALC_TH)
				{
					ALCCount = ALCCount + 1;
					if (ALCCount == 100)//count 2 sec and  larger than ALCThreshold every 
					{
						DrvADC_SetPGAGaindB(3000);
						ALCCount = 0;
						ALCFlag = 0;
					}
				}
				else
					ALCCount = 0;
			}		
			#endif
			
			TotalPcmValue = 0;
		}
		pi16Src++;
	}
	return QDT_SYS_NOERROR;	
}

RETURN_TYPE QDT_PU_ProcessStandByKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData)
{
	switch (ucCmd)
	{
	/* ======  RF Cmd =========================================== */
	/* ======  User Key =========================================== */
	case _USER_KEY_VERSION:
		QDT_Play_HV_FirmwareVersion(&g_ucFWVersion[9], 4);		
		QDT_TRACE("Parent Unit Version is %d:%d:%d:%d \n", g_ucFWVersion[9], g_ucFWVersion[10], g_ucFWVersion[11], g_ucFWVersion[12]);
		break;	
	case _USER_KEY_PARENTUNIT_KEY_POWER:
		QDT_PU_SysPowerOnOff();
		break;		

	/* ======  Long Press Key =========================================== */		
	case _USER_KEY_PARENTUNIT_KEY_LONGPRESS_VOLDOWN: /*clear user data*/
		QDT_Play_HumanVoice(HV_FactoryReset, 2);
		QDT_flash_ClearUserData();		
		break;	
	
	/* ======  Sensor RF Cmd =========================================== */	
	default:
		break;			
	}
	return QDT_SYS_NOERROR;	
}
RETURN_TYPE QDT_PU_PingAudioDocking(void)
{
	RFHEADER SentOutRFHeader;
	qdt_uint8 ucSentOutData[10];
	qdt_uint8 ucDestSerialNumber[5];

	if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_PINGAD_)
	{
		if (g_UserSaveData.ucPairingFreqIdx == 0xFF)  
			g_UserSaveData.ucPairingFreqIdx = 0;

		QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);
		
		ucDestSerialNumber[0] = 0x00;
		ucDestSerialNumber[1] = 0x00;
		ucDestSerialNumber[2] = 0x00;
		ucDestSerialNumber[3] = 0x00; 	
		ucDestSerialNumber[4] = 0x00; 	
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFCMD_PARENTUINT_PING_AD, ucDestSerialNumber);
		//g_Data.ucRFSentOutDelayTime = 1; //don't delay		
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData);	
		g_Data.ucPairingTryCount++;
		QDT_Common_DelayTime(250); 
		if (g_Data.ucPairingTryCount > 10)
		{
			g_UserSaveData.ucPairingFreqIdx++;
			g_Data.ucPairingTryCount = 0;

			if (g_UserSaveData.ucPairingFreqIdx >= RF_FREQ_TABLE_IDX_MAX)
				g_UserSaveData.ucPairingFreqIdx = 0;
			
			QDT_TRACE("Next Select Freq. idx = %d\n", g_UserSaveData.ucPairingFreqIdx);
		}
	}
	return QDT_SYS_NOERROR;	
}

RETURN_TYPE QDT_PU_ProcessPairingKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData)
{
	RFHEADER SentOutRFHeader;
	qdt_uint8 ucSentOutData[10];
	qdt_uint8 ucDestSerialNumber[5];
	switch (ucCmd)
	{
	/* ======  RF Cmd =========================================== */
	case RFACK_PARENTUINT_PING_AD:
	case RFCMD_AUDIODOCKING_SEARCHING_DEVICE: 				/*ParentUint Pairing State 1*/
		
		if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
			break;  //User don't press PairingKey,  don't handle PairingCmd
		
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_SEARCHING_;
	
		g_Data.bIsInPairingState = qdt_true;   				//start Pairing State 
		g_Data.bChangeToAllocRFFreqCH = qdt_false;
		g_Data.ucPairingTryCount = 0;
		
		ucDestSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		ucDestSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		ucDestSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		ucDestSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];		
		ucDestSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];	
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_PARENTUINT_IAMHERE, ucDestSerialNumber);
		//g_Data.ucRFSentOutDelayTime = 1; //don't delay		
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 	
		//g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME; 
		break;
	case RFCMD_AUDIODOCKING_TX_SYNCWORD: 					/*ParentUint Pairing State 2*/

		if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
			break;  //User don't press PairingKey,  don't handle PairingCmd

		QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
		g_UserSaveData.ucAllocRFFreqCH = RF_PARENTUNIT_CH0;

		g_UserSaveData.s_PairedAudioDocking.ucPairedRFFreqCH = RF_PARENTUNIT_CH0;
		g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[0]= ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[1]= ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[2]= ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[3]= ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];
		g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[4]= ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];
		
		g_UserSaveData.ucPairingSyncWord[0] = ucDumpRxData[QDT_RFPKT_DATA_START_IDX];
		g_UserSaveData.ucPairingSyncWord[1] = ucDumpRxData[QDT_RFPKT_DATA_START_IDX+1];
		g_UserSaveData.ucPairingSyncWord[2] = ucDumpRxData[QDT_RFPKT_DATA_START_IDX+2];
		g_UserSaveData.ucPairingSyncWord[3] = ucDumpRxData[QDT_RFPKT_DATA_START_IDX+3];
		g_Data.ucAllocRFFreqCH= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+4];
//#ifdef USER_SELECT_FREQ
//		g_UserSaveData.ucPairingFreqIdx= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+5];
//#endif
		ucDestSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		ucDestSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		ucDestSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		ucDestSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];
		ucDestSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_PARENTUINT_GOT_SYNCWORD, ucDestSerialNumber);
		//g_Data.ucRFSentOutDelayTime = 1; //don't delay
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 		
		//g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME; 
		
		QDT_Common_DelayTime(500); 
		QDT_AddKeyCmdData(RFCMD_AUDIODOCKING_TX_SYNCWORD, ucDumpRxData); //resent command 
		
		g_Data.ucPairingTryCount++;
		if (g_Data.ucPairingTryCount > 10)
		{
			g_Data.bChangeToAllocRFFreqCH = qdt_true;
			QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
			QDT_Common_DelayTime(500);			
			g_Data.ucPairingTryCount = 0;
		}		
		break;
	case RFCMD_AUDIODOCKING_TESTING_SYNCWORD: 				/*ParentUint Pairing State 3*/
		
		if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
			break;  //User don't press PairingKey,  don't handle PairingCmd

		g_Data.bChangeToAllocRFFreqCH = qdt_true;
		QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
		g_UserSaveData.ucAllocRFFreqCH = g_Data.ucAllocRFFreqCH;
		QDT_ClearCmd();

		ucDestSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		ucDestSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		ucDestSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		ucDestSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];
		ucDestSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_PARENTUINT_USING_SYNCWORD, ucDestSerialNumber);
		//g_Data.ucRFSentOutDelayTime = 1; //don't delay
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 		
		//g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME; //don't delay
		
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_FINISHED_;
		g_Data.bChangeToAllocRFFreqCH = qdt_false;
		g_Data.ucPairingTryCount = 0;
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_RUNNING;
		g_Data.bTwinkleRedLED = qdt_false;
		g_Data.bTwinkleGreenLED = qdt_false;	
		//g_UserSaveData.ucPairingFreqIdx = g_Data.ucParentUnitChooseCH;
		QDT_Play_HumanVoice(HV_PairingSuccess,2);	

		//QDT_flash_WriteUserData(&g_UserSaveData);
		QDT_Common_SaveUserData(qdt_false);

		//QDT_Common_SPKResume(); 					//open SPK ouput		
		
		PARENTUNIT_LED_PWR_RED(qdt_OFF);
		PARENTUNIT_LED_PWR_GREEN(qdt_ON);		

		QDT_INFO("Pairing device is AudioDocking DeviceID :: %x:%x:%x:%x:%x\n", ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX],
																		   ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1],
																		   ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2],
																		   ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3],
																		   ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4]);
		QDT_INFO("Allocate RF CH:: %d\n", g_Data.ucAllocRFFreqCH);
		QDT_INFO("Allocate RF Freq:: %d\n", g_UserSaveData.ucPairingFreqIdx);

		
		break;		
	case RFCMD_AUDIODOCKING_ASKGOTOUPGRADESTATE:
		ucDestSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		ucDestSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		ucDestSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		ucDestSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];		
		ucDestSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];	
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_AUDIODOCKING_ASKGOTOUPGRADESTATE, ucDestSerialNumber);
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 		
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_UPGRADE;

		
		break;
	/* ======  User Key =========================================== */	
	case _USER_KEY_VERSION:
		QDT_Play_HV_FirmwareVersion(&g_ucFWVersion[9], 4);		
		QDT_TRACE("Parent Unit Version is %d:%d:%d:%d \n", g_ucFWVersion[9], g_ucFWVersion[10], g_ucFWVersion[11], g_ucFWVersion[12]);
		break;		
	case _USER_KEY_PARENTUNIT_KEY_POWER:
		if (g_UserSaveData.ucPairingState != _QDT_PAIRINGSTATE_FINISHED_)
			g_UserSaveData.ucPairingState  = _QDT_PAIRINGSTATE_WAITFORSTAR_; //To recover pairing state to _QDT_PAIRINGSTATE_WAITFORSTAR_  during pairing procedure
		
		QDT_PU_SysPowerOnOff();
		break;				
	case _USER_KEY_PARENTUNIT_KEY_PTT:
   	 	QDT_Play_HumanVoice(HV_PairingMode,2);
		QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_UNPAIRING;
#ifdef USER_SELECT_FREQ
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_PINGAD_;
#else
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_SEARCHING_;
#endif
		g_Data.bTwinkleRedLED = qdt_false;
		PARENTUNIT_LED_PWR_RED(qdt_OFF);
		g_Data.bTwinkleGreenLED = qdt_true;
		break;			
			
	/* ======  Long Press Key =========================================== */
	case _USER_KEY_PARENTUNIT_KEY_LONGPRESS_VOLDOWN: /*clear user data*/
		QDT_Play_HumanVoice(HV_FactoryReset, 2);
		g_Data.bFactoryResetFlag = qdt_true;		
		QDT_PU_SysPowerOnOff(); //power off
		QDT_flash_ClearUserData();		
		break;		
		
	default:
		break;			
	}
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_PU_ProcessKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData)
{
	RFHEADER SentOutRFHeader;
	qdt_uint8 ucSentOutData[10];
	qdt_uint8 ucDestSerialNumber[5];

	if (g_Data.bIsInPTTMode == qdt_true && (ucCmd < 0x80 && ucCmd > 0x60))
		return QDT_SYS_NOERROR;	//In PTT Mode, system don't handle keyCmd.

		
	switch (ucCmd)
	{
	/* ======  RF Cmd =========================================== */
	case RFCMD_AUDIODOCKING_ASKGOTOUPGRADESTATE:
		ucDestSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		ucDestSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		ucDestSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		ucDestSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];		
		ucDestSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];	

		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_AUDIODOCKING_ASKGOTOUPGRADESTATE, ucDestSerialNumber);
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 		
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_UPGRADE;
	  break;
	case RFACK_AUDIODOCKING_UNPAIRED:
		QDT_PU_UnPaired();
		break;
	//---------------------------------------------------------------------------------------------//	
	case RFCMD_AUDIODOCKING_ONLINE:
		break;
		
	case RFACK_AUDIODOCKING_ANSWERCALLING:	
		QDT_PU_AudioDocking_AnswerCalling();
		break;	
		
	case RFACK_AUDIODOCKING_HANDUP:	
		QDT_PU_AudioDocking_HandUp();
		break;			
		
	case RFCMD_AUDIODOCKING_AUDIOALERT:		
		g_Data.bTwinkleOrangeLED = qdt_true;
		g_Data.bVibrationEnable = qdt_true;
		g_Data.bAudioAlertFlag = qdt_true;
		
		ucDestSerialNumber[0] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[0];
		ucDestSerialNumber[1] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[1];
		ucDestSerialNumber[2] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[2];
		ucDestSerialNumber[3] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[3];	
		ucDestSerialNumber[4] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[4];
		
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_PARENTUINT_AUDIOALERT, ucDestSerialNumber);
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData);
		break;	
	case RFCMD_AUDIODOCKING_SENSORALERT_MOTION:
		g_Data.bTwinkleOrangeLED = qdt_true;
		g_Data.bVibrationEnable = qdt_true;
		g_Data.bSensorAlertMotion = qdt_true;
		ucDestSerialNumber[0] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[0];
		ucDestSerialNumber[1] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[1];
		ucDestSerialNumber[2] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[2];
		ucDestSerialNumber[3] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[3];		
		ucDestSerialNumber[4] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[4];	
		
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_AUDIODOCKING_SENSORALERT_MOTION, ucDestSerialNumber);
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData);
		break;
		
	//---------------------------------------------------------------------------------------------//	

	/* ======  User Key =========================================== */
	case _USER_KEY_VERSION:
		QDT_Play_HV_FirmwareVersion(&g_ucFWVersion[9], 4);		
		QDT_TRACE("Parent Unit Version is %d:%d:%d:%d \n", g_ucFWVersion[9], g_ucFWVersion[10], g_ucFWVersion[11], g_ucFWVersion[12]);
		break;		
	case _USER_KEY_PARENTUNIT_KEY_POWER:
		QDT_PU_SysPowerOnOff();
		break;
		
	case _USER_KEY_PARENTUNIT_KEY_VOLUP:
		if (g_Data.ucPreVol<16)
			g_Data.ucCurVol++;
		if (g_Data.ucCurVol>16)
			g_Data.ucCurVol = 16;
		QDT_DEBUG("Volume UP g_Data.ucCurVol = %d\n",g_Data.ucCurVol);
		break;
    case _USER_KEY_PARENTUNIT_KEY_VOLDOWN:
		
#if 0
			QDT_ERROR("RF4461 crash, reset it \n");
			vRadio_Init();//init RF
			g_Data.bParentCalling = qdt_false;
			g_Data.bParentHandUp= qdt_true; 
			if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
			{
				QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
			}
			else
			{
				QDT_Common_SetSyncWord(PAIRING_SYNCWORD);						
			}
			
			vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
			QDT_Common_MicResume();
			g_Data.bRFTransmitFinished = qdt_true;
			g_Data.bRFReceiveFinished = qdt_true;				
#endif			
		if (g_Data.ucPreVol == 0)
			g_Data.ucCurVol = 0;
		else
			g_Data.ucCurVol--;		

		QDT_DEBUG("Volume DOWN g_Data.ucCurVol = %d\n",g_Data.ucCurVol);
		break;
	case _USER_KEY_PARENTUNIT_KEY_ALERT:
		if (g_Data.bAudioAlertFlag == qdt_true)
		{
			g_Data.bTwinkleOrangeLED = qdt_false;
			PARENTUNIT_LED_ALERT_ORANGE(qdt_OFF);
			g_Data.bVibrationEnable = qdt_false;
			PARENTUNIT_VIBRATOR(qdt_OFF);
			QDT_Play_HumanVoice(HV_BabyCry,1);		
			g_Data.bAudioAlertFlag = qdt_false;
#ifdef SUPPORT_SLEEP_MODE
			g_Data.bUserCancelQuietMode = qdt_true;
			g_Data.bIsQuietMode = qdt_false;	
			g_Data.bEnableSleepLED = qdt_false;
#endif			
		}
		break;
	case RFCMD_FACTORYTEST_RFTEST:
		QDT_Factory_RFTest_ACK();
		break;
	/* ======  Long Press Key =========================================== */
	case _USER_KEY_PARENTUNIT_KEY_LONGPRESS_VOLDOWN: /*clear user data*/
		QDT_Play_HumanVoice(HV_FactoryReset, 2);
		g_Data.bFactoryResetFlag = qdt_true;
		QDT_PU_SysPowerOnOff(); //power off
		QDT_flash_ClearUserData();		
		break;		
	case _USER_KEY_PARENTUNIT_KEY_LONGPRESS_VOLUP: /*Unpaired*/ 

		ucDestSerialNumber[0] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[0];
		ucDestSerialNumber[1] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[1];
		ucDestSerialNumber[2] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[2];
		ucDestSerialNumber[3] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[3];	
		ucDestSerialNumber[4] = g_UserSaveData.s_PairedAudioDocking.ucSerialNumber[4];	
		
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFCMD_PARENTCOMMAND_UNPAIRING, ucDestSerialNumber);
		SentOutRFHeader.ucDatalength[0] = 0x00;
		SentOutRFHeader.ucDatalength[1] = 0x01;
		ucSentOutData[0] = g_UserSaveData.ucAllocRFFreqCH;		
		//g_Data.ucRFSentOutDelayTime = 1; //don't delay
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 		
		//g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME; //don't delay	
		
		QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_LONGPRESS_VOLUP, ucSentOutData);
		QDT_Common_DelayTime(250);
		break;
	case _USER_KEY_PARENTUNIT_KEY_LONGPRESS_ALERT:
		if (g_Data.bIsBatteryMode && g_Data.bUserCancelQuietMode)
		{	
			g_Data.bIsQuietMode = qdt_true;
			g_Data.bUserCancelQuietMode = qdt_false;
			QDT_Common_SPKPause();
		}
		else if (g_Data.bIsBatteryMode && !g_Data.bUserCancelQuietMode)
		{
			g_Data.bIsQuietMode = qdt_false;
			g_Data.bUserCancelQuietMode = qdt_true;
		}
		break;
	/* ======  Sensor test case =========================================== */
	//case RFCMD_AUDIODOCKING_QUERYSENSORSTATUE: 	
	//	QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_SERSORDATA_UPDATESTATUS, ucDestSerialNumber);
	//	SentOutRFHeader.ucDestSerialNumber[0] = 0x00;
	//	SentOutRFHeader.ucDestSerialNumber[1] = 0x00;
	//	SentOutRFHeader.ucDestSerialNumber[2] = 0x00;
	//	SentOutRFHeader.ucDestSerialNumber[3] = 0x00;		
		
	//	SentOutRFHeader.ucDatalength[0] = 0x00;
	//	SentOutRFHeader.ucDatalength[1] = 0x01;
	//	ucSentOutData[0] = 0x41;		
		//g_Data.ucRFSentOutDelayTime = 1; //don't delay
	//	QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 		
	//	QDT_Common_DelayTime(5);
		//g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME; //don't delay	
		//break;
	default:
	
		break;			
	}
	return QDT_SYS_NOERROR;
}
RETURN_TYPE QDT_PU_DumpUpgradeImage(qdt_uint8 *ucRxData)
{
	qdt_uint8  ucChecksum1, ucChecksum2, i;
	if(ucRxData[0]==QDT_UPGRADEFW_START_BYTE && ucRxData[QDT_UPGRADEFW_IMAGE_LENGTH_42BYTE-1]==QDT_UPGRADEFW_END_BYTE)
	{
		ucChecksum1 =  QDT_Common_CheckSumCalc(&ucRxData[1], 19);
		ucChecksum2 =  QDT_Common_CheckSumCalc(&ucRxData[17], 19);
		if (ucChecksum1 == ucRxData[QDT_UPGRADEFW_IMAGE_LENGTH_42BYTE-3] && ucChecksum2 == ucRxData[QDT_UPGRADEFW_IMAGE_LENGTH_42BYTE-2])
		{
			for (i=0; i<QDT_UPGRADEFW_IMAGE_LENGTH_42BYTE;	i++)
			{
				g_ucDumpImageData[i] = ucRxData[i];
			}
			
		}
	}
	else
	{
		g_ucDumpImageData[0] = 0xEE; 
	}
		return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_PU_ProcessUpgradeKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData)
{
	RFHEADER SentOutRFHeader;
	qdt_uint8 ucSentOutData[10];
	qdt_uint8 ucDestSerialNumber[5];
	
	static qdt_bool bEraseFlash =qdt_false;
	switch (ucCmd)
	{
	/* ======  RF Cmd =========================================== */
	case RFCMD_AUDIODOCKING_ASKGOTOUPGRADESTATE:
		ucDestSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		ucDestSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		ucDestSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		ucDestSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];		
		ucDestSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];	
		
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_AUDIODOCKING_ASKGOTOUPGRADESTATE, ucDestSerialNumber);
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 		
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_UPGRADE;
		g_Data.bTwinkleRedLED = qdt_true;
		g_Data.bTwinkleGreenLED = qdt_true;	
		if (!bEraseFlash)
		{
			QDT_Upgrade_EarseImageAddr();	
			bEraseFlash = qdt_true;
		}
		break;
	case RFCMD_AUDIODOCKING_UPGRADE_FWINFO:

		g_UpdateImageInfo.ucImageVersion[0] = ucDumpRxData[QDT_RFPKT_DATA_START_IDX];
		g_UpdateImageInfo.ucImageVersion[1] = ucDumpRxData[QDT_RFPKT_DATA_START_IDX+1];
		g_UpdateImageInfo.ucImageVersion[2] = ucDumpRxData[QDT_RFPKT_DATA_START_IDX+2];
		g_UpdateImageInfo.ucImageVersion[3] = ucDumpRxData[QDT_RFPKT_DATA_START_IDX+3];
		
		g_UpdateImageInfo.u32ImageSize = ucDumpRxData[QDT_RFPKT_DATA_START_IDX+4]<<24|ucDumpRxData[QDT_RFPKT_DATA_START_IDX+5]<<16|ucDumpRxData[QDT_RFPKT_DATA_START_IDX+6]<<8|ucDumpRxData[QDT_RFPKT_DATA_START_IDX+7];
		g_UpdateImageInfo.ucImageType =  ucDumpRxData[QDT_RFPKT_DATA_START_IDX+8];
		g_UpdateImageInfo.u32AddrIdx = 0;
		ucDestSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		ucDestSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		ucDestSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		ucDestSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];		
		ucDestSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];	
		
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_AUDIODOCKING_UPGRADE_FWINFO, ucDestSerialNumber);
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 	
		break;
	case RFCMD_AUDIODOCKING_UPGRADE_FWIMAGE:
		if (g_ucDumpImageData[0] == 0xEE)
			return QDT_SYS_FAILED;
		
		g_UpdateImageInfo.u32AddrIdx = 	g_ucDumpImageData[2]<<24|g_ucDumpImageData[3]<<16|g_ucDumpImageData[4]<<8|g_ucDumpImageData[5];
		QDT_Upgrade_WriteImage((qdt_uint8)_QDT_UPGRADETRANSFERTYPE_RF_, g_UpdateImageInfo.ucImageType, g_UpdateImageInfo.u32AddrIdx, g_ucDumpImageData[6], &g_ucDumpImageData[7]);

		g_UpdateImageInfo.u32AddrIdx+=g_ucDumpImageData[6]; //next image addr idx
		
		ucDestSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		ucDestSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		ucDestSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		ucDestSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];		
		ucDestSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];	
		
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_AUDIODOCKING_UPGRADE_FWIMAGE, ucDestSerialNumber);
		
		SentOutRFHeader.ucDatalength[0] = 0x00;
		SentOutRFHeader.ucDatalength[1] = 0x04;
		ucSentOutData[0] = g_UpdateImageInfo.u32AddrIdx>>24&0xFF;;
		ucSentOutData[1] = g_UpdateImageInfo.u32AddrIdx>>16&0xFF;;
		ucSentOutData[2] = g_UpdateImageInfo.u32AddrIdx>>8&0xFF;;
		ucSentOutData[3] = g_UpdateImageInfo.u32AddrIdx&0xFF;;		

		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 	

		break;		
	case RFCMD_AUDIODOCKING_UPGRADE_FINISH:
		bEraseFlash = qdt_false;
		ucDestSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		ucDestSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		ucDestSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		ucDestSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];		
		ucDestSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];	
		
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_AUDIODOCKING_UPGRADE_FINISH, ucDestSerialNumber);
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 	

		UNLOCKREG();				
		DrvFMC_EnableISP(1);
		UpdateConfig(0xFFFFFF7F, 0xFFFFFFFF);
		QDT_Common_DelayTime(3000);
		g_Data.bTwinkleRedLED = qdt_true;		  
		DrvSYS_ResetChip();
		
		break;	
	/* ======  User Key =========================================== */
	/* ======  Long Press Key =========================================== */
	default:
		break;			
	}
	return QDT_SYS_NOERROR;
}
#ifdef SUPPORT_SLEEP_MODE
RETURN_TYPE QDT_PU_SysWakeup(void)
{
	QDT_TRACE("Parent Unit Wake up....\n");
	PMU_ExitSleepMode();
	
	vRadio_Init();												/* Initial RF-446x */
			
	DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
	QDT_Common_DelayTime(100);
	
	if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
	{
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_UNPAIRING;
		g_Data.bTwinkleRedLED = qdt_true;
		QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
		#ifdef USER_SELECT_FREQ		
		QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
		#endif			
	}
	else
	{
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_WAKEUP;
		g_Data.bTwinkleRedLED = qdt_false;		
		QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
		#ifdef USER_SELECT_FREQ		
		QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
		#endif
	}
	vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
	
	return QDT_SYS_NOERROR;
}
RETURN_TYPE QDT_PU_CheckIsWakeUp()
{
	SYSCLK->APBCLK.SBRAM_EN = 1;		//enable StandBy Ram[256] Will be reset by SPD wakeup	 
	if(SYSCLK->PFLAGCON.PD_FLAG != 0)		//Check with Standby PD flag for first power on, PD_FLAG will be 1 if ISD9160 wakeup from standby PD
	{
		QDT_DEBUG("wake up :: STBRAM_IDX_STBCOUNTER = %x\n", SBRAM->D[STBRAM_IDX_STBCOUNTER]);
		QDT_DEBUG("wake up :: STBRAM_IDX_SYSSTATE = %x\n", SBRAM->D[STBRAM_IDX_SYSSTATE]);
		QDT_DEBUG("wake up :: STBRAM_IDX_ALERTEVENT = %x\n", SBRAM->D[STBRAM_IDX_ALERTEVENT]);		
		if (SBRAM->D[STBRAM_IDX_SYSSTATE] != SYSSTATE_STANDBY)
			QDT_PU_SysWakeup();   	//Reset System state
		else
		{
			//it mean system is in standby, need recover the LED status 
			PARENTUNIT_LED_PWR_RED(qdt_ON);
			PARENTUNIT_LED_PWR_GREEN(qdt_OFF);				
		}
		//handle STB RAM
		if (SBRAM->D[STBRAM_IDX_ALERTEVENT] & 0x01)
			g_Data.bAudioAlertFlag = qdt_true;	
		
	}

	return QDT_SYS_NOERROR;
}
RETURN_TYPE QDT_PU_SysStanbByPowerDown(void)
{
	PARENTUNIT_LED_PWR_RED(qdt_ON);
	PARENTUNIT_LED_PWR_GREEN(qdt_ON);	

	if (g_Data.ucSysState != SYSSTATE_STANDBY)
	{
		PWMA->CMR0=0;
		PWMB->CMR0=0;
		PWMA->CMR2=0;
		PWMA->CMR3=0;
		QDT_Common_DelayTime(1000);
		PARENTUNIT_LED_PWR_RED(qdt_OFF);
		PARENTUNIT_LED_PWR_GREEN(qdt_OFF);
		PARENTUNIT_LED_ALERT_ORANGE(qdt_OFF);
		QDT_PU_DisableGpioPwm();


		//g_Data.ucPreSysState = g_Data.ucSysState;
		//g_Data.ucSysState = SYSSTATE_STANDBY;	
		//g_Data.bTwinkleRedLED = qdt_false;
		//g_Data.bTwinkleGreenLED = qdt_false;
		//g_Data.bTwinkleOrangeLED = qdt_false;

		DrvGPIO_DisableInt(GPB, 7);  		    //disable RF-4461 interrupt Pin
		PARENTUNIT_RF_446x(qdt_OFF);   			//turn off RF-4461

		if (g_UserSaveData.ucPairingState != _QDT_PAIRINGSTATE_FINISHED_)  //pairing is not finish, recover to  _QDT_PAIRINGSTATE_WAITFORSTAR_
			g_UserSaveData.ucPairingState =  _QDT_PAIRINGSTATE_WAITFORSTAR_;
		
		QDT_Common_SaveUserData(qdt_true);
		
		QDT_ClearCmd();
		QDT_Common_SPKPause();					//pause SPK ouput		
		QDT_Common_MicPause(); 					//pause MIC input	
	}
	QDT_TRACE("Parent Unit Sleep....\n");
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_PU_SysEnterSleep(void)
{

	PWMA->CMR0=0;
	PWMB->CMR0=0;
	PWMA->CMR2=0;
	PWMA->CMR3=0;
	QDT_Common_DelayTime(1000);
	PARENTUNIT_LED_PWR_RED(qdt_OFF);
	PARENTUNIT_LED_PWR_GREEN(qdt_OFF);
	PARENTUNIT_LED_ALERT_ORANGE(qdt_OFF);
	//QDT_PU_DisableGpioPwm();


	g_Data.ucPreSysState = g_Data.ucSysState;
	g_Data.ucSysState = SYSSTATE_SLEEP;	


	DrvGPIO_DisableInt(GPB, 7);  		    //disable RF-4461 interrupt Pin
	PARENTUNIT_RF_446x(qdt_OFF);   			//turn off RF-4461

	QDT_Common_SPKPause();					//pause SPK ouput		
	QDT_Common_MicPause(); 					//pause MIC input	
	
	QDT_TRACE("Parent Unit Enter Sleep....\n");
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_PU_SysExitSleep(void)
{

	//if (g_Data.ucPreSysState != SYSSTATE_STANDBY)  // sleep to poweron
	//{		
		PMU_ExitSleepMode();
		QDT_PU_SysPowerOn();	
	//}
	//else										// poweron to standby
	//{
	//	QDT_PU_SysPowerOff();	
	//	PMU_EnterSleepMode();
	//}
	PMU_RTCClose();
	QDT_TRACE("Parent Unit exit Sleep....\n");
	
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_PU_SysRTCExitSleep(void)
{

	//if (g_Data.ucPreSysState != SYSSTATE_STANDBY)  // sleep to poweron
	//{		
	PMU_ExitSleepMode();
	
	PARENTUNIT_RF_446x(qdt_ON);
 	vRadio_Init();	                							/* Initial RF-446x */

	DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
	
	QDT_Common_DelayTime(100);
	QDT_Common_SPKPause();					//pause SPK ouput		
	QDT_Common_MicPause(); 					//pause MIC input	

	
	if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
	{
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_UNPAIRING;
		g_Data.bTwinkleRedLED = qdt_true;
		QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
		#ifdef USER_SELECT_FREQ		
		QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);     //change RF Freq
		#endif				
	}
	else
	{
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_RUNNING;
		g_Data.bTwinkleRedLED = qdt_false;		
		QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
		#ifdef USER_SELECT_FREQ		
		QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
		#endif
	}

	vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
	PMU_RTCClose();
	QDT_TRACE("Parent Unit exit Sleep....\n");
	
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_PU_ProcessSleepModeKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData)
{

    if (__qdt_IsPressPush2TalkButton() && ucCmd == _USER_KEY_NONE)
	{
		ucCmd = _USER_KEY_PARENTUNIT_KEY_POWER;
	}

	switch (ucCmd)
	{
	case _USER_KEY_PARENTUNIT_KEY_POWER:
		g_Data.bEnableSleepLED = qdt_false;
		QDT_PU_SysExitSleep();
		break;
	case _USER_KEY_PARENTUNIT_KEY_ALERT:	
		if (g_Data.bAudioAlertFlag == qdt_true)
		{
			g_Data.bUserCancelQuietMode = qdt_true;
			g_Data.bIsQuietMode = qdt_false;	
			g_Data.bEnableSleepLED = qdt_false;
			QDT_PU_SysExitSleep();
			QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_ALERT, NULL);
		}
		break;		
	default:
		break;			
	}
	
	return QDT_SYS_NOERROR;
}
#endif
#endif //BUILDTYPE_PARENTUINT_PROJECT

