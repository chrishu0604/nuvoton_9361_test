#include <stdio.h>
#include <string.h>
#include "ISD93xx.h"
#include "NVTTypes.h"
#include "bsp.h"
#include "radio.h"
#include "Driver\DrvGPIO.h"
#include "qdt_ram.h"
#include "qdt_utils.h"
#include "qdt_common_func.h"
#include "qdt_human_voice.h"
#include "Lib\LibSiren7.h"
#include "qdt_flash_hal.h"
#include "qdt_audiodocking.h"
#include "qdt_sysState.h"
#include "si446x_api_lib.h"    //RF-664x header file


qdt_uint8 HV_PowerOn[2] = {0,1};
qdt_uint8 HV_PowerOff[2] = {0,2};
qdt_uint8 HV_PairingMode[2] = {3,4};
qdt_uint8 HV_PairingSuccess[2] = {3,5};
qdt_uint8 HV_PairingFail[2] = {3,6};
qdt_uint8 HV_BabyCry[1] = {10};
qdt_uint8 HV_sensor[4];
qdt_uint8 HV_BEE[1] = {44};
qdt_uint8 HV_FactoryReset[2] = {8,9};
#if 1//#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
qdt_uint8 HV_PlsPressVolToSelFreq[1] = {45};
#endif

extern sSiren7_CODEC_CTL sEnDeCtl;
extern sSiren7_ENC_CTX sS7Enc_Ctx;
extern sSiren7_DEC_CTX sS7Dec_Ctx;

extern volatile uint32_t BufferReadyAddr;	
extern volatile uint32_t AudBufEmptyAddr;	

extern volatile uint16_t AudBufAddress[BUFFER_SAMPLECOUNT];

uint32_t dwCurVoiceSize;  		// = PCM_LENGTH
BOOL	 bVoicePlaying;
uint8_t		u8LastTwoBufferCount;
uint32_t AudioSampleCount,AudioDataAddr;

/*---------------------------------------------------------------------------------------------------------*/
/* Play sound initialization                                                                               */
/*---------------------------------------------------------------------------------------------------------*/
void CopySoundData(void)
{
#if 0//#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
	qdt_uint8 s8Out_Voice[COMPBUFSIZE*2];
	signed short AudioVoiceBuffer[COMPBUFSIZE];	
	qdt_uint8 VoiceIndex = 0;
	qdt_uint8 i;

	QDT_flash_ReadBytes(AudioDataAddr, COMPBUFSIZE*2, s8Out_Voice);
	for(i=0; i<COMPBUFSIZE*2;i=i+2)
	{
		AudioVoiceBuffer[VoiceIndex] = s8Out_Voice[i] | (s8Out_Voice[i+1]<<8);
		VoiceIndex++;
	}
	LibS7Decode(&sEnDeCtl, &sS7Dec_Ctx, (signed short *)AudioVoiceBuffer, (signed short *)AudBufAddress);
	QDT_Common_VolumeControlCopy((signed short *)AudBufAddress, (signed short *)AudBufEmptyAddr, BUFFER_SAMPLECOUNT);
	
	AudioDataAddr=AudioDataAddr+(COMPBUFSIZE * 2);  //next voice index address
	AudioSampleCount=AudioSampleCount+BUFFER_SAMPLECOUNT;  //already play sample count
	
	if (AudioSampleCount >= ((dwCurVoiceSize)*8))   		//TotalG722Size unit is byte
		u8LastTwoBufferCount=0;
#endif

#if 1//#ifdef BUILDTYPE_PARENTUINT_PROJECT
	LibS7Decode(&sEnDeCtl, &sS7Dec_Ctx, (signed short *)AudioDataAddr, (signed short *)AudBufAddress);
	QDT_Common_VolumeControlCopy((signed short *)AudBufAddress, (signed short *)AudBufEmptyAddr, BUFFER_SAMPLECOUNT);
	
	AudioDataAddr=AudioDataAddr+(COMPBUFSIZE * 2);  //next voice index address
	AudioSampleCount=AudioSampleCount+BUFFER_SAMPLECOUNT;  //already play sample count
	
	if (AudioSampleCount >= ((dwCurVoiceSize)*8))   		//TotalG722Size unit is byte
		u8LastTwoBufferCount=0;
#endif
}

void PlaySound(uint32_t DataAddr)
{	
	AudioDataAddr= DataAddr;
	u8LastTwoBufferCount=0xFF;
	AudioSampleCount=0;
	CopySoundData();
	bVoicePlaying = TRUE;
	g_Data.bSPKBufferEmpty = TRUE;
}

void PlayLoop(void)
{
		if (g_Data.bSPKBufferEmpty == TRUE)
		{
			CopySoundData();
			g_Data.bSPKBufferEmpty = FALSE;
		}
}

void PlayG722Stream(uint16_t AudIndex)
{
#if 0//#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
	uint32_t  u32AudPointer, u32StartAddr;
	uint32_t pTemp0, pTemp1;
	uint8_t pTemp[4];

	//----------------------------------------------------------------------------------------
	// Based on the header structure of VPE output file to get sound stream address and length
	u32AudPointer = FLASH_VOICE_DATA_ADDR + 8*(AudIndex+1);
	QDT_flash_ReadBytes(u32AudPointer, 4, pTemp);
	pTemp0 = (uint32_t)pTemp[0] | (uint32_t)(pTemp[1]<<8) | (uint32_t)(pTemp[2]<<16) | (uint32_t)(pTemp[3]<<24);
	QDT_flash_ReadBytes(u32AudPointer+8, 4, pTemp);
	pTemp1 = (uint32_t)pTemp[0] | (uint32_t)(pTemp[1]<<8) | (uint32_t)(pTemp[2]<<16) | (uint32_t)(pTemp[3]<<24);
	u32StartAddr= pTemp0 + FLASH_VOICE_DATA_ADDR;
	dwCurVoiceSize = pTemp1 - pTemp0;
	if(dwCurVoiceSize>COMPBUFSIZE*2)
	{ 
		PlaySound(u32StartAddr);
		while (bVoicePlaying == TRUE)
		{
			PlayLoop();	
			//CheckPause();				//Check PAUSE/RESUME key
		}
	}
#endif

#if 1//#ifdef BUILDTYPE_PARENTUINT_PROJECT
	uint32_t  u32AudPointer, u32StartAddr;
	uint32_t *pTemp0, *pTemp1;
	//----------------------------------------------------------------------------------------
	// Based on the header structure of VPE output file to get sound stream address and length
	u32AudPointer = (uint32_t)&AudioDataBegin + 8*(AudIndex+1);
	pTemp0=(uint32_t *)u32AudPointer;
	pTemp1=(uint32_t *)(u32AudPointer+8);
	u32StartAddr= *pTemp0 +	 (uint32_t)&AudioDataBegin;
	dwCurVoiceSize = *pTemp1 - *pTemp0;

	if(dwCurVoiceSize>COMPBUFSIZE*2)
	{ 
		PlaySound(u32StartAddr);
		while (bVoicePlaying == TRUE)
		{
			PlayLoop();	
			//CheckPause();				//Check PAUSE/RESUME key
		}
	}
#endif
}

void QDT_Play_HumanVoice(qdt_uint8 *pSrc, qdt_uint8 uCount)
{
#if 0//#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
	qdt_uint8 i;

	DrvGPIO_DisableInt(GPB, 7); 			//disable RF-4461 interrupt Pin
	AUDIODOCKING_RF_446x(qdt_OFF);			//turn off RF-4461
	
	g_Data.bHumanVoice= qdt_true;
	if (g_Data.bSPKOpened == qdt_true)
	{
		for (i=0; i<uCount; i++)
		{
			PlayG722Stream((qdt_uint16)pSrc[i]);
			QDT_Common_DelayTime(10);
		}
		QDT_Common_SPKPause();
	}
	else
	{
		QDT_Common_SPKResume();
		for (i=0; i<uCount; i++)
		{
			PlayG722Stream((qdt_uint16)pSrc[i]);	
			QDT_Common_DelayTime(10);
		}
		QDT_Common_SPKPause();
	}	
	g_Data.bHumanVoice = qdt_false;
	QDT_Common_DelayTime(100);
	
	AUDIODOCKING_RF_446x(qdt_ON);
	vRadio_Init();												
	QDT_Common_DelayTime(100);
	DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
	if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
	{
		QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
#ifdef USE_AUTO_CHANGE_FREQ		
		QDT_Common_SetFreq(0);	   //change RF Freq
#endif				
		QDT_Common_DelayTime(100);
		g_Data.bRFTransmitFinished = qdt_true;
		g_Data.bRFReceiveFinished = qdt_true;				
		vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
	}
	else
	{
		QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
#ifdef USE_AUTO_CHANGE_FREQ		
		QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
#endif

		QDT_Common_DelayTime(100);
		g_Data.bRFTransmitFinished = qdt_true;
		g_Data.bRFReceiveFinished = qdt_true;				
		vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
	}
#endif

#if 1//#ifdef BUILDTYPE_PARENTUINT_PROJECT
	qdt_uint8 i;

	g_Data.bHumanVoice= qdt_true;
	if (g_Data.bSPKOpened == qdt_true)
	{
		for (i=0; i<uCount; i++)
		{
			PlayG722Stream((qdt_uint16)pSrc[i]);
			QDT_Common_DelayTime(10);
		}
		g_Data.bHumanVoice = qdt_false;
		QDT_Common_SPKPause();
	}
	else
	{
		QDT_Common_SPKResume();
		for (i=0; i<uCount; i++)
		{
			PlayG722Stream((qdt_uint16)pSrc[i]);	
			QDT_Common_DelayTime(10);
		}
		g_Data.bHumanVoice = qdt_false;
		QDT_Common_SPKPause();
	}	
	g_Data.bHumanVoice = qdt_false;
#endif

}

void QDT_SensorDataToIndex(qdt_uint8 ucData, qdt_uint8 index)
{
	switch(ucData)
	{
		case 0:
			HV_sensor[index] = HV_NULL;
			break;
		case 1:
			HV_sensor[index] = HV_ONE;
			break;
		case 2:
			HV_sensor[index] = HV_TWO;
			break;
		case 3:
			HV_sensor[index] = HV_THREE;
			break;
		case 4:
			HV_sensor[index] = HV_FOUR;
			break;
		case 5:
			HV_sensor[index] = HV_FIVE;
			break;
		case 6:
			HV_sensor[index] = HV_SIX;
			break;
		case 7:
			HV_sensor[index] = HV_SEVEN;
			break;
		case 8:
			HV_sensor[index] = HV_EIGHT;
			break;
		case 9:
			HV_sensor[index] = HV_NINE;
			break;
		default:
			break;
	}
}

void QDT_Play_HV_SensorHumidity(qdt_uint8 ucSensorData)
{
	qdt_uint8 data[2];

	data[0] = ucSensorData/10;
	data[1] = ucSensorData%10;
	
	HV_sensor[0] = HV_HUMIDITY;
	switch(data[0])
	{
		case 0:
			HV_sensor[1] = HV_NULL;
			switch(data[1])
			{
				case 0:
					HV_sensor[2] = HV_ZERO;
					break;
				case 1:
					HV_sensor[2] = HV_ONE;
					break;
				case 2:
					HV_sensor[2] = HV_TWO;
					break;
				case 3:
					HV_sensor[2] = HV_THREE;
					break;
				case 4:
					HV_sensor[2] = HV_FOUR;
					break;
				case 5:
					HV_sensor[2] = HV_FIVE;
					break;
				case 6:
					HV_sensor[2] = HV_SIX;
					break;
				case 7:
					HV_sensor[2] = HV_SEVEN;
					break;
				case 8:
					HV_sensor[2] = HV_EIGHT;
					break;
				case 9:
					HV_sensor[2] = HV_NINE;
					break;
				default:
					break;
			}
			break;

		case 1:
			switch(data[1])
			{
				case 0:
					HV_sensor[1] = HV_TEN;
					break;
				case 1:
					HV_sensor[1] = HV_ELEVEN;
					break;
				case 2:
					HV_sensor[1] = HV_TWELVE;
					break;
				case 3:
					HV_sensor[1] = HV_THIRTEEN;
					break;
				case 4:
					HV_sensor[1] = HV_FOURTEEN;
					break;
				case 5:
					HV_sensor[1] = HV_FIFTEEN;
					break;
				case 6:
					HV_sensor[1] = HV_SIXTEEN;
					break;
				case 7:
					HV_sensor[1] = HV_SEVENTEEN;
					break;
				case 8:
					HV_sensor[1] = HV_EIGHTEEN;
					break;
				case 9:
					HV_sensor[1] = HV_NINETEEN;
					break;
				default:
					break;
			}
			HV_sensor[2] = HV_NULL;
			break;
			
		case 2:
			HV_sensor[1] = HV_TWENTY;
			QDT_SensorDataToIndex(data[1], 2);
			break;

		case 3:
			HV_sensor[1] = HV_THIRTY;
			QDT_SensorDataToIndex(data[1], 2);
			break;

		case 4:
			HV_sensor[1] = HV_FORTY;
			QDT_SensorDataToIndex(data[1], 2);
			break;

		case 5:
			HV_sensor[1] = HV_FIFTY;
			QDT_SensorDataToIndex(data[1], 2);
			break;
			
		case 6:
			HV_sensor[1] = HV_SIXTY;
			QDT_SensorDataToIndex(data[1], 2);
			break;
			
		case 7:
			HV_sensor[1] = HV_SEVENTY;
			QDT_SensorDataToIndex(data[1], 2);
			break;
			
		case 8:
			HV_sensor[1] = HV_EIGHTY;
			QDT_SensorDataToIndex(data[1], 2);
			break;
			
		case 9:
			HV_sensor[1] = HV_NINETY;
			QDT_SensorDataToIndex(data[1], 2);
			break;
		default:
			break;
	}
	HV_sensor[3] = HV_PERCENT;
	QDT_Play_HumanVoice(HV_sensor, 4);
}

void QDT_Play_HV_SensorTemperature(qdt_uint8 ucSensorData)
{
	qdt_uint8 data[2];

	data[0] = ucSensorData/10;
	data[1] = ucSensorData%10;

	HV_sensor[0] = HV_TEMPERATURE;
	switch(data[0])
	{
		case 0:
			HV_sensor[1] = HV_NULL;
			switch(data[1])
			{
				case 0:
					HV_sensor[2] = HV_ZERO;
					break;
				case 1:
					HV_sensor[2] = HV_ONE;
					break;
				case 2:
					HV_sensor[2] = HV_TWO;
					break;
				case 3:
					HV_sensor[2] = HV_THREE;
					break;
				case 4:
					HV_sensor[2] = HV_FOUR;
					break;
				case 5:
					HV_sensor[2] = HV_FIVE;
					break;
				case 6:
					HV_sensor[2] = HV_SIX;
					break;
				case 7:
					HV_sensor[2] = HV_SEVEN;
					break;
				case 8:
					HV_sensor[2] = HV_EIGHT;
					break;
				case 9:
					HV_sensor[2] = HV_NINE;
					break;
				default:
					break;
			}
			break;
			
		case 1:
			switch(data[1])
			{
				case 0:
					HV_sensor[1] = HV_TEN;
					break;
				case 1:
					HV_sensor[1] = HV_ELEVEN;
					break;
				case 2:
					HV_sensor[1] = HV_TWELVE;
					break;
				case 3:
					HV_sensor[1] = HV_THIRTEEN;
					break;
				case 4:
					HV_sensor[1] = HV_FOURTEEN;
					break;
				case 5:
					HV_sensor[1] = HV_FIFTEEN;
					break;
				case 6:
					HV_sensor[1] = HV_SIXTEEN;
					break;
				case 7:
					HV_sensor[1] = HV_SEVENTEEN;
					break;
				case 8:
					HV_sensor[1] = HV_EIGHTEEN;
					break;
				case 9:
					HV_sensor[1] = HV_NINETEEN;
					break;
				default:
					break;
			}
			HV_sensor[2] = HV_NULL;
			break;
			
		case 2:
			HV_sensor[1] = HV_TWENTY;
			QDT_SensorDataToIndex(data[1], 2);
			break;

		case 3:
			HV_sensor[1] = HV_THIRTY;
			QDT_SensorDataToIndex(data[1], 2);
			break;

		case 4:
			HV_sensor[1] = HV_FORTY;
			QDT_SensorDataToIndex(data[1], 2);
			break;

		case 5:
			HV_sensor[1] = HV_FIFTY;
			QDT_SensorDataToIndex(data[1], 2);
			break;
			
		case 6:
			HV_sensor[1] = HV_SIXTY;
			QDT_SensorDataToIndex(data[1], 2);
			break;
			
		case 7:
			HV_sensor[1] = HV_SEVENTY;
			QDT_SensorDataToIndex(data[1], 2);
			break;
			
		case 8:
			HV_sensor[1] = HV_EIGHTY;
			QDT_SensorDataToIndex(data[1], 2);
			break;
			
		case 9:
			HV_sensor[1] = HV_NINETY;
			QDT_SensorDataToIndex(data[1], 2);
			break;
		default:
			break;
	}
	HV_sensor[3] = HV_DEGREESCELSIUS;
	QDT_Play_HumanVoice(HV_sensor, 4);
}

#if 1//#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
void QDT_Play_HV_SelectFrequency(qdt_uint8 ucChannelData)
{
	qdt_uint8 Channeldata[2];

	Channeldata[0] = HV_SELECTFREQCH;
	switch(ucChannelData)
	{
		case 0:
			Channeldata[1] = HV_ZERO;
			break;
		case 1:
			Channeldata[1] = HV_ONE;
			break;
		case 2:
			Channeldata[1] = HV_TWO;
			break;
		case 3:
			Channeldata[1] = HV_THREE;
			break;
		case 4:
			Channeldata[1] = HV_FOUR;
			break;
		case 5:
			Channeldata[1] = HV_FIVE;
			break;
		case 6:
			Channeldata[1] = HV_SIX;
			break;
		case 7:
			Channeldata[1] = HV_SEVEN;
			break;
		case 8:
			Channeldata[1] = HV_EIGHT;
			break;
		case 9:
			Channeldata[1] = HV_NINE;
			break;
		default:
			Channeldata[1] = HV_ZERO;
			break;
			
	}
	QDT_Play_HumanVoice(Channeldata, 2);
}
#endif

void QDT_Play_HV_FirmwareVersion(const qdt_uint8 *ucData, qdt_uint8 ucLength)
{
	qdt_uint8 data[1];
	qdt_uint8 i;

	for(i=0; i<ucLength; i++)
	{
		switch(ucData[i])
		{
			case 0:
				data[0] = HV_ZERO;
				break;
			case 1:
				data[0] = HV_ONE;
				break;
			case 2:
				data[0] = HV_TWO;
				break;
			case 3:
				data[0] = HV_THREE;
				break;
			case 4:
				data[0] = HV_FOUR;
				break;
			case 5:
				data[0] = HV_FIVE;
				break;
			case 6:
				data[0] = HV_SIX;
				break;
			case 7:
				data[0] = HV_SEVEN;
				break;
			case 8:
				data[0] = HV_EIGHT;
				break;
			case 9:
				data[0] = HV_NINE;
				break;
			default:
				data[0] = HV_ZERO;
				break;
		}
		QDT_Play_HumanVoice(data, 1);
	}
}
