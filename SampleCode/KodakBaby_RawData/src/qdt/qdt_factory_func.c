#include "Driver\DrvGPIO.h"

#include "qdt_ram.h"
#include "qdt_sysState.h"
#include "qdt_factory_func.h"
#include "qdt_common_func.h"
#include "qdt_flash_hal.h"
#include "qdt_audiodocking.h"
#include "qdt_audiodocking_func.h"
#include "qdt_parentunit.h"
#include "qdt_parentunit_func.h"
#include "qdt_fifo.h"
#include "qdt_human_voice.h"
#include "qdt_uart_func.h"

#include "bsp.h"
#include "radio.h"
#include "si446x_api_lib.h"    //RF-664x header file


extern volatile __align(4) signed short s16Out_words[40];
extern void S7EncDec(void);
extern void playAudio(signed short *AudioRXBuffer);

#define FACTORY_TESTITEMS_MAX 7
#define FACTORY_FREQ_IDX 8

GlobalFactoryData g_FactoryData;
qdt_uint8 ucVoiceArray[5] = {HV_FACTORY, HV_ZERO, HV_ZERO, HV_ZERO, HV_ZERO};
qdt_bool g_bTestEnable = qdt_false;
qdt_bool g_ucRFTestCount = 0;
qdt_uint8 ucTempDumpUARTData[6];



static qdt_uint8 __qdt_factory_transferKeyCmd(qdt_uint8 ucCmd);
static void __qdt_factory_TestFunction(qdt_uint8 idx);
static void __qdt_factory_ResetTestItemData(void);

static qdt_uint8 __qdt_factory_transferKeyCmd(qdt_uint8 ucCmd)
{
	qdt_uint8 ucRetKey = _FACTORY_KEY_NONE;


	if (g_FactoryData.bEnableButtonTest && ucCmd != _USER_KEY_NONE && ucCmd > 80)
	{
		ucVoiceArray[0] = HV_BEEP;
		QDT_Play_HumanVoice(ucVoiceArray, 1);	

		if (ucCmd != _USER_KEY_PARENTUNIT_KEY_POWER && ucCmd != _USER_KEY_AUDIODOCKING_KEY_POWER)
			ucCmd = _FACTORY_KEY_NONE;
	}
	
	switch(ucCmd)
	{
	case _USER_KEY_PARENTUNIT_KEY_VOLUP:
	case _USER_KEY_AUDIODOCKING_KEY_VOLUP:
		ucRetKey =_FACTORY_KEY_UP;
		break;
	case _USER_KEY_PARENTUNIT_KEY_VOLDOWN:
	case _USER_KEY_AUDIODOCKING_KEY_VOLDOWN:	
		ucRetKey =_FACTORY_KEY_DOWN;
		break;		
	case _USER_KEY_PARENTUNIT_KEY_POWER:
	case _USER_KEY_AUDIODOCKING_KEY_POWER:	
		ucRetKey =_FACTORY_KEY_OK;
		break;
	case _USER_KEY_PARENTUNIT_KEY_LONGPRESS_VOLDOWN:
	case _USER_KEY_AUDIODOCKING_KEY_LONGPRESS_VOLDOWN:	
		ucRetKey =_FACTORY_KEY_RESET;
		break;		
	case _FACTORY_KEY_OK:
	case _FACTORY_KEY_UP:
	case _FACTORY_KEY_DOWN:	
	case RFCMD_FACTORYTEST_RFTEST:	
	case RFCMD_FACTORYTEST_BURNIN_SN:
	case RFACK_FACTORYTEST_BURNIN_SN:	
		ucRetKey = ucCmd;
		break;
	default:
		ucRetKey =_FACTORY_KEY_NONE;
		break;
	}


	return ucRetKey;
}
static void __qdt_factory_TestFunction(qdt_uint8 idx)
{

	switch(idx)
	{
	case 1: //LED TEST
		QDT_Factory_LEDTest(g_bTestEnable);
		break;
	case 2: //Btn TEST
		g_FactoryData.bEnableButtonTest = g_bTestEnable;
		break;
	case 3: // MIC/SPK TEST
		QDT_Common_SPKResume();						//resume SPK ouput		
		QDT_Common_MicResume(); 					//resume MIC input	
		g_FactoryData.bEnableMICSPKTest = g_bTestEnable;
		break;
	#ifdef BUILDTYPE_AUDIODOCKING_PROJECT	
	case 4: // NightLight Sensor Test
		QDT_Factory_NightLightSensorTest(g_bTestEnable);
		break;
	#endif
	#ifdef BUILDTYPE_PARENTUINT_PROJECT
	case 4: // Virbrator Sensor Test
		QDT_Factory_VibratorTest(g_bTestEnable);
		break;
	#endif		
	case 5: // RF TEST
		QDT_Factory_RFTest();
		QDT_Common_DelayTime(500); 
		break;	
	case 6:
		QDT_Factory_EnableRF();
		break;
	case 7:
		QDT_Factory_ForceUpdateAKM();
		break;		
	default:
		break;
	}
}

static void __qdt_factory_ResetTestItemData(void)
{
	g_bTestEnable = qdt_true; //new test item, reset ucTestCount 
	g_ucRFTestCount = 0;
	QDT_Common_SPKPause();					//pause SPK ouput		
	QDT_Common_MicPause(); 					//pause MIC input	
	#ifdef BUILDTYPE_AUDIODOCKING_PROJECT			
	QDT_AD_DisableGpioPwm();
	DrvGPIO_DisableInt(GPB, 7);  		    //disable RF-4461 interrupt Pin
	AUDIODOCKING_RF_446x(qdt_OFF);   			//turn off RF-4461
	#endif
	#ifdef BUILDTYPE_PARENTUINT_PROJECT			
	DrvGPIO_DisableInt(GPB, 7);  		    //disable RF-4461 interrupt Pin
	PARENTUNIT_RF_446x(qdt_OFF);   			//turn off RF-4461
	#endif		
	g_FactoryData.bEnableButtonTest	= qdt_false;
	g_FactoryData.bEnableMICSPKTest	= qdt_false;
	g_FactoryData.bEnableNLSensorTest = qdt_false;
	g_FactoryData.bEnableRFTest = qdt_false;
	g_FactoryData.bEnableBurnInSN= qdt_false;
	
}
RETURN_TYPE QDT_Factory_Init(void)
{
	QDT_flash_ReadFactoryData(&g_FactorySaveData);
	if (g_FactorySaveData.ucMagicNumber[0] == 0xFF && g_FactorySaveData.ucMagicNumber[1] == 0xFF)
	{
		QDT_TRACE("New Flash, set factory default value. \n");	
		g_FactorySaveData.ucMagicNumber[0] = 0xEE;
		g_FactorySaveData.ucMagicNumber[1] = 0xBB;
		g_FactorySaveData.ucSerialNumber[0] = 0xFF;
		g_FactorySaveData.ucSerialNumber[1] = 0xFF;
		g_FactorySaveData.ucSerialNumber[2] = 0xFF;
		g_FactorySaveData.ucSerialNumber[3] = 0xFF;
		g_FactorySaveData.ucSerialNumber[4] = 0xFF;		
		g_FactorySaveData.bIsInFactoryMode = qdt_false;
		g_FactorySaveData.ucLEDTestResult = 0;
		g_FactorySaveData.ucMICTestResult = 0;
		g_FactorySaveData.ucSPKTestResult = 0;
		g_FactorySaveData.ucKEYTestResult = 0;
		g_FactorySaveData.ucRFTestResult = 0;
		g_FactorySaveData.ucSENSORTestResult = 0;
		g_FactorySaveData.ucVIBRATORTestResult = 0;
		QDT_flash_WriteFactoryData(&g_FactorySaveData);
	}
	g_FactoryData.bEnableFactoryMode = qdt_false;
	g_FactoryData.bEnableButtonTest = qdt_false;
	g_FactoryData.bEnableMICSPKTest = qdt_false;
	g_FactoryData.bEnableNLSensorTest = qdt_false;	
	g_FactoryData.bEnableRFTest = qdt_false;	
	g_FactoryData.bEnableBurnInSN= qdt_false;
	return QDT_SYS_NOERROR;	
}


RETURN_TYPE QDT_Factory_EnterFactoryMode(void)
{
	QDT_TRACE("QDT_Factory_EnterFactoryMode....\n");
	g_FactorySaveData.bIsInFactoryMode = qdt_true;
	g_FactoryData.bEnableFactoryMode = qdt_true;
//-----------------------------------------------------------------------------------
	#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
	AUDIODOCKING_LED_PWR_RED(qdt_ON);
	AUDIODOCKING_LED_PWR_GREEN(qdt_ON);
	QDT_AD_DisableGpioPwm();
	g_Data.ucPreSysState = g_Data.ucSysState;
	g_Data.ucSysState = SYSSTATE_FACTORY;	
	g_Data.bTwinkleRedLED = qdt_false;
	g_Data.bTwinkleGreenLED = qdt_false;

	DrvGPIO_DisableInt(GPB, 7);  		    //disable RF-4461 interrupt Pin
	AUDIODOCKING_RF_446x(qdt_OFF);			//turn off  RF-4461

	QDT_ClearCmd();
	QDT_Common_SPKPause();					//pause SPK ouput		
	QDT_Common_MicPause(); 					//pause MIC input		
	#endif	

//-----------------------------------------------------------------------------------	
	#ifdef BUILDTYPE_PARENTUINT_PROJECT	
	PARENTUNIT_LED_PWR_RED(qdt_ON);
	PARENTUNIT_LED_PWR_GREEN(qdt_ON);	
	PARENTUNIT_LED_ALERT_ORANGE(qdt_OFF);

	PWMA->CMR0=0;  	//INTENSITY LED turn on
	PWMB->CMR0=0;	//INTENSITY LED turn on
	PWMA->CMR2=0;	//INTENSITY LED turn on
	PWMA->CMR3=0;	//INTENSITY LED turn on	
	QDT_Common_DelayTime(1000);
	QDT_PU_DisableGpioPwm();
	
	g_Data.ucPreSysState = g_Data.ucSysState;
	g_Data.ucSysState = SYSSTATE_FACTORY;	
	g_Data.bTwinkleRedLED = qdt_false;
	g_Data.bTwinkleGreenLED = qdt_false;
	g_Data.bTwinkleOrangeLED = qdt_false;

	DrvGPIO_DisableInt(GPB, 7);  		    //disable RF-4461 interrupt Pin
	PARENTUNIT_RF_446x(qdt_OFF);   			//turn off RF-4461

	QDT_ClearCmd();
	QDT_Common_SPKPause();					//pause SPK ouput		
	QDT_Common_MicPause(); 					//pause MIC input	
	#endif	

	QDT_flash_WriteFactoryData(&g_FactorySaveData);

	//reinitial global variable for Factory Mode
	g_Data.bIsInPTTMode= qdt_false;
	g_Data.bDisableSleepMode = qdt_true;

	ucVoiceArray[0] = HV_FACTORY;
	ucVoiceArray[1] = HV_ON;
	QDT_Play_HumanVoice(ucVoiceArray, 2);
	
  	return QDT_SYS_NOERROR;	
}
RETURN_TYPE QDT_Factory_ExitFactoryMode(void)
{
	QDT_TRACE("QDT_Factory_ExitFactoryMode....\n");
	g_FactorySaveData.bIsInFactoryMode = qdt_false;
	g_FactoryData.bEnableFactoryMode = qdt_false;

	
	
//-----------------------------------------------------------------------------------
	#ifdef BUILDTYPE_AUDIODOCKING_PROJECT

	DrvGPIO_DisableInt(GPB, 7);  		    //disable RF-4461 interrupt Pin
	AUDIODOCKING_RF_446x(qdt_OFF);   			//turn off RF-4461
	QDT_flash_WriteFactoryData(&g_FactorySaveData);

	
	AUDIODOCKING_LED_PWR_RED(qdt_OFF);
	AUDIODOCKING_LED_PWR_GREEN(qdt_ON);

	QDT_AD_EnableGpioPwm();
		
	AUDIODOCKING_RF_446x_PER_CTRL(qdt_ON);
	AUDIODOCKING_RF_446x(qdt_ON);
 	vRadio_Init();	                							/* Initial RF-446x */
	QDT_Common_DelayTime(100);
	
	DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
		

	QDT_Common_SPKPause();					//pause SPK ouput		
	QDT_Common_MicPause(); 					//pause MIC input			

	if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
	{
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_UNPAIRING;
		g_Data.bTwinkleRedLED = qdt_true;
		QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
		#ifdef USER_SELECT_FREQ			
		//WARNING MESSAGE :: [Please press Vol+/Vol- to select Freq.]
		QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	 //change RF Freq
		#endif			
	}
	else
	{
		g_Data.ucPreSysState = g_Data.ucSysState;
  		g_Data.ucSysState = SYSSTATE_RUNNING;
		g_Data.bTwinkleRedLED = qdt_false;		
		QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
		#ifdef USER_SELECT_FREQ		
		QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
		#endif			
		QDT_Common_SPKPause(); 					//pause SPK ouput		
		QDT_Common_MicResume(); 					//open MIC input		
	}

	vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
	#endif	

//-----------------------------------------------------------------------------------	
	#ifdef BUILDTYPE_PARENTUINT_PROJECT	
	DrvGPIO_DisableInt(GPB, 7);  		    //disable RF-4461 interrupt Pin
	PARENTUNIT_RF_446x(qdt_OFF);   			//turn off RF-4461
	QDT_flash_WriteFactoryData(&g_FactorySaveData);

	
	PARENTUNIT_LED_PWR_RED(qdt_OFF);
	PARENTUNIT_LED_PWR_GREEN(qdt_ON);
	QDT_PU_EnableGpioPwm();

	PARENTUNIT_RF_446x(qdt_ON);
 	vRadio_Init();	                							/* Initial RF-446x */

	DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
		
	QDT_Common_DelayTime(100);
	QDT_Common_SPKPause();					//pause SPK ouput		
	QDT_Common_MicPause(); 					//pause MIC input	
		
	if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
	{
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_UNPAIRING;
		g_Data.bTwinkleRedLED = qdt_true;
		QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
		#ifdef USER_SELECT_FREQ		
		QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);     //change RF Freq
		#endif				
	}
	else
	{
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_RUNNING;
		g_Data.bTwinkleRedLED = qdt_false;		
		QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
		#ifdef USER_SELECT_FREQ		
		QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
		#endif
	}

	vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */			
	#endif	

	ucVoiceArray[0] = HV_FACTORY;
	ucVoiceArray[1] = HV_OFF;
	QDT_Play_HumanVoice(ucVoiceArray, 2);	
 	return QDT_SYS_NOERROR;	
}
RETURN_TYPE QDT_Factory_LEDTest(qdt_bool bLEDOnOff)
{
	if (bLEDOnOff)
	{
		#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
		AUDIODOCKING_LED_PWR_RED(qdt_ON);
		AUDIODOCKING_LED_PWR_GREEN(qdt_ON);
		PWMA->CMR0 = 0; //night light turn on 
		#endif

		#ifdef BUILDTYPE_PARENTUINT_PROJECT
		PARENTUNIT_LED_PWR_RED(qdt_ON);
		PARENTUNIT_LED_PWR_GREEN(qdt_ON);
		PARENTUNIT_LED_ALERT_ORANGE(qdt_ON);
		QDT_PU_EnableGpioPwm();
		PWMA->CMR0=1000;  	//INTENSITY LED turn on
		PWMB->CMR0=1000;	//INTENSITY LED turn on
		PWMA->CMR2=1000;	//INTENSITY LED turn on
		PWMA->CMR3=1000;	//INTENSITY LED turn on	
		#endif		
	}
	else
	{
		#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
		AUDIODOCKING_LED_PWR_RED(qdt_OFF);
		AUDIODOCKING_LED_PWR_GREEN(qdt_OFF);
		PWMA->CMR0 = 1000; //night light turn on 
		#endif

		#ifdef BUILDTYPE_PARENTUINT_PROJECT
		PARENTUNIT_LED_PWR_RED(qdt_OFF);
		PARENTUNIT_LED_PWR_GREEN(qdt_OFF);
		PARENTUNIT_LED_ALERT_ORANGE(qdt_OFF);
		PWMA->CMR0=0;  	//INTENSITY LED turn on
		PWMB->CMR0=0;	//INTENSITY LED turn on
		PWMA->CMR2=0;	//INTENSITY LED turn on
		PWMA->CMR3=0;	//INTENSITY LED turn on	
		QDT_Common_DelayTime(1000);
		QDT_PU_DisableGpioPwm();
		#endif			
	}
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Factory_RFTest(void)
{
	RFHEADER SentOutRFHeader;
	qdt_uint8 ucSentOutData[32];
	qdt_uint8 ucDestSerialNumber[5];
	qdt_uint8 idx = 0;

	g_FactoryData.bEnableRFTest = qdt_true;
	
	vRadio_Init();												/* Initial RF-446x */
	DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
	QDT_Common_DelayTime(100);
	QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
	#ifdef USER_SELECT_FREQ		
	QDT_Common_SetFreq(FACTORY_FREQ_IDX);	   //change RF Freq
	#endif
	vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		


	ucDestSerialNumber[0] = 0;
	ucDestSerialNumber[1] = 0;
	ucDestSerialNumber[2] = 0;
	ucDestSerialNumber[3] = 0;		
	ucDestSerialNumber[4] = 0;	
	
	QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFCMD_FACTORYTEST_RFTEST, ucDestSerialNumber);

	SentOutRFHeader.ucQdtUIDType= QDTUID_TYPE_FACTORY;	
	SentOutRFHeader.ucDatalength[0] = 0x00;
	SentOutRFHeader.ucDatalength[1] = 32;
	for (idx = 0; idx < 32; idx++)
	{
		ucSentOutData[idx] = idx;
	}
	QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 	
	
	return QDT_SYS_NOERROR;
}
RETURN_TYPE QDT_Factory_RFTest_ACK(void)
{
	RFHEADER SentOutRFHeader;
	qdt_uint8 ucSentOutData[32];
	qdt_uint8 ucDestSerialNumber[5];
	qdt_uint8 idx = 0;

	//vRadio_Init();												/* Initial RF-446x */
	//DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
	//QDT_Common_DelayTime(100);
	//QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
	//#ifdef USER_SELECT_FREQ		
	//QDT_Common_SetFreq(FACTORY_FREQ_IDX);	   //change RF Freq
	//#endif
	//vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		


	ucDestSerialNumber[0] = 0;
	ucDestSerialNumber[1] = 0;
	ucDestSerialNumber[2] = 0;
	ucDestSerialNumber[3] = 0;		
	ucDestSerialNumber[4] = 0;	
	QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_FACTORYTEST_RFTEST, ucDestSerialNumber);

	SentOutRFHeader.ucQdtUIDType= QDTUID_TYPE_FACTORY;	
	SentOutRFHeader.ucDatalength[0] = 0x00;
	SentOutRFHeader.ucDatalength[1] = 32;
	for (idx = 0; idx < 32; idx++)
	{
		ucSentOutData[idx] = idx;
	}
	QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 	
	
	return QDT_SYS_NOERROR;
}


RETURN_TYPE QDT_Factory_EnableRF(void)
{
	vRadio_Init();												/* Initial RF-446x */
	DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin

	QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
	#ifdef USER_SELECT_FREQ		
	QDT_Common_SetFreq(FACTORY_FREQ_IDX);	   //change RF Freq
	#endif
	vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */	
	
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Factory_VibratorTest(qdt_bool bOnOff)
{
	#ifdef BUILDTYPE_PARENTUINT_PROJECT
	PARENTUNIT_VIBRATOR(bOnOff);
	#endif
	return QDT_SYS_NOERROR;
}
RETURN_TYPE QDT_Factory_NightLightSensorTest(qdt_bool bOnOff)
{
	#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
	g_FactoryData.bEnableNLSensorTest = qdt_true;
	QDT_AD_EnableGpioPwm();
	#endif
	return QDT_SYS_NOERROR;
}

qdt_bool QDT_Factory_CheckRFTestPackage(qdt_uint8 *ucDumpRxData)
{
	qdt_uint8 idx = 0;
	qdt_bool bTestResult = qdt_false;
	for (idx=0; idx<32; idx++)
	{
		if (ucDumpRxData[QDT_RFPKT_DATA_START_IDX+idx] != idx)
		{
			bTestResult = qdt_false;
			break;
		}
		bTestResult = qdt_true;
	}

	if(ucDumpRxData[QDT_RFPKT_RFSUBCMD_START_IDX] == RFACK_FACTORYTEST_RFTEST)
	{
		if (bTestResult)
			ucVoiceArray[1] = HV_SUCCESS;
		else
			ucVoiceArray[1] = HV_FAIL;
		ucVoiceArray[0] = HV_FACTORY;
		QDT_Play_HumanVoice(ucVoiceArray, 2);			

		g_FactoryData.bEnableRFTest = qdt_false;
		QDT_TRACE("QDT_Factory_CheckRFTestPackage = %d\n", bTestResult);
	}

	return bTestResult;
}
RETURN_TYPE QDT_Factory_BurnInSN(qdt_uint8 *ucDumpRxData)
{
	RFHEADER SentOutRFHeader;
	qdt_uint8 ucDestSerialNumber[5];

	if (ucDumpRxData[0] == 0)//Baby SN or Direct Save SN
	{
		g_UserSaveData.ucSerialNumber[0] = g_FactorySaveData.ucSerialNumber[0] = ucDumpRxData[1];
		g_UserSaveData.ucSerialNumber[1] = g_FactorySaveData.ucSerialNumber[1] = ucDumpRxData[2];
		g_UserSaveData.ucSerialNumber[2] = g_FactorySaveData.ucSerialNumber[2] = ucDumpRxData[3];
		g_UserSaveData.ucSerialNumber[3] = g_FactorySaveData.ucSerialNumber[3] = ucDumpRxData[4];
		g_UserSaveData.ucSerialNumber[4] = g_FactorySaveData.ucSerialNumber[4] = ucDumpRxData[5];		
		DrvGPIO_DisableInt(GPB, 7);  		    //disable RF-4461 interrupt Pin
		AUDIODOCKING_RF_446x(qdt_OFF);			//turn off  RF-4461
		QDT_flash_WriteFactoryData(&g_FactorySaveData);
		QDT_flash_WriteUserData(&g_UserSaveData);
		#ifdef 	BUILDTYPE_AUDIODOCKING_PROJECT	
		{
		qdt_uint8 ucRetData[4] = {0,0,0,0};
		QDT_Uart_SentOutCmd(UARTACK_SENT_FACTORY_BURNIN_SN, &ucRetData[0]); //parent  burn in SN OK
		}
		#endif
	}
	else if (ucDumpRxData[0] == 1) //Parent SN
	{
		if (!g_FactoryData.bEnableFactoryMode)
			QDT_Factory_EnterFactoryMode();
		
		vRadio_Init();												/* Initial RF-446x */
		DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
		QDT_Common_DelayTime(100);
		QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
		#ifdef USER_SELECT_FREQ		
		QDT_Common_SetFreq(FACTORY_FREQ_IDX);	   //change RF Freq
		#endif
		vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		


		ucTempDumpUARTData[0] = ucDumpRxData[0];
		ucTempDumpUARTData[1] = ucDumpRxData[1];
		ucTempDumpUARTData[2] = ucDumpRxData[2];
		ucTempDumpUARTData[3] = ucDumpRxData[3];
		ucTempDumpUARTData[4] = ucDumpRxData[4];
		ucTempDumpUARTData[5] = ucDumpRxData[5];
		

		ucDestSerialNumber[0] = 0;
		ucDestSerialNumber[1] = 0;
		ucDestSerialNumber[2] = 0;
		ucDestSerialNumber[3] = 0;		
		ucDestSerialNumber[4] = 0;	
		
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFCMD_FACTORYTEST_BURNIN_SN, ucDestSerialNumber);

		SentOutRFHeader.ucQdtUIDType= QDTUID_TYPE_FACTORY;	
		SentOutRFHeader.ucDatalength[0] = 0x00;
		SentOutRFHeader.ucDatalength[1] = 6;
		
		QDT_Common_SentRFCmd(&SentOutRFHeader, &ucDumpRxData[0]); 		

		QDT_Common_DelayTime(500); 
		g_FactoryData.bEnableBurnInSN= qdt_true;
	}

	
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Factory_BurnInSN_Ack(void)
{
	RFHEADER SentOutRFHeader;
	qdt_uint8 ucSentOutData[32];
	qdt_uint8 ucDestSerialNumber[5];

	vRadio_Init();												/* Initial RF-446x */
	DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
	QDT_Common_DelayTime(100);
	QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
	#ifdef USER_SELECT_FREQ		
	QDT_Common_SetFreq(FACTORY_FREQ_IDX);	   //change RF Freq
	#endif
	vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		


	ucDestSerialNumber[0] = 0;
	ucDestSerialNumber[1] = 0;
	ucDestSerialNumber[2] = 0;
	ucDestSerialNumber[3] = 0;		
	ucDestSerialNumber[4] = 0;	
	QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_FACTORYTEST_BURNIN_SN, ucDestSerialNumber);

	SentOutRFHeader.ucQdtUIDType= QDTUID_TYPE_FACTORY;	
	SentOutRFHeader.ucDatalength[0] = 0x00;
	SentOutRFHeader.ucDatalength[1] = 0;

	QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 	
	
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Factory_ForceUpdateAKM(void)
{
	#ifdef 	BUILDTYPE_AUDIODOCKING_PROJECT			
	qdt_uint8 ucRetCameraData[8] = {0, 0, 0, 0, 0, 0, 0, 0};	
	QDT_Uart_SentOutCmd(UARTCMD_TEST0, &ucRetCameraData[0]); 
	#endif
	return QDT_SYS_NOERROR;
}
RETURN_TYPE QDT_Factory_ProcessKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData)
{
	qdt_uint8 ucFactoryKey = _FACTORY_KEY_NONE;
	static qdt_uint8 ucFactoryTestIdx = 1;	
	static qdt_uint8 ucPreFactoryTestIdx = 0;	


	
	ucFactoryKey = __qdt_factory_transferKeyCmd(ucCmd);

	switch (ucFactoryKey)
	{
	case _FACTORY_KEY_UP:
		
		if (ucFactoryTestIdx >= FACTORY_TESTITEMS_MAX) ucFactoryTestIdx = 1;
		else ucFactoryTestIdx++;
		ucVoiceArray[0] = HV_FACTORY;
		ucVoiceArray[1] = HV_ONE+ucFactoryTestIdx-1;
		QDT_Play_HumanVoice(ucVoiceArray, 2);
		break;
	case _FACTORY_KEY_DOWN:	
		
		if (ucFactoryTestIdx <= 1 ) ucFactoryTestIdx = FACTORY_TESTITEMS_MAX;
		else ucFactoryTestIdx--;
		ucVoiceArray[0] = HV_FACTORY;
		ucVoiceArray[1] = HV_ONE+ucFactoryTestIdx-1;;
		QDT_Play_HumanVoice(ucVoiceArray, 2);		
		break;		
	case _FACTORY_KEY_OK:
		if (ucFactoryTestIdx!=ucPreFactoryTestIdx) 
		{
			__qdt_factory_ResetTestItemData();
		}

		
		__qdt_factory_TestFunction(ucFactoryTestIdx);
		g_bTestEnable = !g_bTestEnable;  	//prepare next time status, ON-->OFF-->ON-->OFF.....
		
		ucPreFactoryTestIdx = ucFactoryTestIdx;
		break;
	case _FACTORY_KEY_RESET:
		QDT_flash_ClearFactoryData();
		QDT_flash_ReadFactoryData(&g_FactorySaveData);
		ucVoiceArray[0] = HV_FACTORY;
		ucVoiceArray[1] = HV_RESET;
		QDT_Play_HumanVoice(ucVoiceArray, 2);	
		break;

	case RFCMD_FACTORYTEST_RFTEST:
		QDT_Factory_RFTest_ACK();	
		break;
		
	case RFCMD_FACTORYTEST_BURNIN_SN:
		ucDumpRxData[0] = 0; //force to 0 , mean direct save in FLASH.
		ucDumpRxData[1] = ucDumpRxData[QDT_RFPKT_DATA_START_IDX+1]; 
		ucDumpRxData[2] = ucDumpRxData[QDT_RFPKT_DATA_START_IDX+2]; 
		ucDumpRxData[3] = ucDumpRxData[QDT_RFPKT_DATA_START_IDX+3]; 
		ucDumpRxData[4] = ucDumpRxData[QDT_RFPKT_DATA_START_IDX+4]; 
		ucDumpRxData[5] = ucDumpRxData[QDT_RFPKT_DATA_START_IDX+5]; 
		QDT_Factory_BurnInSN(&ucDumpRxData[0]);	
		QDT_Factory_BurnInSN_Ack();	
		break;
	case RFACK_FACTORYTEST_BURNIN_SN:
		g_FactoryData.bEnableBurnInSN = qdt_false;
		#ifdef 	BUILDTYPE_AUDIODOCKING_PROJECT	
		{	
	 	qdt_uint8 ucRetData[4] = {0,0,0,0};
		QDT_Uart_SentOutCmd(UARTACK_SENT_FACTORY_BURNIN_SN, &ucRetData[0]);  //parent  burn in SN OK
	    }
		#endif
		break;
	default:
		break;			
	}


	if (g_FactoryData.bEnableMICSPKTest)  //Fact test 3, MIC/SPK test
	{
		if (g_Data.bMicBufferReady)
		{   
			S7EncDec();
		}
		if (g_Data.bHumanVoice == qdt_false && !g_Data.bTransmitBufferIsempty)
		{
 			playAudio(s16Out_words);
			g_Data.bTransmitBufferIsempty = qdt_true;	
		}

	}

	#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
	if (g_FactoryData.bEnableNLSensorTest)
		QDT_AD_NightLEDOnOff();							//Detect PIR sensor to trigger Night LED 
	#endif


	if (g_FactoryData.bEnableRFTest && g_ucRFTestCount <= 20)
	{
		QDT_AddKeyCmdData(_FACTORY_KEY_OK, NULL);
		g_ucRFTestCount++;
	}
	else if (g_FactoryData.bEnableRFTest && g_ucRFTestCount >20)
	{
		g_FactoryData.bEnableRFTest = qdt_false;
		g_ucRFTestCount = 0;

		ucVoiceArray[1] = HV_FAIL;
		ucVoiceArray[0] = HV_FACTORY;
		QDT_Play_HumanVoice(ucVoiceArray, 2);			
	}
	else if (g_FactoryData.bEnableBurnInSN)
	{
		QDT_Factory_BurnInSN(&ucTempDumpUARTData[0]);
	}
	
	
	return QDT_SYS_NOERROR;
}

