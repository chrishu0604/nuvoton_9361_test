#include "qdt_utils.h"
#include "qdt_common_func.h"
#include "qdt_fifo.h"

Byte  CmdBuffer[CMDBUFFERSIZE];


void QDT_InitFIFO2(Byte  * gpFIFOBuffer, Word cLength)
{
    pFifoHandle fh = (pFifoHandle)gpFIFOBuffer;
    
    if (cLength <= 3) return;

    fh->ucSize = (Word)((cLength - 6)/sizeof(CmdData));
    
    QDT_ClearFIFO(fh);
    return;
}

Bool  QDT_GetFIFO(pFifoHandle fh, Byte * rpData, Byte *rpRxData)
{
	Byte i;
    if (fh->ucHead != fh->ucTail)
    {
        *rpData = fh->CmdData[fh->ucHead].ucCmd;
		for (i=0;i<32;i++)
			rpRxData[i] = fh->CmdData[fh->ucHead].ucRxData[i];
        fh->ucHead++;
        if (fh->ucHead >= fh->ucSize) fh->ucHead = 0;
        return qdt_true;
    }
    return qdt_false;
}

Bool  QDT_SetFIFO(pFifoHandle fh, Byte ucData, Byte *rpRxData)
{
	Byte i;
    if ((fh->ucTail + 1 == fh->ucHead) ||
        (fh->ucTail + 1 - fh->ucSize == fh->ucHead))
        return qdt_false;          /* Full */
    fh->CmdData[fh->ucTail].ucCmd = ucData;
	for (i=0;i<32;i++)
		fh->CmdData[fh->ucTail].ucRxData[i] =  rpRxData[i];
    fh->ucTail++;
    if (fh->ucTail >= fh->ucSize) fh->ucTail = 0;
    return qdt_true;
}

void  QDT_ClearFIFO(pFifoHandle fh) 
{
    fh->ucHead = 0;
    fh->ucTail = 0;
}

Bool QDT_FIFOIsEmpty(pFifoHandle fh)
{
    return  fh->ucHead == fh->ucTail;
}

void QDT_GetkeyCmdData(Byte *ucCmd, Byte *ucData)
{
    if (!QDT_GetFIFO((pFifoHandle)CmdBuffer,(Byte *)ucCmd, (Byte *) ucData))
    {
         /*there is no data in FIFO, return NONE cmd*/
         *ucCmd = _USER_KEY_NONE;
		 *ucData = NULL;
    }

	if (*ucCmd != _USER_KEY_NONE && *ucCmd != RFCMD_PARENTCOMMAND_PUSHTOTALKING)
		QDT_DEBUG("Got KeyCmd = 0x%x \n",*ucCmd);
}

