#include "bsp.h"
#include "radio.h"
#include "ISD93xx.h"
#include "Driver\DrvADC.h"
#include "Driver\DrvPDMA.h"
#include "Driver\DrvDPWM.h"
#include "Driver\DrvGPIO.h"
#include "Driver\DrvTIMER.h"
#include "Driver\DrvSYS.h"
#include "Driver\DrvFMC.h"
#include "FMC.h"
#include <stdio.h>
#include <string.h>

#include "qdt_ram.h"
#include "qdt_fifo.h"
#include "qdt_flash_hal.h"
#include "qdt_sysState.h"
#include "qdt_audiodocking.h"
#include "qdt_parentunit.h"
#include "CM36686Setup.h"

#include "qdt_utils.h"
#include "qdt_audiodocking_func.h"
#include "qdt_common_func.h"
#include "qdt_uart_func.h"
#include "qdt_upgrade_func.h"


#include "si446x_api_lib.h"    //RF-664x header file
#include "qdt_human_voice.h"
#include "stdlib.h"

qdt_uint8 HV_UPGRADE_PERCENTAGE[19][3] = {	{HV_FIVE, HV_NULL, HV_PERCENT}, {HV_TEN, HV_NULL, HV_PERCENT}, {HV_FIFTEEN, HV_NULL, HV_PERCENT}, 
                                            {HV_TWENTY, HV_NULL, HV_PERCENT}, {HV_TWENTY, HV_FIVE, HV_PERCENT},
											{HV_THIRTY, HV_NULL, HV_PERCENT}, {HV_THIRTY, HV_FIVE, HV_PERCENT}, 
											{HV_FORTY, HV_NULL, HV_PERCENT}, {HV_FORTY, HV_FIVE, HV_PERCENT}, 
											{HV_FIFTY, HV_NULL, HV_PERCENT}, {HV_FIFTY, HV_FIVE, HV_PERCENT},
											{HV_SIXTY, HV_NULL, HV_PERCENT}, {HV_SIXTY, HV_FIVE, HV_PERCENT},
											{HV_SEVENTY, HV_NULL, HV_PERCENT}, {HV_SEVENTY, HV_FIVE, HV_PERCENT},
											{HV_EIGHTY, HV_NULL, HV_PERCENT}, {HV_EIGHTY, HV_FIVE, HV_PERCENT},
											{HV_NINETY, HV_NULL, HV_PERCENT}, {HV_NINETY, HV_FIVE, HV_PERCENT}
										  };

UpdataImageInfo g_UpdateImageInfo;
UpdataImageInfo g_CurrentImageInfo;

RETURN_TYPE QDT_Upgrade_ReadImage(qdt_uint8 *ucImageData, qdt_uint32 u32AddrIdx, qdt_uint32 ucReadCount)
{
	static qdt_uint16 u32ReadBufferIdx = 0;
	static qdt_uint8 ucReadBuffer[256] = {0};	
	static qdt_uint32 u32RecordAddr = 0xFFFFFFFF;
	static qdt_uint32 uc32ReadAddr = 0;
	static qdt_uint8 ucUpgradePercent = 0;
	static qdt_uint8 ucSpeakValue = 55;


	if (u32RecordAddr == u32AddrIdx)
		return QDT_SYS_FAILED;
	u32RecordAddr = u32AddrIdx;


	if (u32AddrIdx%256==0)
	{
		DrvGPIO_DisableInt(GPB, 7); 			//disable RF-4461 interrupt Pin
		PARENTUNIT_RF_446x(qdt_OFF);			//turn off RF-4461
		
		QDT_flash_ReadBytes(FLASH_IMAGE_PARENTTEMP_ADDR+uc32ReadAddr, 256, ucReadBuffer);
		uc32ReadAddr+=256;
		u32ReadBufferIdx = 0;
		
		PARENTUNIT_RF_446x(qdt_ON);
		vRadio_Init();												/* Initial RF-446x */
		DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
		if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
		{
			QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
			#ifdef USER_SELECT_FREQ		
			QDT_Common_SetFreq(0);	   //change RF Freq
			#endif			
			vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
		}
		else
		{
			QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
			#ifdef USER_SELECT_FREQ		
			QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
			#endif
			vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
			
		}
		g_Data.bRFTransmitFinished = qdt_true;
		g_Data.bRFReceiveFinished = qdt_true;			
	}

	memcpy(ucImageData, ucReadBuffer+u32ReadBufferIdx, ucReadCount);
	u32ReadBufferIdx+=ucReadCount;
	

	ucUpgradePercent = (u32AddrIdx*50/g_UpdateImageInfo.u32ImageSize)+50;
	if (ucUpgradePercent == ucSpeakValue)
	{
		QDT_Play_HumanVoice(HV_UPGRADE_PERCENTAGE[(ucSpeakValue/5)-1], 3);
		ucSpeakValue = ucUpgradePercent+5;
	}
			
#if 0
	static qdt_uint32 u32ReadAddrIdx = 0xFFFFFFFF;
	
	if (u32ReadAddrIdx != u32AddrIdx)
	{
		DrvGPIO_DisableInt(GPB, 7); 			//disable RF-4461 interrupt Pin
		PARENTUNIT_RF_446x(qdt_OFF);			//turn off RF-4461
		
		QDT_flash_ReadBytes(FLASH_IMAGE_PARENTTEMP_ADDR+u32AddrIdx, ucReadCount, ucImageData);

		ucUpgradePercent = (u32AddrIdx*100/g_UpdateImageInfo.u32ImageSize);
		if (ucUpgradePercent == ucSpeakValue)
		{
			QDT_Play_HumanVoice(HV_UPGRADE_PERCENTAGE[(ucSpeakValue/5)-1], 3);
			ucSpeakValue = ucUpgradePercent+5;
		}
			
		
			
		PARENTUNIT_RF_446x(qdt_ON);
		vRadio_Init();												/* Initial RF-446x */
		DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
		if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
		{
			QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
#ifdef USER_SELECT_FREQ		
			QDT_Common_SetFreq(0);	   //change RF Freq
#endif			
			vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
		}
		else
		{
			QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
#ifdef USER_SELECT_FREQ		
			QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
#endif
			vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
			
		}
		g_Data.bRFTransmitFinished = qdt_true;
		g_Data.bRFReceiveFinished = qdt_true;		
		u32ReadAddrIdx =  u32AddrIdx;
	}
	//else
	//{
	//	QDT_flash_ReadBytes(FLASH_IMAGE_PARTITION1_ADDR+u32AddrIdx, ucReadCount, ucImageData);
	//}
#endif	
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Upgrade_EarseImageAddr(void)
{
	qdt_uint8 idx;
	if (1)
	{
		DrvGPIO_DisableInt(GPB, 7); 			//disable RF-4461 interrupt Pin
		PARENTUNIT_RF_446x(qdt_OFF);			//turn off RF-4461
		
		for(idx = 0; idx < 48 ; idx++)
			QDT_flash_EarseSector(FLASH_IMAGE_PARTITION1_ADDR+idx*4096);  //Earse Flash, wait for writing image.
			
		QDT_Common_DelayTime(100);
			
		PARENTUNIT_RF_446x(qdt_ON);
		vRadio_Init();												/* Initial RF-446x */
		DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
		if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
		{
			QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
#ifdef USER_SELECT_FREQ		
			QDT_Common_SetFreq(0);	   //change RF Freq
#endif			
			vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
		}
		else
		{
			QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
#ifdef USER_SELECT_FREQ		
			QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
#endif
			vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
			
		}
		g_Data.bRFTransmitFinished = qdt_true;
		g_Data.bRFReceiveFinished = qdt_true;				
	}
	else
	{
		for(idx = 0; idx < 48 ; idx++)
			QDT_flash_EarseSector(FLASH_IMAGE_PARTITION1_ADDR+idx*4096);  //Earse Flash, wait for writing image.
	}
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Upgrade_WriteImage(qdt_uint8 ucTransferType, qdt_uint8 ucImageType, qdt_uint32 u32Addr, qdt_uint8 uclength, qdt_uint8 *pucImageData)
{
	static qdt_uint32 u32RecordAddr = 0xFFFFFFFF;
	static qdt_uint32 ucWriteBufferIdx = 0;
	static qdt_uint8 ucWriteBuffer[256] = {0};
	static qdt_uint32 uc32WriteAddr = 0;
	qdt_uint16 u16ImageDataLength = 0;
#ifdef BUILDTYPE_AUDIODOCKING_PROJECT		
	static qdt_uint8 ucUpgradePercent = 0;
	static qdt_uint8 ucSpeakValue = 5;
#endif
	if (u32Addr == 0) //reset all parameter if address =0, it mean this is a new image.
	{
		u32RecordAddr = 0xFFFFFFFF;
		ucWriteBufferIdx = 0;
		uc32WriteAddr = 0;
#ifdef BUILDTYPE_AUDIODOCKING_PROJECT				
		ucUpgradePercent = 0;
		ucSpeakValue = 5;	
#endif		
	}

	if (u32RecordAddr == u32Addr)
		return QDT_SYS_FAILED;
	
	if (ucTransferType == _QDT_UPGRADETRANSFERTYPE_RF_)
		u16ImageDataLength = QDT_RF_UPGRADEFW_IMAGE_DATA_LENGTH;
	else
		u16ImageDataLength = QDT_UART_UPGRADEFW_IMAGE_DATA_LENGTH;
	
	u32RecordAddr = u32Addr;
	memcpy(ucWriteBuffer+ucWriteBufferIdx, pucImageData, uclength);
	ucWriteBufferIdx+=uclength;

	if (uclength < u16ImageDataLength)
		uclength = uclength ;

	if (ucWriteBufferIdx%256==0 || uclength < u16ImageDataLength) // (uclength < QDT_RF_UPGRADEFW_IMAGE_DATA_LENGTH)  ==> last data  package
	{
		
		
#ifdef BUILDTYPE_AUDIODOCKING_PROJECT	
		if (ucImageType == _QDT_UPGRADEIMAGETYPE_AUDIODOCKING_)
			QDT_flash_WriteImage(FLASH_IMAGE_PARTITION1_ADDR+uc32WriteAddr, ucWriteBufferIdx, ucWriteBuffer);
		else if (ucImageType == _QDT_UPGRADEIMAGETYPE_PARENTUNIT_ )
			QDT_flash_WriteImage(FLASH_IMAGE_PARENTTEMP_ADDR+uc32WriteAddr, ucWriteBufferIdx, ucWriteBuffer);	
		else if (ucImageType == _QDT_UPGRADEIMAGETYPE_AKM_CRAM_ )
			QDT_flash_WriteImage(FLASH_AKM_CRAM_ADDR+uc32WriteAddr, ucWriteBufferIdx, ucWriteBuffer);				
		else if (ucImageType == _QDT_UPGRADEIMAGETYPE_AKM_PRAM_ )	
			QDT_flash_WriteImage(FLASH_AKM_PRAM_ADDR+uc32WriteAddr, ucWriteBufferIdx, ucWriteBuffer);		
		else if (ucImageType == _QDT_UPGRADEIMAGETYPE_VOICEDATA_)	
			QDT_flash_WriteImage(FLASH_VOICE_DATA_ADDR+uc32WriteAddr, ucWriteBufferIdx, ucWriteBuffer);
		else
			QDT_ERROR("not support image type:: %d\n",ucImageType);
#endif
#ifdef BUILDTYPE_PARENTUINT_PROJECT
		DrvGPIO_DisableInt(GPB, 7); 			//disable RF-4461 interrupt Pin
		PARENTUNIT_RF_446x(qdt_OFF);			//turn off RF-4461
	
		if (ucImageType == _QDT_UPGRADEIMAGETYPE_PARENTUNIT_)
			QDT_flash_WriteImage(FLASH_IMAGE_PARTITION1_ADDR+uc32WriteAddr, ucWriteBufferIdx, ucWriteBuffer);
		else
			QDT_flash_WriteImage(FLASH_VOICE_DATA_ADDR+uc32WriteAddr, ucWriteBufferIdx, ucWriteBuffer);

		PARENTUNIT_RF_446x(qdt_ON);
		vRadio_Init();												/* Initial RF-446x */
		DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
		
		#ifdef USER_SELECT_FREQ 	
		QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
		#endif			
		
		if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
			QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
		else

			QDT_Common_SetSyncWord(PAIRING_SYNCWORD);			
		
		vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
		g_Data.bRFTransmitFinished = qdt_true;
		g_Data.bRFReceiveFinished = qdt_true;		
		
#endif
		uc32WriteAddr+=ucWriteBufferIdx;
		ucWriteBufferIdx = 0;


#ifdef BUILDTYPE_AUDIODOCKING_PROJECT	
		if (ucImageType == _QDT_UPGRADEIMAGETYPE_AUDIODOCKING_)
			ucUpgradePercent = (u32Addr*100/g_UpdateImageInfo.u32ImageSize);
		else
			ucUpgradePercent = (u32Addr*50/g_UpdateImageInfo.u32ImageSize);
		
		if (ucUpgradePercent == ucSpeakValue)
		{
			QDT_Play_HumanVoice(HV_UPGRADE_PERCENTAGE[(ucSpeakValue/5)-1], 3);
			ucSpeakValue = ucUpgradePercent+5;
		}
#endif		
	}
#if 0	
	if (1)
	{
		DrvGPIO_DisableInt(GPB, 7); 			//disable RF-4461 interrupt Pin
		PARENTUNIT_RF_446x(qdt_OFF);			//turn off RF-4461

		if (ucImageType == _QDT_UPGRADEIMAGETYPE_PARENTUNIT_ || ucImageType == _QDT_UPGRADEIMAGETYPE_AUDIODOCKING_)
			QDT_flash_WriteImage(FLASH_IMAGE_PARTITION1_ADDR+u32Addr, uclength, pucImageData);
		else
			QDT_flash_WriteImage(FLASH_VOICE_DATA_ADDR+u32Addr, uclength, pucImageData);
		//QDT_Common_DelayTime(100);
			
		PARENTUNIT_RF_446x(qdt_ON);
		vRadio_Init();												/* Initial RF-446x */
		DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
		if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
		{
			QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
#ifdef USER_SELECT_FREQ		
			QDT_Common_SetFreq(0);	   //change RF Freq
#endif			
			//QDT_Common_DelayTime(100);
			vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
		}
		else
		{
			QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
#ifdef USER_SELECT_FREQ		
			QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
#endif
			//QDT_Common_DelayTime(100);
			vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
			
		}
			
	}
	//else
	//{
		//ucAddrIdx = g_ucDumpImageData[2];
	//	ucImageLength = g_ucDumpImageData[3];
	//	QDT_flash_WriteImage(FLASH_IMAGE_PARTITION1_ADDR+ucAddrIdx, ucImageLength, &g_ucDumpImageData[4]);
	//	g_UpdateImageInfo.u32AddrIdx = ucAddrIdx;
	//}
#endif	
	return QDT_SYS_NOERROR;
}


