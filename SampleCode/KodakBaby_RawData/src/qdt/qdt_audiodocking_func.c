#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
#include "bsp.h"
#include "radio.h"
#include "ISD93xx.h"
#include "Driver\DrvADC.h"
#include "Driver\DrvPDMA.h"
#include "Driver\DrvDPWM.h"
#include "Driver\DrvGPIO.h"
#include "Driver\DrvTIMER.h"
#include "Driver\DrvSYS.h"
#include "Driver\DrvFMC.h"
#include "Driver\DrvUART.h"

#include "FMC.h"
#include <stdio.h>
#include <string.h>

#include "qdt_ram.h"
#include "qdt_fifo.h"
#include "qdt_flash_hal.h"
#include "qdt_sysState.h"
#include "qdt_audiodocking.h"
#include "qdt_parentunit.h"
#include "CM36686Setup.h"

#include "qdt_utils.h"
#include "qdt_audiodocking_func.h"
#include "qdt_common_func.h"
#include "qdt_uart_func.h"
#include "qdt_upgrade_func.h"
#include "qdt_factory_func.h"

#include "si446x_api_lib.h"    //RF-664x header file
#include "qdt_human_voice.h"
#include "stdlib.h"


#define MAX_TRY2SEARCHDEV_COUNT  100
#define ALCThreshold 400000

//qdt_uint8 ucNightLightBrightLevel[12] = {101, 80, 70, 60, 50, 35, 25, 20, 15, 10, 5, 0};
//qdt_uint16 ucNightLightBrightLevel[12] = {1000, 996, 990, 980, 950, 900, 820, 720, 600, 500, 250, 0};
//qdt_uint16 ucNightLightBrightLevel[26] = {1000, 998, 996, 992, 988, 982, 976, 970, 962, 954, 944, 934,
//								920, 900, 880, 850, 800, 750, 700, 600, 500, 400, 300, 200, 100, 0};
qdt_uint16 ucNightLightBrightLevel[26] = {1000, 998, 995, 990, 980, 970, 950, 930, 910, 880, 850, 820,
								790, 760, 720, 680, 630, 570, 520, 460, 400, 340, 280, 200, 100, 0};

qdt_uint8 bBreatheFlag;

extern qdt_uint8 HV_PowerOn[2];
extern qdt_uint8 HV_PowerOff[2];
extern qdt_uint8 HV_PairingMode[2];
extern qdt_uint8 HV_PairingSuccess[2];
extern qdt_uint8 HV_PairingFail[2];
extern const qdt_uint8 ucPairingFreqTable[6][12];
extern qdt_uint8 HV_BEE[1];
extern qdt_uint8 HV_FactoryReset[2];
extern qdt_uint8 HV_PairFailToTryAgain[1];
extern qdt_uint8 HV_PlsPressVolToSelFreq[1];

extern qdt_uint8 g_ucDumpImageData[256];	
extern const qdt_uint8 g_ucFWVersion[13];
								
								
static qdt_bool __qdt_AD_AllocateRFFreqCH(QdtDeviceType_e e_DevType);
static qdt_bool __qdt_AD_HasPaired(void);


static qdt_bool __qdt_AD_AllocateRFFreqCH(QdtDeviceType_e e_DevType)
{
	qdt_uint8 i;
	qdt_bool retValue = qdt_true;
	//qdt_uint8 ucDeviceType;
	qdt_uint8 ucSensorUnitRFFreqCH[8] = {RF_SENSORUNIT_CH2, RF_SENSORUNIT_CH3, RF_SENSORUNIT_CH4, RF_SENSORUNIT_CH5, 
		                                 RF_SENSORUNIT_CH6, RF_SENSORUNIT_CH7, RF_SENSORUNIT_CH8, RF_SENSORUNIT_CH9};
	//ucDeviceType = (u64SenderDeviceUID>>24)&0xC0;
	if (e_DevType == DEVICE_TYPE_PARENTUNIT)  				// Parent Uint Device
	{
		for(i=0; i<SUPPORT_SERSORUNIT_MAXNUM; i++)
		{
			if ((g_UserSaveData.ucPairedParentUnitList & (1<<i)) == 0)
			{
				g_Data.ucAllocListIdx = i;
				g_Data.ucAllocRFFreqCH = RF_PARENTUNIT_CH1;
				break;
			}
		}
		if (i == SUPPORT_SERSORUNIT_MAXNUM)
		{
			QDT_ERROR("Paired ParentUint is over 8\n");
			retValue = qdt_false;
		}

	}
	else if (e_DevType == DEVICE_TYPE_SENSOR_CRIB || e_DevType == DEVICE_TYPE_SENSOR_GATE) 			// Sensor Uint Device
	{
		for(i=0; i<SUPPORT_SERSORUNIT_MAXNUM; i++)
		{
			if ((g_UserSaveData.ucPairedSensorUnitList & (1<<i)) == 0)
			{
				g_Data.ucAllocListIdx = i;
				g_Data.ucAllocRFFreqCH= ucSensorUnitRFFreqCH[i]; //RF_SENSORUNIT_CH2;
				break;
			}
		}

		if (i == SUPPORT_SERSORUNIT_MAXNUM)
		{
			QDT_ERROR("Paired SensorUnit is over 8\n");
			retValue = qdt_false;
		}
	}
	else
	{
		QDT_ERROR("ERROR Device Type!!\n");
		retValue = qdt_false;
	}

	return retValue;
}
static qdt_bool __qdt_AD_HasPaired(void)
{
	if (g_UserSaveData.ucPairedParentUnitNum == 0 && g_UserSaveData.ucPairedSensorUnitNum == 0)
		return qdt_false;
	else
		return qdt_true;
}


void QDT_AD_ResetMP3(void)
{
	DrvGPIO_ClrBit(GPB,15);
	//SysTimerDelay(50);
	//DrvGPIO_SetBit(GPB,15);
	//SysTimerDelay(1800);

	//DrvGPIO_SetBit(GPB,14);  //set XDCS High level
}
void QDT_AD_NightLEDOnOff(void)
{
	static qdt_uint32 u32LoopCounter = 0;
	
	u32LoopCounter++;
	if (u32LoopCounter >= 2000)   //count [xxx] ms to detect NightLED
	{
		if(bBreatheFlag == 0)
	{
		if (CM36683_INT_Level() == qdt_false)
		 {
			read_CM36686_int_flag();
			if(g_Data.int_flat[1] == qdt_true)
			{
/*				
				g_UserSaveData.ucBrightLevel++;
				if (g_UserSaveData.ucBrightLevel >=12 ) g_UserSaveData.ucBrightLevel = 0;
					PWMA->CMR0=ucNightLightBrightLevel[g_UserSaveData.ucBrightLevel];			
*/
				if(g_UserSaveData.ucNightLightEnbled == qdt_OFF)
				{
					g_UserSaveData.ucNightLightEnbled = qdt_ON;
					bBreatheFlag = qdt_true;
					g_Data.ucNLTargetBrightLevel = g_UserSaveData.ucBrightLevel;
					g_Data.ucNLCurrentBrightLevel =0;
					QDT_Uart_SentOutNightLightStatus(qdt_ON);					
					QDT_DEBUG("g_UserSaveData.ucNightLightEnbled = %d, g_UserSaveData.ucBrightLevel = %d\n",g_UserSaveData.ucNightLightEnbled, g_UserSaveData.ucBrightLevel);
				}
				else
				{
					g_UserSaveData.ucNightLightEnbled = qdt_OFF;
					bBreatheFlag = qdt_true;
					g_Data.ucNLTargetBrightLevel = 0;
					g_Data.ucNLCurrentBrightLevel = g_UserSaveData.ucBrightLevel;
					QDT_Uart_SentOutNightLightStatus(qdt_OFF);					
					QDT_DEBUG("g_UserSaveData.ucNightLightEnbled = %d, g_UserSaveData.ucBrightLevel = %d\n",g_UserSaveData.ucNightLightEnbled, g_UserSaveData.ucBrightLevel);										
				}
			}
		}
		}
		else
		{
			//if (g_UserSaveData.ucNightLightEnbled == qdt_ON)
			{
				if (g_Data.ucNLTargetBrightLevel == g_Data.ucNLCurrentBrightLevel)
				{
					bBreatheFlag = 0;
				}
				else if (g_Data.ucNLTargetBrightLevel > g_Data.ucNLCurrentBrightLevel)
				{
					g_Data.ucNLCurrentBrightLevel++;
					PWMA->CMR0 = ucNightLightBrightLevel[g_Data.ucNLCurrentBrightLevel];
				}
				else 
				{
					g_Data.ucNLCurrentBrightLevel--;
					PWMA->CMR0 = ucNightLightBrightLevel[g_Data.ucNLCurrentBrightLevel];
				}					
			}

		}

		u32LoopCounter = 0;
	}

}

RETURN_TYPE QDT_AD_Parents_OnLine(void)
{
	QDT_TRACE("QDT_AD_Parents_OnLine....\n");
	g_Data.bParentUintOnLine= qdt_true;			
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_AD_Parents_Calling(void)
{
	QDT_TRACE("QDT_AD_Parents_Calling....\n");
	g_Data.bParentCalling = qdt_true;
	g_Data.bParentHandUp= qdt_false;	
	g_Data.bStopPollingRXChan = qdt_true;
	g_Data.ucCurRXChan = RF_PARENTUNIT_CH0;
	g_Data.NoAudioDataComingCounter = 0;
	//QDT_Common_SPKResume();
	QDT_Common_MicPause();	
#ifdef USE_SWITCH
	AUDIODOCKING_SPK_SWITCH(qdt_ON);
#endif
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_AD_Parents_HandUp(void)
{
	QDT_TRACE("QDT_AD_Parents_HandUp....\n");
	g_Data.bParentCalling = qdt_false;
	g_Data.bParentHandUp= qdt_true;		
	g_Data.bStopPollingRXChan = qdt_false;
	g_Data.ucWaitingParentACKCount = WAITING_ACK_COUNT;
    g_Data.NoAudioDataComingCounter = 0;
	QDT_Common_SPKPause();
	QDT_Common_MicResume();	
#ifdef USE_SWITCH
	AUDIODOCKING_SPK_SWITCH(qdt_OFF);
#endif
	return QDT_SYS_NOERROR;
}
RETURN_TYPE QDT_AD_Parents_ACK(void)
{
	QDT_TRACE("QDT_AD_Parents_ACK....\n");
	g_Data.ucWaitingParentACKCount = 0;
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_AD_EnableGpioPwm(void)
{
	//SYSCLK->APBCLK.PWM01_EN = 1;
	//PWMA->POE.PWM0=1;		
	if(g_UserSaveData.ucNightLightEnbled == qdt_ON)
	{
		bBreatheFlag = qdt_true;
		g_Data.ucNLTargetBrightLevel = g_UserSaveData.ucBrightLevel;
		g_Data.ucNLCurrentBrightLevel =0;
	}
	else  			//turn off Night Light
	{	
		PWMA->CMR0= 1000;
	}
	return TRUE;
}

RETURN_TYPE QDT_AD_DisableGpioPwm(void)
{
	PWMA->CMR0=1000;
	//SYSCLK->APBCLK.PWM01_EN = 0;
    	//PWMA->POE.PWM0=0;                  
	
	return TRUE;
}

RETURN_TYPE QDT_AD_SysPowerOnOff(void)
{
	if (g_Data.ucSysState == SYSSTATE_STANDBY)  // standby to poweron
	{		
		QDT_TRACE("Audio Docking power on....\n");
		AUDIODOCKING_LED_PWR_RED(qdt_OFF);
		AUDIODOCKING_LED_PWR_GREEN(qdt_ON);

		QDT_Common_Init();										/* Initial QDT variable and function */				
		//QDT_AD_SetNightLightOnOFF(g_UserSaveData.ucNightLightEnbled);
		QDT_AD_EnableGpioPwm();
		
		AUDIODOCKING_RF_446x_PER_CTRL(qdt_ON);
		AUDIODOCKING_RF_446x(qdt_ON);
 		vRadio_Init();	                							/* Initial RF-446x */
		QDT_Common_DelayTime(100);
		

		QDT_Common_SPKPause();					//pause SPK ouput		
		QDT_Common_MicPause(); 					//pause MIC input	
		
		QDT_Play_HumanVoice(HV_PowerOn,2);		
		
		if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
		{
			g_Data.ucPreSysState = g_Data.ucSysState;
			g_Data.ucSysState = SYSSTATE_UNPAIRING;
			g_Data.bTwinkleRedLED = qdt_true;
			QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
#ifdef USER_SELECT_FREQ			
			//WARNING MESSAGE :: [Please press Vol+/Vol- to select Freq.]
			QDT_Play_HumanVoice(HV_PlsPressVolToSelFreq, 1);
			QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	 //change RF Freq
#endif			
		}
		else
		{
			g_Data.ucPreSysState = g_Data.ucSysState;
  			g_Data.ucSysState = SYSSTATE_RUNNING;
			g_Data.bTwinkleRedLED = qdt_false;		
			QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
#ifdef USER_SELECT_FREQ		
			QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
#endif			
			QDT_Common_SPKPause(); 					//pause SPK ouput		
			QDT_Common_MicResume(); 					//open MIC input		
		}

		if (g_UserSaveData.ucPairedSensorUnitList != 0x00) //has paired sersor uint, need to query status when power on
			g_Data.ucBoardCastRFCmd = RFCMD_AUDIODOCKING_QUERYSENSORSTATUE;

		vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		

		AUDIODOCKING_CAMERA_PWR(qdt_ON);
	}
	else										// poweron to standby
	{
		if (g_Data.bFactoryResetFlag != qdt_true)
		QDT_Play_HumanVoice(HV_PowerOff,2);		

		AUDIODOCKING_LED_PWR_RED(qdt_ON);
		AUDIODOCKING_LED_PWR_GREEN(qdt_OFF);
		//QDT_AD_SetNightLightOnOFF(qdt_OFF);
		QDT_AD_DisableGpioPwm();
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_STANDBY;	
		g_Data.bTwinkleRedLED = qdt_false;
		g_Data.bTwinkleGreenLED = qdt_false;

		AUDIODOCKING_RF_446x_PER_CTRL(qdt_OFF);
		AUDIODOCKING_RF_446x(qdt_OFF);			//turn off  RF-4461
		
		if (__qdt_AD_HasPaired())  //to recover ucPairingState when user press power key in pairing mode
			g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_FINISHED_;
		
		
		if (g_UserSaveData.ucPairingState != _QDT_PAIRINGSTATE_FINISHED_)  //pairing is not finish, recover to  _QDT_PAIRINGSTATE_WAITFORSTAR_
			g_UserSaveData.ucPairingState =  _QDT_PAIRINGSTATE_WAITFORSTAR_;
		
		//QDT_flash_WriteUserData(&g_UserSaveData);
		QDT_Common_SaveUserData(qdt_true);

		QDT_ClearCmd();
		QDT_Common_SPKPause();					//pause SPK ouput		
		QDT_Common_MicPause(); 					//pause MIC input		
		
		if(!g_UpdateImageInfo.bStartUpdate) 
		{
		AUDIODOCKING_CAMERA_PWR(qdt_OFF);			//Power down A500-camera
		}
		else
		{
			g_Data.bTwinkleRedLED = qdt_true;
			g_Data.bTwinkleGreenLED = qdt_true;	
		}
		QDT_TRACE("Audio Docking power off....\n");
	}

	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_AD_Product_SyncWord(void)
{
	qdt_uint8 ucFlashUniqueID[8];
	QDT_flash_ReadUniqueID(ucFlashUniqueID);
	g_UserSaveData.ucPairingSyncWord[0] = ucFlashUniqueID[0];
	g_UserSaveData.ucPairingSyncWord[1] = ucFlashUniqueID[2];
	g_UserSaveData.ucPairingSyncWord[2] = ucFlashUniqueID[4];
	g_UserSaveData.ucPairingSyncWord[3] = ucFlashUniqueID[6];

	QDT_DEBUG("QDT_AD_Product_SyncWord = %x:%x:%x:%x \n", g_UserSaveData.ucPairingSyncWord[0], 
		                                                  g_UserSaveData.ucPairingSyncWord[1],
		                                                  g_UserSaveData.ucPairingSyncWord[2],
		                                                  g_UserSaveData.ucPairingSyncWord[3]);
	return QDT_SYS_NOERROR;
}

qdt_bool QDT_AD_SearchDevices(qdt_uint8 *ucDumpRxData)
{
	RFHEADER SentOutRFHeader;
	static qdt_uint8 ucTry2SearchDevCount = 0;
	qdt_uint8 ucSentOutData[10];
	qdt_uint8 ucDestSerialNumber[5];
	qdt_uint8 ucTmpSysState;
	switch (g_UserSaveData.ucPairingState)
	{
	case _QDT_PAIRINGSTATE_WAITFORSTAR_:
		ucTry2SearchDevCount = 0;
		//QDT_AD_Product_SyncWord();
		break;
	case _QDT_PAIRINGSTATE_SEARCHING_:	

#ifdef USER_SELECT_FREQ			
		QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);
#endif

		if (ucTry2SearchDevCount > MAX_TRY2SEARCHDEV_COUNT)   //Try count over max try count, return to pre-system state.
		{
			//WARNING MESSAGE :: [Pairing Failed, Please try again or do factory reset]
			QDT_Play_HumanVoice(HV_PairingFail, 2);
			
			if (__qdt_AD_HasPaired()) 
			{
				g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_FINISHED_;
				g_Data.ucPairingTryCount = 0;	
				ucTry2SearchDevCount = 0;

				g_Data.bTwinkleRedLED = qdt_false;
				g_Data.bTwinkleGreenLED = qdt_false;	
				AUDIODOCKING_LED_PWR_RED(qdt_OFF);
				AUDIODOCKING_LED_PWR_GREEN(qdt_ON);						
			}
			else
			{
			g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_WAITFORSTAR_;
			g_Data.ucPairingTryCount = 0;	
			ucTry2SearchDevCount = 0;

				g_Data.bTwinkleRedLED = qdt_true;
				g_Data.bTwinkleGreenLED = qdt_false;	
				AUDIODOCKING_LED_PWR_RED(qdt_ON);
				AUDIODOCKING_LED_PWR_GREEN(qdt_OFF);						
			}
			
			ucTmpSysState = g_Data.ucPreSysState;
			g_Data.ucPreSysState = g_Data.ucSysState;
			g_Data.ucSysState = ucTmpSysState;   //return to pre-system state.


			QDT_WARNING("Pairing failed, over try pairing count\n");

			break;
		}
		
		g_Data.bIsInPairingState = qdt_true;   //start Pairing State
		ucTry2SearchDevCount++;
		QDT_Common_SPKPause();
		
		ucDestSerialNumber[0] = 0x00;
		ucDestSerialNumber[1] = 0x00;
		ucDestSerialNumber[2] = 0x00;
		ucDestSerialNumber[3] = 0x00;
		ucDestSerialNumber[4] = 0x00;
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFCMD_AUDIODOCKING_SEARCHING_DEVICE, ucDestSerialNumber);
		//g_Data.ucRFSentOutDelayTime = 1;  //don't delay
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData);
		//g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME;
		
		break;
	case _QDT_PAIRINGSTATE_TX_SYNCWORD_:

		if (!__qdt_AD_AllocateRFFreqCH(QDT_Common_GetDeviceType(&ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX])))  //audiodocking can't allocate RFFreqCH device, So enter warming message
		{
			//WARNING MESSAGE :: [Pairing Failed, Please try again or do factory reset]
			QDT_Play_HumanVoice(HV_PairingFail, 2);
			g_Data.bTwinkleRedLED = qdt_true;
			g_Data.bTwinkleGreenLED = qdt_false;	
			
			AUDIODOCKING_LED_PWR_RED(qdt_ON);
			AUDIODOCKING_LED_PWR_GREEN(qdt_OFF);		

			g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_WAITFORSTAR_;
			g_Data.ucPairingTryCount = 0;	

			QDT_WARNING("Pairing failed, over 8 devices\n");
			break;
		}
		
		ucDestSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		ucDestSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		ucDestSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		ucDestSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];	
		ucDestSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];	
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFCMD_AUDIODOCKING_TX_SYNCWORD, ucDestSerialNumber);
		
		SentOutRFHeader.ucDatalength[0] = 0x00;
		SentOutRFHeader.ucDatalength[1] = 0x05;
		ucSentOutData[0] = g_UserSaveData.ucPairingSyncWord[0];
		ucSentOutData[1] = g_UserSaveData.ucPairingSyncWord[1];
		ucSentOutData[2] = g_UserSaveData.ucPairingSyncWord[2];
		ucSentOutData[3] = g_UserSaveData.ucPairingSyncWord[3];
		ucSentOutData[4] = g_Data.ucAllocRFFreqCH;
		
#ifdef USER_SELECT_FREQ
		ucSentOutData[5] = g_UserSaveData.ucPairingFreqIdx;
#endif		

		//g_Data.ucRFSentOutDelayTime = 1;  //don't delay
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData);		
		//g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME;


		g_Data.ucPairingTryCount++;
		if (g_Data.ucPairingTryCount > 10)
		{
			g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_SEARCHING_;
			g_Data.ucPairingTryCount = 0;
		}
		break;
	case _QDT_PAIRINGSTATE_TESTING_SYNCWORD_:
		
		g_Data.bChangeToAllocRFFreqCH = qdt_true;  //Sensor or ParentUint got the pairing CH & SyncWord. 
		QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	 //Change to the Pairing CH & SyncWord and PING them.
		QDT_Common_DelayTime(100); //100ms		
		
		vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		
		
		ucDestSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		ucDestSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		ucDestSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		ucDestSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];
		ucDestSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFCMD_AUDIODOCKING_TESTING_SYNCWORD, ucDestSerialNumber);
		//g_Data.ucRFSentOutDelayTime = 1;  //don't delay
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData);		
		//g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME;


	  	g_Data.ucPairingTryCount++;
		if (g_Data.ucPairingTryCount > 100)
		{
			g_Data.bChangeToAllocRFFreqCH = qdt_false;
			g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_SEARCHING_;
			g_Data.ucAllocRFFreqCH = RF_PARENTUNIT_CH0;
			QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);		
			g_Data.ucPairingTryCount = 0;
		}		
		break;	
	case _QDT_PAIRINGSTATE_FINISHED_:

		if (ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX]==0x80) //Parent Uint Type
		{
			QDT_INFO("Pairing device is ParentUnit DeviceID :: %x:%x:%x:%x:%x\n", ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX],
				                                                               ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1],
				                                                               ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2],
				                                                               ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3],
				                                                               ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4]);
			g_UserSaveData.s_PairedParentUnit[g_Data.ucAllocListIdx].ucPairedRFFreqCH = g_Data.ucAllocRFFreqCH;
			g_UserSaveData.s_PairedParentUnit[g_Data.ucAllocListIdx].ucSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
			g_UserSaveData.s_PairedParentUnit[g_Data.ucAllocListIdx].ucSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
			g_UserSaveData.s_PairedParentUnit[g_Data.ucAllocListIdx].ucSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
			g_UserSaveData.s_PairedParentUnit[g_Data.ucAllocListIdx].ucSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];
			g_UserSaveData.s_PairedParentUnit[g_Data.ucAllocListIdx].ucSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];
			
			g_UserSaveData.ucPairedParentUnitList = g_UserSaveData.ucPairedParentUnitList|(1<<g_Data.ucAllocListIdx);								
			g_UserSaveData.ucPairedParentUnitNum++;
		}
		if (ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX]==0xC0) //Sensor Uint Type
		{
			QDT_INFO("Pairing device is SensorUnit DeviceID :: %x:%x:%x:%x:%x\n", ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX],
				                                                               ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1],
				                                                               ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2],
				                                                               ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3],
				                                                               ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4]);

			g_UserSaveData.s_PairedSensorUnit[g_Data.ucAllocListIdx].ucPairedRFFreqCH = g_Data.ucAllocRFFreqCH;
			g_UserSaveData.s_PairedSensorUnit[g_Data.ucAllocListIdx].ucSensorType = DEVICE_TYPE_SENSOR_CRIB;
			g_UserSaveData.s_PairedSensorUnit[g_Data.ucAllocListIdx].ucSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
			g_UserSaveData.s_PairedSensorUnit[g_Data.ucAllocListIdx].ucSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
			g_UserSaveData.s_PairedSensorUnit[g_Data.ucAllocListIdx].ucSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
			g_UserSaveData.s_PairedSensorUnit[g_Data.ucAllocListIdx].ucSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];			
			g_UserSaveData.s_PairedSensorUnit[g_Data.ucAllocListIdx].ucSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];			
			
			g_UserSaveData.ucPairedSensorUnitList = g_UserSaveData.ucPairedSensorUnitList|(1<<g_Data.ucAllocListIdx);					
			g_UserSaveData.ucPairedSensorUnitNum++;
		}
		if (ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX]==0xE0) //Sensor Uint Type
		{
			QDT_INFO("Pairing device is SensorUnit DeviceID :: %x:%x:%x:%x:%x\n", ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX],
				                                                               ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1],
				                                                               ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2],
				                                                               ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3],
				                                                               ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4]);
		
			g_UserSaveData.s_PairedSensorUnit[g_Data.ucAllocListIdx].ucPairedRFFreqCH = g_Data.ucAllocRFFreqCH;
			g_UserSaveData.s_PairedSensorUnit[g_Data.ucAllocListIdx].ucSensorType = DEVICE_TYPE_SENSOR_GATE;
			g_UserSaveData.s_PairedSensorUnit[g_Data.ucAllocListIdx].ucSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
			g_UserSaveData.s_PairedSensorUnit[g_Data.ucAllocListIdx].ucSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
			g_UserSaveData.s_PairedSensorUnit[g_Data.ucAllocListIdx].ucSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
			g_UserSaveData.s_PairedSensorUnit[g_Data.ucAllocListIdx].ucSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];			
			g_UserSaveData.s_PairedSensorUnit[g_Data.ucAllocListIdx].ucSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];			

			g_UserSaveData.ucPairedSensorUnitList = g_UserSaveData.ucPairedSensorUnitList|(1<<g_Data.ucAllocListIdx);					
			g_UserSaveData.ucPairedSensorUnitNum++;
		}		

		QDT_INFO("Allocate RF CH:: %d\n", g_Data.ucAllocRFFreqCH);
		QDT_INFO("Allocate RF Freq:: %d\n", g_UserSaveData.ucPairingFreqIdx);
		
		g_Data.bChangeToAllocRFFreqCH = qdt_false;
		g_Data.bIsInPairingState = qdt_false;
		g_Data.ucPairingTryCount = 0;
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_RUNNING;
		g_Data.bTwinkleRedLED = qdt_false;
		g_Data.bTwinkleGreenLED = qdt_false;	
		
		//QDT_flash_WriteUserData(&g_UserSaveData);
		QDT_Common_SaveUserData(qdt_false);
		QDT_Play_HumanVoice(HV_PairingSuccess,2);
		
		QDT_Common_MicResume();
		AUDIODOCKING_LED_PWR_RED(qdt_OFF);
		AUDIODOCKING_LED_PWR_GREEN(qdt_ON);
		return qdt_true;	
	default:
		break;			
	}
	return qdt_false;
}
RETURN_TYPE QDT_AD_UnPaired(qdt_uint8 *ucDumpRxData)
{	
	qdt_uint8 i;
	//qdt_uint8 ucPairedListDeviceUID[5];

	if (QDT_Common_GetDeviceType(&ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX]) == DEVICE_TYPE_PARENTUNIT) //Parent Uint Type
	{
		for (i=0; i<8; i++)
		{

			if (QDT_Common_CompareSN(&ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX], &g_UserSaveData.s_PairedParentUnit[i].ucSerialNumber[0]))
			{
				g_UserSaveData.s_PairedParentUnit[i].ucSerialNumber[0] = 0xFF;
				g_UserSaveData.s_PairedParentUnit[i].ucSerialNumber[1] = 0xFF;
				g_UserSaveData.s_PairedParentUnit[i].ucSerialNumber[2] = 0xFF;
				g_UserSaveData.s_PairedParentUnit[i].ucSerialNumber[3] = 0xFF;			
				g_UserSaveData.s_PairedParentUnit[i].ucSerialNumber[4] = 0xFF;	
				g_UserSaveData.ucPairedParentUnitNum--;
				g_UserSaveData.ucPairedParentUnitList = g_UserSaveData.ucPairedParentUnitList&(~(1<<i));		
				break;
			}
		}
	}

	if ((QDT_Common_GetDeviceType(&ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX]) == DEVICE_TYPE_SENSOR_CRIB) || 
		 (QDT_Common_GetDeviceType(&ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX]) == DEVICE_TYPE_SENSOR_GATE) ) //Sensor Uint Type
	{
		for (i=0; i<8; i++)
		{
			if (QDT_Common_CompareSN(&ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX], &g_UserSaveData.s_PairedSensorUnit[i].ucSerialNumber[0]))
			{
				g_UserSaveData.s_PairedSensorUnit[i].ucSerialNumber[0] = 0xFF;
				g_UserSaveData.s_PairedSensorUnit[i].ucSerialNumber[1] = 0xFF;
				g_UserSaveData.s_PairedSensorUnit[i].ucSerialNumber[2] = 0xFF;
				g_UserSaveData.s_PairedSensorUnit[i].ucSerialNumber[3] = 0xFF;
				g_UserSaveData.s_PairedSensorUnit[i].ucSerialNumber[4] = 0xFF;
				g_UserSaveData.ucPairedSensorUnitNum--;
				g_UserSaveData.ucPairedSensorUnitList = g_UserSaveData.ucPairedSensorUnitList&(~(1<<i));	
				break;
			}
		}
	}

	QDT_DEBUG("Unpairing sucessful, unpairing device ID : %x:%x:%x:%x:%x\n",ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX],
																			ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1],
																			ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2],
																			ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3],
																			ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4]);
	//QDT_flash_WriteUserData(&g_UserSaveData);
	QDT_Common_SaveUserData(qdt_false);
	return QDT_SYS_NOERROR;
}
RETURN_TYPE QDT_AD_UpdateSensorStatus(qdt_uint8 *ucDumpRxData)
{
	static qdt_uint8 ucUpdateSensorCount = 0;
	qdt_uint8 i;
	qdt_uint16 uctmp;	

	if (g_UserSaveData.ucPairedSensorUnitNum == 0)
	{
		g_Data.bNeedToBoardCastRFCmd = qdt_false;	 
		g_Data.ucBoardCastRFCmd = RFCMD_NON_CMD;	
		QDT_DEBUG("g_UserSaveData.ucPairedSensorUnitNum = 0, no need update Sensor status \n");			
	}
	else
	{
		QDT_DEBUG("g_UserSaveData.ucPairedSensorUnitNum = %d\n", g_UserSaveData.ucPairedSensorUnitNum);
	for (i=0; i<SUPPORT_SERSORUNIT_MAXNUM; i++)
	{
		if (QDT_Common_CompareSN(&ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX], &g_UserSaveData.s_PairedSensorUnit[i].ucSerialNumber[0])
			&&  !g_UserSaveData.s_PairedSensorUnit[i].bIsUpdated)
		{
			g_UserSaveData.s_PairedSensorUnit[i].bIsUpdated = qdt_true;
			g_UserSaveData.s_PairedSensorUnit[i].ucTempture[0]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX];	
			g_UserSaveData.s_PairedSensorUnit[i].ucTempture[1]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+1];	
			g_UserSaveData.s_PairedSensorUnit[i].ucHumidity[0]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+2];
			g_UserSaveData.s_PairedSensorUnit[i].ucHumidity[1]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+3];
			g_UserSaveData.s_PairedSensorUnit[i].ucAcceleroMeterXAxis[0]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+4];
			g_UserSaveData.s_PairedSensorUnit[i].ucAcceleroMeterXAxis[1]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+5];
			g_UserSaveData.s_PairedSensorUnit[i].ucAcceleroMeterYAxis[0]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+6];
			g_UserSaveData.s_PairedSensorUnit[i].ucAcceleroMeterYAxis[1]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+7];
			g_UserSaveData.s_PairedSensorUnit[i].ucAcceleroMeterZAxis[0]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+8];
			g_UserSaveData.s_PairedSensorUnit[i].ucAcceleroMeterZAxis[1]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+9];
			g_UserSaveData.s_PairedSensorUnit[i].ucMagnetoMeterXAxis[0]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+10];
			g_UserSaveData.s_PairedSensorUnit[i].ucMagnetoMeterXAxis[1]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+11];
			g_UserSaveData.s_PairedSensorUnit[i].ucMagnetoMeterYAxis[0]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+12];
			g_UserSaveData.s_PairedSensorUnit[i].ucMagnetoMeterYAxis[1]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+13];
			g_UserSaveData.s_PairedSensorUnit[i].ucMagnetoMeterZAxis[0]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+14];
			g_UserSaveData.s_PairedSensorUnit[i].ucMagnetoMeterZAxis[1]= ucDumpRxData[QDT_RFPKT_DATA_START_IDX+15];

			QDT_DEBUG("Sender device ID : %x:%x:%x:%x:%x\n",ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX],
																					ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1],
																					ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2],
																					ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3],
																					ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4]);
			
			QDT_DEBUG("g_UserSaveData.s_PairedSensorUnit[%d].ucTempture =%x:%x\n", i, g_UserSaveData.s_PairedSensorUnit[i].ucTempture[0], g_UserSaveData.s_PairedSensorUnit[i].ucTempture[1]);
			QDT_DEBUG("g_UserSaveData.s_PairedSensorUnit[%d].ucHumidity =%x:%x\n", i, g_UserSaveData.s_PairedSensorUnit[i].ucHumidity[0], g_UserSaveData.s_PairedSensorUnit[i].ucHumidity[1]);			
			QDT_DEBUG("g_UserSaveData.s_PairedSensorUnit[%d].ucAcceleroMeterXAxis =%x:%x\n", i, g_UserSaveData.s_PairedSensorUnit[i].ucAcceleroMeterXAxis[0], g_UserSaveData.s_PairedSensorUnit[i].ucAcceleroMeterXAxis[1]);			
			QDT_DEBUG("g_UserSaveData.s_PairedSensorUnit[%d].ucAcceleroMeterYAxis =%x:%x\n", i, g_UserSaveData.s_PairedSensorUnit[i].ucAcceleroMeterYAxis[0], g_UserSaveData.s_PairedSensorUnit[i].ucAcceleroMeterYAxis[1]);				
			QDT_DEBUG("g_UserSaveData.s_PairedSensorUnit[%d].ucAcceleroMeterZAxis =%x:%x\n", i, g_UserSaveData.s_PairedSensorUnit[i].ucAcceleroMeterZAxis[0], g_UserSaveData.s_PairedSensorUnit[i].ucAcceleroMeterZAxis[1]);
			QDT_DEBUG("g_UserSaveData.s_PairedSensorUnit[%d].ucMagnetoMeterXAxis =%x:%x\n", i, g_UserSaveData.s_PairedSensorUnit[i].ucMagnetoMeterXAxis[0], g_UserSaveData.s_PairedSensorUnit[i].ucMagnetoMeterXAxis[1]);
			QDT_DEBUG("g_UserSaveData.s_PairedSensorUnit[%d].ucMagnetoMeterYAxis =%x:%x\n", i, g_UserSaveData.s_PairedSensorUnit[i].ucMagnetoMeterYAxis[0], g_UserSaveData.s_PairedSensorUnit[i].ucMagnetoMeterYAxis[1]);
			QDT_DEBUG("g_UserSaveData.s_PairedSensorUnit[%d].ucMagnetoMeterZAxis =%x:%x\n", i, g_UserSaveData.s_PairedSensorUnit[i].ucMagnetoMeterZAxis[0], g_UserSaveData.s_PairedSensorUnit[i].ucMagnetoMeterZAxis[1]);

				
			ucUpdateSensorCount++;
				
			uctmp = g_UserSaveData.s_PairedSensorUnit[i].ucTempture[1]<<8|g_UserSaveData.s_PairedSensorUnit[i].ucTempture[0];
			uctmp = uctmp/10;
			QDT_Play_HV_SensorTemperature(uctmp);			

			uctmp = g_UserSaveData.s_PairedSensorUnit[i].ucHumidity[1]<<8|g_UserSaveData.s_PairedSensorUnit[i].ucHumidity[0];
			//uctmp = uctmp/10;			
			QDT_Play_HV_SensorHumidity(uctmp);
				
			if (ucUpdateSensorCount == g_UserSaveData.ucPairedSensorUnitNum)  //collect all sensors status, stop to sent out query cmd
			{
				ucUpdateSensorCount = 0;
				g_Data.bNeedToBoardCastRFCmd = qdt_false;    
				g_Data.ucBoardCastRFCmd = RFCMD_NON_CMD;    
				break;
			}

			}
		}
	}

	 g_Data.bStopPollingRXChan = qdt_false; // recover polling Receive CH
	return QDT_SYS_NOERROR;
	
}

RETURN_TYPE QDT_AD_SetNightLightOnOFF(qdt_uint16 u16OnOff)
{

	if (u16OnOff == g_UserSaveData.ucNightLightEnbled)
		return QDT_SYS_NOERROR;	
	
	if (u16OnOff)   //turn on Night Light
	{
		g_UserSaveData.ucNightLightEnbled = qdt_ON;
		bBreatheFlag = qdt_true;
		g_Data.ucNLTargetBrightLevel = g_UserSaveData.ucBrightLevel;
		g_Data.ucNLCurrentBrightLevel =0;
		//itmp = 0;
		//iCount = g_UserSaveData.ucBrightLevel;		
		//itmp = g_UserSaveData.ucBrightLevel;
		//iCount = g_UserSaveData.ucBrightLevel = itempLevel;			
		//PWMA->CMR0=ucNightLightBrightLevel[g_UserSaveData.ucBrightLevel]; 			//Duty (CMR0+1)/(CNR0+1), duty is from 1 ~ (DUTY_RESOLUTION+1)/(DUTY_RESOLUTION+1) low 
	}
	else  			//turn off Night Light
	{	
		g_UserSaveData.ucNightLightEnbled = qdt_OFF;
		bBreatheFlag = qdt_true;
		g_Data.ucNLTargetBrightLevel = 0;
		g_Data.ucNLCurrentBrightLevel = g_UserSaveData.ucBrightLevel;

		//PWMA->CMR0=1000;			
	}
	QDT_DEBUG("Set NightLight = %d\n", g_UserSaveData.ucNightLightEnbled);
	return QDT_SYS_NOERROR;	
}

RETURN_TYPE QDT_AD_SetNightLightLevel(qdt_uint16 u16Level)
{
	g_Data.ucNLTargetBrightLevel = (qdt_uint8)u16Level / 4;
	g_Data.ucNLCurrentBrightLevel = g_UserSaveData.ucBrightLevel;
	g_UserSaveData.ucBrightLevel = (qdt_uint8)u16Level / 4;
	if (g_UserSaveData.ucNightLightEnbled)
	bBreatheFlag = qdt_true;
	//g_UserSaveData.ucBrightLevel = (qdt_uint8) u16Level /4;
	//if (g_UserSaveData.ucNightLightEnbled)
	//	PWMA->CMR0=ucNightLightBrightLevel[g_UserSaveData.ucBrightLevel]; 			//Duty (CMR0+1)/(CNR0+1), duty is from 1 ~ (DUTY_RESOLUTION+1)/(DUTY_RESOLUTION+1) low 

	QDT_DEBUG("Set NightLight Level = %d\n", g_UserSaveData.ucBrightLevel);	
	return QDT_SYS_NOERROR;	
}

RETURN_TYPE QDT_AD_SetNightLightTimer(qdt_uint16 u16TimerMins)
{
	if (u16TimerMins > 0)
	{
		g_Data.ucNightLightTimerStart = qdt_true;
		g_Data.u16NightLightTimerSecs = u16TimerMins*60;
	}
	else
	{
		g_Data.ucNightLightTimerStart = qdt_false;
		g_Data.u16NightLightTimerSecs = 0;	
	}

	QDT_DEBUG("Set NightLight TimerStart  = %d\n", g_Data.ucNightLightTimerStart);	
	QDT_DEBUG("Set NightLight TimerSecs  = %d\n", g_Data.u16NightLightTimerSecs);	
	return QDT_SYS_NOERROR;	
}

RETURN_TYPE QDT_AD_SetAudioAlertLevel(qdt_uint16 u16Level)
{
	g_UserSaveData.ucAudioAlertLevel= (qdt_uint8)u16Level;

	g_Data.ucAudioAlertValue = g_Data.ucAudioAlertTable[g_UserSaveData.ucAudioAlertLevel]; 

	QDT_DEBUG("Set AudioAlertLevel = %d\n", g_UserSaveData.ucAudioAlertLevel);	
	return QDT_SYS_NOERROR;	
}

RETURN_TYPE QDT_AD_SetAudioAlertOnOFF(qdt_uint16 u16OnOff)
{
	if (u16OnOff == g_UserSaveData.ucAudioAlertEnable)
		return QDT_SYS_NOERROR;	
	
	if (u16OnOff)   
		g_UserSaveData.ucAudioAlertEnable = qdt_ON;
	else
		g_UserSaveData.ucAudioAlertEnable = qdt_OFF;
	
	QDT_DEBUG("Set AudioAlertEnable = %d\n", g_UserSaveData.ucAudioAlertEnable);
	return QDT_SYS_NOERROR;	
}


RETURN_TYPE QDT_AD_AudioAlert(signed short *pi16Src, qdt_uint16 u16PcmCount)
{
	qdt_uint16 u16temp;
	qdt_sint16 PcmValue;
	qdt_uint32 TotalPcmValue;
	static qdt_uint8 uCount = 0;
	static qdt_uint8 uEndCount = 0;
	static qdt_uint8 ALCCount = 0;
	static qdt_uint8 ALCFlag = 0;

	TotalPcmValue = 0;
	for(u16temp = 0; u16temp< u16PcmCount; u16temp++)
	{
		PcmValue= *pi16Src;
		if (PcmValue < 0)
			PcmValue = 0xFFFFFFFF - PcmValue + 1; //negative number transfer to positive number
		if (u16temp < (u16PcmCount - 1))
		{
			TotalPcmValue += PcmValue;
		}
		else
		{
			TotalPcmValue += PcmValue;
			#if 1//AudioAlertToRS232
			if (g_Data.bAudioAlertTrigger == qdt_true)
			{
				uCount++;
				if (uCount == 10)
				{
					if (TotalPcmValue < g_Data.ucAudioAlertValue)
					{
						uEndCount++;
						if(uEndCount == 5)
						{
							QDT_Uart_SentOutAudioAlertTriggerEnd();
							g_Data.bAudioAlertTrigger = qdt_false;
							uEndCount = 0;
						}	
					}
					else
					{
						uEndCount = 0;
					}
					uCount = 0;	
				}
			}
			#endif

			
			#if 0//ALC function
			if (ALCFlag == 0)
			{				
				if (TotalPcmValue >= ALCThreshold)
				{
					ALCCount = ALCCount + 1;
					if (ALCCount == 100)//count 2 sec and  larger than ALCThreshold every time
					{
						DrvADC_SetPGAGaindB(2000);
						ALCCount = 0;
						ALCFlag = 1;
					}
				}
				else
					ALCCount = 0;
			}
			else
			{				
				if (TotalPcmValue < ALCThreshold)
				{
					ALCCount = ALCCount + 1;
					if (ALCCount == 100)//count 2 sec and  larger than ALCThreshold every 
					{
						DrvADC_SetPGAGaindB(3000);
						ALCCount = 0;
						ALCFlag = 0;
					}
				}
				else
					ALCCount = 0;
			}	
			#endif
			
			//printf("2AudioDetectLevel u16temp: %d PcmValue: %d TotalPcmValue: %d\n",u16temp, PcmValue, TotalPcmValue);
			if (TotalPcmValue > g_Data.ucAudioAlertValue)
			{
			#if 1//AudioAlertToRS232
				if(g_UserSaveData.ucAudioAlertEnable && g_Data.bAudioAlertTrigger == qdt_false)
				{
					QDT_Uart_SentOutAudioAlertTriggerStart();
					g_Data.bAudioAlertTrigger = qdt_true;
				}
			#endif
				if(!g_Data.bAudioAlertFlag)
			{
				g_Data.bNeedToBoardCastRFCmd = qdt_true;
				g_Data.ucBoardCastRFCmd = RFCMD_AUDIODOCKING_AUDIOALERT;
				g_Data.bAudioAlertFlag= qdt_true;
			}
			}
			TotalPcmValue = 0;
		}
		pi16Src++;
	}
	return QDT_SYS_NOERROR;	
}

RETURN_TYPE QDT_AD_SetSpeakerOnOff(qdt_uint16 u16OnOff)
{
	switch(u16OnOff)
	{
		case 0:
			if (DrvGPIO_GetBit(GPB,5) == 1)
				QDT_Common_SPKPause();	
			break;
		case 1:
			if (DrvGPIO_GetBit(GPB,5) == 1)
				QDT_Common_SPKResume();
			break;
		default:
			break;
	}
	return QDT_SYS_NOERROR;	
}

RETURN_TYPE QDT_AD_ProcessStandByKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData)
{
	switch (ucCmd)
	{
	/* ======  RF Cmd =========================================== */
	/* ======  User Key =========================================== */
	case _USER_KEY_VERSION:
		QDT_Play_HV_FirmwareVersion(&g_ucFWVersion[9], 4);
		QDT_TRACE("Audio Docking Version is %d:%d:%d:%d \n", g_ucFWVersion[9], g_ucFWVersion[10], g_ucFWVersion[11], g_ucFWVersion[12]);
		break;	
	case _USER_KEY_AUDIODOCKING_KEY_POWER:
		QDT_AD_SysPowerOnOff();
		break;		
	/* ======  Long Press Key =========================================== */		
	case _USER_KEY_AUDIODOCKING_KEY_LONGPRESS_VOLDOWN:
		QDT_Play_HumanVoice(HV_FactoryReset, 2);
		QDT_flash_ClearUserData();
		break;
	/* ======  Sensor RF Cmd =========================================== */	
	default:
		break;			
	}
	return QDT_SYS_NOERROR;	
}
RETURN_TYPE QDT_AD_ProcessPairingKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData)
{
	switch (ucCmd)
	{
	/* ======  RF Cmd =========================================== */
	case RFCMD_PARENTUINT_PING_AD:
		if (g_UserSaveData.ucPairingState >= _QDT_PAIRINGSTATE_PINGAD_)
			g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_SEARCHING_;		
		break;	
	case RFACK_PARENTUINT_IAMHERE: 				/*AudioDocking Pairing State 1*/
		if (g_UserSaveData.ucPairingState >= _QDT_PAIRINGSTATE_PINGAD_)
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_TX_SYNCWORD_;
		break;
	case RFACK_PARENTUINT_GOT_SYNCWORD: 					/*AudioDocking Pairing State 2*/
		if (g_UserSaveData.ucPairingState >= _QDT_PAIRINGSTATE_PINGAD_)
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_TESTING_SYNCWORD_;	
		break;
	case RFACK_PARENTUINT_USING_SYNCWORD: 				/*AudioDocking Pairing State 3*/
		if (g_UserSaveData.ucPairingState >= _QDT_PAIRINGSTATE_PINGAD_)
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_FINISHED_;
		break;	
	/* ======  User Key =========================================== */
	case _USER_KEY_VERSION:
		QDT_Play_HV_FirmwareVersion(&g_ucFWVersion[9], 4);
		QDT_TRACE("Audio Docking Version is %d:%d:%d:%d \n", g_ucFWVersion[9],g_ucFWVersion[10], g_ucFWVersion[11], g_ucFWVersion[12]);
		break;	
	case _USER_KEY_AUDIODOCKING_KEY_POWER:
		QDT_AD_SysPowerOnOff();
		break;		
		
	case _USER_KEY_AUDIODOCKING_KEY_PAIRING:
		
#ifdef USER_SELECT_FREQ		
		if (g_UserSaveData.ucPairingFreqIdx == 0xFF)
		{
			g_UserSaveData.ucPairingFreqIdx = 0; //default RF frequence Index = 0, 915M
		}
#endif		

		QDT_Play_HumanVoice(HV_PairingMode,2);
		vRadio_StartRX(0);   //change to RX ParentUint channel
		QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_UNPAIRING;
#ifdef USER_SELECT_FREQ
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_PINGAD_;
#else
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_SEARCHING_;
#endif

		g_Data.bTwinkleRedLED = qdt_false;
		AUDIODOCKING_LED_PWR_RED(qdt_OFF);
		g_Data.bTwinkleGreenLED = qdt_true;
		break;
#ifdef USER_SELECT_FREQ		
	case _USER_KEY_AUDIODOCKING_KEY_VOLUP:

		if (!__qdt_AD_HasPaired())  //if pairing success once, don't allow user change the ch(freq.)
		{																														
		g_UserSaveData.ucPairingFreqIdx++;
		if (g_UserSaveData.ucPairingFreqIdx>=RF_FREQ_TABLE_IDX_MAX)
			g_UserSaveData.ucPairingFreqIdx = 0;
		
		//WARNING MESSAGE :: [Selec Freq. xxx]
		QDT_Play_HV_SelectFrequency(g_UserSaveData.ucPairingFreqIdx);

		QDT_TRACE("(up) USER Select Freq. idx = %d\n", g_UserSaveData.ucPairingFreqIdx);
		}
		
		break;
	case _USER_KEY_AUDIODOCKING_KEY_VOLDOWN:
				
		if (!__qdt_AD_HasPaired())  //if pairing success once, don't allow user change the ch(freq.)		
		{
		if (g_UserSaveData.ucPairingFreqIdx == 0)
			g_UserSaveData.ucPairingFreqIdx=RF_FREQ_TABLE_IDX_MAX-1;
		else
			g_UserSaveData.ucPairingFreqIdx--;
		
		//WARNING MESSAGE :: [Selec Freq. xxx]	
		QDT_Play_HV_SelectFrequency(g_UserSaveData.ucPairingFreqIdx);
		QDT_TRACE("(down) USER Select Freq. idx = %d\n", g_UserSaveData.ucPairingFreqIdx);
		}
		break;
#endif
	/* ======  Long Press Key =========================================== */
	case _USER_KEY_AUDIODOCKING_KEY_LONGPRESS_VOLDOWN:	
		QDT_Play_HumanVoice(HV_FactoryReset, 2);
		g_Data.bFactoryResetFlag = qdt_true;
		QDT_AD_SysPowerOnOff(); //power off		
		QDT_flash_ClearUserData();
		break;
	default:
		break;			
	}
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_AD_ProcessKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData)
{
	RFHEADER SentOutRFHeader;
	qdt_uint8 ucSentOutData[10];
	qdt_uint8 ucDestSerialNumber[5];
	static qdt_uint8 ucCounter = 0;
	
	switch (ucCmd)
	{
	/* ======  RF Cmd =========================================== */
	//case RFACK_PARENTUINT_IAMHERE: 				/*AudioDocking Pairing State 1*/
	//	g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_TX_SYNCWORD_;
	//	break;
	//case RFACK_PARENTUINT_GOT_SYNCWORD: 					/*AudioDocking Pairing State 2*/
	//	g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_TESTING_SYNCWORD_;	
	//	break;
	//case RFACK_PARENTUINT_USING_SYNCWORD: 				/*AudioDocking Pairing State 3*/
	//	g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_FINISHED_;
	//	break;	
	case RFCMD_PARENTCOMMAND_UNPAIRING:
		QDT_AD_UnPaired(ucDumpRxData);
		
		ucDestSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		ucDestSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		ucDestSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		ucDestSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];		
		ucDestSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];		
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_AUDIODOCKING_UNPAIRED, ucDestSerialNumber);
		//g_Data.ucRFSentOutDelayTime = 1; //don't delay
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 		
		//g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME; //don't delay
		
		break;

	case RFCMD_PARENTCOMMAND_ONLINE:
		QDT_AD_Parents_OnLine();
		
		ucDestSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		ucDestSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		ucDestSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		ucDestSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];		
		ucDestSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];	
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFCMD_AUDIODOCKING_ONLINE, ucDestSerialNumber);
		//g_Data.ucRFSentOutDelayTime = 1;  //don't delay
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData);		
		//g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME;	
		
		break;
	case RFCMD_PARENTCOMMAND_CALLING:
		QDT_AD_Parents_Calling();

		ucDestSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		ucDestSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		ucDestSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		ucDestSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];		
		ucDestSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];	
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_AUDIODOCKING_ANSWERCALLING, ucDestSerialNumber);
		//g_Data.ucRFSentOutDelayTime = 1;  //don't delay
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 	
		//g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME;
		
		break;
	case RFCMD_PARENTCOMMAND_HANDUP:
		QDT_AD_Parents_HandUp();

		ucDestSerialNumber[0] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX];
		ucDestSerialNumber[1] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+1];
		ucDestSerialNumber[2] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+2];
		ucDestSerialNumber[3] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+3];	
		ucDestSerialNumber[4] = ucDumpRxData[QDT_RFPKT_SRCSN_START_IDX+4];	
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFACK_AUDIODOCKING_HANDUP, ucDestSerialNumber);
		//g_Data.ucRFSentOutDelayTime = 1;  //don't delay
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData); 	
		//g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME;

		break;		
	case RFCMD_PARENTCOMMAND_HANDUP_ACK:
		break;		
		
	case RFACK_PARENTUINT_AUDIOALERT:
		g_Data.bAudioAlertFlag = qdt_false;
		
		g_Data.bNeedToBoardCastRFCmd = qdt_false;
		g_Data.ucBoardCastRFCmd = RFCMD_NON_CMD;
		//g_Data.bStopPollingRXChan = qdt_false;
		break;
	case RFCMD_SERSORALERT_MOTION:
		g_Data.bNeedToBoardCastRFCmd = qdt_true;
		g_Data.ucBoardCastRFCmd = RFCMD_AUDIODOCKING_SENSORALERT_MOTION;
		//g_Data.bStopPollingRXChan = qdt_false;
		break;
	case RFACK_AUDIODOCKING_SENSORALERT_MOTION:
		g_Data.bNeedToBoardCastRFCmd = qdt_false;
		g_Data.ucBoardCastRFCmd = RFCMD_NON_CMD;
		//g_Data.bStopPollingRXChan = qdt_false;
		break;	
	case RFCMD_FACTORYTEST_RFTEST:
		QDT_Factory_RFTest_ACK();
		break;
	/* ======  User Key =========================================== */
	case _USER_KEY_VERSION:
		QDT_Play_HV_FirmwareVersion(&g_ucFWVersion[9], 4);		
		QDT_TRACE("Audio Docking Version is %d:%d:%d:%d \n", g_ucFWVersion[9], g_ucFWVersion[10], g_ucFWVersion[11], g_ucFWVersion[12]);
		break;	
	case _USER_KEY_AUDIODOCKING_KEY_POWER:
		QDT_AD_SysPowerOnOff();
		break;		
		
	case _USER_KEY_AUDIODOCKING_KEY_PAIRING:
		
#ifdef USER_SELECT_FREQ		
		if (g_UserSaveData.ucPairingFreqIdx == 0xFF)
		{
			//WARNING MESSAGE :: [Please press Vol+/Vol- to select PairingCH]
			QDT_Play_HumanVoice(HV_PlsPressVolToSelFreq,1);
			g_UserSaveData.ucPairingFreqIdx = 0; //default RF frequence Index = 0, 915M
		}
#endif		

		QDT_Play_HumanVoice(HV_PairingMode,2);
		vRadio_StartRX(0);   //change to RX ParentUint channel
		QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_UNPAIRING;
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_SEARCHING_;
		g_Data.bTwinkleRedLED = qdt_false;
		AUDIODOCKING_LED_PWR_RED(qdt_OFF);
		g_Data.bTwinkleGreenLED = qdt_true;
		break;
				
	case _USER_KEY_AUDIODOCKING_KEY_VOLUP:	

		{
		qdt_uint8 i;
		for (i=0; i<SUPPORT_SERSORUNIT_MAXNUM; i++)
		{
			g_UserSaveData.s_PairedSensorUnit[i].bIsUpdated = qdt_false;
		}
		g_Data.bNeedToBoardCastRFCmd = qdt_true;  //for testing
		g_Data.ucBoardCastRFCmd = RFCMD_AUDIODOCKING_QUERYSENSORSTATUE; //for testing
		}
		
		if (g_Data.ucPreVol<16)
			g_Data.ucCurVol++;
		if (g_Data.ucCurVol>16)
			g_Data.ucCurVol = 16;

		QDT_DEBUG("Volume UP g_Data.ucCurVol = %d\n",g_Data.ucCurVol);
		
		break;
	case _USER_KEY_AUDIODOCKING_KEY_VOLDOWN:	
		/*
		vRadio_StartRX(0);   //change to RX ParentUint channel
		QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_UNPAIRING;
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_SEARCHING_;
		g_Data.bTwinkleRedLED = qdt_false;
		AUDIODOCKING_LED_PWR_RED(qdt_OFF);
		g_Data.bTwinkleGreenLED = qdt_true;
		*/
	    //DrvGPIO_SetBit(GPB,14);  //set XDCS High level
		//DrvGPIO_SetBit(GPB,15);
		if (g_Data.ucPreVol == 0)
			g_Data.ucCurVol = 0;
		else
			g_Data.ucCurVol--;		
		QDT_DEBUG("Volume DOWN g_Data.ucCurVol = %d\n",g_Data.ucCurVol);
		break;
	/* ======  Long Press Key =========================================== */
	case _USER_KEY_AUDIODOCKING_KEY_LONGPRESS_PAIRING:
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_UPGRADE;
		g_Data.ucUpgradeState = _QDT_UPGRADESTATE_SENTOUTREQUST_;
		g_Data.bTwinkleRedLED = qdt_true;
		g_Data.bTwinkleGreenLED = qdt_true;	
		break;
	case _USER_KEY_AUDIODOCKING_KEY_LONGPRESS_VOLDOWN:	
		QDT_Play_HumanVoice(HV_FactoryReset, 2);
		g_Data.bFactoryResetFlag = qdt_true;
		QDT_AD_SysPowerOnOff(); //power off		
		QDT_flash_ClearUserData();
		break;
		
	/* ======  Sensor RF Cmd =========================================== */	
	case RFACK_SERSORDATA_UPDATESTATUS:
		
		QDT_AD_UpdateSensorStatus(ucDumpRxData);		
		break;
	case RFCMD_SENSOR_REPORTSTATUS:
		QDT_AD_UpdateSensorStatus(ucDumpRxData);	
		g_Data.bNeedToBoardCastRFCmd = qdt_true;  //for testing
		g_Data.ucBoardCastRFCmd = RFACK_SENSOR_REPORTSTATUS; //for testing		
		break;
	case RFCMD_SENSOR_STOP2SENDACK:
		g_Data.bNeedToBoardCastRFCmd = qdt_false;  //for testing
		g_Data.ucBoardCastRFCmd = RFCMD_NON_CMD; //for testing		
		break;		
	case RFCMD_SENSORDATA_TEST:
		if (ucCounter==1)
			AUDIODOCKING_LED_PWR_RED(qdt_ON);
		else 
			AUDIODOCKING_LED_PWR_RED(qdt_OFF);
			ucCounter++;
		
			if (ucCounter>1) ucCounter=0;
			g_Data.bStopPollingRXChan = qdt_false;
		break;
	default:
		break;			
	}
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_AD_UpgradeParentUnit(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData)
{
	RFHEADER SentOutRFHeader;
	qdt_uint8 ucSentOutData[42];
	qdt_uint8 ucDestSerialNumber[5];
	
	switch (g_Data.ucUpgradeState)
	{
	/* ======  RF Cmd =========================================== */
	case _QDT_UPGRADESTATE_WAITFORSTAR_:
		break;
	case _QDT_UPGRADESTATE_SENTOUTREQUST_:
		ucDestSerialNumber[0] = 0x00;
		ucDestSerialNumber[1] = 0x00;
		ucDestSerialNumber[2] = 0x00;
		ucDestSerialNumber[3] = 0x00;
		ucDestSerialNumber[4] = 0x00;
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFCMD_AUDIODOCKING_ASKGOTOUPGRADESTATE, ucDestSerialNumber);
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData);
		break;
	case _QDT_UPGRADESTATE_PARENTREADY_:


		SentOutRFHeader.ucDatalength[0] = 0x00;
		SentOutRFHeader.ucDatalength[1] = 0x09;
		g_UpdateImageInfo.ucImageVersion[0] = 0x00;
		g_UpdateImageInfo.ucImageVersion[1] = 0x00;
		g_UpdateImageInfo.ucImageVersion[2] = 0x01;
		g_UpdateImageInfo.ucImageVersion[3] = 0x01;

		g_UpdateImageInfo.u32AddrIdx = 0;
		//g_UpdateImageInfo.u32ImageSize = 126832;
		//QDT_Upgrade_ReadImage(&g_ucDumpImageData[7], g_UpdateImageInfo.u32AddrIdx, 32);
		
		ucDestSerialNumber[0] = 0x00;
		ucDestSerialNumber[1] = 0x00;
		ucDestSerialNumber[2] = 0x00;
		ucDestSerialNumber[3] = 0x00;
		ucDestSerialNumber[4] = 0x00;
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFCMD_AUDIODOCKING_UPGRADE_FWINFO, ucDestSerialNumber);
		
		SentOutRFHeader.ucDatalength[0] = 0x00;
		SentOutRFHeader.ucDatalength[1] = 0x09;

		ucSentOutData[0] = g_UpdateImageInfo.ucImageVersion[0];
		ucSentOutData[1] = g_UpdateImageInfo.ucImageVersion[1];
		ucSentOutData[2] = g_UpdateImageInfo.ucImageVersion[2];
		ucSentOutData[3] = g_UpdateImageInfo.ucImageVersion[3];
		ucSentOutData[4] = g_UpdateImageInfo.u32ImageSize>>24&0xFF;
		ucSentOutData[5] = g_UpdateImageInfo.u32ImageSize>>16&0xFF;
		ucSentOutData[6] = g_UpdateImageInfo.u32ImageSize>>8&0xFF;
		ucSentOutData[7] = g_UpdateImageInfo.u32ImageSize&0xFF;
		ucSentOutData[8] = g_UpdateImageInfo.ucImageType;
		
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData);

		break;		
	case _QDT_UPGRADESTATE_TRANSIMAGE_:
		ucDestSerialNumber[0] = 0x00;
		ucDestSerialNumber[1] = 0x00;
		ucDestSerialNumber[2] = 0x00;
		ucDestSerialNumber[3] = 0x00;
		ucDestSerialNumber[4] = 0x00;
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFCMD_AUDIODOCKING_UPGRADE_FWIMAGE, ucDestSerialNumber);
		SentOutRFHeader.ucQdtUIDType = QDTUID_TYPE_UPGRADEFW; 
		SentOutRFHeader.ucDatalength[0] = 0x00;
		SentOutRFHeader.ucDatalength[1] = QDT_RF_UPGRADEFW_PACKAGE_LENGTH;
		//for (i=0;i<32;i++)
		//{
		//	ucSentOutData[i+4] = g_ucDumpImageData[g_UpdateImageInfo.u32AddrIdx*32+i];
		//}
		//ucSentOutData[0] = QDT_UPGRADEFW_START_BYTE;
		//ucSentOutData[1] = 0x00; //non-used
		//ucSentOutData[2] = g_UpdateImageInfo.u32AddrIdx;
		//ucSentOutData[3] = 0x20;
		//ucSentOutData[QDT_UPGRADEFW_IMAGE_LENGTH_42BYTE-2] = QDT_Common_CheckSumCalc(&ucSentOutData[1],QDT_UPGRADEFW_IMAGE_LENGTH_38BYTE-3);
		//ucSentOutData[QDT_UPGRADEFW_IMAGE_LENGTH_42BYTE-1] = QDT_UPGRADEFW_END_BYTE;
		memcpy(ucSentOutData, g_ucDumpImageData, QDT_RF_UPGRADEFW_PACKAGE_LENGTH);
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData);

		break;	
	case _QDT_UPGRADESTATE_FINISHED_:	
		ucDestSerialNumber[0] = 0x00;
		ucDestSerialNumber[1] = 0x00;
		ucDestSerialNumber[2] = 0x00;
		ucDestSerialNumber[3] = 0x00;
		ucDestSerialNumber[4] = 0x00;
		QDT_Common_BuildRFCmdHeader(&SentOutRFHeader, RFCMD_AUDIODOCKING_UPGRADE_FINISH, ucDestSerialNumber);
		QDT_Common_SentRFCmd(&SentOutRFHeader, ucSentOutData);
		
		break;
	default:
		break;			
	}
	return QDT_SYS_NOERROR;	
}

RETURN_TYPE QDT_AD_ProcessUpgradeKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData)
{
	qdt_uint32 u32RemainedFWSize = 0;
	qdt_uint32 u32GotParentNextAddrIdx = 0;
	switch (ucCmd)
	{
	/* ======  RF Cmd =========================================== */
	case RFACK_AUDIODOCKING_ASKGOTOUPGRADESTATE:
		g_Data.ucUpgradeState = _QDT_UPGRADESTATE_PARENTREADY_;
		break;
	case RFACK_AUDIODOCKING_UPGRADE_FWINFO:
		g_Data.ucUpgradeState = _QDT_UPGRADESTATE_TRANSIMAGE_;
		g_UpdateImageInfo.u32AddrIdx=0;
		printf("==========> Upgrade Parent Image transfer start!!! \n");
		//break;
	case RFACK_AUDIODOCKING_UPGRADE_FWIMAGE:
		u32GotParentNextAddrIdx = 	 ucDumpRxData[QDT_RFPKT_DATA_START_IDX]<<24|ucDumpRxData[QDT_RFPKT_DATA_START_IDX+1]<<16
							   		|ucDumpRxData[QDT_RFPKT_DATA_START_IDX+2]<<8|ucDumpRxData[QDT_RFPKT_DATA_START_IDX+3];
		
		if (u32GotParentNextAddrIdx == g_UpdateImageInfo.u32AddrIdx || 
		    g_UpdateImageInfo.u32AddrIdx == 0) //make sure this ack is for request idx
		{	
			u32RemainedFWSize = (g_UpdateImageInfo.u32ImageSize - g_UpdateImageInfo.u32AddrIdx);
			if (u32RemainedFWSize ==0)
			{
				g_Data.ucUpgradeState = _QDT_UPGRADESTATE_FINISHED_;
				printf("==========> Upgrade Parent Image transfer finish!!! \n");
			}
			else if (u32RemainedFWSize >= QDT_RF_UPGRADEFW_IMAGE_DATA_LENGTH)
			{
				g_Data.ucUpgradeState = _QDT_UPGRADESTATE_TRANSIMAGE_;
					
				g_ucDumpImageData[0] = QDT_UPGRADEFW_START_BYTE;
				g_ucDumpImageData[1] = 0x00; //non-used
				g_ucDumpImageData[2] = g_UpdateImageInfo.u32AddrIdx>>24&0xFF;;
				g_ucDumpImageData[3] = g_UpdateImageInfo.u32AddrIdx>>16&0xFF;;
				g_ucDumpImageData[4] = g_UpdateImageInfo.u32AddrIdx>>8&0xFF;;
				g_ucDumpImageData[5] = g_UpdateImageInfo.u32AddrIdx&0xFF;;												
				g_ucDumpImageData[6] = QDT_RF_UPGRADEFW_IMAGE_DATA_LENGTH;
			
				QDT_Upgrade_ReadImage(&g_ucDumpImageData[7], g_UpdateImageInfo.u32AddrIdx, QDT_RF_UPGRADEFW_IMAGE_DATA_LENGTH);

				g_ucDumpImageData[QDT_RF_UPGRADEFW_PACKAGE_LENGTH-3] = QDT_Common_CheckSumCalc(&g_ucDumpImageData[1],19);
				g_ucDumpImageData[QDT_RF_UPGRADEFW_PACKAGE_LENGTH-2] = QDT_Common_CheckSumCalc(&g_ucDumpImageData[17],19);				
				g_ucDumpImageData[QDT_RF_UPGRADEFW_PACKAGE_LENGTH-1] = QDT_UPGRADEFW_END_BYTE;
				printf(".");
				//QDT_TRACE("==========> 1Updata Docking Image transfer..... g_UpdateImageInfo.u32AddrIdx = %ld!!! \n",g_UpdateImageInfo.u32AddrIdx);
				g_UpdateImageInfo.u32AddrIdx = g_UpdateImageInfo.u32AddrIdx+QDT_RF_UPGRADEFW_IMAGE_DATA_LENGTH;
			}
			else
			{
				g_Data.ucUpgradeState = _QDT_UPGRADESTATE_TRANSIMAGE_;
					
				g_ucDumpImageData[0] = QDT_UPGRADEFW_START_BYTE;
				g_ucDumpImageData[1] = 0x00; //non-used
				g_ucDumpImageData[2] = g_UpdateImageInfo.u32AddrIdx>>24&0xFF;;
				g_ucDumpImageData[3] = g_UpdateImageInfo.u32AddrIdx>>16&0xFF;;
				g_ucDumpImageData[4] = g_UpdateImageInfo.u32AddrIdx>>8&0xFF;;
				g_ucDumpImageData[5] = g_UpdateImageInfo.u32AddrIdx&0xFF;;												
				g_ucDumpImageData[6] = u32RemainedFWSize;
			
				QDT_Upgrade_ReadImage(&g_ucDumpImageData[7], g_UpdateImageInfo.u32AddrIdx, u32RemainedFWSize);
				
				g_ucDumpImageData[QDT_RF_UPGRADEFW_PACKAGE_LENGTH-3] = QDT_Common_CheckSumCalc(&g_ucDumpImageData[1],19);
				g_ucDumpImageData[QDT_RF_UPGRADEFW_PACKAGE_LENGTH-2] = QDT_Common_CheckSumCalc(&g_ucDumpImageData[17],19);				
				g_ucDumpImageData[QDT_RF_UPGRADEFW_PACKAGE_LENGTH-1] = QDT_UPGRADEFW_END_BYTE;
				g_UpdateImageInfo.u32AddrIdx = g_UpdateImageInfo.u32AddrIdx+u32RemainedFWSize;
				printf(".");
				//QDT_TRACE("==========> 2Updata Docking Image transfer..... g_UpdateImageInfo.u32AddrIdx = %ld!!! \n",g_UpdateImageInfo.u32AddrIdx);
			}

		}
		break;	
	case RFACK_AUDIODOCKING_UPGRADE_FINISH:
		//QDT_AD_SysPowerOnOff();
		QDT_Uart_SentUpgradeSuccess(1); //sent out uart cmd to notity Camera, parenet upgrade success.
		break;
	/* ======  User Key =========================================== */
	case _USER_KEY_AUDIODOCKING_KEY_POWER:
		QDT_AD_SysPowerOnOff();
		break;		
		
	case _USER_KEY_AUDIODOCKING_KEY_PAIRING:
		
#ifdef USER_SELECT_FREQ		
		if (g_UserSaveData.ucPairingFreqIdx == 0xFF)
		{
			//WARNING MESSAGE :: [Please press Vol+/Vol- to select PairingCH]
			g_UserSaveData.ucPairingFreqIdx = 0; //default RF frequence Index = 0, 915M
		}
#endif		

		QDT_Play_HumanVoice(HV_PairingMode,2);
		vRadio_StartRX(0);   //change to RX ParentUint channel
		QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_UNPAIRING;
#ifdef USER_SELECT_FREQ
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_PINGAD_;
#else
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_SEARCHING_;
#endif
		g_Data.bTwinkleRedLED = qdt_false;
		AUDIODOCKING_LED_PWR_RED(qdt_OFF);
		g_Data.bTwinkleGreenLED = qdt_true;
		break;
	/* ======  Long Press Key =========================================== */
	case _USER_KEY_AUDIODOCKING_KEY_LONGPRESS_VOLDOWN:	
		QDT_Play_HumanVoice(HV_FactoryReset, 2);
		g_Data.bFactoryResetFlag = qdt_true;
		QDT_AD_SysPowerOnOff(); //power off		
		QDT_flash_ClearUserData();
		break;
	default:
		break;			
	}
	return QDT_SYS_NOERROR;
}

#endif //BUILDTYPE_AUDIODOCKING_PROJECT
