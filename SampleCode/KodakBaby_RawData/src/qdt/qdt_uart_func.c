#ifdef BUILDTYPE_AUDIODOCKING_PROJECT

#include "bsp.h"
#include "Driver\DrvGPIO.h"
#include "Driver\DrvUART.h"
#include "Driver\DrvSYS.h"
#include "Driver\DrvFMC.h"
#include "FMC.h"

#include "radio.h"


#include "qdt_ram.h"
#include "qdt_utils.h"
#include "qdt_audiodocking_func.h"
#include "qdt_uart_func.h"
#include "qdt_common_func.h"
#include "qdt_flash_hal.h"
#include "qdt_sysState.h"
#include "qdt_audiodocking.h"
#include "qdt_upgrade_func.h"
#include "qdt_fifo.h"
#include "qdt_factory_func.h"

qdt_uint32 g_ucDumpImageIdx;
qdt_uint8 g_ucDumpImageData[256];

UARTRXBUFFER s_UartRxBuffer;
UARTRXBUFFER s_UartPacketReady;


static void __qdt_Uart_ResetUart(void);
static qdt_uint8 __qdt_Uart_CheckSumCalc(Byte* rpData, Byte ucLength);


static void __qdt_Uart_ResetUart(void)
{
	s_UartRxBuffer.ucDataLen = 0;
	s_UartRxBuffer.bDataReady = qdt_false;
}

static qdt_uint8 __qdt_Uart_CheckSumCalc(Byte* rpData,Byte ucLength)
{
    Byte i,ucData;

    ucData = 0;
    for(i=0;i<ucLength;i++)
        ucData += rpData[i];
        
    ucData = 0 - ucData;
    
    return ucData;
}


void QDT_Uart_initUart(void)
{
	__qdt_Uart_ResetUart();
}
void interrupt_callback_uart_function(void)
{
	uint32_t	u32delayno;
	qdt_uint8  ucChecksum;	
	u32delayno = 0;
	s_UartRxBuffer.cameraPacket.ucBuffer[s_UartRxBuffer.ucDataLen] = UART0->DATA;

	if(s_UartRxBuffer.ucDataLen == 0)
	{
		if (s_UartRxBuffer.cameraPacket.ucBuffer[s_UartRxBuffer.ucDataLen] != UART_PACKET_START_BYTE)
			return;
	}
	
	s_UartRxBuffer.ucDataLen++;
	do
	{
		while (UART0->FSR.RX_EMPTY ==1) 				  /* Check RX empty => failed */
		{
			u32delayno++;
			if ( u32delayno >= 0x10000)
			{		
			    __qdt_Uart_ResetUart();
				DrvUART_ClearInt(UART_PORT0, DRVUART_RDAINT);				
				return;
			}

		}
		s_UartRxBuffer.cameraPacket.ucBuffer[s_UartRxBuffer.ucDataLen] = UART0->DATA;
		s_UartRxBuffer.ucDataLen++;
		u32delayno = 0;
		if (s_UartRxBuffer.ucDataLen == UART_PACKET_LENGTH_8BYTE || s_UartRxBuffer.ucDataLen == UART_PACKET_LENGTH_137BYTE)
		{

			if (s_UartRxBuffer.cameraPacket.ucBuffer[s_UartRxBuffer.ucDataLen-1] == UART_PACKET_END_BYTE)
			{
			ucChecksum = __qdt_Uart_CheckSumCalc(&(s_UartRxBuffer.cameraPacket.ucBuffer[1]), s_UartRxBuffer.ucDataLen-3);
			if (ucChecksum == s_UartRxBuffer.cameraPacket.ucBuffer[s_UartRxBuffer.ucDataLen-2])	
			{
				s_UartRxBuffer.bDataReady = qdt_true;
				s_UartPacketReady = s_UartRxBuffer;
				//DrvUART_Write(UART_PORT0, s_UartRxBuffer.cameraPacket.ucBuffer, s_UartRxBuffer.ucDataLen);
				DrvUART_ClearInt(UART_PORT0, DRVUART_RDAINT);	
				__qdt_Uart_ResetUart();
				break;
			}
		}

			if (s_UartRxBuffer.ucDataLen == UART_PACKET_LENGTH_137BYTE) // The biggest pakage size, still not correct
			{
				DrvUART_ClearInt(UART_PORT0, DRVUART_RDAINT);	
				__qdt_Uart_ResetUart();				
			}
		}
	}while (1);
}

qdt_bool QDT_Uart_GetUARTCmd(CAMERAPACKETDATA * rpCameraPacket, qdt_uint8 * pucPacketLen)
{
    if(s_UartPacketReady.bDataReady)
    {    
        s_UartPacketReady.bDataReady = 0;
        *rpCameraPacket = s_UartPacketReady.cameraPacket;
        *pucPacketLen = s_UartPacketReady.ucDataLen; 
		return qdt_true;
    }

	return qdt_false;
}
RETURN_TYPE QDT_Uart_SentOutNightLightStatus(qdt_bool bOnOff)
{
	qdt_uint8 ucRetCameraData[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	ucRetCameraData[0] = UART_PACKET_START_BYTE;
	ucRetCameraData[1] = UARTCMD_GET_NIGHTLIGHT_ONOFF;
	ucRetCameraData[2] = 0x00;
	ucRetCameraData[3] = bOnOff;
	ucRetCameraData[4] = 0x00;
	ucRetCameraData[5] = 0x00;
	ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5); 
	ucRetCameraData[7] = UART_PACKET_END_BYTE;
	DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);		
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Uart_SentOutAudioAlertTriggerStart(void)
{
	qdt_uint8 ucRetCameraData[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	ucRetCameraData[0] = UART_PACKET_START_BYTE;
	ucRetCameraData[1] = UARTCMD_AUDIOALERT_TRIGGER_START;
	ucRetCameraData[2] = 0x00;
	ucRetCameraData[3] = 0x00;
	ucRetCameraData[4] = 0x00;
	ucRetCameraData[5] = 0x00;
	ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5); 
	ucRetCameraData[7] = UART_PACKET_END_BYTE;
	DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);		
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Uart_SentOutAudioAlertTriggerEnd(void)
{
	qdt_uint8 ucRetCameraData[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	ucRetCameraData[0] = UART_PACKET_START_BYTE;
	ucRetCameraData[1] = UARTCMD_AUDIOALERT_TRIGGER_END;
	ucRetCameraData[2] = 0x00;
	ucRetCameraData[3] = 0x00;
	ucRetCameraData[4] = 0x00;
	ucRetCameraData[5] = 0x00;
	ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
	ucRetCameraData[7] = UART_PACKET_END_BYTE;
	DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);		
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Uart_SentUpgradeSuccess(qdt_uint8 ucUpgradeType)
{
	qdt_uint8 ucRetCameraData[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	ucRetCameraData[0] = UART_PACKET_START_BYTE;
	ucRetCameraData[1] = UARTACK_SENT_UPGRADE_IMAGE_FINISH;
	ucRetCameraData[2] = ucUpgradeType;
	ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
	ucRetCameraData[7] = UART_PACKET_END_BYTE;
	DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);		
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Uart_SentOutCmd(qdt_uint8 ucCmd, qdt_uint8 *ucData)
{
	qdt_uint8 ucRetCameraData[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	ucRetCameraData[0] = UART_PACKET_START_BYTE;
	ucRetCameraData[1] = ucCmd;
	ucRetCameraData[2] = ucData[0];
	ucRetCameraData[3] = ucData[1];
	ucRetCameraData[4] = ucData[2];
	ucRetCameraData[5] = ucData[3];
	ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5); 
	ucRetCameraData[7] = UART_PACKET_END_BYTE;
	DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);		
	return QDT_SYS_NOERROR;
}

qdt_bool QDT_Uart_ProcessUARTCmd(CAMERAPACKETDATA * rpCameraPacket, qdt_uint8 ucPacketLen)
{
	qdt_uint8 ucCameraCmd;
	qdt_uint8 ucCameraData[4];
	qdt_uint8 ucRetCameraData[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	qdt_uint16 u16Param1 = 0;

	ucCameraCmd = rpCameraPacket->ucBuffer[1];
	ucCameraData[0] = rpCameraPacket->ucBuffer[2];
	ucCameraData[1] = rpCameraPacket->ucBuffer[3];
	ucCameraData[2] = rpCameraPacket->ucBuffer[4];	
	ucCameraData[3] = rpCameraPacket->ucBuffer[5];	

	QDT_DEBUG("Got ucCameraCmd = 0x%x \n",ucCameraCmd);	

	switch(ucCameraCmd)
	{
	case UARTCMD_CAMERACONNECTED:
		ucRetCameraData[0] = UART_PACKET_START_BYTE;
		ucRetCameraData[1] = UARTACK_CAMERACONNECTED;
		ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
		ucRetCameraData[7] = UART_PACKET_END_BYTE;
		DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);
		break;
	case UARTCMD_SET_NIGHTLIGHT_ONOFF:
		u16Param1 = ucCameraData[0]<<8|ucCameraData[1];
		QDT_AD_SetNightLightOnOFF(u16Param1);
		break;
	case UARTCMD_GET_NIGHTLIGHT_ONOFF:
		ucRetCameraData[0] = UART_PACKET_START_BYTE;
		ucRetCameraData[1] = UARTCMD_GET_NIGHTLIGHT_ONOFF;
		ucRetCameraData[2] = 0x00;
		ucRetCameraData[3] = g_UserSaveData.ucNightLightEnbled;
		ucRetCameraData[4] = 0x00;
		ucRetCameraData[5] = 0x00;		
		ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
		ucRetCameraData[7] = UART_PACKET_END_BYTE;
		DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);		
		break;		
		
	case UARTCMD_SET_NIGHTLIGHT_LEVEL:
		u16Param1 = ucCameraData[0]<<8|ucCameraData[1];
		QDT_AD_SetNightLightLevel(u16Param1);
		break;		
	case UARTCMD_GET_NIGHTLIGHT_LEVEL:
		ucRetCameraData[0] = UART_PACKET_START_BYTE;
		ucRetCameraData[1] = UARTCMD_GET_NIGHTLIGHT_ONOFF;
		ucRetCameraData[2] = 0x00;
		ucRetCameraData[3] = g_UserSaveData.ucBrightLevel;
		ucRetCameraData[4] = 0x00;
		ucRetCameraData[5] = 0x00;		
		ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
		ucRetCameraData[7] = UART_PACKET_END_BYTE;
		DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);				
		break;		
		
	case UARTCMD_SET_NIGHTLIGHT_TIMER:
		u16Param1 = ucCameraData[0]<<8|ucCameraData[1];
		QDT_AD_SetNightLightTimer(u16Param1);
		break;		
	case UARTCMD_GET_NIGHTLIGHT_TIMER:	
		break;		

	case UARTCMD_GET_SENSORSTATUS:
		g_Data.bNeedToBoardCastRFCmd = qdt_true;
		g_Data.ucBoardCastRFCmd = RFACK_SERSORDATA_UPDATESTATUS;
			
		break;
	case UARTACK_GET_SENSORSTATUS:
		break;
		
	case UARTCMD_SET_AUDIOALERT_LEVEL:
		u16Param1 = ucCameraData[0]<<8|ucCameraData[1];
		QDT_AD_SetAudioAlertLevel(u16Param1);
		break;		
		
	case UARTCMD_GET_AUDIOALERT_LEVEL:
		ucRetCameraData[0] = UART_PACKET_START_BYTE;
		ucRetCameraData[1] = UARTCMD_GET_NIGHTLIGHT_ONOFF;
		ucRetCameraData[2] = 0x00;
		ucRetCameraData[3] = g_UserSaveData.ucAudioAlertLevel;
		ucRetCameraData[4] = 0x00;
		ucRetCameraData[5] = 0x00;		
		ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
		ucRetCameraData[7] = UART_PACKET_END_BYTE;
		DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);				
		break;		

	case UARTCMD_SET_AUDIOALERT_ONOFF:
		u16Param1 = ucCameraData[0]<<8|ucCameraData[1];
		QDT_AD_SetAudioAlertOnOFF(u16Param1);
		break;
		
	case UARTCMD_GET_AUDIOALERT_ONOFF:
		ucRetCameraData[0] = UART_PACKET_START_BYTE;
		ucRetCameraData[1] = UARTCMD_GET_AUDIOALERT_ONOFF;
		ucRetCameraData[2] = 0x00;
		ucRetCameraData[3] = g_UserSaveData.ucAudioAlertEnable;
		ucRetCameraData[4] = 0x00;
		ucRetCameraData[5] = 0x00;		
		ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
		ucRetCameraData[7] = UART_PACKET_END_BYTE;
		DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);
		break;

//== updata image case =====================================================================		
	case UARTCMD_SENT_UPGRADE_IMAGE_START:
		{	
		qdt_uint16 idx = 0;
		qdt_uint8 ucEarseMaxIdx = 0;
		g_UpdateImageInfo.bStartUpdate = qdt_true;
		
		g_UpdateImageInfo.u32AddrIdx = 0;
		g_UpdateImageInfo.ucImageVersion[0] = rpCameraPacket->ucBuffer[2];
		g_UpdateImageInfo.ucImageVersion[1] = rpCameraPacket->ucBuffer[3];
		g_UpdateImageInfo.ucImageVersion[2] = rpCameraPacket->ucBuffer[4];
		g_UpdateImageInfo.ucImageVersion[3] = rpCameraPacket->ucBuffer[5];
		g_UpdateImageInfo.u32ImageSize = rpCameraPacket->ucBuffer[6]<<24|rpCameraPacket->ucBuffer[7]<<16|rpCameraPacket->ucBuffer[8]<<8|rpCameraPacket->ucBuffer[9];
		g_UpdateImageInfo.ucImageType = rpCameraPacket->ucBuffer[10]; 
		
		AUDIODOCKING_RF_446x_PER_CTRL(qdt_OFF);
		AUDIODOCKING_RF_446x(qdt_OFF);			//turn off  RF-4461

		if (g_UpdateImageInfo.ucImageType == _QDT_UPGRADEIMAGETYPE_AUDIODOCKING_)
		{
			g_UpdateImageInfo.u32FlashAddr = FLASH_IMAGE_PARTITION1_ADDR;
			ucEarseMaxIdx = 48; //192K
		}
		else if (g_UpdateImageInfo.ucImageType == _QDT_UPGRADEIMAGETYPE_PARENTUNIT_)
		{
			g_UpdateImageInfo.u32FlashAddr = FLASH_IMAGE_PARENTTEMP_ADDR;
			ucEarseMaxIdx = 48; //192K
		}
		else if  (g_UpdateImageInfo.ucImageType == _QDT_UPGRADEIMAGETYPE_AKM_CRAM_)
		{
			g_UpdateImageInfo.u32FlashAddr = FLASH_AKM_CRAM_ADDR;
			ucEarseMaxIdx = 8; //32K
		}
		else if  (g_UpdateImageInfo.ucImageType == _QDT_UPGRADEIMAGETYPE_AKM_CRAM_)
		{
			g_UpdateImageInfo.u32FlashAddr = FLASH_AKM_PRAM_ADDR;
			ucEarseMaxIdx = 8; //32K
		}
		else
		{
			g_UpdateImageInfo.u32FlashAddr = FLASH_VOICE_DATA_ADDR;
			ucEarseMaxIdx = 48; //192K
		}
		
		for(idx = 0; idx < ucEarseMaxIdx ; idx++)
			QDT_flash_EarseSector(g_UpdateImageInfo.u32FlashAddr+idx*4096);  //Earse Flash, wait for writing image.
			
		QDT_Common_DelayTime(500);
		if (g_Data.ucSysState != SYSSTATE_STANDBY)
			QDT_AD_SysPowerOnOff();

		g_Data.ucPreSysState = g_Data.ucSysState;
		g_Data.ucSysState = SYSSTATE_UPGRADE;
		
		g_Data.bTwinkleRedLED = qdt_true;
		g_Data.bTwinkleGreenLED = qdt_true;

		ucRetCameraData[0] = UART_PACKET_START_BYTE;
		ucRetCameraData[1] = UARTACK_SENT_UPGRADE_IMAGE_START;
		ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
		ucRetCameraData[7] = UART_PACKET_END_BYTE;
		DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);		
		}
		break;
	case UARTCMD_SENT_UPGRADE_IMAGE:
		if (g_UpdateImageInfo.bStartUpdate)
		{
			//qdt_uint8 ucTransIdx = 0;
			qdt_uint8 ucDataLen = 0;
			//qdt_uint32 u32WriteToFlashAddr = 0;
			qdt_uint16 i = 0;
			//ucTransIdx = rpCameraPacket->ucBuffer[2];
			ucDataLen = rpCameraPacket->ucBuffer[6];
			if (1)
			{
				for (i=0; i<ucDataLen;  i++)
				{
					g_ucDumpImageData[i] = rpCameraPacket->ucBuffer[i+7];
				}
				
				//u32WriteToFlashAddr = g_UpdateImageInfo.u32FlashAddr+g_UpdateImageInfo.u32AddrIdx;
				//QDT_flash_WriteImage(u32WriteToFlashAddr, ucDataLen, &g_ucDumpImageData[0]);
				QDT_Upgrade_WriteImage((qdt_uint8)_QDT_UPGRADETRANSFERTYPE_UART_, g_UpdateImageInfo.ucImageType, g_UpdateImageInfo.u32AddrIdx, ucDataLen, &g_ucDumpImageData[0]);
				g_UpdateImageInfo.u32AddrIdx = g_UpdateImageInfo.u32AddrIdx+ucDataLen;
		
				ucRetCameraData[0] = UART_PACKET_START_BYTE;
				ucRetCameraData[1] = UARTACK_SENT_UPGRADE_IMAGE_OK;
				ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
				ucRetCameraData[7] = UART_PACKET_END_BYTE;
				DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);			
			}
			else
			{
				ucRetCameraData[0] = UART_PACKET_START_BYTE;
				ucRetCameraData[1] = UARTACK_SENT_UPGRADE_IMAGE_ERROR;
				ucRetCameraData[2] = UARTMSG_IMAGE_IDX_ERROR;
				ucRetCameraData[3] = UARTMSG_IMAGE_IDX_ERROR;
				ucRetCameraData[4] = UARTMSG_IMAGE_IDX_ERROR;
				ucRetCameraData[5] = UARTMSG_IMAGE_IDX_ERROR;
				ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
				ucRetCameraData[7] = UART_PACKET_END_BYTE;
				DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);			
			
			}

		}
		break;
	case UARTCMD_SENT_UPGRADE_IMAGE_FINISH:
		//if (g_UpdateImageInfo.bStartUpdate)
		{		
			g_UpdateImageInfo.bStartUpdate = qdt_false;

			//QDT_flash_ReadBytes(g_UpdateImageInfo.u32FlashAddr, FLASH_PAGE_SIZE, &g_ucDumpImageData[0]);
			//QDT_Commom_ReStartRF();


			if (g_UpdateImageInfo.ucImageType == _QDT_UPGRADEIMAGETYPE_AUDIODOCKING_)
			{
				ucRetCameraData[0] = UART_PACKET_START_BYTE;
				ucRetCameraData[1] = UARTACK_SENT_UPGRADE_IMAGE_FINISH;
				ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
				ucRetCameraData[7] = UART_PACKET_END_BYTE;
				DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);	

		 		UNLOCKREG();				
		    	DrvFMC_EnableISP(1);
		  		UpdateConfig(0xFFFFFF7F, 0xFFFFFFFF);
				QDT_Common_DelayTime(3000);
		  		//AUDIODOCKING_CAMERA_PWR(qdt_OFF); //power down CAMERA-A500
		  		g_Data.bTwinkleRedLED = qdt_true;		  
		  		//DrvSYS_ResetChip();
			}
			else if (g_UpdateImageInfo.ucImageType == _QDT_UPGRADEIMAGETYPE_PARENTUNIT_)
			{
			
				AUDIODOCKING_RF_446x_PER_CTRL(qdt_ON);
				AUDIODOCKING_RF_446x(qdt_ON);
				vRadio_Init();												/* Initial RF-446x */
				
				if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
				{
					QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
					#ifdef USER_SELECT_FREQ		
					//WARNING MESSAGE :: [Please press Vol+/Vol- to select Freq.]
					QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	 //change RF Freq
					#endif			
				}
				else
				{
					QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
					#ifdef USER_SELECT_FREQ		
					QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
					#endif			
				}			
				g_Data.bRFTransmitFinished = qdt_true;
		       	g_Data.bRFReceiveFinished = qdt_true;		
				g_Data.ucPreSysState = g_Data.ucSysState;
				g_Data.ucSysState = SYSSTATE_UPGRADE;
				
				g_Data.ucUpgradeState = _QDT_UPGRADESTATE_SENTOUTREQUST_;
				g_Data.bTwinkleRedLED = qdt_true;
				g_Data.bTwinkleGreenLED = qdt_true;	
						
			}
			else if  (g_UpdateImageInfo.ucImageType == _QDT_UPGRADEIMAGETYPE_AKM_CRAM_ ||
				      g_UpdateImageInfo.ucImageType == _QDT_UPGRADEIMAGETYPE_AKM_PRAM_ )
			{
				ucRetCameraData[0] = UART_PACKET_START_BYTE;
				ucRetCameraData[1] = UARTACK_SENT_UPGRADE_IMAGE_FINISH;
				ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
				ucRetCameraData[7] = UART_PACKET_END_BYTE;
				DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);	

				//DrvSYS_ResetChip();
			}
			else if (g_UpdateImageInfo.ucImageType == _QDT_UPGRADEIMAGETYPE_VOICEDATA_)
			{
				AUDIODOCKING_RF_446x_PER_CTRL(qdt_ON);
				AUDIODOCKING_RF_446x(qdt_ON);
				vRadio_Init();												/* Initial RF-446x */
				
				if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
				{
					QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
					#ifdef USER_SELECT_FREQ		
					//WARNING MESSAGE :: [Please press Vol+/Vol- to select Freq.]
					QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	 //change RF Freq
					#endif			
				}
				else
				{
					QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
					#ifdef USER_SELECT_FREQ		
					QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
					#endif			
				}			
				g_Data.bRFTransmitFinished = qdt_true;
		       	g_Data.bRFReceiveFinished = qdt_true;		
				g_Data.ucPreSysState = g_Data.ucSysState;
				g_Data.ucSysState = SYSSTATE_UPGRADE;
				
				g_Data.ucUpgradeState = _QDT_UPGRADESTATE_SENTOUTREQUST_;
				g_Data.bTwinkleRedLED = qdt_true;
				g_Data.bTwinkleGreenLED = qdt_true;	
			}
			

		}
		break;		
		case UARTCMD_SENT_UPGRADE_SUCCESS_RESTARTALL:
			AUDIODOCKING_CAMERA_PWR(qdt_OFF); //power down CAMERA-A500
			g_Data.bTwinkleRedLED = qdt_true;		  
			DrvSYS_ResetChip();
			break;
//------------------------------------------------------------------------------------------------		
//== updata voice data  case =====================================================================		
		case UARTCMD_SENT_UPGRADE_VOICEDATA_START:
			{	
			qdt_uint16 idx = 0;
			g_UpdateImageInfo.bStartUpdate = qdt_true;
			
			if (g_Data.ucSysState != SYSSTATE_STANDBY)
				QDT_AD_SysPowerOnOff();


			g_UpdateImageInfo.u32AddrIdx= rpCameraPacket->ucBuffer[10]<<24|rpCameraPacket->ucBuffer[11]<<16|rpCameraPacket->ucBuffer[12]<<8|rpCameraPacket->ucBuffer[13];
			
			for(idx = 0; idx < 48 ; idx++)
				QDT_flash_EarseSector(FLASH_VOICE_DATA_ADDR+idx*4096);  //Earse Flash, wait for writing image.

				
			g_UpdateImageInfo.bStartUpdate = qdt_true;
			g_UpdateImageInfo.u32AddrIdx = 0;
			g_UpdateImageInfo.ucImageVersion[0] = rpCameraPacket->ucBuffer[2];
			g_UpdateImageInfo.ucImageVersion[1] = rpCameraPacket->ucBuffer[3];
			g_UpdateImageInfo.ucImageVersion[2] = rpCameraPacket->ucBuffer[4];
			g_UpdateImageInfo.ucImageVersion[3] = rpCameraPacket->ucBuffer[5];			
			g_UpdateImageInfo.u32ImageSize = rpCameraPacket->ucBuffer[6]<<24|rpCameraPacket->ucBuffer[7]<<16|rpCameraPacket->ucBuffer[8]<<8|rpCameraPacket->ucBuffer[9];
			
			ucRetCameraData[0] = UART_PACKET_START_BYTE;
			ucRetCameraData[1] = UARTACK_SENT_UPGRADE_VOICEDATA_START;
			ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
			ucRetCameraData[7] = UART_PACKET_END_BYTE;
			DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);		
			}
			break;
		case UARTCMD_SENT_UPGRADE_VOICEDATA:
			if (g_UpdateImageInfo.bStartUpdate)
			{
				//qdt_uint8 ucTransIdx = 0;
				qdt_uint8 ucDataLen = 0;
				qdt_uint32 u32WriteToFlashAddr = 0;
				qdt_uint16 i = 0;
				//ucTransIdx = rpCameraPacket->ucBuffer[2];
				ucDataLen = rpCameraPacket->ucBuffer[3];
				if (1)
				{
					for (i=0; i<ucDataLen;	i++)
					{
						g_ucDumpImageData[i] = rpCameraPacket->ucBuffer[i+4];
					}
					
					u32WriteToFlashAddr =g_UpdateImageInfo.u32AddrIdx+g_UpdateImageInfo.u32AddrIdx;
					QDT_flash_WriteImage(u32WriteToFlashAddr, ucDataLen, &g_ucDumpImageData[0]);
					for (i=0; i<256;  i++)
					{
						g_ucDumpImageData[i] = 0;
					}					
					QDT_flash_ReadBytes(g_UpdateImageInfo.u32AddrIdx, FLASH_PAGE_SIZE, &g_ucDumpImageData[0]);

					g_UpdateImageInfo.u32AddrIdx = g_UpdateImageInfo.u32AddrIdx+ucDataLen;
	
					ucRetCameraData[0] = UART_PACKET_START_BYTE;
					ucRetCameraData[1] = UARTACK_SENT_UPGRADE_VOICEDATA_OK;
					ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
					ucRetCameraData[7] = UART_PACKET_END_BYTE;
					DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);			
				}
				else
				{
					ucRetCameraData[0] = UART_PACKET_START_BYTE;
					ucRetCameraData[1] = UARTACK_SENT_UPGRADE_VOICEDATA_ERROR;
					ucRetCameraData[2] = UARTMSG_IMAGE_IDX_ERROR;
					ucRetCameraData[3] = UARTMSG_IMAGE_IDX_ERROR;
					ucRetCameraData[4] = UARTMSG_IMAGE_IDX_ERROR;
					ucRetCameraData[5] = UARTMSG_IMAGE_IDX_ERROR;
					ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
					ucRetCameraData[7] = UART_PACKET_END_BYTE;
					DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);			
				
				}
	
			}
			break;
		case UARTCMD_SENT_UPGRADE_VOICEDATA_FINISH:
			if (g_UpdateImageInfo.bStartUpdate)
			{		
				g_UpdateImageInfo.bStartUpdate = qdt_false;
	
				QDT_flash_ReadBytes(g_UpdateImageInfo.u32AddrIdx, FLASH_PAGE_SIZE, &g_ucDumpImageData[0]);
				//QDT_Commom_ReStartRF();
				ucRetCameraData[0] = UART_PACKET_START_BYTE;
				ucRetCameraData[1] = UARTACK_SENT_UPGRADE_VOICEDATA_FINISH;
				ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
				ucRetCameraData[7] = UART_PACKET_END_BYTE;
				DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);	
				
			}
			break;		
//== FactoryMode  case ====================================================================
	case UARTCMD_SENT_FACTORY_TEST_UART:
		ucRetCameraData[0] = UART_PACKET_START_BYTE;
		ucRetCameraData[1] = UARTACK_SENT_FACTORY_TEST_UART;
		ucRetCameraData[6] = __qdt_Uart_CheckSumCalc(&(ucRetCameraData[1]), 5);
		ucRetCameraData[7] = UART_PACKET_END_BYTE;
		DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);	
	case UARTCMD_SENT_FACTORY_BURNIN_SN:
		QDT_Factory_BurnInSN(&rpCameraPacket->ucBuffer[2]);
		break;
//------------------------------------------------------------------------------------------------		
		
	case UARTCMD_TEST0:
		ucCameraCmd = ucCameraCmd;
		//DrvUART_Write(UART_PORT0, rpCameraPacket->ucBuffer, ucPacketLen);
		break;
	case UARTCMD_TEST1:
		ucCameraCmd = ucCameraCmd;
		//DrvUART_Write(UART_PORT0, rpCameraPacket->ucBuffer, ucPacketLen);
		break;
	default:
		ucRetCameraData[0] = UART_PACKET_START_BYTE;
		ucRetCameraData[1] = rpCameraPacket->ucBuffer[1];
		ucRetCameraData[2] = rpCameraPacket->ucBuffer[2];
		ucRetCameraData[3] = rpCameraPacket->ucBuffer[3];
		ucRetCameraData[4] = rpCameraPacket->ucBuffer[4];
		ucRetCameraData[5] = rpCameraPacket->ucBuffer[5];
		ucRetCameraData[6] = rpCameraPacket->ucBuffer[6];		
		ucRetCameraData[7] = UART_PACKET_END_BYTE;
		DrvUART_Write(UART_PORT0, ucRetCameraData, UART_PACKET_LENGTH_8BYTE);		
		break;
	}
	
	return qdt_true;

}

#endif //BUILDTYPE_AUDIODOCKING_PROJECT



