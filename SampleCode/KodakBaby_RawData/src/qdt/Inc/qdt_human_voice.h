#ifndef __QDT_HUMAN_VOICE_H
#define __QDT_HUMAN_VOICE_H

//=========================================================
// The below is based on AudioData.s

//----------------------------------------------------
// Define Audio sound macro to the index number of VP files

#define HV_POWER		0		   //Item0: first sound
#define HV_ON			1		   //Item1: second sound
#define HV_OFF			2		   //Item2: third sound
#define HV_PAIRING		3		   //Item3: fourth sound
#define HV_MODE			4		   //Item4: fifth sound
#define HV_SUCCESS		5
#define HV_FAIL			6
#define HV_UNPAIRING	7
#define HV_FACTORY		8
#define HV_RESET		9
#define HV_BABYCRY		10
#define HV_NULL			11
#define HV_HUMIDITY       12
#define HV_TEMPERATURE 13
#define HV_ONE			14
#define HV_TWO			15
#define HV_THREE		16
#define HV_FOUR			17
#define HV_FIVE			18
#define HV_SIX			19
#define HV_SEVEN		20
#define HV_EIGHT		21
#define HV_NINE			22
#define HV_TEN			23
#define HV_ELEVEN		24
#define HV_TWELVE		25
#define HV_THIRTEEN		26
#define HV_FOURTEEN		27
#define HV_FIFTEEN		28
#define HV_SIXTEEN		29
#define HV_SEVENTEEN	30
#define HV_EIGHTEEN		31
#define HV_NINETEEN		32
#define HV_TWENTY		33
#define HV_THIRTY		34
#define HV_FORTY		35
#define HV_FIFTY			36
#define HV_SIXTY		37
#define HV_SEVENTY		38
#define HV_EIGHTY		39
#define HV_NINETY		40
#define HV_ZERO			41
#define HV_PERCENT		42
#define HV_DEGREESCELSIUS 43
#define HV_BEEP			 44
#if 1//#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
//#define HV_PAIRFAILTRYAGAIN  45
#define HV_PLSVOLSELFREQ  45
#define HV_SELECTFREQCH  46
#endif

//----------------------------------------------------
// Use VPE to generate the *.vp file for each wav file
// VP file has first 4 bytes for header, program needs to skip them
// Use AudioData.s to put each sound stream into project

extern uint32_t  AudioDataBegin;

void PlayG722Stream(uint16_t AudIndex);
void QDT_Play_HumanVoice(qdt_uint8 *pSrc, qdt_uint8 uCount);
void QDT_Play_HV_SensorTemperature(qdt_uint8 ucSensorData);
void QDT_Play_HV_SensorHumidity(qdt_uint8 ucSensorData);
#if 1//#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
void QDT_Play_HV_SelectFrequency(qdt_uint8 ucChannelData);
#endif
void QDT_Play_HV_FirmwareVersion(const qdt_uint8 *ucData, qdt_uint8 ucLength);


#endif
