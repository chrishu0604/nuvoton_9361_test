#ifndef __QDT_COMMON_H__
#define __QDT_COMMON_H__ 1

/***********************************************************************/
/* Module Type Define                                                                                         */
/***********************************************************************/

#define USE_SW_VOLUMECONTROL

#define UNI_PAIRING_SYNCWORD			0x01
#define PAIRING_SYNCWORD		0x02

#define UNI_PAIRING_SYNCWORD_BYTE0		0xC6	//C
#define UNI_PAIRING_SYNCWORD_BYTE1		0x16	//H
#define UNI_PAIRING_SYNCWORD_BYTE2		0x86	//A
#define UNI_PAIRING_SYNCWORD_BYTE3		0x26	//D

#define QDTUID_TYPE_AUDIO		0x81
#define QDTUID_TYPE_RFCMD		0x82
#define QDTUID_TYPE_UPGRADEFW	0x83
#define QDTUID_TYPE_FACTORY		0x84

#define QDT_UPGRADEFW_START_BYTE			0x82
#define QDT_UPGRADEFW_END_BYTE				0x96
#define QDT_UPGRADEFW_IMAGE_LENGTH_42BYTE	42

#define QDT_RF_UPGRADEFW_IMAGE_DATA_LENGTH	0x20
// |MG-S|CMD|AddrIdx1|AddrIdx2|AddrIdx3|AddrIdx4|DataLength|~ImageData[xx]~|CHSUM|MG0-E|
#define QDT_RF_UPGRADEFW_PACKAGE_LENGTH		(QDT_RF_UPGRADEFW_IMAGE_DATA_LENGTH+10)
#define QDT_UART_UPGRADEFW_IMAGE_DATA_LENGTH	0x80




#define RF_FREQ_TABLE_IDX_MAX				0x07
#define RF_FREQ_FACTORY_IDX					0x08

#define RF_BOARDCAST_CH0               		0
#define RF_PARENTUNIT_CH0					0
#define RF_PARENTUNIT_CH1					1

#define RF_SENSORUNIT_CH1					1
#define RF_SENSORUNIT_CH2					2
#define RF_SENSORUNIT_CH3					3
#define RF_SENSORUNIT_CH4					4
#define RF_SENSORUNIT_CH5					5
#define RF_SENSORUNIT_CH6					6
#define RF_SENSORUNIT_CH7					7
#define RF_SENSORUNIT_CH8					8
#define RF_SENSORUNIT_CH9					9
#define SUPPORT_SERSORUNIT_MAXNUM        	8


#define QDT_RFPKT_HEAD_LENGTH  			  			16
#define QDT_RFPKT_QDTUIDTYPE_START_IDX   			0
#define QDT_RFPKT_SRCSN_START_IDX   				1
#define QDT_RFPKT_RFSUBCMD_START_IDX   				6
#define QDT_RFPKT_DESTSN_START_IDX   		7
//#define QDT_RFPKT_HEADER_START_IDX  					10
#define QDT_RFPKT_DATALENGTH_START_IDX   			12
#define QDT_RFPKT_RESERVED_START_IDX   			14
#define QDT_RFPKT_DATA_START_IDX     				16

#define RADIO_TX_MODE
//#define RADIO_RX_MODE

//#define RADIO_TXRX_MODE

#define RF_SENTOUT_DELAY_TIME 			0x8FF
#define WAITING_ACK_COUNT 				0x40
#define PRESS_BUTTON_THRESHOLD 			0xFF0
#define BUFFER_SAMPLECOUNT 				320	
#define COMPBUFSIZE     20//40   //According to S7BITRATE


/***********************************************************************/
/* Receive Package Type                                                                                             */
/***********************************************************************/
typedef enum {
	QDT_RFPKT_AUDIO = 0x01,
	QDT_RFPKT_RFCOMMAND = 0x02,
} QdtRFPackageType_e;

typedef enum {
	RFCMD_NON_CMD							=0x00,
	RFACK_NON_ACK							=0x00,

	/* 0x00~0x2F ParentUint RF SubCmd */
	RFACK_PARENTUINT_IAMHERE				=0x01,	
	RFACK_PARENTUINT_GOT_SYNCWORD			=0x02,	
	RFACK_PARENTUINT_USING_SYNCWORD 		=0x03,
	
	
	
	RFCMD_PARENTCOMMAND_ONLINE				=0x04,	
	RFCMD_PARENTCOMMAND_CALLING 			=0x05,
	RFCMD_PARENTCOMMAND_HANDUP				=0x06,
	RFCMD_PARENTCOMMAND_HANDUP_ACK			=0x07,
	RFCMD_PARENTCOMMAND_UNPAIRING			=0x08,
	RFCMD_PARENTCOMMAND_PUSHTOTALKING		=0x09,

	RFCMD_PARENTUINT_PING_AD				=0x10,	
	RFACK_PARENTUINT_PING_AD				=0x11,

	/* 0x30~0x5F AudioDocking RF SubCmd */
	RFCMD_AUDIODOCKING_SEARCHING_DEVICE 	=0x20,
	RFCMD_AUDIODOCKING_TX_SYNCWORD			=0x21,
	RFCMD_AUDIODOCKING_TESTING_SYNCWORD 	=0x22,
	
	RFCMD_AUDIODOCKING_ONLINE				=0x23,
	RFACK_AUDIODOCKING_ANSWERCALLING		=0x24,
	RFACK_AUDIODOCKING_HANDUP				=0x25,
	RFACK_AUDIODOCKING_UNPAIRED				=0x26,

	RFCMD_AUDIODOCKING_QUERYSENSORSTATUE	=0x30,
	RFACK_AUDIODOCKING_QUERYSENSORSTATUE	=0x35,

	RFCMD_AUDIODOCKING_AUDIOALERT	=0x31,
	RFACK_PARENTUINT_AUDIOALERT	=0x32,

	RFCMD_AUDIODOCKING_SENSORALERT_MOTION 	=0x33,
	RFACK_AUDIODOCKING_SENSORALERT_MOTION    =0x34,


	RFCMD_AUDIODOCKING_ASKGOTOUPGRADESTATE	= 0x40,
	RFACK_AUDIODOCKING_ASKGOTOUPGRADESTATE	= 0x41,
	RFCMD_AUDIODOCKING_UPGRADE_FWINFO		= 0x42,
	RFACK_AUDIODOCKING_UPGRADE_FWINFO		= 0x43,
	RFCMD_AUDIODOCKING_UPGRADE_FWIMAGE		= 0x44,
	RFACK_AUDIODOCKING_UPGRADE_FWIMAGE		= 0x45,
	RFCMD_AUDIODOCKING_UPGRADE_FINISH		= 0x46,
	RFACK_AUDIODOCKING_UPGRADE_FINISH		= 0x47,
	
    /* 0x50~0x5F Factory Test RF SubCmd */
	RFCMD_FACTORYTEST_RFTEST				= 0x50,
	RFACK_FACTORYTEST_RFTEST				= 0x51,
	RFCMD_FACTORYTEST_BURNIN_SN				= 0x52,
	RFACK_FACTORYTEST_BURNIN_SN				= 0x53,

    /* 0x60~0x7F AudioDocking RF SubCmd */
	RFCMD_SENSORALERT_SLEEP					=0x60,
	RFCMD_SENSORALERT_WAKEUP				=0x61,
	RFCMD_SENSORALERT_AGETATED				=0x62,

	RFCMD_SENSORALERT_DOOROPEN				=0x63,
	RFCMD_SENSORALERT_DOORCLOSE				=0x64,	

	RFACK_SERSORDATA_UPDATESTATUS			=0x71,
	RFCMD_SERSORALERT_MOTION				=0x72,
	RFCMD_SENSOR_REPORTSTATUS				=0x73,
	RFACK_SENSOR_REPORTSTATUS				=0x74,
	RFCMD_SENSOR_STOP2SENDACK				=0x7E,	
	RFCMD_SENSORDATA_TEST					=0x7F,
} QdtRFSubCmd_e;

typedef enum {
	DEVICE_TYPE_AUDIODOCKING					= 0x01,
	DEVICE_TYPE_PARENTUNIT						= 0x02,	
	DEVICE_TYPE_SENSOR_CRIB						= 0x03,
	DEVICE_TYPE_SENSOR_GATE						= 0x04,
} QdtDeviceType_e;

typedef struct tagRFHEADER
{
	Byte ucQdtUIDType;                    	//Quadrant Uinque ID  :: 0x81 for audio and 0x82 for RF SubCmd
   Byte ucSrcSerialNumber[5];                	//RFSenter Device ID	                                    
	Byte ucRFSubCmd;						//RF sub Cmd: RFCMD_PARENTCOMMAND_CALLING, RFCMD_AUDIODOCKING_ANSWERCALLING....etc
	Byte ucDestSerialNumber[5];             //RFReceiver Device ID
	//Byte ucHeader[2];                   	//Quadrant Header formate 
	Byte ucDatalength[2];					//Data length
	Byte ucReserved;                     	//Reserved
	Byte ucCheckSum;                     	//CheckSum	
}RFHEADER;


/*****************************************************************************/
/*  Global Function Declarations                                                                                                */
/*****************************************************************************/
RETURN_TYPE QDT_Common_Init(void);
RETURN_TYPE QDT_Common_DelayTime(qdt_uint16 u16MilliSec);
qdt_uint8 QDT_Common_CheckSumCalc(Byte* rpData,Byte ucLength);
RETURN_TYPE QDT_Common_SetSyncWord(qdt_bool bSetUniSyncWord);
RETURN_TYPE QDT_Common_SaveUserData(qdt_bool bIsInStandByMode);
qdt_uint8 QDT_Common_CheckSumCalc(qdt_uint8 * rpData, qdt_uint8 ucLength);
qdt_uint8 QDT_Common_CheckComboKey(qdt_uint8 ucUserKey);
qdt_bool QDT_Common_CompareSN(qdt_uint8 *pDestSN, qdt_uint8 *pSrcSN);
QdtDeviceType_e QDT_Common_GetDeviceType(qdt_uint8 *pDevSN);


/* RF Function */
void QDT_Common_IsRFalive(void);
RETURN_TYPE QDT_Common_ParseRFPackage(qdt_uint8 * ucRXBuffer);
RETURN_TYPE QDT_Common_BuildRFCmdHeader(RFHEADER * pRFHeader, QdtRFSubCmd_e e_RFSubCmd, qdt_uint8 *ucDestSerialNumber);
RETURN_TYPE QDT_Common_SentRFCmd(RFHEADER *pRFHeader, qdt_uint8 *ucData);
RETURN_TYPE QDT_Common_SentRFAudio(RFHEADER *pRFHeader, qdt_uint8 *ucData);
RETURN_TYPE QDT_Common_SetFreq(qdt_uint8 ucPairingFreqIdx);





RETURN_TYPE QDT_Common_ScanUserKey(void);

RETURN_TYPE QDT_Common_MicPause(void);
RETURN_TYPE QDT_Common_MicResume(void);
RETURN_TYPE QDT_Common_SPKPause(void);
RETURN_TYPE QDT_Common_SPKResume(void);


void QDT_Common_VolumeControlCopy(signed short *pi16Src, signed short *pi16Dest, qdt_uint16 u16PcmCount);                // 0=< u8Vol <=16
void QDT_Common_VolumeUpDown(void);


#endif  // #ifdef __QDT_COMMON_H__
