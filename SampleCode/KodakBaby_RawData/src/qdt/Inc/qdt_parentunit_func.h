#ifndef __QDT_PARENTUNIT_FUNC_H__
#define __QDT_PARENTUNIT_FUNC_H__ 1

#include "qdt_utils.h"

#define DUTY_LED_INTENSITY		1000
#define ALC_TH 400000
	

/*RF ack function*/
RETURN_TYPE QDT_PU_AudioDocking_OnLine(void);
RETURN_TYPE QDT_PU_AudioDocking_AnswerCalling(void);
RETURN_TYPE QDT_PU_AudioDocking_HandUp(void);
RETURN_TYPE QDT_PU_UnPaired(void);


/*handle key function*/
RETURN_TYPE QDT_PU_ProcessUpgradeKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData);
RETURN_TYPE QDT_PU_ProcessStandByKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData);
RETURN_TYPE QDT_PU_ProcessPairingKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData);
RETURN_TYPE QDT_PU_ProcessKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData);
RETURN_TYPE QDT_PU_PushToTalk(void);
RETURN_TYPE QDT_PU_EnableGpioPwm(void);
RETURN_TYPE QDT_PU_DisableGpioPwm(void);
RETURN_TYPE QDT_PU_SysPowerOn(void);
RETURN_TYPE QDT_PU_SysPowerOff(void);
RETURN_TYPE QDT_PU_SysPowerOnOff(void);
RETURN_TYPE QDT_PU_HandleAlertKey(void);
RETURN_TYPE QDT_PU_PingAudioDocking(void);
RETURN_TYPE QDT_PU_SysStanbByPowerDown(void);
RETURN_TYPE QDT_PU_SysEnterSleep(void);
RETURN_TYPE QDT_PU_SysExitSleep(void);

RETURN_TYPE QDT_PU_SoundActivity(signed short *pi16Src, qdt_uint16 u16PcmCount);
RETURN_TYPE QDT_PU_CheckSoundActivity(void);
RETURN_TYPE QDT_PU_PushToTalkLED(qdt_bool bOnOff);
qdt_bool QDT_PU_IsGetLowPowerSignal(void);
qdt_bool QDT_PU_IsDCConnected(void);


RETURN_TYPE QDT_PU_DumpUpgradeImage(qdt_uint8 *ucRxData);



/*main function*/
RETURN_TYPE ParentUnit_main(void);

#endif  // #ifdef __QDT_PARENTUNIT_FUNC_H__
