#ifndef __STATE_H
#define __STATE_H
/***********************************************************************/
/* Sytem State                                                                                         */
/***********************************************************************/
enum tagSystemStateMachine
{
	SYSSTATE_COLDBOOT,
	SYSSTATE_STANDBY,
    SYSSTATE_UNPAIRING,
    SYSSTATE_RUNNING,
    SYSSTATE_SLEEP,
    SYSSTATE_WAKEUP,
    SYSSTATE_UPGRADE,
    SYSSTATE_FACTORY
};

/***********************************************************************/
/* Pairing State                                                                                         */
/***********************************************************************/
enum tagPairingState
{
	_QDT_PAIRINGSTATE_WAITFORSTAR_		= 0x00,
	_QDT_PAIRINGSTATE_PINGAD_			= 0x01,		
	_QDT_PAIRINGSTATE_SEARCHING_ 		= 0x02,
	_QDT_PAIRINGSTATE_TX_SYNCWORD_ 		= 0x03,
	_QDT_PAIRINGSTATE_TESTING_SYNCWORD_	= 0x04,
	_QDT_PAIRINGSTATE_FINISHED_ 		= 0x05,	
};

#endif
