#ifndef QDT_UPGRADE_FUNC_H_
#define QDT_UPGRADE_FUNC_H_

#include "qdt_utils.h"

enum tagUpgradeState
{
	_QDT_UPGRADESTATE_WAITFORSTAR_		= 0x00,
	_QDT_UPGRADESTATE_SENTOUTREQUST_	= 0x01,		
	_QDT_UPGRADESTATE_PARENTREADY_		= 0x02,		
	_QDT_UPGRADESTATE_TRANSIMAGE_ 		= 0x03,
	_QDT_UPGRADESTATE_WAITACK_ 			= 0x04,
	_QDT_UPGRADESTATE_FINISHED_ 		= 0x05,	
};
enum tagUpgradeImageType
{
	_QDT_UPGRADEIMAGETYPE_AUDIODOCKING_		= 0x00,
	_QDT_UPGRADEIMAGETYPE_PARENTUNIT_	= 0x01,		
	_QDT_UPGRADEIMAGETYPE_VOICEDATA_		= 0x02,		
	_QDT_UPGRADEIMAGETYPE_AKM_CRAM_		= 0x03,		
	_QDT_UPGRADEIMAGETYPE_AKM_PRAM_		= 0x04,		
	_QDT_UPGRADEIMAGETYPE_MAX_ 		= 0x05,
};
enum tagUpgradeTransferType
{
	_QDT_UPGRADETRANSFERTYPE_UART_		= 0x00,
	_QDT_UPGRADETRANSFERTYPE_RF_	= 0x01,		
};


typedef struct tagUpdataImageInfo
{	
	qdt_bool  bStartUpdate:1;
	qdt_uint8 ucImageType;
	qdt_uint8 ucImageVersion[4];  
	qdt_uint32 u32FlashAddr;
	qdt_uint32 u32AddrIdx;	
	qdt_uint32 u32ImageSize;	
}UpdataImageInfo;

extern UpdataImageInfo g_UpdateImageInfo;
extern UpdataImageInfo g_CurrentImageInfo;

RETURN_TYPE QDT_Upgrade_ReadImage(qdt_uint8 *ucImageData, qdt_uint32 u32AddrIdx, qdt_uint32 ucReadCount);
RETURN_TYPE QDT_Upgrade_WriteImage(qdt_uint8 ucTransferType, qdt_uint8 ucImageType, qdt_uint32 u32Addr, qdt_uint8 uclength, qdt_uint8 *pucImageData);
RETURN_TYPE QDT_Upgrade_EarseImageAddr(void);


#endif 

