#ifndef RAM_H_
#define RAM_H_

#include "qdt_utils.h"

typedef struct tagGlobalData
{
	qdt_uint8 ucSysState;
	qdt_uint8 ucPreSysState;
	qdt_uint8 uc10MSCount;
	qdt_uint8 uc500MSCount;
	qdt_uint8 uc1000MSCount;

	qdt_uint64 u64MySerialNumber;
	qdt_uint8 ucPairingTryCount;
	qdt_uint8 ucAllocListIdx;
	qdt_uint8 ucAllocRFFreqCH;
	qdt_bool bUpdateFlashData:1;			//record if need to write user data into flash and indicate RF don't exectute interrupt

	qdt_bool bTwinkleGreenLED:1;
	qdt_bool bTwinkleRedLED:1;
	qdt_bool bTwinkleOrangeLED:1;
	qdt_bool bParentUintOnLine:1;
	qdt_bool bAudioDockingOnLine:1;

    qdt_bool bRFTransmitFinished:1;
	qdt_bool bRFReceiveFinished:1;
	qdt_bool bParentCalling:1;
	qdt_bool bParentHandUp:1;
	qdt_bool bAudidDockingAnswerCalling:1;
	qdt_bool bAudidDockingHandUp:1;
	
	qdt_bool bMicBufferReady:1;
	qdt_bool bSPKBufferEmpty:1;
	qdt_bool bTransmitBufferIsempty:1;

	qdt_bool bMicOpened:1;
	qdt_bool bSPKOpened:1;

	qdt_bool bNeedToBoardCastRFCmd:1;   	//if need Boardcast RF cmd to other device from audio docking
	qdt_uint8 ucBoardCastRFCmd;				//Boardcast RF cmd

	//qdt_uint32 ucRFSentOutDelayTime;
	qdt_uint8 ucWaitingParentACKCount;	
	qdt_uint8 ucWaitADHandupACKCount;	

	qdt_uint8 bAlertComing:1;
	qdt_uint8 bHandleAlertStatus:1;

	qdt_uint8 NoAudioDataComingCounter;

	qdt_uint8 int_flat[8];
	qdt_uint8 ucPreVol;
	qdt_uint8 ucCurVol;

	qdt_uint8 ucNightLightTimerStart;
	qdt_uint16 u16NightLightTimerSecs;
	qdt_uint8 ucNLTargetBrightLevel;
	qdt_uint8 ucNLCurrentBrightLevel;
	
	/*for testing variable*/
	qdt_uint8 ucCurRXChan;
	qdt_bool  bStopPollingRXChan:1;
	qdt_bool  bChangeToAllocRFFreqCH:1;
	qdt_bool  bIsInPairingState:1;
	qdt_bool  bAllow2UsingBoardCastChan:1;
	qdt_bool  bDisablePTTButton:1;
	qdt_bool  bIsInPTTMode:1;

	qdt_uint8 ucParentUnitChooseCH;
	qdt_uint8 ucParentUintTXChan;

	qdt_bool bHumanVoice:1;
	qdt_uint8 ucSoundActivity;
	qdt_bool bAudioAlertFlag:1;
	qdt_bool bVibrationEnable:1;
	qdt_bool bSensorAlertMotion:1;
	qdt_bool bFactoryResetFlag:1;
	qdt_uint16 ucAudioAlertValue;
	qdt_uint32 ucAudioAlertTable[5];
	//------upgrade---------//
	qdt_uint8 ucUpgradeState;
	qdt_bool bWaitParentAck:1;

	qdt_bool bIsBatteryMode:1;
	qdt_bool bIsQuietMode:1;
	qdt_bool bIsLowBattery:1;
	qdt_bool bUserCancelQuietMode:1;
    qdt_bool bAudioAlertTrigger:1;
	qdt_bool bDisableSleepMode:1;
	qdt_bool bEnableSleepLED:1;
}GlobalData;

typedef struct tagPairedAudioDocking
{
	qdt_uint8 ucVoiceIdx;
	qdt_uint8 ucSerialNumber[5];
	qdt_uint8 ucPairedRFFreqCH;
}PairedAudioDocking;

typedef struct tagPairedParentUnit
{
	qdt_uint8 ucVoiceIdx;
	qdt_uint8 ucSerialNumber[5];
	qdt_uint8 ucPairedRFFreqCH;
}PairedParentUnit;

typedef struct tagPairedSensorUnit
{
	qdt_uint8 ucVoiceIdx;
	qdt_uint8 ucSensorType;
	qdt_uint8 ucSerialNumber[5];
	qdt_uint8 ucPairedRFFreqCH;
	qdt_bool  bIsUpdated:1;
	qdt_uint8 ucTempture[2];
	qdt_uint8 ucHumidity[2];
	qdt_uint8 ucAcceleroMeterXAxis[2];
	qdt_uint8 ucAcceleroMeterYAxis[2];
	qdt_uint8 ucAcceleroMeterZAxis[2];
	qdt_uint8 ucMagnetoMeterXAxis[2];
	qdt_uint8 ucMagnetoMeterYAxis[2];
	qdt_uint8 ucMagnetoMeterZAxis[2];	
}PairedSensorUnit;

typedef struct tagUserSaveData
{
	qdt_uint8 ucMagicNumber[2];      	
	qdt_uint8 ucSerialNumber[5];	
	qdt_uint8 ucPairedParentUnitNum;
	qdt_uint8 ucPairedParentUnitList;  
	qdt_uint8 ucPairedSensorUnitNum;
	qdt_uint8 ucPairedSensorUnitList;           
	qdt_uint8 ucPairingState;			
	qdt_uint8 ucPairingSyncWord[4];    	
	qdt_uint8 ucPairingFreqIdx;  
	qdt_uint8 ucAllocRFFreqCH;				
	qdt_uint8 ucVolValue;					
	PairedAudioDocking s_PairedAudioDocking;
	PairedParentUnit   s_PairedParentUnit[8];
	PairedSensorUnit   s_PairedSensorUnit[8];
	
	/*Night Light Variable*/
	qdt_uint8 ucNightLightEnbled;
	qdt_uint8 ucBrightLevel;

	qdt_uint8 ucAudioAlertLevel;
	qdt_uint8 ucAudioAlertEnable;
}UserSaveData;

typedef struct tagFactorySaveData
{
	qdt_uint8 ucMagicNumber[2];      	
	qdt_uint8 ucSerialNumber[5];
	qdt_uint8 bIsInFactoryMode;
	qdt_uint8 ucLEDTestResult;  
	qdt_uint8 ucMICTestResult;
	qdt_uint8 ucSPKTestResult;           
	qdt_uint8 ucKEYTestResult;		
	qdt_uint8 ucRFTestResult;	
	qdt_uint8 ucSENSORTestResult;	
	qdt_uint8 ucVIBRATORTestResult;	
}FactorySaveData;

typedef struct tagGlobalFactoryData
{
	qdt_bool bEnableFactoryMode:1;
	qdt_bool bEnableButtonTest:1;
	qdt_bool bEnableMICSPKTest:1;
	qdt_bool bEnableNLSensorTest:1;
	qdt_bool bEnableRFTest:1;
	qdt_bool bEnableBurnInSN:1;
}GlobalFactoryData;

extern GlobalData g_Data;
extern GlobalFactoryData g_FactoryData;
extern UserSaveData g_UserSaveData;
extern FactorySaveData g_FactorySaveData;

#endif
