#ifndef QDT_PARENTUNIT_H_
#define QDT_PARENTUNIT_H_

#define PARENTUNIT_LED_PWR_RED(bOnOff)	  \
    do{if(bOnOff)DrvGPIO_ClrBit(GPB,0);else DrvGPIO_SetBit(GPB,0);}while(0)
#define PARENTUNIT_LED_PWR_GREEN(bOnOff)	\
	do{if(bOnOff)DrvGPIO_ClrBit(GPA,14);else DrvGPIO_SetBit(GPA,14);}while(0)		
		
#define PARENTUNIT_LED_ALERT_ORANGE(bOnOff)	  \
    do{if(bOnOff)DrvGPIO_ClrBit(GPA,13);else DrvGPIO_SetBit(GPA,13);}while(0)
#define PARENTUNIT_VIBRATOR(bOnOff)	\
    do{if(bOnOff)DrvGPIO_SetBit(GPB,3);else DrvGPIO_ClrBit(GPB,3);}while(0)
#define PARENTUNIT_RF_446x(bOnOff)	\
    do{if(bOnOff)DrvGPIO_ClrBit(GPA,15);else DrvGPIO_SetBit(GPA,15);}while(0) 	 


#define PARENTUNIT_LED_INTENSITY_R1(bOnOff)	\
    do{if(bOnOff)DrvGPIO_ClrBit(GPB,13);else DrvGPIO_SetBit(GPB,13);}while(0)
#define PARENTUNIT_LED_INTENSITY_R2(bOnOff)	\
    do{if(bOnOff)DrvGPIO_ClrBit(GPB,14);else DrvGPIO_SetBit(GPB,14);}while(0)
#define PARENTUNIT_LED_INTENSITY_G1(bOnOff)	\
    do{if(bOnOff)DrvGPIO_ClrBit(GPA,12);else DrvGPIO_SetBit(GPA,12);}while(0)
#define PARENTUNIT_LED_INTENSITY_G2(bOnOff)	\
    do{if(bOnOff)DrvGPIO_ClrBit(GPB,15);else DrvGPIO_SetBit(GPB,15);}while(0)		
#endif
