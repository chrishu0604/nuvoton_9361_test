#ifndef __QDT_FLASH_HAL__
#define __QDT_FLASH_HAL__ 1

#include "qdt_utils.h"
#include "qdt_ram.h"
//--------------------------------------------//
// FACTORYDATA		0x000000 ~ 0x001000 (4K)                |MGNUM|MGNUM|SER1|SER2|SER3|SER4|FWVER1|FWVER2|FWVER3|FWVER4|......
// ------------------------------------------- //
// USERDATA 			0x001000 ~ 0x002000 (4K)
// ------------------------------------------- //
// IMAGE Part1			0x010000 ~ 0x040000 (192K)
// ------------------------------------------- //
// IMAGE Part2			0x040000 ~ 0x070000 (192K)
// ------------------------------------------- //
// PARENT TEMP IMAGE 	0x070000 ~ 0x0A0000 (192K)
// ------------------------------------------- //
// NULL 				0x0A0000 ~ 0x100000
// ------------------------------------------- //
// VOICE GUIDE 	0x100000 ~ 0x200000
// ------------------------------------------- //

#define FLASH_FACTORYDATA_ADDR 			0x000000
#define FLASH_USERDATA_ADDR 			0x001000
#define FLASH_IMAGE_PARTITION1_ADDR 	0x010000
#define FLASH_IMAGE_PARTITION2_ADDR 	0x040000
#define FLASH_IMAGE_PARENTTEMP_ADDR 	0x070000
#define FLASH_VOICE_DATA_ADDR 			0x100000
#define FLASH_AKM_CRAM_ADDR					0x1F0000
#define FLASH_AKM_PRAM_ADDR					0x1F8000

#define FLASH_PAGE_SIZE 256


void QDT_flash_EarseSector(qdt_uint32 ucAddr);
void QDT_flash_WriteBytes(qdt_uint32 u32Addr, qdt_uint32 ucWriteCount, qdt_uint8 *pucWriteData);
void QDT_flash_ReadBytes(qdt_uint32 u32Addr, qdt_uint32 u32ReadCount, qdt_uint8 *pucReadData);
void QDT_flash_ReadUniqueID(qdt_uint8 *pucUniqueID);
void QDT_flash_ReadUserData(UserSaveData *pUserSaveData);
void QDT_flash_WriteUserData(UserSaveData *pUserSaveData);
void QDT_flash_ClearUserData(void);
void QDT_flash_ReadFactoryData(FactorySaveData*pFactorySaveData);
void QDT_flash_WriteFactoryData(FactorySaveData *pFactorySaveData);
void QDT_flash_ClearFactoryData(void);
void QDT_flash_WriteImage(qdt_uint32 u32Addr, qdt_uint32 ucWriteCount, qdt_uint8 *pucWriteData);


#endif 
