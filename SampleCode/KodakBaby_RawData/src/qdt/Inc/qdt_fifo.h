#ifndef __QDT_FIFO_H
#define __QDT_FIFO_H

#include "qdt_utils.h"

#define CMDBUFFERSIZE  330

typedef struct tagCmdData
{
    Byte ucCmd;
	Byte ucRxData[32];
} CmdData;    	
typedef CmdData * pCmdData;

typedef struct FifoStruct
{
    Word ucHead;     
    Word ucTail;     
    Word ucSize;     
    CmdData CmdData[1];  
} FifoStruct;
typedef FifoStruct * pFifoHandle;

#define QDT_InitFIFO(gpFIFOBuffer, cLength) (QDT_InitFIFO2(gpFIFOBuffer, cLength), (pFifoHandle)gpFIFOBuffer)

void  QDT_InitFIFO2(Byte * gpFIFOBuffer, Word cLength);
Bool  QDT_GetFIFO(pFifoHandle fh, Byte * rpData, Byte   *rpRxData);
Bool  QDT_SetFIFO(pFifoHandle fh, Byte ucData, Byte *rpRxData);
void  QDT_ClearFIFO(pFifoHandle fh);
Bool  QDT_FIFOIsEmpty(pFifoHandle fh);


#define QDT_InitCmdBuffer() \
    QDT_InitFIFO(CmdBuffer, CMDBUFFERSIZE)

#define QDT_AddKeyCmdData(ucCmd, rpRxData) \
    QDT_SetFIFO((pFifoHandle)CmdBuffer, ucCmd, rpRxData)

#define QDT_ClearCmd() \
    QDT_ClearFIFO((pFifoHandle)CmdBuffer);

void QDT_GetkeyCmdData(Byte *ucCmd, Byte *ucData);

extern Byte  CmdBuffer[CMDBUFFERSIZE];

/*RF command / ack define*/
/*
#define RFCMD_NON_CMD 							0x00
#define RFACK_NON_ACK 							0x00

#define RFACK_PARENTUINT_IAMHERE 				0x01	
#define RFACK_PARENTUINT_GOT_SYNCWORD			0x02	
#define RFACK_PARENTUINT_USING_SYNCWORD			0x03


#define RFCMD_PARENTCOMMAND_ONLINE 				0x04	
#define RFCMD_PARENTCOMMAND_CALLING				0x05
#define RFCMD_PARENTCOMMAND_HANDUP				0x06
#define RFCMD_PARENTCOMMAND_HANDUP_ACK			0x07

#define RFCMD_AUDIODOCKING_SEARCHING_DEVICE		0x20
#define RFCMD_AUDIODOCKING_TX_SYNCWORD			0x21
#define RFCMD_AUDIODOCKING_TESTING_SYNCWORD		0x22


#define RFCMD_AUDIODOCKING_ONLINE				0x23
#define RFACK_AUDIODOCKING_ANSWERCALLING		0x24
#define RFACK_AUDIODOCKING_HANDUP				0x25
*/

/*User key define*/
#define _USER_KEY_NONE 							0x00
#define _USER_KEY_VERSION 						0x80

#define _USER_KEY_AUDIODOCKING_KEY_POWER	  	0x81
#define _USER_KEY_AUDIODOCKING_KEY_VOLUP      	0x82
#define _USER_KEY_AUDIODOCKING_KEY_VOLDOWN   	0x83
#define _USER_KEY_AUDIODOCKING_KEY_PAIRING  	0x84

#define _USER_KEY_AUDIODOCKING_KEY_LONGPRESS_PAIRING 	0x9A
#define _USER_KEY_AUDIODOCKING_KEY_LONGPRESS_VOLDOWN 	0x9B

#define _USER_KEY_AUDIODOCKING_KEY_VERSION 	0x9F



#define _USER_KEY_PARENTUNIT_KEY_POWER	    	0xA1
#define _USER_KEY_PARENTUNIT_KEY_VOLUP      	0xA2
#define _USER_KEY_PARENTUNIT_KEY_VOLDOWN    	0xA3
#define _USER_KEY_PARENTUNIT_KEY_LOWBATTERY 	0xA4
#define _USER_KEY_PARENTUNIT_KEY_PTT        	0xA5
#define _USER_KEY_PARENTUNIT_KEY_ALERT      	0xA6

#define _USER_KEY_PARENTUNIT_KEY_LONGPRESS_VOLUP 	0xBA
#define _USER_KEY_PARENTUNIT_KEY_LONGPRESS_VOLDOWN 	0xBB
#define _USER_KEY_PARENTUNIT_KEY_LONGPRESS_ALERT 	0xBC




#define GPIO_AUDIODOCKING_KEY_POWER 			4   //b
#define GPIO_AUDIODOCKING_KEY_VOLUP 			5
#define GPIO_AUDIODOCKING_KEY_VOLDOWN 			2 //b
#define GPIO_AUDIODOCKING_KEY_PAIRING 			11 //b

#define GPIO_PARENTUNIT_KEY_POWER 				4 
#define GPIO_PARENTUNIT_KEY_VOLUP 				5 
#define GPIO_PARENTUNIT_KEY_VOLDOWN 			6
#define GPIO_PARENTUNIT_KEY_LOWBATTERY 			7
#define GPIO_PARENTUNIT_KEY_PTT 				15  //b
#define GPIO_PARENTUNIT_KEY_ALERT 				16  //b

//#define GPIO_PARENTUNIT_KEY_PTT 				10
//#define GPIO_PARENTUNIT_KEY_ALERT 				11





#endif
