#ifndef __QDT_AUDIODOCKING_FUNC_H__
#define __QDT_AUDIODOCKING_FUNC_H__ 1

#include "qdt_utils.h"

#define AudioAlertLevel 900000
#define DUTY_RESOLUTION		1000//101	

/* RF ack function */
RETURN_TYPE QDT_AD_Parents_OnLine(void);
RETURN_TYPE QDT_AD_Parents_Calling(void);
RETURN_TYPE QDT_AD_Parents_HandUp(void);
RETURN_TYPE QDT_AD_Parents_ACK(void);
RETURN_TYPE QDT_AD_UnPaired(qdt_uint8 *ucDumpRxData);
RETURN_TYPE QDT_AD_UpdateSensorStatus(qdt_uint8 *ucDumpRxData);
RETURN_TYPE QDT_AD_UpgradeParentUnit(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData);



/* handle key function */
RETURN_TYPE QDT_AD_ProcessUpgradeKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData);
RETURN_TYPE QDT_AD_ProcessStandByKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData);
RETURN_TYPE QDT_AD_ProcessPairingKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData);
RETURN_TYPE QDT_AD_ProcessKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData);
RETURN_TYPE QDT_AD_EnableGpioPwm(void);
RETURN_TYPE QDT_AD_DisableGpioPwm(void);

RETURN_TYPE QDT_AD_SysPowerOnOff(void);

/*handle uart function*/
RETURN_TYPE QDT_AD_SetNightLightOnOFF(qdt_uint16 u16OnOff);
RETURN_TYPE QDT_AD_SetNightLightLevel(qdt_uint16 u16Level);
RETURN_TYPE QDT_AD_SetNightLightTimer(qdt_uint16 u16Timer);



RETURN_TYPE QDT_AD_Product_SyncWord(void);
qdt_bool QDT_AD_SearchDevices(qdt_uint8 *ucData);
void QDT_AD_NightLEDOnOff(void);
RETURN_TYPE QDT_AD_SetAudioAlertLevel(qdt_uint16 u16Level);
RETURN_TYPE QDT_AD_SetAudioAlertOnOFF(qdt_uint16 u16OnOff);


//audio alert function
RETURN_TYPE QDT_AD_AudioAlert(signed short *pi16Src, qdt_uint16 u16PcmCount);


RETURN_TYPE QDT_AD_SetSpeakerOnOff(qdt_uint16 u16OnOff);


/*main function*/
RETURN_TYPE AudioDocking_main(void);

#endif  // #ifdef __QDT_AUDIODOCKING_FUNC_H__
