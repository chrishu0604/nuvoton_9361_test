#ifndef QDT_FACTORY_FUNC_H_
#define QDT_FACTORY_FUNC_H_

#include "qdt_utils.h"

RETURN_TYPE QDT_Factory_Init(void);
RETURN_TYPE QDT_Factory_EnterFactoryMode(void);
RETURN_TYPE QDT_Factory_ExitFactoryMode(void);
RETURN_TYPE QDT_Factory_LEDTest(qdt_bool bLEDOnOff);
RETURN_TYPE QDT_Factory_RFTest(void);
RETURN_TYPE QDT_Factory_RFTest_ACK(void);
RETURN_TYPE QDT_Factory_VibratorTest(qdt_bool bOnOff);
RETURN_TYPE QDT_Factory_NightLightSensorTest(qdt_bool bOnOff);
qdt_bool QDT_Factory_CheckRFTestPackage(qdt_uint8 *ucDumpRxData);
RETURN_TYPE QDT_Factory_ProcessKeyCmd(qdt_uint8 ucCmd, qdt_uint8 *ucDumpRxData);
RETURN_TYPE QDT_Factory_EnableRF(void);
RETURN_TYPE QDT_Factory_BurnInSN(qdt_uint8 *ucDumpRxData);
RETURN_TYPE QDT_Factory_ForceUpdateAKM(void);

#define _FACTORY_KEY_NONE 							0x00
#define _FACTORY_KEY_OK 							0x01
#define _FACTORY_KEY_UP 							0x02
#define _FACTORY_KEY_DOWN 							0x03
#define _FACTORY_KEY_RETURN 					    0x04
#define _FACTORY_KEY_RESET							0x05



#endif 

