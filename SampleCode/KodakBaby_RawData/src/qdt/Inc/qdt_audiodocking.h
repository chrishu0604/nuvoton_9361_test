#ifndef QDT_AUDIODOCKING_H_
#define QDT_AUDIODOCKING_H_

#define AUDIODOCKING_LED_PWR_RED(bOnOff)	  \
    do{if(bOnOff)DrvGPIO_ClrBit(GPB,0);else DrvGPIO_SetBit(GPB,0);}while(0)
		
#define AUDIODOCKING_LED_PWR_GREEN(bOnOff)	\
	do{if(bOnOff)DrvGPIO_ClrBit(GPA,14);else DrvGPIO_SetBit(GPA,14);}while(0)

#define AUDIODOCKING_RF_446x(bOnOff)	\
    do{if(bOnOff)DrvGPIO_ClrBit(GPA,15);else DrvGPIO_SetBit(GPA,15);}while(0) 		
#define AUDIODOCKING_RF_446x_PER_CTRL(bOnOff)	\
	do{if(bOnOff)DrvGPIO_SetBit(GPB,10);else DrvGPIO_ClrBit(GPB,10);}while(0)	


#define AUDIODOCKING_CAMERA_PWR(bOnOff)	\
    do{if(bOnOff)DrvGPIO_SetBit(GPB,13);else DrvGPIO_ClrBit(GPB,13);}while(0) 
		
#define AUDIODOCKING_LED_NIGHTLIGHT(bOnOff)	\
	do{if(bOnOff)DrvGPIO_ClrBit(GPA,12);else DrvGPIO_SetBit(GPA,12);}while(0)
		
#define AUDIODOCKING_SPK_SWITCH(bOnOff)	  \
		do{if(bOnOff)DrvGPIO_ClrBit(GPB,6);else DrvGPIO_SetBit(GPB,6);}while(0)

#define POLLING_CHANNEL_MAX 18	

#endif
