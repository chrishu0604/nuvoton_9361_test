#ifndef __QDT_UTILS_H__
#define __QDT_UTILS_H__ 1

#include <stdio.h>

typedef unsigned char qdt_uint8;
typedef unsigned char Byte;
typedef unsigned int qdt_uint16;
typedef unsigned short Word;
typedef unsigned long qdt_uint32;
typedef unsigned long Dword;
typedef unsigned long long qdt_uint64;

typedef unsigned char qdt_bool;
typedef unsigned char Bool;
typedef signed int qdt_sint16;
typedef signed long int  qdt_sint32; 

#define qdt_true 		1
#define qdt_false 		0
#define qdt_ON 			1
#define qdt_OFF 		0


typedef signed char RETURN_TYPE;
#define	QDT_SYS_NOERROR				0
#define	QDT_SYS_FAILED				-1

typedef enum {
	QDT_LEVEL_NONE = 0,
	QDT_LEVEL_ERROR,
	QDT_LEVEL_WARNING,
	QDT_LEVEL_DEBUG,
	QDT_LEVEL_TRACE,
	QDT_LEVEL_INFO,
	QDT_LEVEL_COUNT
} QdtDebugLevel;


#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
#define __qdt_debug_min  QDT_LEVEL_COUNT
#endif

#ifdef BUILDTYPE_PARENTUINT_PROJECT
#define __qdt_debug_min  QDT_LEVEL_COUNT
#endif

#define QDT_LEVEL_LOG(level,format,args...)                          \
    do{                                                       \
		char *levelstring [QDT_LEVEL_COUNT] = {"QDT_LEVEL_NONE", "QDT_LEVEL_ERROR",   \
		                                       "QDT_LEVEL_WARNING", "QDT_LEVEL_DEBUG", \
		                                       "QDT_LEVEL_TRACE", "QDT_LEVEL_INFO"}; \
        if (level <= __qdt_debug_min)                         \
        {                                                     \
			printf("[%s][%s:%d] " format, levelstring[level], __FILE__, __LINE__, ## args);  \
        }                                                     \
    }while(0)

#define QDT_ERROR(format,args...)    QDT_LEVEL_LOG (QDT_LEVEL_ERROR,   format,##args)
#define QDT_WARNING(format,args...)  QDT_LEVEL_LOG (QDT_LEVEL_WARNING, format,##args)
#define QDT_DEBUG(format,args...)    QDT_LEVEL_LOG (QDT_LEVEL_DEBUG,   format,##args)
#define QDT_TRACE(format,args...)    QDT_LEVEL_LOG (QDT_LEVEL_TRACE,   format,##args)
#define QDT_INFO(format,args...)     QDT_LEVEL_LOG (QDT_LEVEL_INFO,    format,##args)


#endif  // #ifdef __QDT_UTILS_H__
