#include "bsp.h"
#include "radio.h"
#include "ISD93xx.h"
#include "Driver\DrvADC.h"
#include "Driver\DrvPDMA.h"
#include "Driver\DrvDPWM.h"
#include "Driver\DrvGPIO.h"
#include "Driver\DrvTIMER.h"


#include "qdt_ram.h"
#include "qdt_fifo.h"
#include "qdt_flash_hal.h"
#include "qdt_sysState.h"
#include "qdt_audiodocking.h"
#include "qdt_parentunit.h"
#include "qdt_common_func.h"
#include "qdt_parentunit_func.h"
#include "qdt_audiodocking_func.h"
#include "qdt_upgrade_func.h"
#include "qdt_human_voice.h"
#include "qdt_factory_func.h"

#include "si446x_api_lib.h"    //RF-664x header file
#include "radio_comm.h"


/*---------------------------------------------------------------------------------------------------------*/
/* Define global variables                                                                                 */
/*---------------------------------------------------------------------------------------------------------*/
UserSaveData g_UserSaveData;
FactorySaveData g_FactorySaveData;
qdt_bool  g_bComboKeyState = 0;



qdt_bool bIsPressUserKey[10] = 	{	qdt_false, qdt_false, qdt_false, qdt_false, qdt_false,
	                             	qdt_false, qdt_false, qdt_false, qdt_false, qdt_false
	                           	};

const qdt_uint8 ucPairingFreqTable[RF_FREQ_TABLE_IDX_MAX][12] = 	
		{
/*		
			{0x11, 0x40, 0x08, 0x00, 0x3C, 0x08, 0x00, 0x00, 0x22, 0x22, 0x20, 0xFF}, //915M
			{0x11, 0x40, 0x08, 0x00, 0x3C, 0x0B, 0x33, 0x33, 0x22, 0x22, 0x20, 0xFF}, //921M 
			{0x11, 0x40, 0x08, 0x00, 0x3C, 0x0A, 0x22, 0x22, 0x22, 0x22, 0x20, 0xFF}, //919M 
			{0x11, 0x40, 0x08, 0x00, 0x3C, 0x09, 0x11, 0x11, 0x22, 0x22, 0x20, 0xFF}, //917M 
			{0x11, 0x40, 0x08, 0x00, 0x3B, 0x0E, 0xEE, 0xEE, 0x22, 0x22, 0x20, 0xFF}, //913M 
			{0x11, 0x40, 0x08, 0x00, 0x3B, 0x0D, 0xDD, 0xDD, 0x22, 0x22, 0x20, 0xFF}, //911M 
			{0x11, 0x40, 0x08, 0x00, 0x3B, 0x0C, 0xCC, 0xCC, 0x22, 0x22, 0x20, 0xFF}  //909M 
*/			
	{0x11, 0x40, 0x08, 0x00, 0x3B, 0x09, 0x11, 0x11, 0x22, 0x22, 0x20, 0xFF},	  //902M
	{0x11, 0x40, 0x08, 0x00, 0x3B, 0x0B, 0x33, 0x33, 0x22, 0x22, 0x20, 0xFF},	  //906M
	{0x11, 0x40, 0x08, 0x00, 0x3B, 0x0D, 0x55, 0x55, 0x22, 0x22, 0x20, 0xFF},	  //910M
	{0x11, 0x40, 0x08, 0x00, 0x3B, 0x0F, 0x77, 0x77, 0x22, 0x22, 0x20, 0xFF},	  //914M
	{0x11, 0x40, 0x08, 0x00, 0x3C, 0x09, 0x99, 0x99, 0x22, 0x22, 0x20, 0xFF},	  //918M
	{0x11, 0x40, 0x08, 0x00, 0x3C, 0x0B, 0xBB, 0xBB, 0x22, 0x22, 0x20, 0xFF},	  //922M
	{0x11, 0x40, 0x08, 0x00, 0x3C, 0x0D, 0xDD, 0xDD, 0x22, 0x22, 0x20, 0xFF}	  //926M

		};
const qdt_uint8 ucFactoryFreqTable[12] = { 0x11, 0x40, 0x08, 0x00, 0x37, 0x0D, 0x55, 0x55, 0x22, 0x22, 0x20, 0xFF };

const qdt_uint8 g_ucFWVersion[13] = {'F', 'W', 'V', 'E', 'R', 'S', 'I', 'O', 'N', 0x00, 0x00, 0x1, 0x08};

/*---------------------------------------------------------------------------------------------------------*/
/* extern global variables                                                                                 */
/*---------------------------------------------------------------------------------------------------------*/															
extern volatile __align(4) signed short s16Out_words[40];															
 
															
/**************************************************************/
/* extern function                                                                                       */
/**************************************************************/
extern void playAudio(signed short *AudioRXBuffer);
extern void SysTimerDelay(uint32_t us);

/**************************************************************/
/* static  function                                                                                       */
/**************************************************************/
static qdt_uint8 __qdt_MappingUserKeyArrayIdx(qdt_uint8 ucUserKey);
static qdt_bool __qdt_IsPressUserKey(qdt_uint8 ucUserKey);
static qdt_bool __qdt_IsRepeatKey(qdt_uint8 ucUserKey);
static qdt_bool __qdt_IsLongPressKey(qdt_uint8 ucUserKey);
static void __qdt_DumpRFReceiveData(qdt_uint8 * ucRXBuffer);

static qdt_uint8 __qdt_MappingUserKeyArrayIdx(qdt_uint8 ucUserKey)
{
	qdt_uint8 ucKeyArrayidx = 0;
#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
	switch(ucUserKey)
	{
	case GPIO_AUDIODOCKING_KEY_POWER:
		ucKeyArrayidx = 1;
		break;
	case GPIO_AUDIODOCKING_KEY_VOLUP:	
		ucKeyArrayidx = 2;
		break;	
	case GPIO_AUDIODOCKING_KEY_VOLDOWN:	
		ucKeyArrayidx = 3;
		break;	
	case GPIO_AUDIODOCKING_KEY_PAIRING:	
		ucKeyArrayidx = 4;
		break;	
	default:
		ucKeyArrayidx = 0;
		break;	
	}
#endif

#ifdef BUILDTYPE_PARENTUINT_PROJECT
	switch (ucUserKey)
	{
	case GPIO_PARENTUNIT_KEY_POWER:
		ucKeyArrayidx = 1;
		break;		
	case GPIO_PARENTUNIT_KEY_VOLUP:
		ucKeyArrayidx = 2;
		break;		
	case GPIO_PARENTUNIT_KEY_VOLDOWN:
		ucKeyArrayidx = 3;
		break;		
	case GPIO_PARENTUNIT_KEY_LOWBATTERY:
		ucKeyArrayidx = 4;
		break;		
	case GPIO_PARENTUNIT_KEY_ALERT:
		ucKeyArrayidx = 5;
		break;		
	default:
		ucKeyArrayidx = 0;
		break;	
	}
#endif

	return ucKeyArrayidx;
}


static qdt_bool __qdt_IsRepeatKey(qdt_uint8 ucUserKey)
{
#if 0 //don't support repeat key
	if (ucUserKey == GPIO_AUDIODOCKING_KEY_VOLUP ||
		ucUserKey == GPIO_AUDIODOCKING_KEY_VOLDOWN ||
		ucUserKey == GPIO_PARENTUNIT_KEY_VOLUP ||
		ucUserKey == GPIO_PARENTUNIT_KEY_VOLDOWN)
		return qdt_true;
#endif
	return qdt_false;
}

static qdt_bool __qdt_IsLongPressKey(qdt_uint8 ucUserKey)
{
	if (ucUserKey == GPIO_AUDIODOCKING_KEY_PAIRING ||
		ucUserKey == GPIO_AUDIODOCKING_KEY_VOLDOWN ||
		
		ucUserKey == GPIO_PARENTUNIT_KEY_PTT ||
		ucUserKey == GPIO_PARENTUNIT_KEY_VOLDOWN   ||
		ucUserKey == GPIO_PARENTUNIT_KEY_VOLUP  ||
		ucUserKey == GPIO_PARENTUNIT_KEY_ALERT)
		return qdt_true;

	return qdt_false;
}

static qdt_bool __qdt_IsPressUserKey(qdt_uint8 ucUserKey)
{
	static qdt_uint32 u32RepeatTime = 0;
	static qdt_uint32 u32longPressTime = 0;
	qdt_uint8 ucKeyArrayidx = 0;
  static qdt_uint8 ucPresskey =qdt_true;

#ifdef 	BUILDTYPE_AUDIODOCKING_PROJECT
	if (ucUserKey == GPIO_AUDIODOCKING_KEY_POWER || 
		  ucUserKey == GPIO_AUDIODOCKING_KEY_VOLDOWN ||
		  ucUserKey == GPIO_AUDIODOCKING_KEY_PAIRING)
	{
			ucPresskey = DrvGPIO_GetBit(GPB, ucUserKey);
	}
	else
#endif		
#ifdef 	BUILDTYPE_PARENTUINT_PROJECT
	if (ucUserKey == GPIO_PARENTUNIT_KEY_PTT || 
		  	ucUserKey == GPIO_PARENTUNIT_KEY_ALERT)
	{
			ucPresskey = DrvGPIO_GetBit(GPB, (ucUserKey-10));
	}
	else
#endif		

			ucPresskey = DrvGPIO_GetBit(GPA, ucUserKey);
	

	ucKeyArrayidx = __qdt_MappingUserKeyArrayIdx(ucUserKey);
	
	if(!ucPresskey)//if(!DrvGPIO_GetBit(GPA, ucUserKey))  //press key
	{
		if (bIsPressUserKey[ucKeyArrayidx] == qdt_true)    //handle repeat key
		{
			if ((DrvTIMER_GetTicks(TMR0) > u32RepeatTime) && __qdt_IsRepeatKey(ucUserKey)) 
			{
				bIsPressUserKey[ucKeyArrayidx] = qdt_true;
				u32RepeatTime = DrvTIMER_GetTicks(TMR0)+1000;
				return qdt_true;
			}			
		}
		else
		{
			bIsPressUserKey[ucKeyArrayidx] = qdt_true;
			u32RepeatTime = DrvTIMER_GetTicks(TMR0)+1000;
			u32longPressTime = DrvTIMER_GetTicks(TMR0)+5000;
			return qdt_false;
		}
	}		
	else  //release key
	{
		if (bIsPressUserKey[ucKeyArrayidx] == qdt_true) //release PRESSED key
		{
			if (DrvTIMER_GetTicks(TMR0) > u32longPressTime && __qdt_IsLongPressKey(ucUserKey)) //handle long press key
			{
				bIsPressUserKey[ucKeyArrayidx] = qdt_false;
				
				if (ucUserKey == GPIO_AUDIODOCKING_KEY_PAIRING)
					QDT_AddKeyCmdData(_USER_KEY_AUDIODOCKING_KEY_LONGPRESS_PAIRING, NULL);
				else if (ucUserKey == GPIO_AUDIODOCKING_KEY_VOLDOWN)
					QDT_AddKeyCmdData(_USER_KEY_AUDIODOCKING_KEY_LONGPRESS_VOLDOWN, NULL);
				else if (ucUserKey == GPIO_PARENTUNIT_KEY_VOLDOWN)
					QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_LONGPRESS_VOLDOWN, NULL);
				else if (ucUserKey == GPIO_PARENTUNIT_KEY_ALERT)
					QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_LONGPRESS_ALERT, NULL);
				else if (ucUserKey == GPIO_PARENTUNIT_KEY_VOLUP)
					QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_LONGPRESS_VOLUP, NULL);


				
				return qdt_false;
			}
			
			bIsPressUserKey[ucKeyArrayidx] = qdt_false;  
			return qdt_true;	
		}
	}
#if 0
	static qdt_uint32 u32RepeatTime = 0;
	static qdt_uint32 u32longPressTime = 0;
	static qdt_uint8 ucRecPreUserKey = _USER_KEY_NONE;
	static qdt_uint8 ucReleaseKey = qdt_true;    			//for detect long press key btn. 

	if(!DrvGPIO_GetBit(GPA, ucUserKey)) 
	{
		if (ucUserKey == ucRecPreUserKey)
		{
			if ((DrvTIMER_GetTicks(TMR0) > u32RepeatTime) /*&& __qdt_IsRepeatKey(ucUserKey)*/)  //handle repeat key
			{
				u32RepeatTime = DrvTIMER_GetTicks(TMR0)+1000;
				ucReleaseKey = qdt_false; 
				return qdt_true;
			}
			//else if (DrvTIMER_GetTicks(TMR0) > u32longPressTime && __qdt_IsLongPressKey(ucUserKey))
			//{
			//	
			//}
			else
			{
				ucReleaseKey = qdt_false; 
				return qdt_false;
			}
		}
		else
		{
			u32RepeatTime = DrvTIMER_GetTicks(TMR0)+1000;
			u32longPressTime = DrvTIMER_GetTicks(TMR0)+5000;
			ucReleaseKey = qdt_true;     				 
		}
		ucRecPreUserKey = ucUserKey;
		ucReleaseKey = qdt_true; 
		return qdt_true;	
	}
#endif	
	return qdt_false;
}

void __qdt_DumpRFReceiveData(qdt_uint8 * ucRXBuffer)
{
	qdt_uint8 i = 0;
	qdt_uint8 ucSubCmd;
	qdt_uint8 ucDumpRFDataBuffer[32];
	ucSubCmd = ucRXBuffer[QDT_RFPKT_RFSUBCMD_START_IDX];
	
	for (i=0; i<32; i++)
	{
		ucDumpRFDataBuffer[i] = ucRXBuffer[i];
	} 

	QDT_AddKeyCmdData(ucSubCmd, ucDumpRFDataBuffer);
}


qdt_bool __qdt_IsBroadcastRFSubCmd(QdtRFSubCmd_e e_RFSubCmd)
{
	if (e_RFSubCmd == RFCMD_AUDIODOCKING_SEARCHING_DEVICE)
		return qdt_true;

	return qdt_false;
	
}

qdt_bool QDT_Common_CompareSN(qdt_uint8 *pDestSN, qdt_uint8 *pSrcSN)
{
	
    qdt_uint8 i=0;
	qdt_bool retVal = qdt_true;

	for (i=0;i<5;i++)
	{
		if (pDestSN[i] != pSrcSN[i])
		{
			retVal = qdt_false;
			break;
		}
	}
    return retVal;
}

QdtDeviceType_e QDT_Common_GetDeviceType(qdt_uint8 *pDevSN)
{
	
	qdt_uint64 u64DevSN;
	qdt_uint8 ucDevType;
	u64DevSN = pDevSN[0]*4294967296+
		       pDevSN[1]*16777216+
		       pDevSN[2]*65535+
		       pDevSN[3]*256+
		       pDevSN[4];

	ucDevType = (u64DevSN/100000) % 10;
	//QDT_DEBUG("ucDevType = %d\n",ucDevType);

    return (QdtDeviceType_e)ucDevType;
}



qdt_uint8 QDT_Common_CheckSumCalc(qdt_uint8 * rpData, qdt_uint8 ucLength)
{
	
    qdt_uint8 i,ucData;

    ucData = 0;
    for(i=0;i<ucLength;i++)
        ucData += rpData[i];
        
    ucData = 0 - ucData;
    
    return ucData;
}

RETURN_TYPE QDT_Common_DelayTime(qdt_uint16 u16MilliSec)
{
	qdt_uint16 i;
	for(i=0; i < u16MilliSec; i++)
		SysTimerDelay(1000);	
	
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Common_Init(void)
{
    /* restore user data*/
	//QDT_flash_ClearUserData();
	qdt_uint8 ucFlashUniqueID[8];
	QDT_flash_ReadUniqueID(ucFlashUniqueID);
	QDT_flash_ReadUserData(&g_UserSaveData);
	QDT_Factory_Init();
	if (g_UserSaveData.ucMagicNumber[0] == 0xFF && g_UserSaveData.ucMagicNumber[1] == 0xFF)
	{
		QDT_TRACE("New Flash, set default value. \n");
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_WAITFORSTAR_;		
		g_UserSaveData.ucPairingSyncWord[0] = UNI_PAIRING_SYNCWORD_BYTE0;
		g_UserSaveData.ucPairingSyncWord[1] = UNI_PAIRING_SYNCWORD_BYTE1;
		g_UserSaveData.ucPairingSyncWord[2] = UNI_PAIRING_SYNCWORD_BYTE2;
		g_UserSaveData.ucPairingSyncWord[3] = UNI_PAIRING_SYNCWORD_BYTE3;
		g_UserSaveData.ucAllocRFFreqCH = RF_PARENTUNIT_CH0;
		g_UserSaveData.ucPairingFreqIdx = 0;
		g_UserSaveData.ucPairedSensorUnitNum = 0;
		g_UserSaveData.ucPairedSensorUnitList = 0;
		g_UserSaveData.ucPairedParentUnitNum = 0;
		g_UserSaveData.ucPairedParentUnitList = 0;
		//g_Data.ucSysState = SYSSTATE_UNPAIRING;

		
		/*Night Light variable*/
		g_UserSaveData.ucNightLightEnbled = qdt_false;
		g_UserSaveData.ucBrightLevel = 15;

		g_UserSaveData.ucAudioAlertLevel = 2;
		g_UserSaveData.ucAudioAlertEnable= 0;
	}
	
#ifdef BUILDTYPE_AUDIODOCKING_PROJECT  //!!! need remove later !!!!
	if (g_FactorySaveData.ucSerialNumber[0] == 0xFF &&
		g_FactorySaveData.ucSerialNumber[1] == 0xFF &&
		g_FactorySaveData.ucSerialNumber[2] == 0xFF &&
		g_FactorySaveData.ucSerialNumber[3] == 0xFF &&
		g_FactorySaveData.ucSerialNumber[4] == 0xFF )
	{
			g_UserSaveData.ucSerialNumber[0] = 0x5A;
			g_UserSaveData.ucSerialNumber[1] = 0x4D;
			g_UserSaveData.ucSerialNumber[2] = 0x20;
			g_UserSaveData.ucSerialNumber[3] = 0x00;		
			g_UserSaveData.ucSerialNumber[4] = ucFlashUniqueID[7];	
	}
	else
	{
			g_UserSaveData.ucSerialNumber[0] = g_FactorySaveData.ucSerialNumber[0];
			g_UserSaveData.ucSerialNumber[1] = g_FactorySaveData.ucSerialNumber[1];
			g_UserSaveData.ucSerialNumber[2] = g_FactorySaveData.ucSerialNumber[2];
			g_UserSaveData.ucSerialNumber[3] = g_FactorySaveData.ucSerialNumber[3];
			g_UserSaveData.ucSerialNumber[4] = g_FactorySaveData.ucSerialNumber[4];		
		
	}

#endif
			
#ifdef BUILDTYPE_PARENTUINT_PROJECT  //!!! need remove later !!!!
	if (g_FactorySaveData.ucSerialNumber[0] == 0xFF &&
		g_FactorySaveData.ucSerialNumber[1] == 0xFF &&
		g_FactorySaveData.ucSerialNumber[2] == 0xFF &&
		g_FactorySaveData.ucSerialNumber[3] == 0xFF &&
		g_FactorySaveData.ucSerialNumber[4] == 0xFF )
	{
			g_UserSaveData.ucSerialNumber[0] = 0x5A;
			g_UserSaveData.ucSerialNumber[1] = 0x4F;
			g_UserSaveData.ucSerialNumber[2] = 0x00;
			g_UserSaveData.ucSerialNumber[3] = 0x00;		
			g_UserSaveData.ucSerialNumber[4] = ucFlashUniqueID[7];	
	}
	else
	{
			g_UserSaveData.ucSerialNumber[0] = g_FactorySaveData.ucSerialNumber[0];
			g_UserSaveData.ucSerialNumber[1] = g_FactorySaveData.ucSerialNumber[1];
			g_UserSaveData.ucSerialNumber[2] = g_FactorySaveData.ucSerialNumber[2];
			g_UserSaveData.ucSerialNumber[3] = g_FactorySaveData.ucSerialNumber[3];
			g_UserSaveData.ucSerialNumber[4] = g_FactorySaveData.ucSerialNumber[4];		
		
	}	
#endif


	/* initial global variable */
	g_Data.uc10MSCount = 0;
	g_Data.uc500MSCount = 0;
	g_Data.uc1000MSCount = 0;
	
	//g_Data.ucPreSysState = g_Data.ucSysState = SYSSTATE_STANDBY;

	//g_Data.u64MySerialNumber = 	g_UserSaveData.ucSerialNumber[0]<<32|g_UserSaveData.ucSerialNumber[1]<<24|
	//							g_UserSaveData.ucSerialNumber[2]<<16|g_UserSaveData.ucSerialNumber[3]<<8|
	//							g_UserSaveData.ucSerialNumber[4];
	
	g_Data.ucPairingTryCount = 0;
	g_Data.ucAllocListIdx = 0;
	g_Data.ucAllocRFFreqCH = RF_PARENTUNIT_CH0;
	g_Data.bChangeToAllocRFFreqCH = qdt_false;

	
	g_Data.bUpdateFlashData = qdt_false;   	

	g_Data.bTwinkleGreenLED = qdt_false;
	g_Data.bTwinkleGreenLED = qdt_false;
	g_Data.bTwinkleOrangeLED =qdt_false;
	
    g_Data.bRFTransmitFinished = qdt_true;
	g_Data.bRFReceiveFinished= qdt_true;
	
	g_Data.bParentCalling = qdt_false;
	g_Data.bParentHandUp = qdt_true;
	g_Data.bAudidDockingAnswerCalling = qdt_false;
	g_Data.bAudidDockingHandUp = qdt_true;

	g_Data.ucWaitADHandupACKCount = 0;

	g_Data.bMicOpened = qdt_true;  //don't care initial value, because it need be set in Parent & AudioDouck initial procedure
	g_Data.bSPKOpened = qdt_true;  //don't care initial value, because it need be set in Parent & AudioDouck initial procedure

	g_Data.bNeedToBoardCastRFCmd = qdt_false;
	g_Data.ucBoardCastRFCmd = RFCMD_NON_CMD;

	g_Data.NoAudioDataComingCounter = 0; //count if audio data coming

	/* Alert function variable */
	g_Data.bAlertComing = qdt_false;
	g_Data.bHandleAlertStatus = qdt_false;


    /* audio volume initial value */
	g_Data.ucPreVol=8;
	g_Data.ucCurVol=8;	

	/*Night Light variable*/
	g_Data.u16NightLightTimerSecs = 0;
	g_Data.ucNightLightTimerStart = qdt_false;
	g_Data.ucNLTargetBrightLevel = 0;
	g_Data.ucNLCurrentBrightLevel = 0;
	//g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME;

	g_Data.bStopPollingRXChan = qdt_false;
	g_Data.ucCurRXChan = 0;

#ifdef BUILDTYPE_AUDIODOCKING_PROJECT  
	g_Data.bAllow2UsingBoardCastChan= qdt_true;
#endif
#ifdef BUILDTYPE_PARENTUINT_PROJECT 
	g_Data.bAllow2UsingBoardCastChan= qdt_false;
#endif

	g_Data.bDisablePTTButton = qdt_false;
        g_Data.ucSoundActivity = 0;
    g_Data.bAudioAlertFlag= qdt_false;
	g_Data.bVibrationEnable = qdt_false;
#ifdef BUILDTYPE_PARENTUINT_PROJECT 
	g_Data.bIsInPTTMode= qdt_false;
#endif
    g_Data.bSensorAlertMotion = qdt_false;
    g_Data.bFactoryResetFlag = qdt_false;
#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
	g_Data.ucAudioAlertTable[0] = 700000;
	g_Data.ucAudioAlertTable[1] = 800000;
	g_Data.ucAudioAlertTable[2] = 900000;
	g_Data.ucAudioAlertTable[3] = 1050000;
	g_Data.ucAudioAlertTable[4] = 1200000;

	g_Data.ucAudioAlertValue = g_Data.ucAudioAlertTable[g_UserSaveData.ucAudioAlertLevel];
#endif

	g_Data.ucUpgradeState = _QDT_UPGRADESTATE_WAITFORSTAR_;
	g_Data.bWaitParentAck = qdt_false;

	g_Data.bIsBatteryMode = qdt_false;
	g_Data.bIsQuietMode = qdt_false;
	g_Data.bUserCancelQuietMode = qdt_false;

    g_Data.bAudioAlertTrigger = qdt_false;
	g_Data.bDisableSleepMode = qdt_false;
	g_Data.bEnableSleepLED = qdt_false;
	
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Common_ParseRFPackage(qdt_uint8 * ucRXBuffer)
{
  	qdt_uint8 ucPayloadIndex = 0;
	qdt_uint8 ucRFPackageIndex = 0;
	qdt_uint8 ucQdtUIDType, ucRFSubCmd;	
	qdt_uint16 ucRFCmdDataLen = 0;
	qdt_uint8 ucBroadcastSN[5]= {0x00, 0x00, 0x00, 0x00, 0x00};
	//qdt_uint32 u32DestSerialNumber = 0;
	//qdt_uint32 u32SrcSerialNumber = 0;
	qdt_uint8 ucCheckSum = 0;
	signed short AudioRXBuffer[20];	

	ucCheckSum = QDT_Common_CheckSumCalc(ucRXBuffer, 15);

	if (ucCheckSum != ucRXBuffer[15]) //checksum error
	{
		//for(ucPayloadIndex=0; ucPayloadIndex<32; ucPayloadIndex++)
		//	printf("Dump RxData[%d] = %x\n", ucPayloadIndex, ucRXBuffer[ucPayloadIndex]);
		
		QDT_ERROR("QDT_Common_ParseRFPackage CheckSum error..\n");	
		return QDT_SYS_FAILED;
	}
	
	//u32DestSerialNumber = ucRXBuffer[QDT_RFPKT_DESTSN_START_IDX]<<24|ucRXBuffer[QDT_RFPKT_DESTSN_START_IDX+1]<<16|ucRXBuffer[QDT_RFPKT_DESTSN_START_IDX+2]<<8|ucRXBuffer[QDT_RFPKT_DESTSN_START_IDX+3];
	//u32SrcSerialNumber = ucRXBuffer[QDT_RFPKT_SRCSN_START_IDX]<<24|ucRXBuffer[QDT_RFPKT_SRCSN_START_IDX+1]<<16|ucRXBuffer[QDT_RFPKT_SRCSN_START_IDX+2]<<8|ucRXBuffer[QDT_RFPKT_SRCSN_START_IDX+3];
	ucQdtUIDType = ucRXBuffer[QDT_RFPKT_QDTUIDTYPE_START_IDX];
	ucRFSubCmd = ucRXBuffer[QDT_RFPKT_RFSUBCMD_START_IDX];
	ucRFCmdDataLen = ucRXBuffer[QDT_RFPKT_DATALENGTH_START_IDX]<<8|ucRXBuffer[QDT_RFPKT_DATALENGTH_START_IDX+1];
	
	if (ucQdtUIDType == QDTUID_TYPE_AUDIO && !g_Data.bHandleAlertStatus && !g_Data.bHumanVoice)
	{
		ucRFPackageIndex = 0;
		
		for(ucPayloadIndex = (QDT_RFPKT_HEAD_LENGTH+ucRFCmdDataLen); ucPayloadIndex < (QDT_RFPKT_HEAD_LENGTH+ucRFCmdDataLen+40); ucPayloadIndex=ucPayloadIndex+2)
		{	
			AudioRXBuffer[ucRFPackageIndex] = ucRXBuffer[ucPayloadIndex] | (ucRXBuffer[ucPayloadIndex+1]<<8) /*| (ucRXBuffer[4*ucIndex+2]<<16) | (ucRXBuffer[4*ucIndex+3]<<24)*/;
			ucRFPackageIndex++;
		}
		playAudio(AudioRXBuffer);

		g_Data.bDisablePTTButton = qdt_false;
		if (QDT_Common_GetDeviceType(&ucRXBuffer[QDT_RFPKT_SRCSN_START_IDX]) == DEVICE_TYPE_PARENTUNIT/*(u32SrcSerialNumber&0x80000000) == 0x80000000*/) //audio come from ParentUnit
		{
			g_Data.bDisablePTTButton = qdt_true;
		}

		if(ucRFSubCmd != RFCMD_NON_CMD && 
			(QDT_Common_CompareSN(&ucRXBuffer[QDT_RFPKT_DESTSN_START_IDX], &ucBroadcastSN[0]))||
			(QDT_Common_CompareSN(&ucRXBuffer[QDT_RFPKT_DESTSN_START_IDX], &g_UserSaveData.ucSerialNumber[0]))
			/*(u32DestSerialNumber == 0x00000000 || u32DestSerialNumber == g_Data.u64MySerialNumber) */) //check if RF packet sent to me, if not, Don't Handle it!!
			__qdt_DumpRFReceiveData(ucRXBuffer);													                                //0x00000000 is special Broadcast serial number
		
		
	}
	else if (ucQdtUIDType == QDTUID_TYPE_RFCMD)
	{
		//TO DOING: add  command into Cmdbuffer, waiting for main loop to handle command
		if(	(QDT_Common_CompareSN(&ucRXBuffer[QDT_RFPKT_DESTSN_START_IDX], &ucBroadcastSN[0]))||
			(QDT_Common_CompareSN(&ucRXBuffer[QDT_RFPKT_DESTSN_START_IDX], &g_UserSaveData.ucSerialNumber[0]))
			) //check if RF packet sent to me, if not, Don't Handle it!!
        	__qdt_DumpRFReceiveData(ucRXBuffer);                                                    //0x00000000 is special Broadcast serial number
	}
#ifdef BUILDTYPE_PARENTUINT_PROJECT 	
	else if (ucQdtUIDType == QDTUID_TYPE_UPGRADEFW)
	{
		//TO DOING: add  command into Cmdbuffer, waiting for main loop to handle command
		if(	(QDT_Common_CompareSN(&ucRXBuffer[QDT_RFPKT_DESTSN_START_IDX], &ucBroadcastSN[0]))||
			(QDT_Common_CompareSN(&ucRXBuffer[QDT_RFPKT_DESTSN_START_IDX], &g_UserSaveData.ucSerialNumber[0]))
			) //check if RF packet sent to me, if not, Don't Handle it!!
        {
        	__qdt_DumpRFReceiveData(ucRXBuffer);   
			if (ucRFSubCmd == RFCMD_AUDIODOCKING_UPGRADE_FWIMAGE)
				QDT_PU_DumpUpgradeImage(&ucRXBuffer[16]);
		}
	}
#endif	
	else if (ucQdtUIDType == QDTUID_TYPE_FACTORY)
	{
		if (ucRFSubCmd == RFCMD_FACTORYTEST_RFTEST)
		{	        	
			if (QDT_Factory_CheckRFTestPackage(&ucRXBuffer[0]))
				__qdt_DumpRFReceiveData(ucRXBuffer);      

			g_FactorySaveData.bIsInFactoryMode = qdt_true;
		}
		else if (ucRFSubCmd == RFACK_FACTORYTEST_RFTEST)
			QDT_Factory_CheckRFTestPackage(&ucRXBuffer[0]);
		else if (ucRFSubCmd == RFCMD_FACTORYTEST_BURNIN_SN || ucRFSubCmd == RFACK_FACTORYTEST_BURNIN_SN)
			__qdt_DumpRFReceiveData(ucRXBuffer);
	}
	else
	{
		QDT_ERROR("QDT_Common_ParseRFPackage ucQdtUIDType error :: %x\n", ucQdtUIDType);
		return QDT_SYS_FAILED;
	}
	
	return QDT_SYS_NOERROR;
}

void QDT_Common_VolumeControlCopy(signed short *pi16Src, signed short *pi16Dest, qdt_uint16 u16PcmCount)                // 0=< u8Vol <=16
{
        uint8_t u8Vol;
        uint16_t u16temp;
        int16_t i16PcmValue;

        u8Vol = g_Data.ucPreVol;

        for(u16temp=0; u16temp<u16PcmCount; u16temp++)
        {
                if((*pi16Src<=50)&&(*pi16Src>=-50))        //Change volume during zero crossing
                {
                      u8Vol = g_Data.ucCurVol;
                      g_Data.ucPreVol = g_Data.ucCurVol;
                }
                if((u8Vol & BIT4) > 0)
            		i16PcmValue= *pi16Src;
        	  else
        	  {
            		i16PcmValue= 0;
            		if((u8Vol & BIT3) > 0)
                		i16PcmValue= (*pi16Src/2) + i16PcmValue;
            		if((u8Vol & BIT2) > 0)
                		i16PcmValue= (*pi16Src/4) + i16PcmValue;
            		if((u8Vol & BIT1) > 0)
                		i16PcmValue= (*pi16Src/8) + i16PcmValue;
            		if((u8Vol & BIT0) > 0)
                		i16PcmValue= (*pi16Src/16) + i16PcmValue;
         	  }                      
         	  pi16Src++;
         	  *pi16Dest = i16PcmValue;
         	  pi16Dest++;    
        }
		
		g_UserSaveData.ucVolValue = g_Data.ucCurVol;
		
}

void QDT_Common_VolumeControl(qdt_sint16 *pi16Src, qdt_uint16 u16PcmCount)		   // 0=< u8Vol <=16
{
	qdt_uint8 u8Vol;
	qdt_uint16 u16temp;
	qdt_sint16 PcmValue;

	u8Vol = g_Data.ucPreVol;
	for(u16temp=0; u16temp<u16PcmCount; u16temp++)
	{
		
		if((*pi16Src<50)&&(*pi16Src>-50))  	//Change volume during zero crossing
		{
		
				u8Vol = g_Data.ucCurVol;
				g_Data.ucPreVol = g_Data.ucCurVol;
		}

		if((u8Vol & BIT4) > 0)
		 	PcmValue= *pi16Src;
		else
		{
			PcmValue= 0;
			if((u8Vol & BIT3) > 0)
		 		PcmValue= (*pi16Src/2) + PcmValue;
			if((u8Vol & BIT2) > 0)
		 		PcmValue= (*pi16Src/4) + PcmValue;
			if((u8Vol & BIT1) > 0)
		 		PcmValue= (*pi16Src/8) + PcmValue;
			if((u8Vol & BIT0) > 0)
		 		PcmValue= (*pi16Src/16) + PcmValue;
		}
		
		*pi16Src = PcmValue;	
		pi16Src++;
	}

}

void QDT_Common_VolumeUpDown(void)
{
	if(DrvGPIO_GetBit(GPA, 5) == qdt_false)
	{
		if (g_Data.ucPreVol<16)
			g_Data.ucCurVol++;
		QDT_TRACE("volume + u8CurVol %d\n",g_Data.ucCurVol);
	}
	if(DrvGPIO_GetBit(GPA, 6) == qdt_false)
	{
		if (g_Data.ucPreVol>0)
			g_Data.ucCurVol--;
		QDT_TRACE("volume -  u8CurVol	%d\n",g_Data.ucCurVol);
	}
}

RETURN_TYPE QDT_Common_SaveUserData(qdt_bool bIsInStandByMode)

{
	if (!bIsInStandByMode)
	{
		DrvGPIO_DisableInt(GPB, 7);  		    //disable RF-4461 interrupt Pin
		PARENTUNIT_RF_446x(qdt_OFF);   			//turn off RF-4461
		
		QDT_flash_WriteUserData(&g_UserSaveData);
		//QDT_flash_ReadUserData(&g_UserSaveData);
		QDT_Common_DelayTime(100);
		
		PARENTUNIT_RF_446x(qdt_ON);
 		vRadio_Init();	                							/* Initial RF-446x */
		DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
		if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
		{
			QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
#ifdef USER_SELECT_FREQ		
			QDT_Common_SetFreq(0);	   //change RF Freq
#endif				
		  	QDT_Common_DelayTime(100);
		   	vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		

		}
		else
		{
			QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
#ifdef USER_SELECT_FREQ		
			QDT_Common_SetFreq(g_UserSaveData.ucPairingFreqIdx);	   //change RF Freq
#endif
			QDT_Common_DelayTime(100);
			vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);	/* Set RF-466x into RX mode */		

		}
		
	}
	else
	{
		QDT_flash_WriteUserData(&g_UserSaveData);
	}
	
	return QDT_SYS_NOERROR;	
}

RETURN_TYPE QDT_Common_BuildRFCmdHeader(RFHEADER * pRFHeader, QdtRFSubCmd_e e_RFSubCmd, qdt_uint8 *ucDestSerialNumber)
{


	pRFHeader->ucQdtUIDType= QDTUID_TYPE_RFCMD;
	pRFHeader->ucRFSubCmd= e_RFSubCmd;
	pRFHeader->ucSrcSerialNumber[0] = g_UserSaveData.ucSerialNumber[0];
	pRFHeader->ucSrcSerialNumber[1] = g_UserSaveData.ucSerialNumber[1];
	pRFHeader->ucSrcSerialNumber[2] = g_UserSaveData.ucSerialNumber[2];
	pRFHeader->ucSrcSerialNumber[3] = g_UserSaveData.ucSerialNumber[3];
	pRFHeader->ucSrcSerialNumber[4] = g_UserSaveData.ucSerialNumber[4];
	
	if (__qdt_IsBroadcastRFSubCmd(e_RFSubCmd))  //if RFSubCmd need to broadcast, the destination serial number set to 0
	{
		pRFHeader->ucDestSerialNumber[0] = 0x00;
		pRFHeader->ucDestSerialNumber[1] = 0x00;
		pRFHeader->ucDestSerialNumber[2] = 0x00;
		pRFHeader->ucDestSerialNumber[3] = 0x00;		
		pRFHeader->ucDestSerialNumber[4] = 0x00;	
	}
	else
	{
		pRFHeader->ucDestSerialNumber[0] = ucDestSerialNumber[0];
		pRFHeader->ucDestSerialNumber[1] = ucDestSerialNumber[1];
		pRFHeader->ucDestSerialNumber[2] = ucDestSerialNumber[2];
		pRFHeader->ucDestSerialNumber[3] = ucDestSerialNumber[3];		
		pRFHeader->ucDestSerialNumber[4] = ucDestSerialNumber[4];	
	}	

	pRFHeader->ucDatalength[0] = 0x00;
	pRFHeader->ucDatalength[1] = 0x00;	
	pRFHeader->ucReserved = 0x00;	
	
	return QDT_SYS_NOERROR;	
}


RETURN_TYPE QDT_Common_SentRFCmd(RFHEADER *pRFHeader, qdt_uint8 *ucData)
{
	qdt_uint8 i = 0;
	qdt_uint16 u16Datalength = 0;

	if (!g_Data.bRFTransmitFinished)
	{
		QDT_TRACE("Last RFCmd doesn't finish!!.........return\n");
		return QDT_SYS_FAILED;
	}
	
	/*Build QDT Header Info*/
	pRadioConfiguration->Radio_Custom_Long_Payload[0] = pRFHeader->ucQdtUIDType;            //ucQdtUIDType
	pRadioConfiguration->Radio_Custom_Long_Payload[1] = pRFHeader->ucSrcSerialNumber[0];  	//ucSrcSerialNumber1
	pRadioConfiguration->Radio_Custom_Long_Payload[2] = pRFHeader->ucSrcSerialNumber[1];	//ucSrcSerialNumber2
	pRadioConfiguration->Radio_Custom_Long_Payload[3] = pRFHeader->ucSrcSerialNumber[2];	//ucSrcSerialNumber3
	pRadioConfiguration->Radio_Custom_Long_Payload[4] = pRFHeader->ucSrcSerialNumber[3];	//ucSrcSerialNumber4
	pRadioConfiguration->Radio_Custom_Long_Payload[5] = pRFHeader->ucSrcSerialNumber[4];	//ucSrcSerialNumber5	
	pRadioConfiguration->Radio_Custom_Long_Payload[6] = pRFHeader->ucRFSubCmd;; 			//ucRFSubCmd
	pRadioConfiguration->Radio_Custom_Long_Payload[7] = pRFHeader->ucDestSerialNumber[0]; 	//ucDestSerialNumber1
	pRadioConfiguration->Radio_Custom_Long_Payload[8] = pRFHeader->ucDestSerialNumber[1];	//ucDestSerialNumber2
	pRadioConfiguration->Radio_Custom_Long_Payload[9] = pRFHeader->ucDestSerialNumber[2]; 	//ucDestSerialNumber3
	pRadioConfiguration->Radio_Custom_Long_Payload[10] = pRFHeader->ucDestSerialNumber[3];	//ucDestSerialNumber4
	pRadioConfiguration->Radio_Custom_Long_Payload[11] = pRFHeader->ucDestSerialNumber[4];	//ucDestSerialNumber5	
	pRadioConfiguration->Radio_Custom_Long_Payload[12] = pRFHeader->ucDatalength[0];		//ucDatalength1
	pRadioConfiguration->Radio_Custom_Long_Payload[13] = pRFHeader->ucDatalength[1];;		//ucDatalength2
	pRadioConfiguration->Radio_Custom_Long_Payload[14] = pRFHeader->ucReserved;			//ucReserved1
	pRadioConfiguration->Radio_Custom_Long_Payload[15] = QDT_Common_CheckSumCalc(pRadioConfiguration->Radio_Custom_Long_Payload, 15);		//ucCheckSum
	
	u16Datalength = (pRFHeader->ucDatalength[0]<<8) | (pRFHeader->ucDatalength[1]);
	
	
	for (i=0; i<u16Datalength; i++)
	{
		pRadioConfiguration->Radio_Custom_Long_Payload[QDT_RFPKT_HEAD_LENGTH+i] = ucData[i];
	}
		
#if 0
for(i = 0; i < 32; i++)
{
  pRadioConfiguration->Radio_Custom_Long_Payload[i] = i;
 }

#endif		
#ifdef 	BUILDTYPE_PARENTUINT_PROJECT
		//g_Data.ucRFSentOutDelayTime--;
		//if (g_Data.ucRFSentOutDelayTime==0)
#endif		
		{  
			vRadio_StartTx(pRadioConfiguration->Radio_ChannelNumber, (U8 *) &pRadioConfiguration->Radio_Custom_Long_Payload[0u]);
			//g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME;
		}

	return QDT_SYS_NOERROR;	
	
}

RETURN_TYPE QDT_Common_SentRFAudio(RFHEADER *pRFHeader, qdt_uint8 *ucData)
{
  	qdt_uint8 ucPayloadIndex = 0;
  	qdt_uint8 ucEncodeBufferIndex = 0;
	qdt_uint16 ucRFCmdDataLen = 0;

	/*Build QDT Header Info*/
	pRadioConfiguration->Radio_Custom_Long_Payload[0] = QDTUID_TYPE_AUDIO;                 	//ucQdtUIDType
	pRadioConfiguration->Radio_Custom_Long_Payload[1] = pRFHeader->ucSrcSerialNumber[0];  	//ucSrcSerialNumber1
	pRadioConfiguration->Radio_Custom_Long_Payload[2] = pRFHeader->ucSrcSerialNumber[1];	//ucSrcSerialNumber2
	pRadioConfiguration->Radio_Custom_Long_Payload[3] = pRFHeader->ucSrcSerialNumber[2];	//ucSrcSerialNumber3
	pRadioConfiguration->Radio_Custom_Long_Payload[4] = pRFHeader->ucSrcSerialNumber[3];	//ucSrcSerialNumber4
	pRadioConfiguration->Radio_Custom_Long_Payload[5] = pRFHeader->ucSrcSerialNumber[4];	//ucSrcSerialNumber5
	pRadioConfiguration->Radio_Custom_Long_Payload[6] = pRFHeader->ucRFSubCmd;; 			//ucRFSubCmd
	pRadioConfiguration->Radio_Custom_Long_Payload[7] = pRFHeader->ucDestSerialNumber[0]; 	//ucDestSerialNumber1
	pRadioConfiguration->Radio_Custom_Long_Payload[8] = pRFHeader->ucDestSerialNumber[1];	//ucDestSerialNumber2
	pRadioConfiguration->Radio_Custom_Long_Payload[9] = pRFHeader->ucDestSerialNumber[2]; 	//ucDestSerialNumber3
	pRadioConfiguration->Radio_Custom_Long_Payload[10] = pRFHeader->ucDestSerialNumber[3];	//ucDestSerialNumber4
	pRadioConfiguration->Radio_Custom_Long_Payload[11] = pRFHeader->ucDestSerialNumber[4];	//ucDestSerialNumber6	
	pRadioConfiguration->Radio_Custom_Long_Payload[12] = pRFHeader->ucDatalength[0];		//ucDatalength1
	pRadioConfiguration->Radio_Custom_Long_Payload[13] = pRFHeader->ucDatalength[1];;		//ucDatalength2
	pRadioConfiguration->Radio_Custom_Long_Payload[14] = pRFHeader->ucReserved;			//ucReserved1
	pRadioConfiguration->Radio_Custom_Long_Payload[15] = QDT_Common_CheckSumCalc(pRadioConfiguration->Radio_Custom_Long_Payload, 15);		//ucCheckSum


	ucRFCmdDataLen = pRadioConfiguration->Radio_Custom_Long_Payload[QDT_RFPKT_DATALENGTH_START_IDX]<<8|pRadioConfiguration->Radio_Custom_Long_Payload[QDT_RFPKT_DATALENGTH_START_IDX+1];

  
 	for(ucPayloadIndex = (QDT_RFPKT_HEAD_LENGTH+ucRFCmdDataLen); ucPayloadIndex < (QDT_RFPKT_HEAD_LENGTH+ucRFCmdDataLen+40); ucPayloadIndex=ucPayloadIndex+2)
  	{
	  pRadioConfiguration->Radio_Custom_Long_Payload[ucPayloadIndex+1] = (s16Out_words[ucEncodeBufferIndex]>>8) & 0xFF;;
	  pRadioConfiguration->Radio_Custom_Long_Payload[ucPayloadIndex] = s16Out_words[ucEncodeBufferIndex] & 0xFF;;
	  ucEncodeBufferIndex++;
 	 }

  	vRadio_StartTx(pRadioConfiguration->Radio_ChannelNumber, (U8 *) &pRadioConfiguration->Radio_Custom_Long_Payload[0u]);
	
  	return TRUE;	
}

RETURN_TYPE QDT_Common_SetSyncWord(qdt_bool bSynWordType)
{
	qdt_uint8 ucSyncWordTable[8] = {0x11, 0x05, 0x00, 0x03, UNI_PAIRING_SYNCWORD_BYTE0, UNI_PAIRING_SYNCWORD_BYTE1, UNI_PAIRING_SYNCWORD_BYTE2, UNI_PAIRING_SYNCWORD_BYTE3};
   	/*
	if (bSynWordType == UNI_PAIRING_SYNCWORD)
		si446x_set_property(0x11, 0x05, 0x00, 0x03, UNI_PAIRING_SYNCWORD_BYTE0, UNI_PAIRING_SYNCWORD_BYTE1, UNI_PAIRING_SYNCWORD_BYTE2, UNI_PAIRING_SYNCWORD_BYTE3);
	else
		si446x_set_property(0x11, 0x05, 0x00, 0x03, g_UserSaveData.ucPairingSyncWord[0], g_UserSaveData.ucPairingSyncWord[1], g_UserSaveData.ucPairingSyncWord[2], g_UserSaveData.ucPairingSyncWord[3]);
	*/
	if (bSynWordType == PAIRING_SYNCWORD)
	{
		ucSyncWordTable[4] = g_UserSaveData.ucPairingSyncWord[0];
		ucSyncWordTable[5] = g_UserSaveData.ucPairingSyncWord[1];
		ucSyncWordTable[6] = g_UserSaveData.ucPairingSyncWord[2];
		ucSyncWordTable[7] = g_UserSaveData.ucPairingSyncWord[3];		
	}
	else
	{
		ucSyncWordTable[4] = UNI_PAIRING_SYNCWORD_BYTE0;
		ucSyncWordTable[5] = UNI_PAIRING_SYNCWORD_BYTE1;
		ucSyncWordTable[6] = UNI_PAIRING_SYNCWORD_BYTE2;
		ucSyncWordTable[7] = UNI_PAIRING_SYNCWORD_BYTE3;				
	}
		
	radio_comm_SendCmdGetResp(8, &ucSyncWordTable[0], 0, 0);
	return QDT_SYS_NOERROR;	
}

RETURN_TYPE QDT_Common_SetFreq(qdt_uint8 ucPairingFreqIdx)
{
	static qdt_uint8 ucRecPairingFreqIdx = 0xFF;
	if (ucPairingFreqIdx == ucRecPairingFreqIdx)
		return QDT_SYS_NOERROR;	
/*	
	si446x_set_property(ucPairingFreqTable[ucPairingFreqIdx][0], ucPairingFreqTable[ucPairingFreqIdx][1],
						ucPairingFreqTable[ucPairingFreqIdx][2], ucPairingFreqTable[ucPairingFreqIdx][3],
						ucPairingFreqTable[ucPairingFreqIdx][4], ucPairingFreqTable[ucPairingFreqIdx][5],
						ucPairingFreqTable[ucPairingFreqIdx][6], ucPairingFreqTable[ucPairingFreqIdx][7],
						ucPairingFreqTable[ucPairingFreqIdx][8], ucPairingFreqTable[ucPairingFreqIdx][9],
						ucPairingFreqTable[ucPairingFreqIdx][10], ucPairingFreqTable[ucPairingFreqIdx][11]);
	ucRecPairingFreqIdx = ucPairingFreqIdx;
	QDT_DEBUG("Set g_UserSaveData.ucPairingFreqIdx = %d\n", ucPairingFreqIdx);
	si446x_get_property(ucPairingFreqTable[ucPairingFreqIdx][0], ucPairingFreqTable[ucPairingFreqIdx][1],
						ucPairingFreqTable[ucPairingFreqIdx][2]);
*/
	if (ucPairingFreqIdx < RF_FREQ_TABLE_IDX_MAX)
    	radio_comm_SendCmdGetResp(12, (U8 *)&ucPairingFreqTable[ucPairingFreqIdx], 0, 0);
	else
		radio_comm_SendCmdGetResp(12, (U8 *)&ucFactoryFreqTable[0], 0, 0);
	
	QDT_DEBUG("Set g_UserSaveData.ucPairingFreqIdx = %d\n", ucPairingFreqIdx);

	return QDT_SYS_NOERROR;	
}


RETURN_TYPE QDT_Common_MicPause(void)
{
	uint8_t u8PdmaChClk;
	if (g_Data.bMicOpened == qdt_false) return QDT_SYS_NOERROR;
	u8PdmaChClk= PDMA->GCR.PDMA0_HCLK_EN;
	PDMA->GCR.PDMA0_HCLK_EN= u8PdmaChClk&(~BIT0);	   	//Pause, PDMA channel 1 (BIT0) clock disable
	g_Data.bMicOpened = qdt_false;
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Common_MicResume(void)
{
	uint8_t u8PdmaChClk;
	if (g_Data.bMicOpened == qdt_true) return QDT_SYS_NOERROR;
	u8PdmaChClk= PDMA->GCR.PDMA0_HCLK_EN;
	PDMA->GCR.PDMA0_HCLK_EN= u8PdmaChClk|BIT0;		//Resume, PDMA channel 1 (BIT0) clock enable
	g_Data.bMicOpened = qdt_true;
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Common_SPKPause(void)
{
	uint8_t u8PdmaChClk;
	if (g_Data.bSPKOpened == qdt_false || g_Data.bHumanVoice == qdt_true) return QDT_SYS_NOERROR;
	u8PdmaChClk= PDMA->GCR.PDMA1_HCLK_EN;
	PDMA->GCR.PDMA1_HCLK_EN= u8PdmaChClk&(~BIT1);	   	//Pause, PDMA channel 1 (BIT1) clock disable
	DrvDPWM_Disable();
	g_Data.bSPKOpened = qdt_false;
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Common_SPKResume(void)
{
	uint8_t u8PdmaChClk;
	if (g_Data.bSPKOpened == qdt_true) return QDT_SYS_NOERROR;
	u8PdmaChClk= PDMA->GCR.PDMA1_HCLK_EN;
	PDMA->GCR.PDMA1_HCLK_EN= u8PdmaChClk|BIT1;		//Resume, PDMA channel 1 (BIT1) clock enable
	DrvDPWM_Enable();
	g_Data.bSPKOpened = qdt_true;
	return QDT_SYS_NOERROR;
}

void QDT_Common_IsRFalive(void)
{
	static qdt_uint32 u32RFNIRQ_NoResponseCounter = 0;
	if (!DrvGPIO_GetBit(GPB, 7))
	{
		u32RFNIRQ_NoResponseCounter++;
		if (u32RFNIRQ_NoResponseCounter > 0xFFF)  // RF hangup, reset it.
		{
			QDT_ERROR("RF4461 crash, reset it \n");
			vRadio_Init();//init RF
			g_Data.bParentCalling = qdt_false;
			g_Data.bParentHandUp= qdt_true; 
			if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
			{
				QDT_Common_SetSyncWord(UNI_PAIRING_SYNCWORD);	
			}
			else
			{
				QDT_Common_SetSyncWord(PAIRING_SYNCWORD);						
			}
			
			QDT_Common_MicResume();
			g_Data.bRFTransmitFinished = qdt_true;
			g_Data.bRFReceiveFinished = qdt_true;				
		}
	}
	else
		u32RFNIRQ_NoResponseCounter = 0;
}

RETURN_TYPE QDT_Common_ScanUserKey(void)
{
#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
	if(__qdt_IsPressUserKey(GPIO_AUDIODOCKING_KEY_POWER))  
		QDT_AddKeyCmdData(_USER_KEY_AUDIODOCKING_KEY_POWER, NULL);
	else if(__qdt_IsPressUserKey(GPIO_AUDIODOCKING_KEY_VOLUP))
		QDT_AddKeyCmdData(_USER_KEY_AUDIODOCKING_KEY_VOLUP, NULL);
	else if(__qdt_IsPressUserKey(GPIO_AUDIODOCKING_KEY_VOLDOWN))
		QDT_AddKeyCmdData(_USER_KEY_AUDIODOCKING_KEY_VOLDOWN, NULL);
	else if(__qdt_IsPressUserKey(GPIO_AUDIODOCKING_KEY_PAIRING))
		QDT_AddKeyCmdData(_USER_KEY_AUDIODOCKING_KEY_PAIRING, NULL);
#endif

#ifdef BUILDTYPE_PARENTUINT_PROJECT
	if(__qdt_IsPressUserKey(GPIO_PARENTUNIT_KEY_POWER))  
		QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_POWER, NULL);
	else if(__qdt_IsPressUserKey(GPIO_PARENTUNIT_KEY_VOLUP))
		QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_VOLUP, NULL);
	else if(__qdt_IsPressUserKey(GPIO_PARENTUNIT_KEY_VOLDOWN))
		QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_VOLDOWN, NULL);
	//else if(__qdt_IsPressUserKey(GPIO_PARENTUNIT_KEY_LOWBATTERY))
	//	QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_LOWBATTERY, NULL);	
	else if(__qdt_IsPressUserKey(GPIO_PARENTUNIT_KEY_ALERT))
		QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_ALERT, NULL);	


	if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_ || g_bComboKeyState == 4 || g_FactorySaveData.bIsInFactoryMode)  	//scan PTT(pairing) key, only on Pairing state. 
	{																        //if pairing is done, PTT(pairing) key be used as PTT key
		if(__qdt_IsPressUserKey(GPIO_PARENTUNIT_KEY_PTT))
			QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_PTT, NULL);				
	}
#endif

	return QDT_SYS_NOERROR;
}
		
qdt_uint8 QDT_Common_CheckComboKey(qdt_uint8 ucUserKey)
{
	if (ucUserKey != _USER_KEY_NONE && ucUserKey >=80)
	{

		if (g_bComboKeyState == 0 && 
			(ucUserKey == _USER_KEY_AUDIODOCKING_KEY_VOLUP || 
			 ucUserKey == _USER_KEY_PARENTUNIT_KEY_VOLUP))
			g_bComboKeyState = 1;
		else if (g_bComboKeyState == 1 && 
			(ucUserKey == _USER_KEY_AUDIODOCKING_KEY_VOLDOWN || 
			 ucUserKey == _USER_KEY_PARENTUNIT_KEY_VOLDOWN))
			g_bComboKeyState = 2;
		else if (g_bComboKeyState == 2 && 
			(ucUserKey == _USER_KEY_AUDIODOCKING_KEY_VOLUP || 
			 ucUserKey == _USER_KEY_PARENTUNIT_KEY_VOLUP))
			g_bComboKeyState = 3;
		else if (g_bComboKeyState == 3 && 
			(ucUserKey == _USER_KEY_AUDIODOCKING_KEY_VOLDOWN || 
			 ucUserKey == _USER_KEY_PARENTUNIT_KEY_VOLDOWN))
			g_bComboKeyState = 4;
		else if (g_bComboKeyState == 4 && 
			(ucUserKey == _USER_KEY_AUDIODOCKING_KEY_POWER || 
			 ucUserKey == _USER_KEY_PARENTUNIT_KEY_POWER))
		{
			QDT_AddKeyCmdData(_USER_KEY_VERSION, NULL);
			ucUserKey = _USER_KEY_NONE;
			g_bComboKeyState = 0;
		}
		else if (g_bComboKeyState == 4 && 
			(ucUserKey == _USER_KEY_AUDIODOCKING_KEY_VOLUP || 
			 ucUserKey == _USER_KEY_PARENTUNIT_KEY_VOLUP))
		{
			QDT_Play_HV_SelectFrequency(g_UserSaveData.ucPairingFreqIdx);
			g_bComboKeyState = 0;	
		}
		else if (g_bComboKeyState == 4 && 
			(ucUserKey == _USER_KEY_AUDIODOCKING_KEY_PAIRING || 
			 ucUserKey == _USER_KEY_PARENTUNIT_KEY_PTT))
		{
			//QDT_AddKeyCmdData(_USER_KEY_ENTERFACTORY, NULL);		
			
			if (!g_FactoryData.bEnableFactoryMode)
				QDT_Factory_EnterFactoryMode();
			else
				QDT_Factory_ExitFactoryMode();

			ucUserKey = _USER_KEY_NONE;
			g_bComboKeyState = 0;	
		}		
		else 
			g_bComboKeyState = 0; 
	}

#if 0
	static qdt_uint8 ucIdx = 0;
	static qdt_uint8 ucComboKey[5] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
	static qdt_bool  bEnableComboKey = qdt_false;

	if (bEnableComboKey)
	{
		ucComboKey[ucIdx] = ucUserKey;
		ucIdx++;

		if 	(ucIdx >=5 &&	(ucComboKey[0] == _USER_KEY_AUDIODOCKING_KEY_VOLUP) &&
			  				(ucComboKey[0] == _USER_KEY_AUDIODOCKING_KEY_VOLDOWN) &&
			  				(ucComboKey[0] == _USER_KEY_AUDIODOCKING_KEY_VOLUP) &&
			  				(ucComboKey[0] == _USER_KEY_AUDIODOCKING_KEY_VOLDOWN) &&
			  				(ucComboKey[0] == _USER_KEY_AUDIODOCKING_KEY_POWER))
		{
			QDT_AddKeyCmdData(_USER_KEY_VERSION, NULL);
		}
		else
		{
		}
		
		
	}
	
	if (ucUserKey == _USER_KEY_AUDIODOCKING_KEY_VOLUP || ucUserKey == _USER_KEY_PARENTUNIT_KEY_VOLUP)
	{
		bEnableComboKey = qdt_true;
		ucIdx = 0;
		ucComboKey[ucIdx] = ucUserKey;
		ucIdx++;
	}
#endif

	
	return ucUserKey;
}


