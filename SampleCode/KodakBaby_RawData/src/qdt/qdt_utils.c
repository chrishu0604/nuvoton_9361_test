#include "bsp.h"
#include "radio.h"
#include "ISD9xx.h"
#include "Driver\DrvADC.h"
#include "Driver\DrvPDMA.h"
#include "Driver\DrvDPWM.h"
#include "Driver\DrvGPIO.h"
#include "Driver\DrvTIMER.h"


#include "qdt_ram.h"
#include "qdt_fifo.h"
#include "qdt_flash_hal.h"
#include "qdt_sysState.h"
#include "qdt_audiodocking.h"
#include "qdt_parentunit.h"
#include "CM36686Setup.h"

#include "qdt_utils.h"
#include "qdt_audiodocking_func.h"
#include "qdt_parentunit_func.h"

#include "si446x_api_lib.h"    //RF-664x header file



/*---------------------------------------------------------------------------------------------------------*/
/* Define global variables                                                                                 */
/*---------------------------------------------------------------------------------------------------------*/
UserSaveData g_UserSaveData;


/**************************************************************/
/* static  function                                                                                       */
/**************************************************************/
static qdt_bool qdt_IsPressUserKey(qdt_uint8 ucUserKey);

/**************************************************************/
/* extern function                                                                                       */
/**************************************************************/
extern void playAudio(signed short *AudioRXBuffer);
extern void SysTimerDelay(uint32_t us);

RETURN_TYPE QDT_Common_DelayTime(qdt_uint16 u16MilliSec)
{
	qdt_uint16 i;
	for(i=0; i < u16MilliSec; i++)
		SysTimerDelay(1000);	
	
	return QDT_SYS_NOERROR;
}


RETURN_TYPE QDT_Common_Init(void)
{
	UserSaveData t_UserSaveData;
	/* restore user data*/
	QDT_flash_ReadUserData(&t_UserSaveData);
	if (t_UserSaveData.ucMagicNumber[0] == 0xFF && t_UserSaveData.ucMagicNumber[1] == 0xFF)
	{
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_WAITFORSTAR_;		
		g_UserSaveData.ucPairingSyncWord[0] = UNI_SYNCWORD_BYTE0;
		g_UserSaveData.ucPairingSyncWord[1] = UNI_SYNCWORD_BYTE1;
		g_UserSaveData.ucPairingSyncWord[2] = UNI_SYNCWORD_BYTE2;
		g_UserSaveData.ucPairingSyncWord[3] = UNI_SYNCWORD_BYTE3;
		//g_Data.ucSysState = SYSSTATE_UNPAIRING;
	}
	else
	{
		g_UserSaveData.ucPairingState = t_UserSaveData.ucPairingState;		
		g_UserSaveData.ucPairingSyncWord[0] = t_UserSaveData.ucPairingSyncWord[0];
		g_UserSaveData.ucPairingSyncWord[1] = t_UserSaveData.ucPairingSyncWord[1];
		g_UserSaveData.ucPairingSyncWord[2] = t_UserSaveData.ucPairingSyncWord[2];
		g_UserSaveData.ucPairingSyncWord[3] = t_UserSaveData.ucPairingSyncWord[3];	

		//if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_FINISHED_)
		//	g_Data.ucSysState = SYSSTATE_RUNNING;
	}
	
	/* initial global variable */
	g_Data.ucSysState = SYSSTATE_STANDBY;

	g_Data.ucPairingTryCount = 0;
	g_Data.bUpdateUserSaveData = qdt_false;

	g_Data.bTwinkleGreenLED = qdt_false;
	g_Data.bTwinkleGreenLED = qdt_false;
	g_Data.bTwinkleOrangeLED =qdt_false;
	
    g_Data.bRFTransmitFinished = qdt_true;
	g_Data.bRFReceiveFinished= qdt_true;
	
	g_Data.bParentCalling = qdt_false;
	g_Data.bParentHandUp = qdt_true;
	g_Data.bAudidDockingAnswerCalling = qdt_false;
	g_Data.bAudidDockingHandUp = qdt_true;

	g_Data.bMicOpened = qdt_true;  //don't care initial value, because it need be set in Parent & AudioDouck initial procedure
	g_Data.bSPKOpened = qdt_true;  //don't care initial value, because it need be set in Parent & AudioDouck initial procedure



	/* Alert function variable */
	g_Data.bAlertComing = qdt_false;
	g_Data.bHandleAlertStatus = qdt_false;


    /* audio volume initial value */
	g_Data.ucPreVol=8;
	g_Data.ucCurVol=8;	


	g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME;


    /* initial qdt function */
	QDT_InitCmdBuffer();
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Common_ParseRFPackage(qdt_uint8 * ucRXBuffer)
{
  	qdt_uint8 ucIndex = 0;
	qdt_uint8 ucRFPackageIndex = 0;
	qdt_uint8 ucQdtUID;
  	qdt_uint8 ucPackageType;
  	qdt_uint8 ucSubCmd;  	
	signed short AudioRXBuffer[20];	

	ucQdtUID = ucRXBuffer[0];
	ucPackageType = ucRXBuffer[1];
	ucSubCmd = ucRXBuffer[2];

	if (ucQdtUID != QDTUID)
		return QDT_SYS_FAILED;   
	
	if (ucPackageType == QDT_RFPACKAGE_AUDIO && !g_Data.bHandleAlertStatus)
	{
		ucRFPackageIndex = 0;
		for(ucIndex = 4; ucIndex < 24; ucIndex++)  //handle audio package(package length is 42 bytes), combine 2 CHAR  into 1 SHORT SIGNED type. 
		{	
			AudioRXBuffer[ucRFPackageIndex] = ucRXBuffer[2*ucIndex] | (ucRXBuffer[2*ucIndex+1]<<8) /*| (ucRXBuffer[4*ucIndex+2]<<16) | (ucRXBuffer[4*ucIndex+3]<<24)*/;
			ucRFPackageIndex++;
		}
		playAudio(AudioRXBuffer);
		
	}
	else if (ucPackageType == QDT_RFPACKAGE_RFCOMMAND)
	{
		if (ucSubCmd == RFCMD_AUDIODOCKING_TX_SYNCWORD)
		{
			g_UserSaveData.ucPairingSyncWord[0] = ucRXBuffer[8];
			g_UserSaveData.ucPairingSyncWord[1] = ucRXBuffer[9];
			g_UserSaveData.ucPairingSyncWord[2] = ucRXBuffer[10];
			g_UserSaveData.ucPairingSyncWord[3] = ucRXBuffer[11];
		}
		//TO DOING: add  command into Cmdbuffer, waiting for main loop to handle command
        QDT_AddKeyCmdData(ucSubCmd);
	}
	else
	{
		return QDT_SYS_FAILED;
	}

	
#if 0	
	
	ucChannelType = ucRXBuffer[0];
	ucCommand = ucRXBuffer[1];
	
	if (ucChannelType == QDT_RFPACKAGE_AUDIO && !g_Data.bHandleAlertStatus)
	{
		ucRFPackageIndex = 0;
		for(ucIndex = 1; ucIndex < 21; ucIndex++)  //handle audio package(package length is 42 bytes), combine 2 CHAR  into 1 SHORT SIGNED type. 
		{	
			AudioRXBuffer[ucRFPackageIndex] = ucRXBuffer[2*ucIndex] | (ucRXBuffer[2*ucIndex+1]<<8) /*| (ucRXBuffer[4*ucIndex+2]<<16) | (ucRXBuffer[4*ucIndex+3]<<24)*/;
			ucRFPackageIndex++;
		}
		playAudio(AudioRXBuffer);
	}
	else if (ucChannelType == QDT_RFPACKAGE_COMMAND)
	{
		if (ucCommand == _USER_KEY_AUDIODOCKING_KEY_PAIRING)
		{
			qdt_uint8 i = 0;
			i++;
		}
		//TO DOING: add  command into Cmdbuffer, waiting for main loop to handle command
        QDT_AddKeyCmdData(ucCommand);
	}
	else
	{
		return QDT_SYS_FAILED;
	}
#endif	
	return QDT_SYS_NOERROR;
}


void QDT_Common_VolumeControlCopy(signed short *pi16Src, signed short *pi16Dest, qdt_uint16 u16PcmCount)                // 0=< u8Vol <=16
{
        uint8_t u8Vol;
        uint16_t u16temp;
        int16_t i16PcmValue;

        u8Vol = g_Data.ucPreVol;

        for(u16temp=0; u16temp<u16PcmCount; u16temp++)
        {
                if((*pi16Src<=50)&&(*pi16Src>=-50))        //Change volume during zero crossing
                {
                      u8Vol = g_Data.ucCurVol;
                      g_Data.ucPreVol = g_Data.ucCurVol;
                }
                if((u8Vol & BIT4) > 0)
            		i16PcmValue= *pi16Src;
        	  else
        	  {
            		i16PcmValue= 0;
            		if((u8Vol & BIT3) > 0)
                		i16PcmValue= (*pi16Src/2) + i16PcmValue;
            		if((u8Vol & BIT2) > 0)
                		i16PcmValue= (*pi16Src/4) + i16PcmValue;
            		if((u8Vol & BIT1) > 0)
                		i16PcmValue= (*pi16Src/8) + i16PcmValue;
            		if((u8Vol & BIT0) > 0)
                		i16PcmValue= (*pi16Src/16) + i16PcmValue;
         	  }                      
         	  pi16Src++;
         	  *pi16Dest = i16PcmValue;
         	  pi16Dest++;    
        }
}

void QDT_Common_VolumeControl(qdt_sint16 *pi16Src, qdt_uint16 u16PcmCount)		   // 0=< u8Vol <=16
{
	qdt_uint8 u8Vol;
	qdt_uint16 u16temp;
	qdt_sint16 PcmValue;

	u8Vol = g_Data.ucPreVol;
	for(u16temp=0; u16temp<u16PcmCount; u16temp++)
	{
		
		if((*pi16Src<50)&&(*pi16Src>-50))  	//Change volume during zero crossing
		{
		
				u8Vol = g_Data.ucCurVol;
				g_Data.ucPreVol = g_Data.ucCurVol;
		}

		if((u8Vol & BIT4) > 0)
		 	PcmValue= *pi16Src;
		else
		{
			PcmValue= 0;
			if((u8Vol & BIT3) > 0)
		 		PcmValue= (*pi16Src/2) + PcmValue;
			if((u8Vol & BIT2) > 0)
		 		PcmValue= (*pi16Src/4) + PcmValue;
			if((u8Vol & BIT1) > 0)
		 		PcmValue= (*pi16Src/8) + PcmValue;
			if((u8Vol & BIT0) > 0)
		 		PcmValue= (*pi16Src/16) + PcmValue;
		}
		
		*pi16Src = PcmValue;	
		pi16Src++;
	}

}

void QDT_AD_NightLEDOnOff(void)
{
	static uint8_t night_led_flag=0;

	if (CM36683_INT_Level() == qdt_false)
	 {
		read_CM36686_int_flag();
		if(g_Data.int_flat[1] == qdt_true)
		{
			if(night_led_flag == 0)
			{
				//PWMA->POE.PWM0=1;			
				DrvGPIO_ClrBit(GPA,12);
				night_led_flag = 1;
			}
			else
			{
				//PWMA->POE.PWM0=0;				
				DrvGPIO_SetBit(GPA,12);
				night_led_flag = 0;
			}
		}
	}
}

void QDT_Common_VolumeUpDown(void)
{
	if(DrvGPIO_GetBit(GPA, 5) == qdt_false)
	{
		if (g_Data.ucPreVol<16)
			g_Data.ucCurVol++;
		QDT_TRACE("volume + u8CurVol %d\n",g_Data.ucCurVol);
	}
	if(DrvGPIO_GetBit(GPA, 6) == qdt_false)
	{
		if (g_Data.ucPreVol>0)
			g_Data.ucCurVol--;
		QDT_TRACE("volume -  u8CurVol	%d\n",g_Data.ucCurVol);
	}
}

RETURN_TYPE QDT_Common_SentRFCmd(RFHEADER RFHeader, qdt_uint8 *ucData)
{
	qdt_uint8 i = 0;
	qdt_uint16 u16Datalength = 0;
	pRadioConfiguration->Radio_Custom_Long_Payload[0] = RFHeader.ucQdtUID;
	pRadioConfiguration->Radio_Custom_Long_Payload[1] = RFHeader.ucPackageType;
	pRadioConfiguration->Radio_Custom_Long_Payload[2] = RFHeader.ucRFSubCmd;
	pRadioConfiguration->Radio_Custom_Long_Payload[3] = RFHeader.ucHeader[0];
	pRadioConfiguration->Radio_Custom_Long_Payload[4] = RFHeader.ucHeader[1];
	pRadioConfiguration->Radio_Custom_Long_Payload[5] = RFHeader.ucDatalength[0];
	pRadioConfiguration->Radio_Custom_Long_Payload[6] = RFHeader.ucDatalength[1];
	pRadioConfiguration->Radio_Custom_Long_Payload[7] = RFHeader.ucReserved;	
	
	u16Datalength = (RFHeader.ucDatalength[0]<<8) | (RFHeader.ucDatalength[1]);
	
	for (i=0; i<u16Datalength; i++)
	{
		pRadioConfiguration->Radio_Custom_Long_Payload[8+i] = ucData[i];
	}
		
#ifdef 	BUILDTYPE_PARENTUINT_PROJECT
		g_Data.ucRFSentOutDelayTime--;
		if (g_Data.ucRFSentOutDelayTime==0)
#endif		
		{  
			//if (g_Data.bRFReceiveFinished)
				vRadio_StartTx(pRadioConfiguration->Radio_ChannelNumber, (U8 *) &pRadioConfiguration->Radio_Custom_Long_Payload[0u]);
			g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME;
		}

#if 0
	pRadioConfiguration->Radio_Custom_Long_Payload[0] = QDTUID;
	pRadioConfiguration->Radio_Custom_Long_Payload[1] = QDT_RFPACKAGE_RFCOMMAND;
	pRadioConfiguration->Radio_Custom_Long_Payload[2] = ucRFCmd;
#ifdef 	BUILDTYPE_PARENTUINT_PROJECT
	g_Data.ucRFSentOutDelayTime--;
	if (g_Data.ucRFSentOutDelayTime==0)
#endif		
	{  
	    //if (g_Data.bRFReceiveFinished)
			vRadio_StartTx(pRadioConfiguration->Radio_ChannelNumber, (U8 *) &pRadioConfiguration->Radio_Custom_Long_Payload[0u]);
		g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME;
	}
#endif
	return TRUE;
	
}
RETURN_TYPE QDT_Common_SetSyncWord(qdt_bool bSynWordType)
{
	if (bSynWordType == UNI_SYNCWORD)
		si446x_set_property(0x11, 0x05, 0x00, 0x03, UNI_SYNCWORD_BYTE0, UNI_SYNCWORD_BYTE1, UNI_SYNCWORD_BYTE2, UNI_SYNCWORD_BYTE3);
	else
		si446x_set_property(0x11, 0x05, 0x00, 0x03, g_UserSaveData.ucPairingSyncWord[0], g_UserSaveData.ucPairingSyncWord[1], g_UserSaveData.ucPairingSyncWord[2], g_UserSaveData.ucPairingSyncWord[3]);
	
	return QDT_SYS_NOERROR;	
}

RETURN_TYPE QDT_Common_MicPause(void)
{
	uint8_t u8PdmaChClk;
	u8PdmaChClk= PDMA->GCR.HCLK_EN;
	PDMA->GCR.HCLK_EN= u8PdmaChClk&(~BIT0);	   	//Pause, PDMA channel 1 (BIT0) clock disable
	g_Data.bMicOpened = qdt_false;
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Common_MicResume(void)
{
	uint8_t u8PdmaChClk;
	u8PdmaChClk= PDMA->GCR.HCLK_EN;
	PDMA->GCR.HCLK_EN= u8PdmaChClk|BIT0;		//Resume, PDMA channel 1 (BIT0) clock enable
	g_Data.bMicOpened = qdt_true;
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Common_SPKPause(void)
{
	uint8_t u8PdmaChClk;
	u8PdmaChClk= PDMA->GCR.HCLK_EN;
	PDMA->GCR.HCLK_EN= u8PdmaChClk&(~BIT1);	   	//Pause, PDMA channel 1 (BIT1) clock disable
	DrvDPWM_Disable();
	g_Data.bSPKOpened = qdt_false;
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Common_SPKResume(void)
{
	uint8_t u8PdmaChClk;
	u8PdmaChClk= PDMA->GCR.HCLK_EN;
	PDMA->GCR.HCLK_EN= u8PdmaChClk|BIT1;		//Resume, PDMA channel 1 (BIT1) clock enable
	DrvDPWM_Enable();
	g_Data.bSPKOpened = qdt_true;
	return QDT_SYS_NOERROR;
}
RETURN_TYPE QDT_AD_Parents_OnLine(void)
{
	RFHEADER RFHeader;
	qdt_uint8 ucSentOutData[10];
	
	QDT_TRACE("QDT_AD_Parents_OnLine....\n");
	g_Data.bParentUintOnLine= qdt_true;
	RFHeader.ucQdtUID = QDTUID;
	RFHeader.ucPackageType = QDT_RFPACKAGE_RFCOMMAND;
	RFHeader.ucRFSubCmd= RFCMD_AUDIODOCKING_ONLINE;
	RFHeader.ucHeader[0] = 0x00;
	RFHeader.ucHeader[1] = 0x00;
	RFHeader.ucDatalength[0] = 0x00;
	RFHeader.ucDatalength[1] = 0x00;
	RFHeader.ucReserved = 0x00; 					
	QDT_Common_SentRFCmd(RFHeader, ucSentOutData);	
	return QDT_SYS_NOERROR;
}


RETURN_TYPE QDT_AD_Parents_Calling(void)
{
	RFHEADER RFHeader;
	qdt_uint8 ucSentOutData[10];

	QDT_TRACE("QDT_AD_Parents_Calling....\n");
	g_Data.bParentCalling = qdt_true;
	g_Data.bParentHandUp= qdt_false;
	RFHeader.ucQdtUID = QDTUID;
	RFHeader.ucPackageType = QDT_RFPACKAGE_RFCOMMAND;
	RFHeader.ucRFSubCmd= RFACK_AUDIODOCKING_ANSWERCALLING;
	RFHeader.ucHeader[0] = 0x00;
	RFHeader.ucHeader[1] = 0x00;
	RFHeader.ucDatalength[0] = 0x00;
	RFHeader.ucDatalength[1] = 0x00;
	RFHeader.ucReserved = 0x00; 					
	QDT_Common_SentRFCmd(RFHeader, ucSentOutData);		
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_AD_Parents_HandUp(void)
{
	RFHEADER RFHeader;
	qdt_uint8 ucSentOutData[10];

	QDT_TRACE("QDT_AD_Parents_HandUp....\n");
	g_Data.bParentCalling = qdt_false;
	g_Data.bParentHandUp= qdt_true;	
	RFHeader.ucQdtUID = QDTUID;
	RFHeader.ucPackageType = QDT_RFPACKAGE_RFCOMMAND;
	RFHeader.ucRFSubCmd= RFACK_AUDIODOCKING_HANDUP;
	RFHeader.ucHeader[0] = 0x00;
	RFHeader.ucHeader[1] = 0x00;
	RFHeader.ucDatalength[0] = 0x00;
	RFHeader.ucDatalength[1] = 0x00;
	RFHeader.ucReserved = 0x00; 					
	QDT_Common_SentRFCmd(RFHeader, ucSentOutData);	

	g_Data.ucWaitingParentACKCount = WAITING_ACK_COUNT;
	return QDT_SYS_NOERROR;
}
RETURN_TYPE QDT_AD_Parents_ACK(void)
{
	QDT_TRACE("QDT_AD_Parents_ACK....\n");
	g_Data.ucWaitingParentACKCount = 0;
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_PU_AudioDocking_OnLine(void)
{
	QDT_TRACE("QDT_AudioDucking_OnLine....\n");
	g_Data.bAudioDockingOnLine = qdt_true;
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_PU_AudioDocking_AnswerCalling(void)
{
	QDT_TRACE("QDT_PU_AudioDocking_AnswerCalling....\n");
	g_Data.bAudidDockingAnswerCalling = qdt_true;	
	g_Data.bAudidDockingHandUp = qdt_false;
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_PU_AudioDocking_HandUp(void)
{
	QDT_TRACE("QDT_PU_AudioDocking_HandUp....\n");
	g_Data.bAudidDockingAnswerCalling = qdt_false;
	g_Data.bAudidDockingHandUp = qdt_true;	
	//QDT_Common_SentRFCmd(RFCMD_PARENTCOMMAND_HANDUP_ACK);
	return QDT_SYS_NOERROR;
}


void QDT_Common_IsRFalive(void)
{
	static qdt_uint32 u32RFNIRQ_NoResponseCounter = 0;
	if (!DrvGPIO_GetBit(GPB, 7))
	{
		u32RFNIRQ_NoResponseCounter++;
		if (u32RFNIRQ_NoResponseCounter > 0xFFF)  // RF hangup, reset it.
		{
			vRadio_Init();//init RF
			g_Data.bParentCalling = qdt_false;
			g_Data.bParentHandUp= qdt_true; 
			QDT_Common_MicResume();
			g_Data.bRFTransmitFinished = qdt_true;
			g_Data.bRFReceiveFinished = qdt_true;				
		}
	}
	else
		u32RFNIRQ_NoResponseCounter = 0;
}
RETURN_TYPE QDT_PFN_HandleAlertKey()
{
	g_Data.bHandleAlertStatus = qdt_true;
	PlayG722Stream(4);
	g_Data.bHandleAlertStatus = qdt_false;
	return TRUE;
}

static qdt_bool qdt_IsPressUserKey(qdt_uint8 ucUserKey)
{
	static qdt_uint32 u32RepeatTime = 0;
	static qdt_uint8 ucRecPreUserKey = _USER_KEY_NONE;

	if(!DrvGPIO_GetBit(GPA, ucUserKey))
	{
		if (ucUserKey == ucRecPreUserKey)
		{
			if (DrvTIMER_GetTicks(TMR0) > u32RepeatTime )  //handle repeat key
			{
					u32RepeatTime = DrvTIMER_GetTicks(TMR0)+1000;
					return qdt_true;
			}
			else
			{
				return qdt_false;
			}
		}
		else
		{
			u32RepeatTime = DrvTIMER_GetTicks(TMR0)+1000;
		}
		ucRecPreUserKey = ucUserKey;
		return qdt_true;	
	}
	return qdt_false;
}

RETURN_TYPE QDT_Common_ScanUserKey(void)
{
#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
		if(qdt_IsPressUserKey(GPIO_AUDIODOCKING_KEY_POWER))  
			QDT_AddKeyCmdData(_USER_KEY_AUDIODOCKING_KEY_POWER);
		else if(qdt_IsPressUserKey(GPIO_AUDIODOCKING_KEY_VOLUP))
			QDT_AddKeyCmdData(_USER_KEY_AUDIODOCKING_KEY_VOLUP);
		else if(qdt_IsPressUserKey(GPIO_AUDIODOCKING_KEY_VOLDOWN))
			QDT_AddKeyCmdData(_USER_KEY_AUDIODOCKING_KEY_VOLDOWN);
		else if(qdt_IsPressUserKey(GPIO_AUDIODOCKING_KEY_PAIRING))
			QDT_AddKeyCmdData(_USER_KEY_AUDIODOCKING_KEY_PAIRING);		
#endif

#ifdef BUILDTYPE_PARENTUINT_PROJECT
			if(qdt_IsPressUserKey(GPIO_PARENTUNIT_KEY_POWER))  
				QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_POWER);
			else if(qdt_IsPressUserKey(GPIO_PARENTUNIT_KEY_VOLUP))
				QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_VOLUP);
			else if(qdt_IsPressUserKey(GPIO_PARENTUNIT_KEY_VOLDOWN))
				QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_VOLDOWN);
			else if(qdt_IsPressUserKey(GPIO_PARENTUNIT_KEY_LOWBATTERY))
				QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_LOWBATTERY);		
			else if(qdt_IsPressUserKey(GPIO_PARENTUNIT_KEY_ALERT))
				QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_ALERT); 	


			if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)  	//scan PTT(pairing) key, only on Pairing state. 
			{																//if pairing is done, PTT(pairing) key be used as PTT key
				if(qdt_IsPressUserKey(GPIO_PARENTUNIT_KEY_PTT))
					QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_PTT);				
			}
#endif

	return QDT_SYS_NOERROR;
}
RETURN_TYPE QDT_AD_SysPowerOnOff(void)
{
	if (g_Data.ucSysState == SYSSTATE_STANDBY)  // standby to poweron
	{		
		AUDIODOCKING_LED_RED(qdt_OFF);
		AUDIODOCKING_LED_GREEN(qdt_ON);
		if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
		{
			g_Data.ucSysState = SYSSTATE_UNPAIRING;
			g_Data.bTwinkleRedLED = qdt_true;
			QDT_Common_SetSyncWord(UNI_SYNCWORD);	
		}
		else
		{
			g_Data.ucSysState = SYSSTATE_RUNNING;
			g_Data.bTwinkleRedLED = qdt_false;		
			QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
			QDT_Common_SPKPause(); 					//open SPK ouput		
			QDT_Common_MicResume(); 					//open MIC input		
		}
	}
	else										// poweron to standby
	{
		AUDIODOCKING_LED_RED(qdt_ON);
		AUDIODOCKING_LED_GREEN(qdt_ON);
		g_Data.ucSysState = SYSSTATE_STANDBY;	
		g_Data.bTwinkleRedLED = qdt_false;
		g_Data.bTwinkleGreenLED = qdt_false;
		QDT_ClearCmd();
	}
	
	return QDT_SYS_NOERROR;
}
RETURN_TYPE QDT_PU_SysPowerOnOff(void)
{
	if (g_Data.ucSysState == SYSSTATE_STANDBY)  // standby to poweron
	{		
		PARENTUNIT_LED_PWR_RED(qdt_OFF);
		PARENTUNIT_LED_PWR_GREEN(qdt_ON);
		if (g_UserSaveData.ucPairingState == _QDT_PAIRINGSTATE_WAITFORSTAR_)
		{
			g_Data.ucSysState = SYSSTATE_UNPAIRING;
			g_Data.bTwinkleRedLED = qdt_true;
			QDT_Common_SetSyncWord(UNI_SYNCWORD);	
		}
		else
		{
			g_Data.ucSysState = SYSSTATE_RUNNING;
			g_Data.bTwinkleRedLED = qdt_false;		
			QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
		}

	}
	else										// poweron to standby
	{
		PARENTUNIT_LED_PWR_RED(qdt_ON);
		PARENTUNIT_LED_PWR_GREEN(qdt_ON);
		PARENTUNIT_LED_ALERT_ORANGE(qdt_OFF);
		PARENTUNIT_LED_INTENSITY_G1(qdt_OFF);
		g_Data.ucSysState = SYSSTATE_STANDBY;	
		g_Data.bTwinkleRedLED = qdt_false;
		g_Data.bTwinkleGreenLED = qdt_false;
		g_Data.bTwinkleOrangeLED = qdt_false;
		QDT_ClearCmd();
	}
	
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Common_ProcessCmd(qdt_uint8 ucCmd)
{
	RFHEADER RFHeader;
	qdt_uint8 ucSentOutData[10];

	switch (ucCmd)
	{
	/* ======  RF Cmd =========================================== */
	case RFCMD_AUDIODOCKING_SEARCHING_DEVICE: 				/*ParentUint Pairing State 1*/
		RFHeader.ucQdtUID = QDTUID;
		RFHeader.ucPackageType = QDT_RFPACKAGE_RFCOMMAND;
		RFHeader.ucRFSubCmd= RFACK_PARENTUINT_IAMHERE;
		RFHeader.ucHeader[0] = 0x00;
		RFHeader.ucHeader[1] = 0x00;
		RFHeader.ucDatalength[0] = 0x00;
		RFHeader.ucDatalength[1] = 0x00;
		RFHeader.ucReserved = 0x00; 	
		
		g_Data.ucRFSentOutDelayTime = 1; //don't delay		
		QDT_Common_SentRFCmd(RFHeader, ucSentOutData); 	
		g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME; //don't delay
		break;
	case RFCMD_AUDIODOCKING_TX_SYNCWORD: 					/*ParentUint Pairing State 2*/
		QDT_Common_SetSyncWord(UNI_SYNCWORD);	
		
		RFHeader.ucQdtUID = QDTUID;
		RFHeader.ucPackageType = QDT_RFPACKAGE_RFCOMMAND;
		RFHeader.ucRFSubCmd= RFACK_PARENTUINT_GOT_SYNCWORD;
		RFHeader.ucHeader[0] = 0x00;
		RFHeader.ucHeader[1] = 0x00;
		RFHeader.ucDatalength[0] = 0x00;
		RFHeader.ucDatalength[1] = 0x00;
		RFHeader.ucReserved = 0x00; 		
		g_Data.ucRFSentOutDelayTime = 1; //don't delay
		QDT_Common_SentRFCmd(RFHeader, ucSentOutData); 		
		g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME; //don't delay
		
		QDT_Common_DelayTime(250); 
		QDT_AddKeyCmdData(RFCMD_AUDIODOCKING_TX_SYNCWORD); //resent command 
		g_Data.ucPairingTryCount++;
		if (g_Data.ucPairingTryCount > 10)
		{
			QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
			QDT_Common_DelayTime(250);			
			g_Data.ucPairingTryCount = 0;
		}		
		break;
	case RFCMD_AUDIODOCKING_TESTING_SYNCWORD: 				/*ParentUint Pairing State 3*/
		QDT_Common_SetSyncWord(PAIRING_SYNCWORD);	
		QDT_ClearCmd();
		
		RFHeader.ucQdtUID = QDTUID;
		RFHeader.ucPackageType = QDT_RFPACKAGE_RFCOMMAND;
		RFHeader.ucRFSubCmd= RFACK_PARENTUINT_USING_SYNCWORD;
		RFHeader.ucHeader[0] = 0x00;
		RFHeader.ucHeader[1] = 0x00;
		RFHeader.ucDatalength[0] = 0x00;
		RFHeader.ucDatalength[1] = 0x00;
		RFHeader.ucReserved = 0x00; 	
		
		g_Data.ucRFSentOutDelayTime = 1; //don't delay
		QDT_Common_SentRFCmd(RFHeader, ucSentOutData); 		
		g_Data.ucRFSentOutDelayTime = RF_SENTOUT_DELAY_TIME; //don't delay
		
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_FINISHED_;
		g_Data.ucPairingTryCount = 0;
		g_Data.ucSysState = SYSSTATE_RUNNING;
		g_Data.bTwinkleRedLED = qdt_false;
		g_Data.bTwinkleGreenLED = qdt_false;	

		QDT_Common_SPKResume(); 					//open SPK ouput		
		QDT_flash_WriteUserData(&g_UserSaveData);
		PARENTUNIT_LED_PWR_RED(qdt_OFF);
		PARENTUNIT_LED_PWR_GREEN(qdt_ON);		
		break;		

	case RFACK_PARENTUINT_IAMHERE: 				/*AudioDocking Pairing State 1*/
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_TX_SYNCWORD_;
		break;
	case RFACK_PARENTUINT_GOT_SYNCWORD: 					/*AudioDocking Pairing State 2*/
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_TESTING_SYNCWORD_;	
		break;
	case RFACK_PARENTUINT_USING_SYNCWORD: 				/*AudioDocking Pairing State 3*/
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_FINISHED_;
		break;	

	case RFCMD_PARENTCOMMAND_ONLINE:
		QDT_AD_Parents_OnLine();
		break;
	case RFCMD_PARENTCOMMAND_CALLING:
		QDT_AD_Parents_Calling();
		QDT_Common_SPKResume();
		QDT_Common_MicPause();
		break;
	case RFCMD_PARENTCOMMAND_HANDUP:
		QDT_AD_Parents_HandUp();
		QDT_Common_SPKPause();
		QDT_Common_MicResume();
		break;		
	case RFCMD_PARENTCOMMAND_HANDUP_ACK:
		break;		
	case RFCMD_AUDIODOCKING_ONLINE:
		break;
	case RFACK_AUDIODOCKING_ANSWERCALLING:	
		QDT_PU_AudioDocking_AnswerCalling();
		QDT_Common_MicResume();
		QDT_Common_SPKPause();
		break;	
	case RFACK_AUDIODOCKING_HANDUP:	
		QDT_PU_AudioDocking_HandUp();
		QDT_Common_MicPause();
		QDT_Common_SPKResume();			
		break;			

	/* ======  User Key =========================================== */
	case _USER_KEY_AUDIODOCKING_KEY_POWER:
		QDT_AD_SysPowerOnOff();
		break;		
	case _USER_KEY_PARENTUNIT_KEY_POWER:
		QDT_PU_SysPowerOnOff();
		break;
	case _USER_KEY_AUDIODOCKING_KEY_PAIRING:
		QDT_Common_SetSyncWord(UNI_SYNCWORD);	
		g_Data.ucSysState = SYSSTATE_UNPAIRING;
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_SEARCHING_;
		g_Data.bTwinkleRedLED = qdt_false;
		AUDIODOCKING_LED_RED(qdt_OFF);
		g_Data.bTwinkleGreenLED = qdt_true;
		break;
		
	case _USER_KEY_PARENTUNIT_KEY_PTT:
		QDT_Common_SetSyncWord(UNI_SYNCWORD);	
		g_Data.ucSysState = SYSSTATE_UNPAIRING;
		g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_SEARCHING_;
		g_Data.bTwinkleRedLED = qdt_false;
		PARENTUNIT_LED_PWR_RED(qdt_OFF);
		g_Data.bTwinkleGreenLED = qdt_true;
		break;		
		
	case _USER_KEY_AUDIODOCKING_KEY_VOLUP:	
	case _USER_KEY_PARENTUNIT_KEY_VOLUP:
		if (g_Data.ucPreVol<16)
			g_Data.ucCurVol++;
		if (g_Data.ucCurVol>16)
			g_Data.ucCurVol = 16;
#if 0	
	{
		qdt_uint8 Pro2Cmd[256];
		qdt_bool bExit;
		qdt_uint16 i;
		
		for(i=0;i<=255;i++)
			Pro2Cmd[i] = 0xfe;
		
		QDT_flash_EarseSector(0x000000);	
		QDT_flash_WriteBytes(0x000000, 256, Pro2Cmd);	
		for(i=0;i<=255;i++)
			Pro2Cmd[i] = 0;		
		QDT_flash_ReadBytes(0x000000, 256, Pro2Cmd);	
		
   		bExit = 1;
	}
#endif		
		QDT_DEBUG("Volume UP g_Data.ucCurVol = %d\n",g_Data.ucCurVol);
		break;
	case _USER_KEY_AUDIODOCKING_KEY_VOLDOWN:	
    case _USER_KEY_PARENTUNIT_KEY_VOLDOWN:
		if (g_Data.ucPreVol>0)
			g_Data.ucCurVol--;		
		QDT_DEBUG("Volume DOWN g_Data.ucCurVol = %d\n",g_Data.ucCurVol);
		break;
	case _USER_KEY_PARENTUNIT_KEY_ALERT:
		QDT_PFN_HandleAlertKey();
		break;
	default:
		break;			
	}
	return QDT_SYS_NOERROR;
}

RETURN_TYPE QDT_Product_SyncWord(void)
{
	qdt_uint8 ucFlashUniqueID[8];
	QDT_flash_ReadUniqueID(ucFlashUniqueID);
	g_UserSaveData.ucPairingSyncWord[0] = ucFlashUniqueID[0];
	g_UserSaveData.ucPairingSyncWord[1] = ucFlashUniqueID[2];
	g_UserSaveData.ucPairingSyncWord[2] = ucFlashUniqueID[4];
	g_UserSaveData.ucPairingSyncWord[3] = ucFlashUniqueID[6];
	return QDT_SYS_NOERROR;
}

qdt_bool QDT_AD_SearchParentUints_Device(void)
{
	RFHEADER RFHeader;
	qdt_uint8 ucSentOutData[10];
	switch (g_UserSaveData.ucPairingState)
	{
	case _QDT_PAIRINGSTATE_WAITFORSTAR_:
		QDT_Product_SyncWord();
		break;
	case _QDT_PAIRINGSTATE_SEARCHING_:
		RFHeader.ucQdtUID = QDTUID;
		RFHeader.ucPackageType = QDT_RFPACKAGE_RFCOMMAND;
		RFHeader.ucRFSubCmd= RFCMD_AUDIODOCKING_SEARCHING_DEVICE;
		RFHeader.ucHeader[0] = 0x00;
		RFHeader.ucHeader[1] = 0x00;
		RFHeader.ucDatalength[0] = 0x00;
		RFHeader.ucDatalength[1] = 0x00;
		RFHeader.ucReserved = 0x00;		
		g_Data.ucRFSentOutDelayTime = 1; //don't delay
		QDT_Common_SentRFCmd(RFHeader, ucSentOutData);
		break;
	case _QDT_PAIRINGSTATE_TX_SYNCWORD_:
		g_Data.ucRFSentOutDelayTime = 1; //don't delay
		
		RFHeader.ucQdtUID = QDTUID;
		RFHeader.ucPackageType = QDT_RFPACKAGE_RFCOMMAND;
		RFHeader.ucRFSubCmd= RFCMD_AUDIODOCKING_TX_SYNCWORD;
		RFHeader.ucHeader[0] = 0x00;
		RFHeader.ucHeader[1] = 0x00;
		RFHeader.ucDatalength[0] = 0x00;
		RFHeader.ucDatalength[1] = 0x04;
		RFHeader.ucReserved = 0x00;			
		ucSentOutData[0] = g_UserSaveData.ucPairingSyncWord[0];
		ucSentOutData[1] = g_UserSaveData.ucPairingSyncWord[1];
		ucSentOutData[2] = g_UserSaveData.ucPairingSyncWord[2];
		ucSentOutData[3] = g_UserSaveData.ucPairingSyncWord[3];
		QDT_Common_SentRFCmd(RFHeader, ucSentOutData);

		g_Data.ucPairingTryCount++;
		if (g_Data.ucPairingTryCount > 10)
		{
			g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_SEARCHING_;
			g_Data.ucPairingTryCount = 0;
		}
		break;
	case _QDT_PAIRINGSTATE_TESTING_SYNCWORD_:
		QDT_Common_SetSyncWord(PAIRING_SYNCWORD);		
		QDT_Common_DelayTime(100); //100ms		
		g_Data.ucRFSentOutDelayTime = 1; //don't delay

		RFHeader.ucQdtUID = QDTUID;
		RFHeader.ucPackageType = QDT_RFPACKAGE_RFCOMMAND;
		RFHeader.ucRFSubCmd= RFCMD_AUDIODOCKING_TESTING_SYNCWORD;
		RFHeader.ucHeader[0] = 0x00;
		RFHeader.ucHeader[1] = 0x00;
		RFHeader.ucDatalength[0] = 0x00;
		RFHeader.ucDatalength[1] = 0x00;
		RFHeader.ucReserved = 0x00;			
		QDT_Common_SentRFCmd(RFHeader, ucSentOutData);		

	  	g_Data.ucPairingTryCount++;
		if (g_Data.ucPairingTryCount > 10)
		{
			g_UserSaveData.ucPairingState = _QDT_PAIRINGSTATE_SEARCHING_;
			QDT_Common_SetSyncWord(UNI_SYNCWORD);		
			g_Data.ucPairingTryCount = 0;
		}		
		break;	
	case _QDT_PAIRINGSTATE_FINISHED_:
		g_Data.ucPairingTryCount = 0;
		g_Data.ucSysState = SYSSTATE_RUNNING;
		g_Data.bTwinkleRedLED = qdt_false;
		g_Data.bTwinkleGreenLED = qdt_false;	

		QDT_flash_WriteUserData(&g_UserSaveData);
		AUDIODOCKING_LED_RED(qdt_OFF);
		AUDIODOCKING_LED_GREEN(qdt_ON);
		return qdt_true;	
	default:
		break;			
	}
	return qdt_false;
}


