#include <stdio.h>
#include <string.h>
#include "bsp.h"
#include "qdt_flash_hal.h"
#include "Driver\DrvGPIO.h"
#include "Driver\DrvQSPI.h"
#include "Driver\DrvPDMA.h"
#include "qdt_ram.h"

#define FLASH_SPI_OUTPUT_PORT 0

/*SPI Register Definition*/
#define _WINBOND_EARSE_SECTOR_ 		0x20
#define _WINBOND_PAGE_PROGRAM_ 		0x02
#define _WINBOND_READ_DATA_ 		0x03
#define _WINBOND_WRITE_DISABLE_ 	0x04
#define _WINBOND_WRITE_ENABLE_ 		0x06
#define _WINBOND_READ_STATUS_R1_	0x05
#define _WINBOND_READ_STATUS_R2_	0x35

extern BOOL DrvSPI_SingleWrite(E_DRVSPI_PORT eSpiPort, uint32_t *pu32Data);
extern BOOL DrvSPI_SingleRead(E_DRVSPI_PORT eSpiPort, uint32_t *pu32Data);


static void __qdt_flash_hal_EnableCS(void);
static void __qdt_flash_hal_DisableCS(void);

static void __qdt_flash_hal_SpiWriteByte(qdt_uint8 byteToWrite);
static qdt_uint8 __qdt_flash_hal_SpiReadByte(void);

static void __qdt_flash_hal_SpiWriteData(qdt_uint32 byteCount, qdt_uint8* pData);
static void __qdt_flash_hal_SpiReadData(qdt_uint32 byteCount, qdt_uint8* pData);

static void __qdt_flash_delay(void);

static void __qdt_flash_delay(void)
{
	qdt_uint32 loopCount;
	for (loopCount = 2500; loopCount != 0; loopCount--);		
}

#if 0
qdt_bool DrvSPI_Flash_SingleRead(E_DRVSPI_PORT eSpiPort, qdt_uint32 *pu32Data)
{
	qdt_uint32 loopCount;

	if(FLASH_SPI_PORT[eSpiPort]->CNTRL.GO_BUSY==1)
		return qdt_false;

	FLASH_SPI_PORT[eSpiPort]->CNTRL.GO_BUSY = 1;
	for (loopCount = 50; loopCount != 0; loopCount--); 
	*pu32Data = FLASH_SPI_PORT[eSpiPort]->RX[FLASH_SPI_OUTPUT_PORT];
	
	return qdt_true;
}

qdt_bool DrvSPI_Flash_SingleWrite(E_DRVSPI_PORT eSpiPort, qdt_uint32 *pu32Data)
{
	if(FLASH_SPI_PORT[eSpiPort]->CNTRL.GO_BUSY==1)
		return qdt_false;
	FLASH_SPI_PORT[eSpiPort]->TX[FLASH_SPI_OUTPUT_PORT] = *pu32Data;
	FLASH_SPI_PORT[eSpiPort]->CNTRL.GO_BUSY = 1;	
	return qdt_true;
}

BOOL DrvSPI_Flash_SingleReadWrite(E_DRVSPI_PORT eSpiPort, qdt_uint32 *pu32Data, qdt_uint32 pu32DataIn)
{
	if(FLASH_SPI_PORT[eSpiPort]->CNTRL.GO_BUSY==1)
		return qdt_false;
	FLASH_SPI_PORT[eSpiPort]->TX[FLASH_SPI_OUTPUT_PORT] = pu32DataIn;
	*pu32Data = FLASH_SPI_PORT[eSpiPort]->RX[FLASH_SPI_OUTPUT_PORT];
	FLASH_SPI_PORT[eSpiPort]->CNTRL.GO_BUSY = 1;
	return qdt_true;
}
#endif
static void __qdt_flash_hal_EnableCS(void)
{
	DrvGPIO_ClrBit(GPA, 2);	
}

static void __qdt_flash_hal_DisableCS(void)
{
	qdt_uint32 loopCount;
    for (loopCount = 255; loopCount != 0; loopCount--);	
	DrvGPIO_SetBit(GPA, 2);	
}


static void __qdt_flash_hal_SpiWriteByte(qdt_uint8 byteToWrite)
{
  	uint32_t SentByte;
	SentByte = byteToWrite & 0x000000FF;
    //for (loopCount = 10; loopCount != 0; loopCount--);		
  	while(!DrvSPI_SingleWrite(eDRVSPI_PORT0,&SentByte));
}

static qdt_uint8 __qdt_flash_hal_SpiReadByte(void)
{
	uint32_t retByte = 0x00;
	while(!DrvSPI_SingleRead(eDRVSPI_PORT0,&retByte));
	
	return retByte;
}

static void __qdt_flash_hal_SpiWriteData(qdt_uint32 byteCount, qdt_uint8* pData)
{
	//__qdt_flash_hal_EnableCS();
	
	while (byteCount--) {
	  __qdt_flash_hal_SpiWriteByte(*pData++);
	}
	
	//__qdt_flash_hal_DisableCS();
}

static void __qdt_flash_hal_SpiReadData(qdt_uint32 byteCount, qdt_uint8* pData)
{
	//__qdt_flash_hal_EnableCS();
	DrvSPI_ClrRxFifo(eDRVSPI_PORT0); //clear dummy rx data
	DrvSPI_SetRxTransCnt(eDRVSPI_PORT0, 1, byteCount);
	DrvSPI_EnableRxMode(eDRVSPI_PORT0);
	while (byteCount--) {
	  *pData++ = __qdt_flash_hal_SpiReadByte();
	}
	DrvSPI_DisableRxMode(eDRVSPI_PORT0);	
	//__qdt_flash_hal_DisableCS();
}

void QDT_flash_EarseSector(qdt_uint32 ucAddr)
{
	qdt_uint8 Pro2Cmd[4];
	qdt_bool bEarseFinshed = qdt_false;

	Pro2Cmd[0] = _WINBOND_WRITE_ENABLE_;
	__qdt_flash_hal_EnableCS();
	__qdt_flash_hal_SpiWriteData(1, Pro2Cmd);
	__qdt_flash_hal_DisableCS();

	Pro2Cmd[0] = _WINBOND_EARSE_SECTOR_;
	Pro2Cmd[1] = ucAddr>>16;
	Pro2Cmd[2] = ucAddr>>8;
	Pro2Cmd[3] = ucAddr;
	__qdt_flash_hal_EnableCS();
	__qdt_flash_hal_SpiWriteData(4, Pro2Cmd);
	__qdt_flash_hal_DisableCS();
	
	while(!bEarseFinshed)
	{
		Pro2Cmd[0] = _WINBOND_READ_STATUS_R1_;
		__qdt_flash_hal_EnableCS();
		__qdt_flash_hal_SpiWriteData(1, Pro2Cmd);
		__qdt_flash_hal_SpiReadData(1, Pro2Cmd);	
		__qdt_flash_hal_DisableCS();
		if ((Pro2Cmd[0]&0x01) == 0x00) 
			bEarseFinshed = qdt_true;
	}		
	
	Pro2Cmd[0] = _WINBOND_WRITE_DISABLE_;
	__qdt_flash_hal_EnableCS();
	__qdt_flash_hal_SpiWriteData(1, Pro2Cmd);	
	__qdt_flash_hal_DisableCS();
	

}
void QDT_flash_WriteBytes(qdt_uint32 u32Addr, qdt_uint32 ucWriteCount, qdt_uint8 *pucWriteData)
{
	qdt_uint8 Pro2Cmd[4];
	qdt_bool bWriteFinshed = qdt_false;
	
	Pro2Cmd[0] = _WINBOND_WRITE_ENABLE_;
	__qdt_flash_hal_EnableCS();
	__qdt_flash_hal_SpiWriteData(1, Pro2Cmd);	
	__qdt_flash_hal_DisableCS();
	
	Pro2Cmd[0] = _WINBOND_PAGE_PROGRAM_;
	Pro2Cmd[1] = u32Addr>>16;
	Pro2Cmd[2] = u32Addr>>8;
	Pro2Cmd[3] = u32Addr;

	__qdt_flash_hal_EnableCS();
	__qdt_flash_hal_SpiWriteData(4, Pro2Cmd);
	__qdt_flash_hal_SpiWriteData(ucWriteCount, pucWriteData);
	__qdt_flash_hal_DisableCS();


	while(!bWriteFinshed)
	{
		Pro2Cmd[0] = _WINBOND_READ_STATUS_R1_;
		__qdt_flash_hal_EnableCS();
		__qdt_flash_hal_SpiWriteData(1, Pro2Cmd);
		__qdt_flash_hal_SpiReadData(1, Pro2Cmd);	
		__qdt_flash_hal_DisableCS();
		if ((Pro2Cmd[0]&0x01) == 0x00) 
			bWriteFinshed = qdt_true;
	}	
	__qdt_flash_delay();

	Pro2Cmd[0] = _WINBOND_WRITE_DISABLE_;
	__qdt_flash_hal_EnableCS();
	__qdt_flash_hal_SpiWriteData(1, Pro2Cmd);	
	__qdt_flash_hal_DisableCS();


	
}
void QDT_flash_ReadBytes(qdt_uint32 u32Addr, qdt_uint32 u32ReadCount, qdt_uint8 *pucReadData)
{
	qdt_uint8 Pro2Cmd[4];
	Pro2Cmd[0] = 0x03;
	Pro2Cmd[1] = u32Addr>>16;
	Pro2Cmd[2] = u32Addr>>8;
	Pro2Cmd[3] = u32Addr;

	__qdt_flash_hal_EnableCS();
	__qdt_flash_hal_SpiWriteData(4, Pro2Cmd);
	__qdt_flash_hal_SpiReadData(u32ReadCount, pucReadData);
	__qdt_flash_delay();
	__qdt_flash_hal_DisableCS();
}

void QDT_flash_ReadUniqueID(qdt_uint8 *pucUniqueID)
{
	qdt_uint8 Pro2Cmd[8];
	Pro2Cmd[0] = 0x4B;
	Pro2Cmd[1] = 0x01;	//dummy
	Pro2Cmd[2] = 0x02;	//dummy
	Pro2Cmd[3] = 0x03;	//dummy	
	Pro2Cmd[4] = 0x04;	//dummy	
	__qdt_flash_hal_EnableCS();
	__qdt_flash_hal_SpiWriteData(5, Pro2Cmd);
	__qdt_flash_hal_SpiReadData(8, pucUniqueID);
	__qdt_flash_delay();
	__qdt_flash_hal_DisableCS();

}
void QDT_flash_InitUserData(void)  //waiting for implement
{
	qdt_uint8 ucReadData[512];
	QDT_flash_ReadBytes(FLASH_USERDATA_ADDR, 2, ucReadData);

	if (ucReadData[0] == 0xFF && ucReadData[1] == 0xFF)
	{
		;
	}
	
}

void QDT_flash_ReadUserData(UserSaveData *pUserSaveData)
{
	qdt_uint8 ucReadData[512];

	QDT_TRACE("QDT_flash_ReadUserData\n");
	
	QDT_flash_ReadBytes(FLASH_USERDATA_ADDR, sizeof(UserSaveData), ucReadData);
	memcpy(pUserSaveData, ucReadData ,sizeof(UserSaveData));
}

void QDT_flash_WriteUserData(UserSaveData *pUserSaveData)
{
	qdt_uint8 ucWriteData[512];
	qdt_uint16 u16Size;

	QDT_TRACE("QDT_flash_WriteUserData\n");
	
	g_Data.bUpdateFlashData = qdt_true;
	u16Size = sizeof(UserSaveData);
	memcpy(ucWriteData, pUserSaveData , sizeof(UserSaveData));

	ucWriteData[0]	=	pUserSaveData->ucMagicNumber[0] = 0x55;				
	ucWriteData[1]	=	pUserSaveData->ucMagicNumber[1] = 0xAA;	
	
	QDT_flash_EarseSector(FLASH_USERDATA_ADDR);

	if (u16Size < FLASH_PAGE_SIZE)
	{
		QDT_flash_WriteBytes(FLASH_USERDATA_ADDR, u16Size, ucWriteData);
	}
	else
	{
		QDT_flash_WriteBytes(FLASH_USERDATA_ADDR, FLASH_PAGE_SIZE, ucWriteData);
		QDT_flash_WriteBytes(FLASH_USERDATA_ADDR+FLASH_PAGE_SIZE, u16Size-FLASH_PAGE_SIZE, &ucWriteData[256]);
	}
	g_Data.bUpdateFlashData = qdt_false;

}


void QDT_flash_ClearUserData(void)
{
	QDT_TRACE("QDT_flash_ClearUserData\n");
	g_Data.bUpdateFlashData = qdt_true;
	QDT_flash_EarseSector(FLASH_USERDATA_ADDR);
	g_Data.bUpdateFlashData = qdt_false;
}


void QDT_flash_ReadFactoryData(FactorySaveData *pFactorySaveData)
{
	qdt_uint8 ucReadData[64];

	QDT_TRACE("QDT_flash_ReadFactoryData\n");
	
	QDT_flash_ReadBytes(FLASH_FACTORYDATA_ADDR, sizeof(FactorySaveData), ucReadData);
	memcpy(pFactorySaveData, ucReadData ,sizeof(FactorySaveData));
}

void QDT_flash_WriteFactoryData(FactorySaveData *pFactorySaveData)
{
	qdt_uint8 ucWriteData[64];
	qdt_uint16 u16Size;

	QDT_TRACE("QDT_flash_WriteFactoryData\n");
	
	g_Data.bUpdateFlashData = qdt_true;
	u16Size = sizeof(FactorySaveData);
	memcpy(ucWriteData, pFactorySaveData , sizeof(FactorySaveData));

	ucWriteData[0]	=	pFactorySaveData->ucMagicNumber[0] = 0xEE;				
	ucWriteData[1]	=	pFactorySaveData->ucMagicNumber[1] = 0xBB;	
	
	QDT_flash_EarseSector(FLASH_FACTORYDATA_ADDR);

	if (u16Size < FLASH_PAGE_SIZE)
	{
		QDT_flash_WriteBytes(FLASH_FACTORYDATA_ADDR, u16Size, ucWriteData);
	}
	else
	{
		QDT_flash_WriteBytes(FLASH_FACTORYDATA_ADDR, FLASH_PAGE_SIZE, ucWriteData);
		QDT_flash_WriteBytes(FLASH_FACTORYDATA_ADDR+FLASH_PAGE_SIZE, u16Size-FLASH_PAGE_SIZE, &ucWriteData[64]);
	}
	g_Data.bUpdateFlashData = qdt_false;

}


void QDT_flash_ClearFactoryData(void)
{
	QDT_TRACE("QDT_flash_ClearFactoryData\n");
	g_Data.bUpdateFlashData = qdt_true;
	QDT_flash_EarseSector(FLASH_FACTORYDATA_ADDR);
	g_Data.bUpdateFlashData = qdt_false;
}

void QDT_flash_WriteImage(qdt_uint32 u32Addr, qdt_uint32 ucWriteCount, qdt_uint8 *pucWriteData)
{

	QDT_TRACE("QDT_flash_WriteImage\n");
	
	//QDT_flash_EarseSector(u32Addr);
	QDT_flash_WriteBytes(u32Addr, ucWriteCount, pucWriteData);
	
}

