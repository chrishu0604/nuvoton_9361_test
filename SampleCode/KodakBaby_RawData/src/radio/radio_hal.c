/*!
 * File:
 *  radio_hal.c
 *
 * Description:
 *  This file contains RADIO HAL.
 *
 * Silicon Laboratories Confidential
 * Copyright 2011 Silicon Laboratories, Inc.
 */

                /* ======================================= *
                 *              I N C L U D E              *
                 * ======================================= */

#include "bsp.h"
#include "radio_hal.h"
#include "Driver\DrvGPIO.h"
#include "Driver\DrvQSPI.h"
#include "Driver\DrvPDMA.h"


                /* ======================================= *
                 *          D E F I N I T I O N S          *
                 * ======================================= */

                /* ======================================= *
                 *     G L O B A L   V A R I A B L E S     *
                 * ======================================= */

                /* ======================================= *
                 *      L O C A L   F U N C T I O N S      *
                 * ======================================= */

                /* ======================================= *
                 *     P U B L I C   F U N C T I O N S     *
                 * ======================================= */
extern BOOL DrvSPI_SingleWrite(E_DRVSPI_PORT eSpiPort, uint32_t *pu32Data);
extern BOOL DrvSPI_SingleRead(E_DRVSPI_PORT eSpiPort, uint32_t *pu32Data);

void radio_hal_AssertShutdown(void)
{
	DrvGPIO_SetBit(GPA, 15);	
/*#if (defined SILABS_PLATFORM_RFSTICK) || (defined SILABS_PLATFORM_LCDBB) || (defined SILABS_PLATFORM_WMB)
  RF_PWRDN = 1;
#else
  PWRDN = 1;
#endif*/ //Jack mask
}

void radio_hal_DeassertShutdown(void)
{
	DrvGPIO_ClrBit(GPA, 15);
/*#if (defined SILABS_PLATFORM_RFSTICK) || (defined SILABS_PLATFORM_LCDBB) || (defined SILABS_PLATFORM_WMB)
  RF_PWRDN = 0;
#else
  PWRDN = 0;
#endif*/ //Jack mask
}

void radio_hal_ClearNsel(void)
{
	int loopCount;
    //for (loopCount = 255; loopCount != 0; loopCount--);	
	DrvGPIO_ClrBit(GPB, 1);	
    //RF_NSEL = 0;//Jack mask
}

void radio_hal_SetNsel(void)
{
	int loopCount;
    //for (loopCount = 255; loopCount != 0; loopCount--);	
	DrvGPIO_SetBit(GPB, 1);	
    //RF_NSEL = 1;//Jack mask
}

BIT radio_hal_NirqLevel(void)
{
	BIT retValue;
    retValue = (BIT )DrvGPIO_GetBit(GPB, 7);
	return retValue;
    //return RF_NIRQ;//Jack mask
}

void radio_hal_SpiWriteByte(U8 byteToWrite)
{
    uint32_t SentByte;
	SentByte = byteToWrite & 0x000000FF;
	//DrvSPI_SingleWrite(eDRVSPI_PORT0,&SentByte);
    while(!DrvSPI_SingleWrite(eDRVSPI_PORT0,&SentByte));
	//au8SrcArray[0] = byteToWrite;
	//DrvSPI_StartPDMA(eDRVSPI_PORT0, eDRVSPI_TX_DMA, TRUE);
	//DrvPDMA_CHEnablelTransfer(eDRVPDMA_CHANNEL_1);	
}

U8 radio_hal_SpiReadByte(void)	
{
	uint32_t retByte = 0x00;
	while(!DrvSPI_SingleRead(eDRVSPI_PORT0,&retByte));
	return retByte;//(U8)(au8DestArray[0]);
}

void radio_hal_SpiWriteData(U8 byteCount, U8* pData)
{
	while (byteCount--) {
	  radio_hal_SpiWriteByte(*pData++);
	}
	
#if 0 
#if (defined SILABS_PLATFORM_RFSTICK) || (defined SILABS_PLATFORM_LCDBB) || (defined SILABS_PLATFORM_WMB)
  vSpi_WriteDataSpi1(byteCount, pData);
#else
  SpiWriteData(byteCount, pData);
#endif
#endif
}

void radio_hal_SpiReadData(U8 byteCount, U8* pData)
{
	
	DrvSPI_ClrRxFifo(eDRVSPI_PORT0); //clear dummy rx data
	DrvSPI_SetRxTransCnt(eDRVSPI_PORT0, 1, byteCount);
	DrvSPI_EnableRxMode(eDRVSPI_PORT0);

	while (byteCount--) {	  	
	  *pData++ = radio_hal_SpiReadByte();
	}

	DrvSPI_DisableRxMode(eDRVSPI_PORT0);
#if 0
#if (defined SILABS_PLATFORM_RFSTICK) || (defined SILABS_PLATFORM_LCDBB) || (defined SILABS_PLATFORM_WMB)
  vSpi_ReadDataSpi1(byteCount, pData);
#else
  SpiReadData(byteCount, pData);
#endif
#endif
}

#ifdef RADIO_DRIVER_EXTENDED_SUPPORT
BIT radio_hal_Gpio0Level(void)
{
  BIT retVal = FALSE;

#ifdef SILABS_PLATFORM_DKMB
  retVal = FALSE;
#endif
#ifdef SILABS_PLATFORM_UDP
  retVal = EZRP_RX_DOUT;
#endif
#if (defined SILABS_PLATFORM_RFSTICK) || (defined SILABS_PLATFORM_LCDBB)
  retVal = RF_GPIO0;
#endif
#if (defined SILABS_PLATFORM_WMB930)
  retVal = FALSE;
#endif
#if defined (SILABS_PLATFORM_WMB912)
  #ifdef SILABS_IO_WITH_EXTENDER
    //TODO
    retVal = FALSE;
  #endif
#endif

  return retVal;
}

BIT radio_hal_Gpio1Level(void)
{
  BIT retVal = FALSE;

#ifdef SILABS_PLATFORM_DKMB
  retVal = FSK_CLK_OUT;
#endif
#ifdef SILABS_PLATFORM_UDP
  retVal = FALSE; //No Pop
#endif
#if (defined SILABS_PLATFORM_RFSTICK) || (defined SILABS_PLATFORM_LCDBB) || (defined SILABS_PLATFORM_WMB930)
  retVal = RF_GPIO1;
#endif
#if defined (SILABS_PLATFORM_WMB912)
  #ifdef SILABS_IO_WITH_EXTENDER
    //TODO
    retVal = FALSE;
  #endif
#endif

  return retVal;
}

BIT radio_hal_Gpio2Level(void)
{
  BIT retVal = FALSE;

#ifdef SILABS_PLATFORM_DKMB
  retVal = DATA_NFFS;
#endif
#ifdef SILABS_PLATFORM_UDP
  retVal = FALSE; //No Pop
#endif
#if (defined SILABS_PLATFORM_RFSTICK) || (defined SILABS_PLATFORM_LCDBB) || (defined SILABS_PLATFORM_WMB930)
  retVal = RF_GPIO2;
#endif
#if defined (SILABS_PLATFORM_WMB912)
  #ifdef SILABS_IO_WITH_EXTENDER
    //TODO
    retVal = FALSE;
  #endif
#endif

  return retVal;
}

BIT radio_hal_Gpio3Level(void)
{
  BIT retVal = FALSE;

#if (defined SILABS_PLATFORM_RFSTICK) || (defined SILABS_PLATFORM_LCDBB) || (defined SILABS_PLATFORM_WMB930)
  retVal = RF_GPIO3;
#elif defined (SILABS_PLATFORM_WMB912)
  #ifdef SILABS_IO_WITH_EXTENDER
    //TODO
    retVal = FALSE;
  #endif
#endif

  return retVal;
}

#endif
