#include <stdio.h>
#include <string.h>
#include "..\Lib\libNuG711.h"
#include "g711_func.h"


extern void g711_linear2alaw(short *speech, char *bitstream, unsigned short u16Len)
{
	int i;
    
    for(i=0;i<(u16Len);i++)
	{	
		*bitstream++ = (char)linear2alaw(*speech++);  
	}
}

extern void g711_linear2ulaw(short *speech, char *bitstream, unsigned short u16Len)
{
	int i;
    
    for(i=0;i<(u16Len);i++)
	{	
		*bitstream++ = (char)linear2ulaw(*speech++);  
	}
}

extern void g711_alaw2linear(char *bitstream,short *speech, unsigned short u16Len)
{
																									 
	int i;

	for(i=0;i<(u16Len);i++)
	{	
        *speech++ = alaw2linear(*bitstream++); 
	}
}

extern void g711_ulaw2linear(char *bitstream,short *speech, unsigned short u16Len)
{
																									 
	int i;

	for(i=0;i<(u16Len);i++)
	{	
        *speech++ = ulaw2linear(*bitstream++); 
	}
}

extern void g711_alaw2ulaw(char *alaw_val, char *ulaw_val, unsigned short u16Len)
{
																									 
	int i;

	for(i=0;i<(u16Len);i++)
	{	
        *ulaw_val++ = alaw2ulaw(*alaw_val++); 
	}
}

extern void g711_ulaw2alaw(char *ulaw_val, char *alaw_val, unsigned short u16Len)
{
																									 
	int i;

	for(i=0;i<(u16Len);i++)
	{	
        *alaw_val++ = ulaw2alaw(*ulaw_val++); 
	}
}








