#ifdef SUPPORT_SLEEP_MODE
#include <stdio.h>
#include "ISD93xx.h"
#include "Driver\DrvGPIO.h"
#include "Driver\DrvRTC.h"
#include "Driver\DrvUART.h"
#include "Driver\DrvSYS.h"
#include "Driver\DrvTimer.h"
#include "PMU.h"


#include "qdt_utils.h"
#include "qdt_ram.h"
#include "qdt_parentunit.h"
#include "qdt_parentunit_func.h"



#define SYSTICK_ENABLE              0
#define SYSTICK_CLKSOURCE           2
#define SCB_SCR_SLEEPDEEP           0x14

#define RTC_TLR_ADDR	0x4000800C
#define RTC_CLR_ADDR	0x40008010
#define RTC_TAR_ADDR	0x4000801C
#define RTC_CAR_ADDR	0x40008020


extern void SysTimerDelay(uint32_t us);

void PMU_TickISR(void)		
{
   //u32Timer0Cnt++;
  //printf("Tick Int!\n");
  // Disable RTC Tick Interrupt
  // DrvRTC_DisableInt(DRVRTC_TICK_INT);
}

void PMU_Alarm_ISR(void)
{
	//QDT_PU_SysExitSleep();
	QDT_PU_SysWakeup();
	QDT_ERROR("enter PMU_Alarm_ISR\n");
}


void PMU_RTCInit(void)
{
	if (DrvRTC_Init()!=E_SUCCESS)
	{
		QDT_ERROR("DrvRTC_Init failed\n");
	}
	
	DrvRTC_EnableWakeUp();	
	RTC->RIIR.AI 		= 1; 
  	RTC->RIIR.TI 		= 1; 
  	RTC->RIER.TIER 		= 0; 
  	RTC->RIER.AIER 		= 1;	

	DrvRTC_SetTickMode(DRVRTC_TICK_1_SEC);
	
	//DrvRTC_EnableInt(DRVRTC_TICK_INT, PMU_TickISR);
	DrvRTC_EnableInt(DRVRTC_ALARM_INT, PMU_Alarm_ISR);
}

void PMU_RTCClose(void)
{
	DrvRTC_DisableInt(DRVRTC_ALARM_INT);
	DrvRTC_Close();
}
void PMU_SetCurrentTime(uint32_t u32TLR_Time)
{
	DrvRTC_WriteEnable();		//only active for 200ms
	outpw(RTC_TLR_ADDR, u32TLR_Time);	  //efected after 60us
	SysTimerDelay(60);		//wait writint to RTC IP
	SysTimerDelay(60);		//wait loading back to RTC->TLR
}
uint32_t u32Counter;

void PMU_Init(void)
{

	SYSCLK->APBCLK.SBRAM_EN = 1;		//enable StandBy Ram[256] Will be reset by SPD wakeup	 
	u32Counter= SBRAM->D[0];
	
	// Standby power down will also has RSTS.PMU, RSTS.POR and RSTS.PAD flag 
	// POR or reset also has RSTS.PMU flag
	if(SYSCLK->PFLAGCON.PD_FLAG == 0)		//Check with Standby PD flag for first power on, PD_FLAG will be 1 if ISD9160 wakeup from standby PD
	{
		SYSCLK->APBCLK.RTC_EN = 1;			 //Enable RTC clock 
		SBRAM->D[0]=0;
		u32Counter=0;
	}
	SYSCLK->PFLAGCON.PD_FLAG = 1;		//Clear the Standby PD flag 
	
/*	
	PMU_RTCInit();

	PMU_SetCurrentTime(0x00180000); 		//18:00:00
	PMU_SetAlarmTime(0x00180030);			//18:00:05	
	
	//while(u32Counter<20)
	if (u32Counter < 2)
	{
		printf("SBRAM->D[0] = %lx\n", SBRAM->D[0]);
		u32Counter++;
		SBRAM->D[0]=u32Counter;
	
		//if((u32Counter&1)==1)  
		//{
		//DrvGPIO_SetBit(GPA,12);
		//}else{
		//DrvGPIO_ClrBit(GPA,12);
		//}	  
		SysTimerDelay(1000);
		PMU_StandbyPowerDown();
		//while(u32Timer0Cnt==0);	  
	}
	
	//PARENTUNIT_LED_PWR_RED(qdt_OFF);
	//PARENTUNIT_LED_PWR_GREEN(qdt_ON);	
*/	
	
}


void PMU_EnterStandbyPowerDown(void)
{
	PMU_RTCInit();
	PMU_SetCurrentTime(0x00180000); 		//18:00:00
	PMU_SetAlarmTime(0x00180100);			//18:01:00	
	u32Counter++;
	SBRAM->D[STBRAM_IDX_STBCOUNTER] = u32Counter;   //record tmp counter
	SBRAM->D[STBRAM_IDX_SYSSTATE] = g_Data.ucSysState;
	SBRAM->D[STBRAM_IDX_ALERTEVENT] = g_Data.bAudioAlertFlag;
	QDT_PU_SysStanbByPowerDown();
	QDT_DEBUG("enter sleep mode :: STBRAM_IDX_STBCOUNTER = %x\n", SBRAM->D[STBRAM_IDX_STBCOUNTER]);
	QDT_DEBUG("enter sleep mode :: STBRAM_IDX_SYSSTATE = %x\n", SBRAM->D[STBRAM_IDX_SYSSTATE]);
	QDT_DEBUG("enter sleep mode :: STBRAM_IDX_ALERTEVENT = %x\n", SBRAM->D[STBRAM_IDX_ALERTEVENT]);
	SysTimerDelay(1000);
	PMU_StandbyPowerDown();
}

void PMU_SetAlarmTime(uint32_t u32TAR_Time)
{
	uint32_t u32TempCLR, u32TempTLR;

 	u32TempCLR=inpw(RTC_CLR_ADDR);
	u32TempTLR=inpw(RTC_TLR_ADDR);
	
	DrvRTC_WriteEnable();	  			//only active for 200ms
	outpw(RTC_TAR_ADDR, u32TAR_Time); 	//efected after 60us, can not be interrupted by other RTC write
	SysTimerDelay(60);
	if(u32TempTLR >= u32TAR_Time)				//if current time is over alarm time
		outpw(RTC_CAR_ADDR, (u32TempCLR+1));	//alarm date+1
	else
		outpw(RTC_CAR_ADDR, u32TempCLR);	//keep same date
	SysTimerDelay(120);
}
void PMU_StandbyPowerDown(void)
{
	uint32_t u32Reg = 0;
	UNLOCKREG();

#if 0
	SCB->SCR = SCB_SCR_SLEEPDEEP;	 	
	SYSCLK->PWRCON.STANDBY_PD = 1; //Go into Standby Power Down	upon WFI/WFE command
	SYSCLK->PWRCON.STOP = 0;	//Don't go into Stop mode upon WFI/WFE command
	SYSCLK->PWRCON.DEEP_PD = 0;	// Don't go into Deep Power Down upon WFI/WFE command
#else
	u32Reg = inpw(0x50000220);
	QDT_DEBUG("1 u32Reg= %x\n", u32Reg);
	QDT_DEBUG("SYSCLK->CLKSLEEP.CPU_EN = %x\n", SYSCLK->CLKSLEEP.CPU_EN);


	SYSCLK->CLKSLEEP.CPU_EN = 1; //0          
	SYSCLK->CLKSLEEP.PDMA_EN = 0; //1         
	SYSCLK->CLKSLEEP.ISP_EN = 0;    //2       
	//SYSCLK->CLKSLEEP.RESERVE0 = 0;  //3     
	SYSCLK->CLKSLEEP.WDG_EN = 0;	//4          
	SYSCLK->CLKSLEEP.RTC_EN = 1;	//5          
	SYSCLK->CLKSLEEP.TMR0_EN = 0;	//6        
	SYSCLK->CLKSLEEP.TMR1_EN = 0;	//7        
	SYSCLK->CLKSLEEP.I2C0_EN = 0;	//8        
	//SYSCLK->CLKSLEEP.RESERVE1:3;	//9-11     
	SYSCLK->CLKSLEEP.SPI0_EN = 0;	//12       
	SYSCLK->CLKSLEEP.DPWM_EN = 0;	//13       
	//SYSCLK->CLKSLEEP.RESERVE2:2;	//14-15    
	SYSCLK->CLKSLEEP.UART0_EN = 1;	//16       
	//SYSCLK->CLKSLEEP.RESERVE3 = 0;	//17     
	SYSCLK->CLKSLEEP.BIQALC_EN = 0;	//18     
	SYSCLK->CLKSLEEP.CRC_EN = 0;	//19         
	SYSCLK->CLKSLEEP.PWM01_EN = 0;	//20       
	SYSCLK->CLKSLEEP.PWM23_EN = 0;	//21       
	SYSCLK->CLKSLEEP.ACMP_EN = 0;	//22       
	//SYSCLK->CLKSLEEP.RESERVE5:3;	//23-25    
	SYSCLK->CLKSLEEP.SBRAM_EN = 0;	//26       
	//SYSCLK->CLKSLEEP.RESERVE6 = 0;	//27     
	SYSCLK->CLKSLEEP.ADC_EN = 0;	//28         
	SYSCLK->CLKSLEEP.I2S_EN = 0;	//29         
	SYSCLK->CLKSLEEP.ANA_EN = 0;	//30         
	SYSCLK->CLKSLEEP.PWM45_EN = 0;	//31       
#endif
	LOCKREG();
	__wfi();
}

void PMU_EnterSleepMode(void)
{
	uint32_t u32Reg = 0;
	UNLOCKREG();
	u32Reg = inpw(0x50000220);
	QDT_DEBUG("PMU_EnterSleepMode u32Reg= %x\n", u32Reg);
	SYSCLK->CLKSLEEP.CPU_EN = 1; //0          
	SYSCLK->CLKSLEEP.PDMA_EN = 0; //1         
	SYSCLK->CLKSLEEP.ISP_EN = 0;    //2       
	//SYSCLK->CLKSLEEP.RESERVE0 = 0;  //3     
	SYSCLK->CLKSLEEP.WDG_EN = 0;	//4          
	SYSCLK->CLKSLEEP.RTC_EN = 1;	//5          
	SYSCLK->CLKSLEEP.TMR0_EN = 0;	//6        
	SYSCLK->CLKSLEEP.TMR1_EN = 0;	//7        
	SYSCLK->CLKSLEEP.I2C0_EN = 0;	//8        
	//SYSCLK->CLKSLEEP.RESERVE1:3;	//9-11     
	SYSCLK->CLKSLEEP.SPI0_EN = 0;	//12       
	SYSCLK->CLKSLEEP.DPWM_EN = 1;	//13       
	//SYSCLK->CLKSLEEP.RESERVE2:2;	//14-15    
	SYSCLK->CLKSLEEP.UART0_EN = 1;	//16       
	//SYSCLK->CLKSLEEP.RESERVE3 = 0;	//17     
	SYSCLK->CLKSLEEP.BIQALC_EN = 0;	//18     
	SYSCLK->CLKSLEEP.CRC_EN = 0;	//19         
	SYSCLK->CLKSLEEP.PWM01_EN = 1;	//20       
	SYSCLK->CLKSLEEP.PWM23_EN = 1;	//21       
	SYSCLK->CLKSLEEP.ACMP_EN = 0;	//22       
	//SYSCLK->CLKSLEEP.RESERVE5:3;	//23-25    
	SYSCLK->CLKSLEEP.SBRAM_EN = 0;	//26       
	//SYSCLK->CLKSLEEP.RESERVE6 = 0;	//27     
	SYSCLK->CLKSLEEP.ADC_EN = 0;	//28         
	SYSCLK->CLKSLEEP.I2S_EN = 0;	//29         
	SYSCLK->CLKSLEEP.ANA_EN = 0;	//30         
	SYSCLK->CLKSLEEP.PWM45_EN = 1;	//31       
	LOCKREG();
	//__wfi();
}
void PMU_ExitSleepMode(void)
{
	uint32_t u32Reg = 0;
	UNLOCKREG();
	u32Reg = inpw(0x50000220);
	QDT_DEBUG("PMU_ExitSleepMode u32Reg= %x\n", u32Reg);
	outpw(0x50000220, 0xFFFFFFFF);
	LOCKREG();
	//__wfi();
}

#endif
