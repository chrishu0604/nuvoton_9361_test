/*! @file radio.c
 * @brief This file contains functions to interface with the radio chip.
 *
 * @b COPYRIGHT
 * @n Silicon Laboratories Confidential
 * @n Copyright 2012 Silicon Laboratories, Inc.
 * @n http://www.silabs.com
 */

#include "bsp.h"
#include "radio.h"
#include "radio_hal.h"
#include "si446x_api_lib.h"
#include "qdt_utils.h"
#include "Ram.h"
#include "Driver\DrvGPIO.h"


/*****************************************************************************
 *  Local Macros & Definitions
 *****************************************************************************/

SEGMENT_VARIABLE(bPositionInPayload, U16, SEG_XDATA) = 0u;

SEGMENT_VARIABLE(bNumOfRestBytes, U16, SEG_XDATA) = 0u;

U8* pPositionInPayload;




SEGMENT_VARIABLE(bRxPositionInPayload, U16, SEG_XDATA) = 0u;

SEGMENT_VARIABLE(bRxNumOfFreeBytes, U16, SEG_XDATA) = RADIO_MAX_LONG_PACKET_LENGTH;

SEGMENT_VARIABLE(fixRadioPacket[RADIO_MAX_LONG_PACKET_LENGTH], U8, SEG_XDATA);


U8* pRxPositionInPayload =  &fixRadioPacket[0u];


/*****************************************************************************
 *  Global Variables
 *****************************************************************************/

SEGMENT_VARIABLE(Radio_Configuration_Data_Array[], U8, SEG_CODE) = \
              RADIO_CONFIGURATION_DATA_ARRAY;

SEGMENT_VARIABLE(Radio_Configuration_Data_Custom_Long_Payload_Array[], U8, SEG_CODE) = \
              RADIO_CONFIGURATION_DATA_CUSTOM_LONG_PAYLOAD;

SEGMENT_VARIABLE(RadioConfiguration, tRadioConfiguration, SEG_CODE) = \
                        RADIO_CONFIGURATION_DATA;

SEGMENT_VARIABLE_SEGMENT_POINTER(pRadioConfiguration, tRadioConfiguration, SEG_CODE, SEG_CODE) = \
                        &RadioConfiguration;

/*****************************************************************************
 *  Local Function Declarations
 *****************************************************************************/
void vRadio_PowerUp(void);

/*!
 *  Power up the Radio.
 *
 *  @note
 *
 */
void vRadio_PowerUp(void)
{
  SEGMENT_VARIABLE(wDelay,  U16, SEG_XDATA) = 0u;

  /* Hardware reset the chip */
  si446x_reset();

  /* Wait until reset timeout or Reset IT signal */
  for (; wDelay < pRadioConfiguration->Radio_Delay_Cnt_After_Reset; wDelay++);
}

/*!
 *  Radio Initialization.
 *
 *  @author Sz. Papp
 *
 *  @note
 *
 */
void vRadio_Init(void)
{
  U16 wDelay;

  /* Power Up the radio chip */
  vRadio_PowerUp();
	

  /* Load radio configuration */
  while (SI446X_SUCCESS != si446x_configuration_init(pRadioConfiguration->Radio_ConfigurationArray))
  {
    QDT_ERROR("SI446X init failed, re-init\n");
    for (wDelay = 0x7FFF; wDelay--; ) ;
    /* Power Up the radio chip */
    vRadio_PowerUp();
  }
  
  QDT_TRACE("SI446X init successful\n");

  // Read ITs, clear pending ones
  si446x_get_int_status(0u, 0u, 0u); 


  si446x_part_info();
}

/*!
 *  Check if Packet sent IT flag is pending.
 *
 *  @return   TRUE / FALSE
 *
 *  @note
 *
 */
extern qdt_bool transmit_finish;
extern qdt_bool bTransmitBufferIsempty;

BIT gRadio_CheckTransmitted(void)
{
	//int loopCount;

//  if (radio_hal_NirqLevel() == FALSE)
  {
    /* Read ITs, clear pending ones */
//    si446x_get_int_status(0u, 0u, 0u);

    /* check the reason for the IT */

	//for (loopCount = 255; loopCount != 0; loopCount--);

#if 0
    if (Si446xCmd.GET_INT_STATUS.PH_PEND & SI446X_CMD_GET_INT_STATUS_REP_PH_PEND_PACKET_SENT_PEND_BIT)
    {
DrvGPIO_SetBit(GPB, 0);
      /* Nothing is sent to TX FIFO */
      bPositionInPayload = 0u;

      /* Position to the very beginning of the custom long payload */
      pPositionInPayload = (U8*) &pRadioConfiguration->Radio_Custom_Long_Payload;
	  //transmit_finish = qdt_true;
	  //bTransmitBufferIsempty = qdt_true;
	  
      vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);
      return TRUE;
    }
#endif
    if (Si446xCmd.GET_INT_STATUS.PH_PEND & SI446X_CMD_GET_INT_STATUS_REP_PH_PEND_TX_FIFO_ALMOST_EMPTY_PEND_BIT)
    {
        /* Calculate the number of remaining bytes has to be sent to TX FIFO */
        bNumOfRestBytes = RadioConfiguration.Radio_PacketLength - bPositionInPayload;
        if(bNumOfRestBytes > RADIO_TX_ALMOST_EMPTY_THRESHOLD)
        { // remaining byte more than threshold
//DrvGPIO_SetBit(GPB, 0);

          /* Fill TX FIFO with the number of THRESHOLD bytes */
          si446x_write_tx_fifo(RADIO_TX_ALMOST_EMPTY_THRESHOLD, pPositionInPayload);

          /* Calculate how many bytes are sent to TX FIFO */
          bPositionInPayload += RADIO_TX_ALMOST_EMPTY_THRESHOLD;

          /* Position to the next first byte that can be sent to TX FIFO in next round */
          pPositionInPayload += RADIO_TX_ALMOST_EMPTY_THRESHOLD;

        }
        else
        { // remaining byte less or equal than threshold
//DrvGPIO_SetBit(GPA, 5);

          /* Fill TX FIFO with the number of rest bytes */
          si446x_write_tx_fifo(bNumOfRestBytes, pPositionInPayload);

          /* Calculate how many bytes are sent to TX FIFO */
          bPositionInPayload += bNumOfRestBytes;

          /* Position to the next first byte that can be sent to TX FIFO in next round */
          pPositionInPayload += bNumOfRestBytes;
        }
//DrvGPIO_ClrBit(GPB, 0);
//DrvGPIO_ClrBit(GPA, 5);
    }
    else if(Si446xCmd.GET_INT_STATUS.PH_PEND & SI446X_CMD_GET_INT_STATUS_REP_PH_PEND_PACKET_SENT_PEND_BIT)
	{
//DrvGPIO_SetBit(GPB, 0);
	  /* Nothing is sent to TX FIFO */
	  bPositionInPayload = 0u;

	  /* Position to the very beginning of the custom long payload */
	  pPositionInPayload = (U8*) &pRadioConfiguration->Radio_Custom_Long_Payload;
	  //transmit_finish = qdt_true;
	  //bTransmitBufferIsempty = qdt_true;
	  
	  vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);
	  return TRUE;
	}
  }

  return FALSE;
}






/*!
 *  Set Radio to TX mode, fixed packet length.
 *
 *  @param channel Freq. Channel, Packet to be sent
 *
 *  @note
 *
 */
void  vRadio_StartTx(U8 channel, U8 *pioFixRadioPacket)
{
//DrvGPIO_SetBit(GPA, 5);
  /* Reset TX FIFO */
  si446x_fifo_info(SI446X_CMD_FIFO_INFO_ARG_FIFO_TX_BIT);

  // Read ITs, clear pending ones
  si446x_get_int_status(0u, 0u, 0u);

  /* Position to the very beginning of the custom long payload */
  pPositionInPayload = pioFixRadioPacket;

  /* Fill the TX fifo with datas */
  if( RADIO_MAX_PACKET_LENGTH < RadioConfiguration.Radio_PacketLength)
  {
    /* Data to be sent is more than the size of TX FIFO */
    si446x_write_tx_fifo(RADIO_MAX_PACKET_LENGTH, pPositionInPayload);

    /* Calculate how many bytes are sent to TX FIFO */
    bPositionInPayload += RADIO_MAX_PACKET_LENGTH;

    /* Position to the next first byte that can be sent to TX FIFO in next round */
    pPositionInPayload += RADIO_MAX_PACKET_LENGTH;
  }
  else
  {
    // Data to be sent is less or equal than the size of TX FIFO
    si446x_write_tx_fifo(RadioConfiguration.Radio_PacketLength, pioFixRadioPacket);
  }


  /* Start sending packet, channel 0, START immediately, Packet length according to PH, go READY when done */
  si446x_start_tx(channel, 0x30,  0x00);
//DrvGPIO_ClrBit(GPA, 5);
}

/*!
 *  Check if Packet received IT flag is pending.
 *
 *  @return   TRUE - Packet successfully received / FALSE - No packet pending.
 *
 *  @note
 *
 */
BIT gRadio_CheckReceived(void)
{

//int pos =0;
//  if (radio_hal_NirqLevel() == FALSE)
  {
    /* Read ITs, clear pending ones */
//    si446x_get_int_status(0u, 0u, 0u);

    /* check the reason for the IT */
#if 1//20150210
     if (Si446xCmd.GET_INT_STATUS.MODEM_PEND & SI446X_CMD_GET_INT_STATUS_REP_MODEM_STATUS_SYNC_DETECT_BIT)
     {
       /* Blink once LED2 to show Sync Word detected */
       // vHmi_ChangeLedState(eHmi_Led2_c, eHmi_LedBlinkOnce_c);
     }
#endif


#if 1
    if (Si446xCmd.GET_INT_STATUS.PH_PEND & SI446X_CMD_GET_INT_STATUS_REP_PH_PEND_PACKET_RX_PEND_BIT)
    {
      /* Calculate the number of free bytes in the array */
      bRxNumOfFreeBytes = RADIO_MAX_LONG_PACKET_LENGTH - bRxPositionInPayload;

      if (bRxNumOfFreeBytes >= RADIO_MAX_PACKET_LENGTH)
      {// free space in buffer more than RX FIFO size
//DrvGPIO_SetBit(GPA, 5);

        /* Read the RX FIFO with the number of RX FIFO size */
        si446x_read_rx_fifo(RADIO_MAX_PACKET_LENGTH, pRxPositionInPayload);
//DrvGPIO_ClrBit(GPA, 5);

        /* Calculate how many bytes are already stored in the array */
        bRxPositionInPayload += RADIO_MAX_PACKET_LENGTH;

        /* Position to the next free byte that can be written in the next RX FIFO reading */
        pRxPositionInPayload += RADIO_MAX_PACKET_LENGTH;
      }
      else
      {
//DrvGPIO_SetBit(GPA, 6);
        /* Read the RX FIFO with the number of free bytes */
        si446x_read_rx_fifo(bRxNumOfFreeBytes, pRxPositionInPayload);
//DrvGPIO_ClrBit(GPA, 6);

        /* Calculate how many bytes are already stored in the array */
        bRxPositionInPayload += bRxNumOfFreeBytes;

        /* Position to the next free byte that can be written in the next RX FIFO reading */
        pRxPositionInPayload += bRxNumOfFreeBytes;

      }

      /* Calculate how many bytes are already stored in the array */
       bRxPositionInPayload = 0u;

       /* Set writing pointer to the beginning of the array */
       pRxPositionInPayload = &fixRadioPacket[0u];

       /* free space */
       bRxNumOfFreeBytes = RADIO_MAX_LONG_PACKET_LENGTH;

       /* Start the radio */
       vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);
//DrvGPIO_ClrBit(GPA, 5);
//DrvGPIO_ClrBit(GPA, 6);


      return TRUE;
    }

    else if(Si446xCmd.GET_INT_STATUS.PH_PEND & SI446X_CMD_GET_INT_STATUS_REP_PH_STATUS_RX_FIFO_ALMOST_FULL_BIT)
    {
      /* Calculate the number of free bytes in the array */
      bRxNumOfFreeBytes = RADIO_MAX_LONG_PACKET_LENGTH - bRxPositionInPayload;

      if (bRxNumOfFreeBytes >= RADIO_RX_ALMOST_FULL_THRESHOLD)
      { // free space in the array is more than the threshold
//DrvGPIO_SetBit(GPA, 5);

        /* Read the RX FIFO with the number of THRESHOLD bytes */
        si446x_read_rx_fifo(RADIO_RX_ALMOST_FULL_THRESHOLD, pRxPositionInPayload);
//DrvGPIO_ClrBit(GPA, 5);

        /* Calculate how many bytes are already stored in the array */
        bRxPositionInPayload += RADIO_RX_ALMOST_FULL_THRESHOLD;

        /* Position to the next free byte that can be written in the next RX FIFO reading */
        pRxPositionInPayload += RADIO_RX_ALMOST_FULL_THRESHOLD;
      }
      else
      {
//DrvGPIO_SetBit(GPA, 6);
        /* Not enough free space reserved in the program */
        QDT_ERROR("Not enough free space reserved in the program\n");
//DrvGPIO_ClrBit(GPA, 6);
      }
    }

#if 1//20150210
    if (Si446xCmd.GET_INT_STATUS.PH_PEND & SI446X_CMD_GET_INT_STATUS_REP_PH_STATUS_CRC_ERROR_BIT)
    {
      /* Reset FIFO */
      si446x_fifo_info(SI446X_CMD_FIFO_INFO_ARG_FIFO_RX_BIT);
    }
#endif	
//DrvGPIO_ClrBit(GPA, 5);
//DrvGPIO_ClrBit(GPA, 6);
	
  }

  return FALSE;
#endif
}

/*!
 *  Set Radio to RX mode, fixed packet length.
 *
 *  @param channel Freq. Channel
 *
 *  @note
 *
 */
void vRadio_StartRX(U8 channel)
{
  // Read ITs, clear pending ones
  si446x_get_int_status(0u, 0u, 0u);

  /* Reset FIFO */
  si446x_fifo_info(SI446X_CMD_FIFO_INFO_ARG_FIFO_RX_BIT);

  /* Start Receiving packet, channel 0, START immediately, Packet length according to PH */
  si446x_start_rx(channel, 0u, 0x00,
                  SI446X_CMD_START_RX_ARG_NEXT_STATE1_RXTIMEOUT_STATE_ENUM_NOCHANGE,
                  SI446X_CMD_START_RX_ARG_NEXT_STATE2_RXVALID_STATE_ENUM_READY,
                  SI446X_CMD_START_RX_ARG_NEXT_STATE3_RXINVALID_STATE_ENUM_RX );

}

BIT gRadio_CheckTxRxStatus(void)
{
	//if (radio_hal_NirqLevel() == FALSE)
	{
	  /* Read ITs, clear pending ones */
	  si446x_get_int_status(0u, 0u, 0u);

	  if (gRadio_CheckTransmitted())
	  {
	  	transmit_finish = qdt_true;
	  	bTransmitBufferIsempty = qdt_true;
		DrvGPIO_ClrBit(GPB,6);
	  }

      if (!g_Data.ucStopToRx)
      {
	  	if (gRadio_CheckReceived())
	  	{
//DrvGPIO_SetBit(GPA, 5);
	  		QDT_ParseRFPackage(&fixRadioPacket[0]);
	  	}
      }
	}

}


