#ifndef RAM_H_
#define RAM_H_

#include "qdt_utils.h"

typedef struct tagGlobalData
{
	qdt_bool ucStopToRx:1;
	qdt_bool ucParentCalling:1;
}GlobalData;

extern GlobalData g_Data;


#endif
