/*
	Total Items: 47
*/
/*
	Item 0 (Index 0)
	d:\ISD9160\HumanVoice_20150430\WavFile\power.vp
		size: 880 offset: 0x00000188
	Inf: 0x7C003E80
*/
#define VP_0_OFFSET (VP_BASE + 0x00000188)
#define VP_0_SIZE   0x00000370

/*
	Item 1 (Index 1)
	d:\ISD9160\HumanVoice_20150430\WavFile\on.vp
		size: 720 offset: 0x000004F8
	Inf: 0x7C003E80
*/
#define VP_1_OFFSET (VP_BASE + 0x000004F8)
#define VP_1_SIZE   0x000002D0

/*
	Item 2 (Index 2)
	d:\ISD9160\HumanVoice_20150430\WavFile\off.vp
		size: 680 offset: 0x000007C8
	Inf: 0x7C003E80
*/
#define VP_2_OFFSET (VP_BASE + 0x000007C8)
#define VP_2_SIZE   0x000002A8

/*
	Item 3 (Index 3)
	d:\ISD9160\HumanVoice_20150430\WavFile\pairing.vp
		size: 1040 offset: 0x00000A70
	Inf: 0x7C003E80
*/
#define VP_3_OFFSET (VP_BASE + 0x00000A70)
#define VP_3_SIZE   0x00000410

/*
	Item 4 (Index 4)
	d:\ISD9160\HumanVoice_20150430\WavFile\mode.vp
		size: 640 offset: 0x00000E80
	Inf: 0x7C003E80
*/
#define VP_4_OFFSET (VP_BASE + 0x00000E80)
#define VP_4_SIZE   0x00000280

/*
	Item 5 (Index 5)
	d:\ISD9160\HumanVoice_20150430\WavFile\success.vp
		size: 1240 offset: 0x00001100
	Inf: 0x7C003E80
*/
#define VP_5_OFFSET (VP_BASE + 0x00001100)
#define VP_5_SIZE   0x000004D8

/*
	Item 6 (Index 6)
	d:\ISD9160\HumanVoice_20150430\WavFile\fail.vp
		size: 800 offset: 0x000015D8
	Inf: 0x7C003E80
*/
#define VP_6_OFFSET (VP_BASE + 0x000015D8)
#define VP_6_SIZE   0x00000320

/*
	Item 7 (Index 7)
	d:\ISD9160\HumanVoice_20150430\WavFile\unpairing.vp
		size: 1280 offset: 0x000018F8
	Inf: 0x7C003E80
*/
#define VP_7_OFFSET (VP_BASE + 0x000018F8)
#define VP_7_SIZE   0x00000500

/*
	Item 8 (Index 8)
	d:\ISD9160\HumanVoice_20150430\WavFile\factory.vp
		size: 920 offset: 0x00001DF8
	Inf: 0x7C003E80
*/
#define VP_8_OFFSET (VP_BASE + 0x00001DF8)
#define VP_8_SIZE   0x00000398

/*
	Item 9 (Index 9)
	d:\ISD9160\HumanVoice_20150430\WavFile\reset.vp
		size: 1040 offset: 0x00002190
	Inf: 0x7C003E80
*/
#define VP_9_OFFSET (VP_BASE + 0x00002190)
#define VP_9_SIZE   0x00000410

/*
	Item 10 (Index 10)
	d:\ISD9160\HumanVoice_20150430\WavFile\audioalert.vp
		size: 1600 offset: 0x000025A0
	Inf: 0x7C003E80
*/
#define VP_10_OFFSET (VP_BASE + 0x000025A0)
#define VP_10_SIZE   0x00000640

/*
	Item 11 (Index 11)
	d:\ISD9160\HumanVoice_20150430\WavFile\null.vp
		size: 0 offset: 0x00002BE0
	Inf: 0x7C003E80
*/
#define VP_11_OFFSET (VP_BASE + 0x00002BE0)
#define VP_11_SIZE   0x00000000

/*
	Item 12 (Index 12)
	d:\ISD9160\HumanVoice_20150430\WavFile\humidity.vp
		size: 1080 offset: 0x00002BE0
	Inf: 0x7C003E80
*/
#define VP_12_OFFSET (VP_BASE + 0x00002BE0)
#define VP_12_SIZE   0x00000438

/*
	Item 13 (Index 13)
	d:\ISD9160\HumanVoice_20150430\WavFile\temperature.vp
		size: 1080 offset: 0x00003018
	Inf: 0x7C003E80
*/
#define VP_13_OFFSET (VP_BASE + 0x00003018)
#define VP_13_SIZE   0x00000438

/*
	Item 14 (Index 14)
	d:\ISD9160\HumanVoice_20150430\WavFile\one.vp
		size: 640 offset: 0x00003450
	Inf: 0x7C003E80
*/
#define VP_14_OFFSET (VP_BASE + 0x00003450)
#define VP_14_SIZE   0x00000280

/*
	Item 15 (Index 15)
	d:\ISD9160\HumanVoice_20150430\WavFile\two.vp
		size: 480 offset: 0x000036D0
	Inf: 0x7C003E80
*/
#define VP_15_OFFSET (VP_BASE + 0x000036D0)
#define VP_15_SIZE   0x000001E0

/*
	Item 16 (Index 16)
	d:\ISD9160\HumanVoice_20150430\WavFile\three.vp
		size: 680 offset: 0x000038B0
	Inf: 0x7C003E80
*/
#define VP_16_OFFSET (VP_BASE + 0x000038B0)
#define VP_16_SIZE   0x000002A8

/*
	Item 17 (Index 17)
	d:\ISD9160\HumanVoice_20150430\WavFile\four.vp
		size: 680 offset: 0x00003B58
	Inf: 0x7C003E80
*/
#define VP_17_OFFSET (VP_BASE + 0x00003B58)
#define VP_17_SIZE   0x000002A8

/*
	Item 18 (Index 18)
	d:\ISD9160\HumanVoice_20150430\WavFile\five.vp
		size: 880 offset: 0x00003E00
	Inf: 0x7C003E80
*/
#define VP_18_OFFSET (VP_BASE + 0x00003E00)
#define VP_18_SIZE   0x00000370

/*
	Item 19 (Index 19)
	d:\ISD9160\HumanVoice_20150430\WavFile\six.vp
		size: 1000 offset: 0x00004170
	Inf: 0x7C003E80
*/
#define VP_19_OFFSET (VP_BASE + 0x00004170)
#define VP_19_SIZE   0x000003E8

/*
	Item 20 (Index 20)
	d:\ISD9160\HumanVoice_20150430\WavFile\seven.vp
		size: 960 offset: 0x00004558
	Inf: 0x7C003E80
*/
#define VP_20_OFFSET (VP_BASE + 0x00004558)
#define VP_20_SIZE   0x000003C0

/*
	Item 21 (Index 21)
	d:\ISD9160\HumanVoice_20150430\WavFile\eight.vp
		size: 600 offset: 0x00004918
	Inf: 0x7C003E80
*/
#define VP_21_OFFSET (VP_BASE + 0x00004918)
#define VP_21_SIZE   0x00000258

/*
	Item 22 (Index 22)
	d:\ISD9160\HumanVoice_20150430\WavFile\nine.vp
		size: 640 offset: 0x00004B70
	Inf: 0x7C003E80
*/
#define VP_22_OFFSET (VP_BASE + 0x00004B70)
#define VP_22_SIZE   0x00000280

/*
	Item 23 (Index 23)
	d:\ISD9160\HumanVoice_20150430\WavFile\ten.vp
		size: 600 offset: 0x00004DF0
	Inf: 0x7C003E80
*/
#define VP_23_OFFSET (VP_BASE + 0x00004DF0)
#define VP_23_SIZE   0x00000258

/*
	Item 24 (Index 24)
	d:\ISD9160\HumanVoice_20150430\WavFile\eleven.vp
		size: 960 offset: 0x00005048
	Inf: 0x7C003E80
*/
#define VP_24_OFFSET (VP_BASE + 0x00005048)
#define VP_24_SIZE   0x000003C0

/*
	Item 25 (Index 25)
	d:\ISD9160\HumanVoice_20150430\WavFile\twelve.vp
		size: 960 offset: 0x00005408
	Inf: 0x7C003E80
*/
#define VP_25_OFFSET (VP_BASE + 0x00005408)
#define VP_25_SIZE   0x000003C0

/*
	Item 26 (Index 26)
	d:\ISD9160\HumanVoice_20150430\WavFile\thirteen.vp
		size: 920 offset: 0x000057C8
	Inf: 0x7C003E80
*/
#define VP_26_OFFSET (VP_BASE + 0x000057C8)
#define VP_26_SIZE   0x00000398

/*
	Item 27 (Index 27)
	d:\ISD9160\HumanVoice_20150430\WavFile\fourteen.vp
		size: 1160 offset: 0x00005B60
	Inf: 0x7C003E80
*/
#define VP_27_OFFSET (VP_BASE + 0x00005B60)
#define VP_27_SIZE   0x00000488

/*
	Item 28 (Index 28)
	d:\ISD9160\HumanVoice_20150430\WavFile\fifteen.vp
		size: 1120 offset: 0x00005FE8
	Inf: 0x7C003E80
*/
#define VP_28_OFFSET (VP_BASE + 0x00005FE8)
#define VP_28_SIZE   0x00000460

/*
	Item 29 (Index 29)
	d:\ISD9160\HumanVoice_20150430\WavFile\sixteen.vp
		size: 1320 offset: 0x00006448
	Inf: 0x7C003E80
*/
#define VP_29_OFFSET (VP_BASE + 0x00006448)
#define VP_29_SIZE   0x00000528

/*
	Item 30 (Index 30)
	d:\ISD9160\HumanVoice_20150430\WavFile\seventeen.vp
		size: 1360 offset: 0x00006970
	Inf: 0x7C003E80
*/
#define VP_30_OFFSET (VP_BASE + 0x00006970)
#define VP_30_SIZE   0x00000550

/*
	Item 31 (Index 31)
	d:\ISD9160\HumanVoice_20150430\WavFile\eighteen.vp
		size: 880 offset: 0x00006EC0
	Inf: 0x7C003E80
*/
#define VP_31_OFFSET (VP_BASE + 0x00006EC0)
#define VP_31_SIZE   0x00000370

/*
	Item 32 (Index 32)
	d:\ISD9160\HumanVoice_20150430\WavFile\nineteen.vp
		size: 1200 offset: 0x00007230
	Inf: 0x7C003E80
*/
#define VP_32_OFFSET (VP_BASE + 0x00007230)
#define VP_32_SIZE   0x000004B0

/*
	Item 33 (Index 33)
	d:\ISD9160\HumanVoice_20150430\WavFile\twenty.vp
		size: 800 offset: 0x000076E0
	Inf: 0x7C003E80
*/
#define VP_33_OFFSET (VP_BASE + 0x000076E0)
#define VP_33_SIZE   0x00000320

/*
	Item 34 (Index 34)
	d:\ISD9160\HumanVoice_20150430\WavFile\thirty.vp
		size: 760 offset: 0x00007A00
	Inf: 0x7C003E80
*/
#define VP_34_OFFSET (VP_BASE + 0x00007A00)
#define VP_34_SIZE   0x000002F8

/*
	Item 35 (Index 35)
	d:\ISD9160\HumanVoice_20150430\WavFile\forty.vp
		size: 760 offset: 0x00007CF8
	Inf: 0x7C003E80
*/
#define VP_35_OFFSET (VP_BASE + 0x00007CF8)
#define VP_35_SIZE   0x000002F8

/*
	Item 36 (Index 36)
	d:\ISD9160\HumanVoice_20150430\WavFile\fifty.vp
		size: 760 offset: 0x00007FF0
	Inf: 0x7C003E80
*/
#define VP_36_OFFSET (VP_BASE + 0x00007FF0)
#define VP_36_SIZE   0x000002F8

/*
	Item 37 (Index 37)
	d:\ISD9160\HumanVoice_20150430\WavFile\sixty.vp
		size: 1120 offset: 0x000082E8
	Inf: 0x7C003E80
*/
#define VP_37_OFFSET (VP_BASE + 0x000082E8)
#define VP_37_SIZE   0x00000460

/*
	Item 38 (Index 38)
	d:\ISD9160\HumanVoice_20150430\WavFile\seventy.vp
		size: 1000 offset: 0x00008748
	Inf: 0x7C003E80
*/
#define VP_38_OFFSET (VP_BASE + 0x00008748)
#define VP_38_SIZE   0x000003E8

/*
	Item 39 (Index 39)
	d:\ISD9160\HumanVoice_20150430\WavFile\eighty.vp
		size: 640 offset: 0x00008B30
	Inf: 0x7C003E80
*/
#define VP_39_OFFSET (VP_BASE + 0x00008B30)
#define VP_39_SIZE   0x00000280

/*
	Item 40 (Index 40)
	d:\ISD9160\HumanVoice_20150430\WavFile\ninety.vp
		size: 840 offset: 0x00008DB0
	Inf: 0x7C003E80
*/
#define VP_40_OFFSET (VP_BASE + 0x00008DB0)
#define VP_40_SIZE   0x00000348

/*
	Item 41 (Index 41)
	d:\ISD9160\HumanVoice_20150430\WavFile\zero.vp
		size: 840 offset: 0x000090F8
	Inf: 0x7C003E80
*/
#define VP_41_OFFSET (VP_BASE + 0x000090F8)
#define VP_41_SIZE   0x00000348

/*
	Item 42 (Index 42)
	d:\ISD9160\HumanVoice_20150430\WavFile\percent.vp
		size: 1160 offset: 0x00009440
	Inf: 0x7C003E80
*/
#define VP_42_OFFSET (VP_BASE + 0x00009440)
#define VP_42_SIZE   0x00000488

/*
	Item 43 (Index 43)
	d:\ISD9160\HumanVoice_20150430\WavFile\degreescelsius.vp
		size: 2160 offset: 0x000098C8
	Inf: 0x7C003E80
*/
#define VP_43_OFFSET (VP_BASE + 0x000098C8)
#define VP_43_SIZE   0x00000870

/*
	Item 44 (Index 44)
	d:\ISD9160\HumanVoice_20150430\WavFile\bb.vp
		size: 200 offset: 0x0000A138
	Inf: 0x7C003E80
*/
#define VP_44_OFFSET (VP_BASE + 0x0000A138)
#define VP_44_SIZE   0x000000C8

/*
	Item 45 (Index 45)
	d:\ISD9160\HumanVoice_20150430\WavFile\plsvolkeytoselectchan.vp
		size: 4440 offset: 0x0000A200
	Inf: 0x7C003E80
*/
#define VP_45_OFFSET (VP_BASE + 0x0000A200)
#define VP_45_SIZE   0x00001158

/*
	Item 46 (Index 46)
	d:\ISD9160\HumanVoice_20150430\WavFile\selectchannel.vp
		size: 1920 offset: 0x0000B358
	Inf: 0x7C003E80
*/
#define VP_46_OFFSET (VP_BASE + 0x0000B358)
#define VP_46_SIZE   0x00000780

