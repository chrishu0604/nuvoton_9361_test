#ifdef BUILDTYPE_AUDIODOCKING_PROJECT

#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include "ISD93xx.h"
#include "Driver\DrvUART.h"
#include "Driver\DrvGPIO.h"
#include "Driver\DrvSYS.h"
#include "Driver\DrvQSPI.h"
#include "Driver\DrvPDMA.h"
#include "Driver\DrvADC.h"
#include "Driver\DrvI2C.h"
#include "Driver\DrvI2S.h"
#include "Driver\DrvTIMER.h"
#include "NVTTypes.h"

#include "bsp.h"
#include "radio.h"
#include "qdt_ram.h"
#include "qdt_utils.h"
#include "qdt_fifo.h"
#include "qdt_flash_hal.h"
#include "qdt_sysState.h"
#include "qdt_audiodocking.h"
#include "qdt_audiodocking_func.h"
#include "qdt_common_func.h"
#include "qdt_uart_func.h"
#include "qdt_factory_func.h"

#include "CM36686Setup.h"
#include "AK7755Setup.h"

//#include "si446x_api_lib.h"

/*---------------------------------------------------------------------------------------------------------*/
/* Extern Function Prototypes                                                                              */
/*---------------------------------------------------------------------------------------------------------*/
extern void InitialSPIPortMaster(void);
extern void PlayBufferSet(void);

extern void StartPclkCount(void);
extern void InitCalibrateOSC (void);
extern BOOL IsCountingStop(void);

extern void RecordStart(void);

/*---------------------------------------------------------------------------------------------------------*/
/* Define Function Prototypes                                                                              */
/*---------------------------------------------------------------------------------------------------------*/
void InitialUART(void);
void RecordStart(void);
void PlayStart(void);

void S7Init(void);
void S7EncDec(void);

void SysTimerDelay(uint32_t us);
void Delay(uint32_t delayCnt);


void RF446x_Pollhandler(void);
BIT vSampleCode_SendFixPacket(void);


/*------------------------------------------------------------------------*/
/*                            Local Macros                                */
/*------------------------------------------------------------------------*/
#define PACKET_SEND_INTERVAL  5000u

/*------------------------------------------------------------------------*/
/*                          Local variables                               */
/*------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------------------------------------*/
/* Define global variables                                                                                 */
/*---------------------------------------------------------------------------------------------------------*/
GlobalData g_Data;

extern volatile uint32_t CallBackCounter;	//in RecordPCM.c
extern qdt_bool bPdmaTxDone;
extern qdt_bool bPdmaRxDone;
extern qdt_bool bPdmaRxDone, bPdmaTxDone, bIsTestOver;

extern volatile __align(4) signed short s16Out_words[40];
extern const qdt_uint8 g_ucFWVersion[13];


uint8_t bMain_IT_Status;
uint16_t lPer_MsCnt;


uint8_t ucTransmitBufferIdx = 0;
//signed short AudioRXBuffer[20];


//====================
// Functions & main

void UartInit(void)
{
	STR_UART_T sParam;
	//DrvGPIO_InitFunction(FUNC_UART0);
    //DrvGPIO_InitFunction(FUNC_UART0_FLOW);
    DrvGPIO_UART_TXRX_PA8_PA9();
	//DrvGPIO_UART_RTSCTS_PA10_PA11();
	
    sParam.u32BaudRate 		= 230400;
    sParam.u8cDataBits 		= DRVUART_DATABITS_8;
    sParam.u8cStopBits 		= DRVUART_STOPBITS_1;
    sParam.u8cParity 		= DRVUART_PARITY_NONE;
    sParam.u8cRxTriggerLevel= DRVUART_FIFO_4BYTES;
	//sParam.u8TimeOut 			= 0;

	if(DrvUART_Open(UART_PORT0,&sParam) == 0) 
	{
		DrvUART_EnableInt(UART_PORT0, DRVUART_RDAINT,interrupt_callback_uart_function);
		UART0->IER.AUTO_CTS_EN = 1;
     	//UART0->IER.AUTO_RTS_EN = 1;

	}
//while(1)
//	DrvUART_Write(UART_PORT0, ucTest, 1);
	printf("\n\n\n\n\nAUDIO DOCKING MCU BUILD: %s %s\n", __TIME__, __DATE__);
	printf("Version: %x.%x.%x.%x \n", g_ucFWVersion[9], g_ucFWVersion[10], g_ucFWVersion[11], g_ucFWVersion[12]);
	printf("#################### UART_DEBUG_READY ####################\n");
}

void interrupt_callback_CheckRF_function(uint32_t u32GpaStatus, uint32_t u32GpbStatus)
{

	GPIOB->ISRC = GPIOB->ISRC;
	GPIOA->ISRC = GPIOA->ISRC;
	gRadio_CheckTxRxStatus();
}

void InitGPIOWakeupInSPD(void)
{
    // Clear interrupts.
	GPIOA->ISRC = GPIOA->ISRC;
	GPIOB->ISRC = GPIOB->ISRC;
	////INT from GPA pin1 as an example wake up
  
    DrvGPIO_SetIntCallback(interrupt_callback_CheckRF_function);
    DrvGPIO_SetDebounceTime(3, DBCLKSRC_HCLK);
    DrvGPIO_EnableDebounce(GPB, 7);
	DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE); 		//can not wakeup when pin keep at low before entering DPD
}

void GpioInit(void)
{
	DrvGPIO_Open(GPA,15, IO_OUTPUT); //RF-Si4463 shoutdown PIN
	DrvGPIO_Open(GPB,1, IO_OUTPUT); //RF-Si4463 SPI SSB PIN
	DrvGPIO_Open(GPB,7, IO_INPUT); //RF-Si4463 nIRQ PIN	
	DrvGPIO_Open(GPB,2, IO_OUTPUT); //PWR_CTRL PIN


	//DrvGPIO_Open(GPB,5, IO_INPUT); //for testing ONE SPI path
	//DrvGPIO_Open(GPB,6, IO_INPUT); //for testing ONE SPI path
	DrvGPIO_Open(GPB,5, IO_OUTPUT); //for testing ONE SPI path
	DrvGPIO_Open(GPB,6, IO_OUTPUT); //for testing ONE SPI path
	
	DrvGPIO_Open(GPA,13, IO_INPUT); //CM36686 INT PIN
	DrvGPIO_Open(GPB,4, IO_INPUT); //Power key
	DrvGPIO_Open(GPA,5, IO_INPUT); //volume up key
	DrvGPIO_Open(GPB,2, IO_INPUT); //volume down key
	DrvGPIO_Open(GPB,11, IO_INPUT); //Pairing/Page key
	DrvGPIO_Open(GPA,14, IO_OUTPUT); //Power LED
	DrvGPIO_Open(GPB,0, IO_OUTPUT);  //low battery LED
	
	DrvGPIO_Open(GPB,15, IO_OUTPUT); //VS100x-mp3 decoder  reset pin.
	DrvGPIO_Open(GPB,14, IO_OUTPUT); //VS100x-mp3 decoder  XDSC pin.
	DrvGPIO_Open(GPB,13, IO_OUTPUT); //VS100x-mp3 decoder  XDSC pin.

	DrvGPIO_Open(GPB,8, IO_INPUT); //DC_PlugIn_Det.
	DrvGPIO_Open(GPB,10, IO_OUTPUT); //RF PWR_CTRL
	

	SYS->GPA_ALT.GPA2              =0;  //Disable 9160 SPI CS funtion. it be controled at qdt_flash_hal.c
	DrvGPIO_Open(GPA,2, IO_OUTPUT); //SPI FLASH SSB0
	QDT_TRACE("GPIO Init successful\n");

	AUDIODOCKING_LED_PWR_RED(qdt_ON);
	AUDIODOCKING_LED_PWR_GREEN(qdt_OFF);
	AUDIODOCKING_RF_446x_PER_CTRL(qdt_OFF);
	AUDIODOCKING_RF_446x(qdt_OFF);

	AUDIODOCKING_CAMERA_PWR(qdt_OFF);			//Power down A500-camera

	//QDT_AD_ResetMP3();
	DrvGPIO_ClrBit(GPB,15);      //tmp code. for mp3-vs1053
	//AUDIODOCKING_A500_PWR(qdt_ON);

#ifdef USE_AKM
        /*Trigger AKM reset pin*/
	DrvGPIO_Open(GPB,12, IO_OUTPUT); //AKM reset ping
	DrvGPIO_ClrBit(GPB,12);
	QDT_Common_DelayTime(100);
	DrvGPIO_SetBit(GPB,12);

	/*QDT_Common_DelayTime(5);
	DrvGPIO_Open(GPB,12, IO_OUTPUT); //AKM reset ping
	DrvGPIO_SetBit(GPB,12);
	QDT_Common_DelayTime(10);
	DrvGPIO_ClrBit(GPB,12);
	QDT_Common_DelayTime(1);
	DrvGPIO_SetBit(GPB,12);
	QDT_Common_DelayTime(5);*/
#endif	

	DrvGPIO_Open(GPA,12, IO_OUTPUT);
	AUDIODOCKING_LED_NIGHTLIGHT(qdt_OFF);
	
#ifdef USE_SWITCH
	AUDIODOCKING_SPK_SWITCH(qdt_OFF);
#endif
}

void LdoOn(void)
{
	SYSCLK->APBCLK.ANA_EN=1;
	ANA->LDOPD.PD=0;
	ANA->LDOSET=3;
}
/*---------------------------------------------------------------------------------------------------------*/
/* SysTimerDelay                                                                                           */
/*---------------------------------------------------------------------------------------------------------*/
void SysTimerDelay(uint32_t us)
{
#ifdef USE_SYSTEM_CLOCK_48MHZ	
    SysTick->LOAD = us * 48; /* Assume the internal 49MHz RC used */
#endif
#ifdef USE_SYSTEM_CLOCK_98MHZ	
    SysTick->LOAD = us * 98; /* Assume the internal 49MHz RC used */
#endif
    SysTick->VAL  =  (0x00);
    SysTick->CTRL = (1 << SYSTICK_CLKSOURCE) | (1<<SYSTICK_ENABLE);

    /* Waiting for down-count to zero */
    while((SysTick->CTRL & (1 << 16)) == 0);
}

void InitialSystemClock(void)
{
#if 0
    /* Unlock the protected registers */	
	UNLOCKREG();
	SYSCLK->PWRCON.OSC49M_EN = 1;
	SYSCLK->CLKSEL0.HCLK_S = 0; /* Select HCLK source as 48MHz */ 
	SYSCLK->CLKDIV.HCLK_N  = 0;	/* Select no division          */
	SYSCLK->CLKSEL0.OSCFSel = 0;	/* 1= 32MHz, 0=48MHz */
	//SYSCLK->CLKSEL0.STCLK_S = 3; /* Use internal HCLK */
	SYSCLK->PWRCON.XTL32K_EN = 1;
#endif
	DrvSYS_UnlockKeyAddr();
    DrvSYS_SetOscCtrl(E_SYS_OSC49M, 1);
    DrvSYS_SetOscCtrl(E_SYS_OSC10K, 1);
    DrvSYS_SetOscCtrl(E_SYS_XTL32K, 1);
#ifdef USE_SYSTEM_CLOCK_48MHZ	
	DrvSYS_SetHCLK(E_DRVSYS_48M, 1);
#endif
#ifdef USE_SYSTEM_CLOCK_98MHZ	
	DrvSYS_SetHCLK(E_DRVSYS_96M, 1);
#endif

	DrvSYS_LockKeyAddr();	
}
void InitialI2C(void)
{
	/*  GPIO initial and select operation mode for I2C*/
    //DrvGPIO_InitFunction(FUNC_I2C0); //Set I2C I/O
    DrvGPIO_I2C_PA10_PA11();
	//DrvI2C_Open(I2C_PORT0, (DrvSYS_GetHCLK() * 1000), 12000);  //clock = 24Kbps
	DrvI2C_Open(I2C_PORT0, (DrvSYS_GetHCLK() * 1000), 400000);  //clock = 48Kbps
	DrvI2C_EnableInt(I2C_PORT0); //Enable I2C0 interrupt and set corresponding NVIC bit
	//DrvI2C_Ctrl(I2C_PORT0, 1, 0, 0, 0);

}

/*---------------------------------------------------------------------------------------------------------*/
/* InitialI2S                                                                                              */
/*---------------------------------------------------------------------------------------------------------*/
void InitialI2S(uint32_t u32SampleRate)
{
    S_DRVI2S_DATA_T st;

    DrvSYS_SetIPClockSource(E_SYS_I2S_CLKSRC, 2);
	
	/* GPIO initial and select operation mode for I2S*/
	//void DrvGPIO_I2S_PA4_PB9_PA6_PA7(void)
	{
		SYS->GPA_ALT.GPA4 = 1;	// FS
		SYS->GPB_ALT.GPB9 = 1;	// BCLK
		SYS->GPA_ALT.GPA6 = 1;	// SDI
		SYS->GPA_ALT.GPA7 = 1;	// SDO
	}

	//DrvGPIO_I2S_MCLK_PB1(); 			//Set MCLK I/O
	//DrvGPIO_I2S_MCLK_PA0();        //Set MCLK I/O

//    SYSCLK->CLKSEL2.I2S_S=3;

    /* Set I2S Parameter */
    switch(u32SampleRate) 
    {
     case 8000:  st.u32SampleRate    = 8000; break;
     case 16000: st.u32SampleRate    = 16000; break;
     case 48000: st.u32SampleRate    = 48000; break;
     default:    st.u32SampleRate    = 16000; break;
    }
    st.u8WordWidth       = DRVI2S_DATABIT_16;
    st.u8AudioFormat     = DRVI2S_MONO;       
    st.u8DataFormat      = DRVI2S_FORMAT_I2S;   
    st.u8Mode            = DRVI2S_MODE_MASTER;
    st.u8RxFIFOThreshold = DRVI2S_FIFO_LEVEL_WORD_2;
    st.u8TxFIFOThreshold = DRVI2S_FIFO_LEVEL_WORD_2;
    DrvI2S_Open(&st);
    
	//DrvI2S_SetMCLK(4096000);  //MCLK = 4.096MHz
	DrvI2S_SetMCLK(12288000);  //MCLK = 12MHz
	DrvI2S_EnableMCLK(1);	   //enable MCLK	 


/*    switch(u32SampleRate) 
    {
     case 8000:  I2S->CLKDIV.BCLK_DIV = 95; break;
     case 16000: I2S->CLKDIV.BCLK_DIV = 47; break;
     case 48000: I2S->CLKDIV.BCLK_DIV = 15; break;
     default:    I2S->CLKDIV.BCLK_DIV = 47; break;
    }*/

//    I2S->CLKDIV.MCLK_DIV = 6 ;   // 6 

//	DrvI2S_SetMCLK(12000000);  //MCLK = 12MHz
//	DrvI2S_EnableMCLK(1);



    /* Enable I2S Tx/Rx function */
    DrvI2S_EnableRx(TRUE);
    DrvI2S_EnableTx(TRUE);

//DrvI2S_SetMCLK(12000000);  //MCLK = 12MHz
//I2S->CON.MCLKEN = 1;

}

void InitialGpioPwm(void)
{
	SYS->GPA_ALT.GPA12=1;		//PWMA Ch0 function on GPA12
//	SYS->GPA_ALT.GPA13=1;		//PWMA Ch1 function on GPA13

	SYSCLK->PWRCON.OSC10K_EN=1;		//Enable 10K oscillation;
	SYSCLK->CLKSEL1.PWM01_S=2;		//Clock source, 0=>10K, 1=>32K, 2=>HCLK
	SYSCLK->APBCLK.PWM01_EN = 1;	//Enable PWM clock source

	//===============
	//PWM0~1
	PWMA->PPR.CP01=255;	//Pre-scaler divider = CP01+1 =256

	//-------------
	//PWM0:Blue
	PWMA->POE.PWM0=1;			//GPA12 PWM output default is disabled.
	PWMA->PCR.CH0MOD=1;			//Auto-load mode
	PWMA->PCR.CH0INV=1;		//Signal inverted
	PWMA->CSR.CSR0=4;			//Set Timer1 clock divider = 1;
	PWMA->CNR0=DUTY_RESOLUTION;	//Duty (CMR0+1)/(CNR0+1)
	PWMA->CMR0=1000;				//Duty (CMR0+1)/(CNR0+1), duty is from 1 ~ (DUTY_RESOLUTION+1)/(DUTY_RESOLUTION+1) low 
	PWMA->PCR.CH0EN=1;			//Timer enable
	//-------------
	
}

void TMR_CallBack10msTimer()
{
	static qdt_bool bLEDOnOff = 0;
	g_Data.uc10MSCount++;
	g_Data.uc500MSCount++;
	g_Data.uc1000MSCount++;

	if (g_Data.uc10MSCount == 100)  //1000ms
	{
	    g_Data.uc10MSCount = 0;
		//printf("1s call back\n");
		//if (g_Data.ucSysState == SYSSTATE_RUNNING)
			//QDT_AD_NightLEDOnOff();

	}
	if (g_Data.uc500MSCount == 50) //500ms
	{
		g_Data.uc500MSCount = 0;

		if (g_Data.bTwinkleGreenLED && g_Data.bTwinkleRedLED)
		{
			AUDIODOCKING_LED_PWR_GREEN(bLEDOnOff);
			AUDIODOCKING_LED_PWR_RED(bLEDOnOff);
			bLEDOnOff = ~ bLEDOnOff;
		}
		else if (g_Data.bTwinkleGreenLED)
		{
			AUDIODOCKING_LED_PWR_GREEN(bLEDOnOff);
			bLEDOnOff = ~ bLEDOnOff;
		}		
		else if (g_Data.bTwinkleRedLED)
		{
			AUDIODOCKING_LED_PWR_RED(bLEDOnOff);
			bLEDOnOff = ~ bLEDOnOff;
		}		
		
	
#ifdef BUILDTYPE_AUDIODOCKING_PROJECT
		//To recover system absnoraml, if Audiodocking can't recieve audio data over 5s, return to boardcase audio.
		//!-- Start --!
		if (g_Data.bParentCalling == qdt_true)
		{
			g_Data.NoAudioDataComingCounter++;

			if (g_Data.NoAudioDataComingCounter> 10)
		{		
			QDT_DEBUG("Audiodocking can't recieve audio data over 5s, return to boardcase audio...\n");
			QDT_AD_Parents_HandUp();
				//g_Data.NoAudioDataComingCounter = 0;
		}
		}
		//!-- End --!	
#endif
	}


	if (g_Data.uc1000MSCount == 100) //1000ms
	{
		g_Data.uc1000MSCount = 0;
		
		if (g_Data.ucNightLightTimerStart && g_Data.u16NightLightTimerSecs >0)
		{
			g_Data.u16NightLightTimerSecs--;
			if (g_Data.u16NightLightTimerSecs == 0)
			{
				QDT_DEBUG("Timer turn off -> NightLight\n");
				QDT_AD_SetNightLightOnOFF(qdt_OFF);
				g_Data.ucNightLightTimerStart = qdt_false;
			}
		}

	}
	
}


void InitialTimer(void)
{
	DrvTIMER_Init();
	DrvSYS_SetIPClockSource(E_SYS_TMR0_CLKSRC,2);

    /* Using TIMER0 PERIODIC_MODE , 2 tick /sec */
	DrvTIMER_Open(TMR0, 1000, PERIODIC_MODE);  
                		    
	
	DrvTIMER_SetTimerEvent(TMR0,10, (TIMER_CALLBACK)TMR_CallBack10msTimer,0);		
#if 0	
	/* Install Callback function "call_back"  when Interrupt happen twice time */
	DrvTIMER_SetTimerEvent(TMR0,10, (TIMER_CALLBACK)TMR_CallBack10msTimer,0);		
	/* Enable TIMER0 Intettupt */

  	//u32Priority = NVIC_GetPriority(GPAB_IRQn);
 	 NVIC_SetPriority(GPAB_IRQn, 1);
	//u32Priority = NVIC_GetPriority(GPAB_IRQn);
  	NVIC_SetPriority(TMR0_IRQn, 3);
	//u32Priority = NVIC_GetPriority(TMR0_IRQn);
	DrvTIMER_EnableInt(TMR0);	
#endif
}

BIT vSampleCode_SendFixPacket(void)
{
  	qdt_uint8 ucPayloadIndex = 0;
  	qdt_uint8 ucEncodeBufferIndex = 0;
	qdt_uint16 ucRFCmdDataLen = 0;

	/*Build QDT Header Info*/
	pRadioConfiguration->Radio_Custom_Long_Payload[0] = QDTUID_TYPE_AUDIO;                 	//ucQdtUIDType
	pRadioConfiguration->Radio_Custom_Long_Payload[1] = g_UserSaveData.ucSerialNumber[0];  	//ucSrcSerialNumber1
	pRadioConfiguration->Radio_Custom_Long_Payload[2] = g_UserSaveData.ucSerialNumber[1];	//ucSrcSerialNumber2
	pRadioConfiguration->Radio_Custom_Long_Payload[3] = g_UserSaveData.ucSerialNumber[2];	//ucSrcSerialNumber3
	pRadioConfiguration->Radio_Custom_Long_Payload[4] = g_UserSaveData.ucSerialNumber[3];	//ucSrcSerialNumber4
	pRadioConfiguration->Radio_Custom_Long_Payload[5] = g_UserSaveData.ucSerialNumber[4];	//ucSrcSerialNumber4	
	if (g_Data.bNeedToBoardCastRFCmd)
		pRadioConfiguration->Radio_Custom_Long_Payload[6] = g_Data.ucBoardCastRFCmd; 						//ucRFSubCmd
	else
		pRadioConfiguration->Radio_Custom_Long_Payload[6] = RFCMD_NON_CMD; 						//ucRFSubCmd
		
	pRadioConfiguration->Radio_Custom_Long_Payload[7] = 0x00;  								//ucDestSerialNumber1
	pRadioConfiguration->Radio_Custom_Long_Payload[8] = 0x00;								//ucDestSerialNumber2
	pRadioConfiguration->Radio_Custom_Long_Payload[9] = 0x00; 								//ucDestSerialNumber3
	pRadioConfiguration->Radio_Custom_Long_Payload[10] = 0x00;								//ucDestSerialNumber4
	pRadioConfiguration->Radio_Custom_Long_Payload[11] = 0x00;								//ucDestSerialNumber4	
	pRadioConfiguration->Radio_Custom_Long_Payload[12] = 0x00;								//ucDatalength1
	pRadioConfiguration->Radio_Custom_Long_Payload[13] = 0x00;								//ucDatalength2
	pRadioConfiguration->Radio_Custom_Long_Payload[14] = 0x00;								//ucReserved
	pRadioConfiguration->Radio_Custom_Long_Payload[15] = QDT_Common_CheckSumCalc(pRadioConfiguration->Radio_Custom_Long_Payload, 15);		//ucCheckSum

	ucRFCmdDataLen = pRadioConfiguration->Radio_Custom_Long_Payload[QDT_RFPKT_DATALENGTH_START_IDX]<<8|pRadioConfiguration->Radio_Custom_Long_Payload[QDT_RFPKT_DATALENGTH_START_IDX+1];
  
  
 	for(ucPayloadIndex = (QDT_RFPKT_HEAD_LENGTH+ucRFCmdDataLen); ucPayloadIndex < (QDT_RFPKT_HEAD_LENGTH+ucRFCmdDataLen+40); ucPayloadIndex=ucPayloadIndex+2)
  	{
	  pRadioConfiguration->Radio_Custom_Long_Payload[ucPayloadIndex+1] = (s16Out_words[ucEncodeBufferIndex]>>8) & 0xFF;;
	  pRadioConfiguration->Radio_Custom_Long_Payload[ucPayloadIndex] = s16Out_words[ucEncodeBufferIndex] & 0xFF;;
	  ucEncodeBufferIndex++;
 	 }

#if 0
	if (g_FactoryData.bEnableMICSPKTest)
	{
		signed short AudioRXBuffer[20];
		qdt_uint8 ucRFPackageIndex = 0;
		for(ucPayloadIndex = (QDT_RFPKT_HEAD_LENGTH+ucRFCmdDataLen); ucPayloadIndex < (QDT_RFPKT_HEAD_LENGTH+ucRFCmdDataLen+40); ucPayloadIndex=ucPayloadIndex+2)
		{	
			AudioRXBuffer[ucRFPackageIndex] = pRadioConfiguration->Radio_Custom_Long_Payload[ucPayloadIndex] | (pRadioConfiguration->Radio_Custom_Long_Payload[ucPayloadIndex+1]<<8) /*| (ucRXBuffer[4*ucIndex+2]<<16) | (ucRXBuffer[4*ucIndex+3]<<24)*/;
			ucRFPackageIndex++;
		}
		playAudio(AudioRXBuffer);
	}
#endif

	
  	vRadio_StartTx(pRadioConfiguration->Radio_ChannelNumber, (U8 *) &pRadioConfiguration->Radio_Custom_Long_Payload[0u]);
    //u32ServiceTimeTick = DrvTIMER_GetTicks(TMR0);
 	 /* Packet sending initialized */
  	return TRUE;
}

/*---------------------------------------------------------------------------------------------------------*/
/*  Main Function									                                           			   */
/*---------------------------------------------------------------------------------------------------------*/
RETURN_TYPE AudioDocking_main (void)
{
	qdt_uint8 ucCmd;
	qdt_uint8 ucDumpRxData[32];
	Byte	 ucPacketLen;
	CAMERAPACKETDATA *CameraPacket;
	CameraPacket = (CAMERAPACKETDATA*) malloc(sizeof(CameraPacket));

	
	/*Initial MCU 9160/9361 HW Funtion*/
	InitialSystemClock();						
	LdoOn();  									//Enable GPA0~GPA7 power if no external power providing
	DrvADC_AnaOpen();
	UartInit();	 								//Set UART Configuration 
	InitialSPIPortMaster(); 					//Initial SPI and Set SPI as master
	GpioInit();									//Init AudioDocking GPIO config
	DrvPDMA_Init();								//Set PDMI Configuration

	/* Initial I2C function for PIR(CM36686) sensor */
	InitialI2C(); 
	InitialGpioPwm();     						// Set PWM Config
	CM36686Setup();								// Initial PIR(CM36686) sensor Config 
	
	InitialTimer();								//Initial Timer 

	/* Initial RF-446x */
// 	vRadio_Init();					
	
	/* initial qdt function */
	QDT_InitCmdBuffer();
	QDT_AD_Product_SyncWord();
	g_Data.ucSysState = SYSSTATE_STANDBY;

	/*Calibrate OSC*/
	InitCalibrateOSC();
	IsCountingStop();
	

	InitialADC();	  		//ADC initialization

#ifdef USE_AKM
	//Initial I2S engine
#ifdef USE_AUDIO_SAMPLEREATE_8K
	InitialI2S(8000);	/* Initial S7 Decode/Encode Function */
#endif
#ifdef USE_AUDIO_SAMPLEREATE_16K
	InitialI2S(16000);	/* Initial S7 Decode/Encode Function */
#endif

	AK7755_Init();
#endif


	S7Init();
	RecordStart();
	while(CallBackCounter == 0)	;
	PlayBufferSet();
	S7EncDec();
	while(CallBackCounter == 1)	;
	PlayStart();


	/* Set RF-466x into RX mode */
	//vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);

	/* Initial GPIO interrupt for RF-NIRQ */
    InitGPIOWakeupInSPD();

	QDT_Common_MicPause(); 							// Pause MIC input					
	QDT_Common_SPKPause();								// Pause SPK ouput	

	while(1)
	{
	    QDT_Common_ScanUserKey();   								//scan User press key btn
		QDT_GetkeyCmdData(&ucCmd, ucDumpRxData); 					//get audio docking RF command or need handle command 
		ucCmd = QDT_Common_CheckComboKey(ucCmd);
		switch (g_Data.ucSysState)
		{
		case SYSSTATE_STANDBY:
			QDT_AD_ProcessStandByKeyCmd(ucCmd, ucDumpRxData);				//handle stand by function
			break;
		case SYSSTATE_UNPAIRING:
			QDT_AD_ProcessPairingKeyCmd(ucCmd, ucDumpRxData);				//handle command main function
			QDT_AD_SearchDevices(ucDumpRxData);
			if (g_UserSaveData.ucPairingState != _QDT_PAIRINGSTATE_WAITFORSTAR_)
			QDT_Common_DelayTime(250);  				/*every 250ms sent out a Pairing Cmd*/
			
			QDT_AD_NightLEDOnOff();							//Detect PIR sensor to trigger Night LED 
			QDT_Common_IsRFalive(); 						//checking RF is alive
			break;	
		case SYSSTATE_RUNNING:
			if (g_Data.bMicBufferReady && g_Data.bRFTransmitFinished)
			{   
				//QDT_Uart_SentOutAudioRawData((signed short *)BufferReadyAddr);
				S7EncDec();
			}
			if (g_Data.bRFTransmitFinished && !g_Data.bTransmitBufferIsempty && g_Data.bParentCalling == qdt_false 
				&& g_Data.bRFReceiveFinished)
			{	
				if (g_Data.bHumanVoice == qdt_false)
 					vSampleCode_SendFixPacket();
			}				
			QDT_AD_ProcessKeyCmd(ucCmd, ucDumpRxData);				//handle command main function			
			QDT_AD_NightLEDOnOff();							//Detect PIR sensor to trigger Night LED 
			
			QDT_Common_IsRFalive(); 						//checking RF is alive
			
			break;				
		case SYSSTATE_UPGRADE:
			QDT_AD_UpgradeParentUnit(ucCmd, ucDumpRxData);
			QDT_AD_ProcessUpgradeKeyCmd(ucCmd, ucDumpRxData);
			QDT_Common_DelayTime(10); 
			break;
		case SYSSTATE_FACTORY:
			QDT_Factory_ProcessKeyCmd(ucCmd, ucDumpRxData);
			QDT_Common_IsRFalive(); 						//checking RF is alive
			break;
		default:
			QDT_AD_ProcessKeyCmd(ucCmd, ucDumpRxData);				//handle command main function
			break;
		}

		//handle UART cmd from Camera-7620 and Factory command
#if 1		
        {             
		
            if(QDT_Uart_GetUARTCmd(CameraPacket, &ucPacketLen))
                QDT_Uart_ProcessUARTCmd(CameraPacket, ucPacketLen);		
        }	
#endif		
		if(IsCountingStop()==TRUE)				//Calibrate the OSC freq to 49.152MHz
			StartPclkCount();
		
	}
}

#endif //BUILDTYPE_AUDIODOCKING_PROJECT
