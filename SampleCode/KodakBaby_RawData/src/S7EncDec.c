/*----------------------------------------------------------------------------------------*/
/*                                                                                        */
/* Copyright(c) 2011 Nuvoton Technology Corp. All rights reserved.                        */
/*                                                                                        */
/*----------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------*/
/* Include related headers                                                                                 */
/*---------------------------------------------------------------------------------------------------------*/
#include <stdio.h>
#include "ISD93xx.h"
#include "NVTTypes.h"
#include "bsp.h"
#include "qdt_utils.h"

#ifdef USE_AUDIO_CODEC_G711
#include "g711_func.h"
#elif (defined USE_AUDIO_CODEC_G722)
#include "Lib\LibSiren7.h"
#endif
/*---------------------------------------------------------------------------------------------------------*/
/* Macro, type and constant definitions                                                                    */
/*---------------------------------------------------------------------------------------------------------*/
#ifdef USE_AUDIO_CODEC_G711
#define BUFFER_SAMPLECOUNT 720	
#define COMPBUFSIZE     720//40   //According to S7BITRATE
volatile __align(4) BYTE s8Out_bytes[COMPBUFSIZE];

#elif (defined USE_AUDIO_CODEC_G722)

#define AUDIOBUFFERSIZE 320
#define DPWMSAMPLERATE  16000		 		// both 16Kbps & 32Kbps take around 13ms for encode then decode
#define S7BITRATE       16000//32000
#define S7BANDWIDTH     7000
#define COMPBUFSIZE     20//40   //According to S7BITRATE

volatile __align(4) signed short s16Out_words[COMPBUFSIZE];
#else
#define BUFFER_SAMPLECOUNT 360	
#define COMPBUFSIZE     360//40   //According to S7BITRATE
volatile __align(4) signed short s16Out_words[COMPBUFSIZE];
#endif
/*----------------------------------------------------------------------------------------*/
/* Global variables                                                                       */
/*----------------------------------------------------------------------------------------*/
#ifdef USE_AUDIO_CODEC_G722
static sSiren7_CODEC_CTL sEnDeCtl;
static sSiren7_ENC_CTX sS7Enc_Ctx;
static sSiren7_DEC_CTX sS7Dec_Ctx;
#endif

extern volatile uint32_t BufferReadyAddr;		//in RecordPCM.c
extern BOOL	bMicBufferReady;					//in RecordPCM.c
extern volatile uint32_t AudBufEmptyAddr;		//in PlayPCM.c


extern BIT gSampleCode_SendVariablePacket(void);
extern qdt_bool transmit_finish;
extern qdt_bool bTransmitBufferIsempty;

#if  1
volatile __align(4) signed short testSignal[320] = 
{
	0,12540,23171,30273,32767,30273,23170,12539,0,-12540,-23170,-30273,-32767,-30273,-23170,-12539,
	-1,12540,23170,30273,32766,30273,23170,12539,1,-12539,-23170,-30273,-32767,-30273,-23170,-12539,
	0,12539,23170,30273,32767,30274,23170,12539,1,-12540,-23169,-30273,-32767,-30273,-23170,-12539,
	-1,12539,23170,30272,32767,30273,23170,12539,0,-12539,-23170,-30273,-32767,-30273,-23170,-12539,
	1,12540,23170,30273,32767,30273,23170,12539,0,-12539,-23170,-30273,-32767,-30273,-23170,-12540,
	1,12540,23170,30272,32767,30273,23170,12539,1,-12540,-23170,-30274,-32766,-30273,-23170,-12540,
	0,12540,23169,30273,32767,30272,23169,12540,0,-12539,-23170,-30273,-32767,-30273,-23169,-12539,
	0,12540,23170,30272,32766,30273,23170,12539,0,-12540,-23169,-30273,-32767,-30273,-23170,-12539,
	0,12539,23170,30273,32767,30273,23170,12539,1,-12540,-23170,-30272,-32768,-30273,-23170,-12539,
	0,12539,23170,30272,32767,30273,23170,12540,-1,-12539,-23170,-30273,-32768,-30273,-23170,-12539,
	-1,12539,23169,30273,32767,30273,23170,12540,0,-12540,-23170,-30273,-32767,-30273,-23169,-12539,
	0,12540,23170,30273,32767,30272,23170,12539,0,-12540,-23170,-30273,-32767,-30273,-23169,-12540,
	0,12539,23170,30273,32767,30272,23170,12539,1,-12540,-23170,-30272,-32767,-30273,-23169,-12540,
	0,12539,23170,30273,32767,30273,23170,12540,-1,-12539,-23170,-30273,-32766,-30272,-23170,-12539,
	-1,12540,23170,30272,32767,30272,23169,12539,0,-12540,-23170,-30272,-32767,-30272,-23169,-12540,
	0,12540,23170,30273,32767,30273,23170,12538,-1,-12540,-23169,-30273,-32767,-30272,-23170,-12539,
	0,12539,23170,30272,32767,30273,23169,12539,0,-12540,-23170,-30273,-32767,-30273,-23169,-12540,
	0,12539,23170,30273,32767,30273,23169,12540,0,-12539,-23170,-30273,-32767,-30273,-23170,-12540,
	0,12539,23170,30272,32767,30273,23170,12539,0,-12539,-23170,-30273,-32768,-30272,-23169,-12539,
	0,12539,23170,30273,32767,30273,23170,12540,0,-12540,-23170,-30273,-32768,-30273,-23169,-12540,
};
#endif

/*----------------------------------------------------------------------------------------*/
/* Define functions prototype                                                             */
/*----------------------------------------------------------------------------------------*/

void S7EncDec(void)
{
	unsigned short b = 0xFFFF;
	//LibS7Encode(&sEnDeCtl, &sS7Enc_Ctx, testSignal, s16Out_words);  
#ifdef USE_AUDIO_CODEC_G711
	g711_linear2alaw((short *)BufferReadyAddr, (char *)s8Out_bytes, BUFFER_SAMPLECOUNT);
	//g711_linear2alaw((short *)testSignal, (char *)s8Out_bytes, BUFFER_SAMPLECOUNT);
	//g711_alaw2linear((char *)s8Out_bytes, (signed short *)AudBufEmptyAddr,	BUFFER_SAMPLECOUNT);
#elif (defined USE_AUDIO_CODEC_G722)
	LibS7Encode(&sEnDeCtl, &sS7Enc_Ctx, (signed short *)BufferReadyAddr, s16Out_words);  

#else
	memcpy(s16Out_words, (signed short *)BufferReadyAddr, sizeof(unsigned short)*BUFFER_SAMPLECOUNT);

#endif
    bTransmitBufferIsempty = qdt_false;
	//LibS7Decode(&sEnDeCtl, &sS7Dec_Ctx, s16Out_words, (signed short *)AudBufEmptyAddr);  
	bMicBufferReady=FALSE;

}

#ifdef USE_AUDIO_CODEC_G711
void playAudio(unsigned char *AudioRXBuffer)
{
	g711_alaw2linear((char *)AudioRXBuffer, (signed short *)AudBufEmptyAddr,	BUFFER_SAMPLECOUNT);
}
#elif (defined USE_AUDIO_CODEC_G722)
void playAudio(signed short *AudioRXBuffer)
{
	//LibS7Decode(&sEnDeCtl, &sS7Dec_Ctx, s16Out_words, (signed short *)AudBufEmptyAddr);  

	LibS7Decode(&sEnDeCtl, &sS7Dec_Ctx, AudioRXBuffer, (signed short *)AudBufEmptyAddr);  	
	//bMicBufferReady=FALSE;	
}
#else
void playAudio(signed short *AudioRXBuffer)
{
	memcpy((signed short *)AudBufEmptyAddr, AudioRXBuffer, sizeof(unsigned short)*BUFFER_SAMPLECOUNT);
}
#endif


#ifdef USE_AUDIO_CODEC_G722
void S7Init(void)
{
    LibS7Init(&sEnDeCtl,S7BITRATE,S7BANDWIDTH);
	
	QDT_DEBUG("sEnDeCtl.bit_rate = %d\n", sEnDeCtl.bit_rate);
	QDT_DEBUG("sEnDeCtl.bandwidth = %d\n", sEnDeCtl.bandwidth);
	QDT_DEBUG("sEnDeCtl.number_of_bits_per_frame = %d\n", sEnDeCtl.number_of_bits_per_frame);
	QDT_DEBUG("sEnDeCtl.number_of_regions = %d\n", sEnDeCtl.number_of_regions);
	QDT_DEBUG("sEnDeCtl.frame_size = %d\n", sEnDeCtl.frame_size);   
	
    LibS7EnBufReset(sEnDeCtl.frame_size,&sS7Enc_Ctx);
    LibS7DeBufReset(sEnDeCtl.frame_size,&sS7Dec_Ctx);
}
#endif

