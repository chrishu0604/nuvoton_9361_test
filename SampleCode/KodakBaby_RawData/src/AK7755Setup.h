
// autogenerate PRAM file
#ifndef _AK7755SETUP_H
#define	_AK7755SETUP_H

#define AK7755_SlaveAddr	0x18
#define SIZE_TRANSFER 	15
#define CMD_CRAM_DL		0xB4
#define	CMD_PRAM_DL		0xB8
#define AK77XX_DSP_PRAM_BUF_SIZE 198
#define AK77XX_DSP_CRAM_BUF_SIZE	51

#define SIZE_OF_PRAM	AK77XX_DSP_PRAM_BUF_SIZE
#define SIZE_OF_CRAM	AK77XX_DSP_CRAM_BUF_SIZE

#define AK77XX_DSP_PRAM_SIZE   4860
#define AK77XX_DSP_CRAM_SIZE	6144


typedef enum { kI2CWritingAK7755, kI2CReadingAK7755, kI2CAK7755Idle} I2CAK7755RWMode_t;

void AK7755_Init(void);
void AK7755_PTTMode_Init(void);
void AK7755_MusicMode_Init(void);
void AK7755_CRAM_Init(void);
void AK7755_PRAM_Init(void);

#endif		// end of _AK77XX_

