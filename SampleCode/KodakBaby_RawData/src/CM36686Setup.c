#include <stdio.h>
#include "ISD93xx.h"
#include "Driver\DrvI2C.h"
#include "Driver\DrvGPIO.h"
#include "CM36686Setup.h"
#include "qdt_ram.h"

uint8_t Device_Addr0 = 0x60;	 			/* CM36686 Device ID */
uint8_t Tx_Data0[3];
uint8_t DataCnt0;
uint8_t Rx_Data_High;
uint8_t Rx_Data_Low;
uint8_t DataLen0;
volatile uint8_t EndFlag0 = 0;
typedef enum { kI2CWritingCM36686_A, kI2CReadingCM36686_A, kI2CIdle} I2CRWMode_t;
I2CRWMode_t I2CRWMode = kI2CIdle;


void I2C0_Callback_Tx(uint32_t status)
{
	if (status == 0x08)						/* START has been transmitted */
	{
		I2C0->DATA = 0;
		I2C0->DATA = (Device_Addr0<<1);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x18)				/* SLA+W has been transmitted and ACK has been received */
	{
		I2C0->DATA = Tx_Data0[DataLen0++];
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x20)				/* SLA+W has been transmitted and NACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 1, 1, 1, 0);
	}
	else if (status == 0x28)				/* DATA has been transmitted and ACK has been received */
	{
		if (DataLen0 != 3)
		{
			DrvI2C_WriteData(I2C_PORT0, Tx_Data0[DataLen0++]);
			DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
		}
		else
		{
			DrvI2C_Ctrl(I2C_PORT0, 0, 1, 1, 0);
			EndFlag0 = 1;
		}
	}
	else
	{
		QDT_WARNING("Status 0x%x is NOT processed\n", status);
	}
}
void I2C0_Callback_Rx(uint32_t status)
{
	if (status == 0x08)					   	/* START has been transmitted and prepare SLA+W */
	{
		DrvI2C_WriteData(I2C_PORT0, Device_Addr0<<1);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x18)				/* SLA+W has been transmitted and ACK has been received */
	{
		DrvI2C_WriteData(I2C_PORT0, Tx_Data0[DataLen0++]);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x20)				/* SLA+W has been transmitted and NACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 1, 1, 1, 0);
	}
	else if (status == 0x28)				/* DATA has been transmitted and ACK has been received */
	{
		if (DataLen0 != 1)
		{
			DrvI2C_WriteData(I2C_PORT0, Tx_Data0[DataLen0++]);
			DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
		}
		else
		{
			DrvI2C_Ctrl(I2C_PORT0, 1, 0, 1, 0);			//repeat start
		}
	}
	else if (status == 0x10)				/* Repeat START has been transmitted and prepare SLA+R */
	{
		DrvI2C_WriteData(I2C_PORT0, Device_Addr0<<1 | 0x01);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x40)				/* SLA+R has been transmitted and ACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 1);
	}
	else if (status == 0x48)				/* SLA+R has been transmitted and Not-ACK has been received */
	{
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x50)				/* DATA has been received and ACK has been returned */
	{
		Rx_Data_Low = DrvI2C_ReadData(I2C_PORT0);
		DrvI2C_Ctrl(I2C_PORT0, 0, 0, 1, 0);
	}
	else if (status == 0x58)				/* DATA has been received and NACK has been returned */
	{
		Rx_Data_High = DrvI2C_ReadData(I2C_PORT0);
		DrvI2C_Ctrl(I2C_PORT0, 0, 1, 1, 0);
		EndFlag0 = 1;
	}
	else
	{
		QDT_WARNING("Status 0x%x is NOT processed\n", status);
	}
}

void Write_CM36686_A (uint8_t addr, uint8_t Data_lsb, uint8_t Data_msb)
{
	if (I2CRWMode != kI2CWritingCM36686_A)
	{
		I2CRWMode = kI2CWritingCM36686_A;
		DrvI2C_UninstallCallBack(I2C_PORT0, I2CFUNC);
		DrvI2C_InstallCallback(I2C_PORT0, I2CFUNC, I2C0_Callback_Tx);
	}

	Device_Addr0 = 0x60;
	Tx_Data0[0] = addr;
	Tx_Data0[1] = Data_lsb;
	Tx_Data0[2] = Data_msb;
	DataLen0 = 0;
	EndFlag0 = 0;
		DrvI2C_Ctrl(I2C_PORT0, 1, 0, 0, 0);
	while (EndFlag0 == 0);
	EndFlag0 = 0;
	I2CRWMode = kI2CIdle;
}
uint32_t Read_CM36686_A (uint8_t addr)
{
	
	if (I2CRWMode != kI2CReadingCM36686_A)
	{
		I2CRWMode = kI2CReadingCM36686_A;
		DrvI2C_UninstallCallBack(I2C_PORT0, I2CFUNC);
		DrvI2C_InstallCallback(I2C_PORT0, I2CFUNC, I2C0_Callback_Rx);
	}
	Device_Addr0 = 0x60;
	Tx_Data0[0] = addr;
	DataLen0 = 0;
	EndFlag0 = 0;
	DrvI2C_Ctrl(I2C_PORT0, 1, 0, 0, 0);
	while (EndFlag0 == 0);
	EndFlag0 = 0;
	I2CRWMode = kI2CIdle;
	return (Rx_Data_Low <<8 | Rx_Data_High);
}

void WriteVerify_CM36686_A (uint8_t addr, uint32_t dataL, uint32_t dataH)
{
	uint32_t retVal;
	uint8_t data[2];

	Write_CM36686_A(addr, dataL, dataH);
	retVal = Read_CM36686_A(addr);
	//QDT_DEBUG("WriteVerify_CM36686_A   Read_CM36686_A  retVal : %x \n",retVal);
	data[0] = retVal>>8;
	data[1] = retVal & 0xff;
	if((data[0] != dataL) || (data[1] != dataH))
		QDT_ERROR("I2C - Addr %x Expect %x got %x\n",addr,data,retVal);
}

//-----------------------------------------------------
// FUNCTION NAME: set_als_cmd
//
// DESCRIPTION:
//-----------------------------------------------------
void set_als_cmd_disable_int(void)
{
	//I2Cm_bWriteBytes(CM36686_SLAVE_ADD, ALS_CONF, 0x44, 0x00, 2);
	//Write_CM36686_A(ALS_CONF, 0x44, 0x00);
	WriteVerify_CM36686_A(ALS_CONF, 0x44, 0x00);
}

//-----------------------------------------------------
// FUNCTION NAME: set_als_cmd
//
// DESCRIPTION:
//-----------------------------------------------------
void set_als_cmd_enable_int(void)
{
	WriteVerify_CM36686_A(ALS_CONF, 0x46, 0x00);
}

//-----------------------------------------------------
// FUNCTION NAME: set_als_int_thd
//
// DESCRIPTION:
//-----------------------------------------------------
void set_als_int_thd(uint32_t thd_high, uint32_t thd_Low)
{
	uint8_t lsb, msb;
	lsb = (uint8_t)(thd_high&0x00ff);
	msb = (uint8_t)(thd_high>>8);
	WriteVerify_CM36686_A(ALS_THDH, lsb, msb);
	
	lsb = (uint8_t)(thd_Low&0x00ff);
	msb = (uint8_t)(thd_Low>>8);
	WriteVerify_CM36686_A(ALS_THDL, lsb, msb);
}

//-----------------------------------------------------
// FUNCTION NAME: set_ps_cmd1
//
// DESCRIPTION:
//-----------------------------------------------------
void set_ps_cmd1(void)
{
	WriteVerify_CM36686_A(PS_CONF1_2, 0x10, 0x03);  //enable interrupt
	//WriteVerify_CM36686_A(PS_CONF1_2, 0x01, 0x00);  //disable interrupt 
}

//-----------------------------------------------------
// FUNCTION NAME: set_ps_cmd2
//
// DESCRIPTION:
//-----------------------------------------------------
void set_ps_cmd2(void)
{
	WriteVerify_CM36686_A(PS_CONF3, 0x10, 0x20);
}

//-----------------------------------------------------
// FUNCTION NAME: set_ps2_thd
//
// DESCRIPTION:
//-----------------------------------------------------
void set_ps_thdL(uint8_t ps1_thdl, uint8_t ps1_thdh)
{
	WriteVerify_CM36686_A(PS1_THDL, ps1_thdl, ps1_thdh);
}

//-----------------------------------------------------
// FUNCTION NAME: set_ps2_thd
//
// DESCRIPTION:
//-----------------------------------------------------
void set_ps_thdH(uint8_t ps1_thdl, uint8_t ps1_thdh)
{
	WriteVerify_CM36686_A(PS2_THDH, ps1_thdl, ps1_thdh);
}


//-----------------------------------------------------
// FUNCTION NAME: read_CM36686_als
//
// DESCRIPTION:
//-----------------------------------------------------
uint32_t read_CM36686_als(void)
{
	uint8_t lsb,msb;
	uint32_t als_value;
	uint32_t data;
	
	data = Read_CM36686_A(ALS_DATA);
	//QDT_DEBUG("read_CM36686_als	data : %x	 \n",data);
	lsb = data>>8;
	msb = data & 0xff;
	als_value = (uint32_t)msb;
	als_value = ((als_value<<8)|(uint32_t)lsb);
	return als_value;
}

//-----------------------------------------------------
// FUNCTION NAME: read_CM36686_ps
//
// DESCRIPTION:
//-----------------------------------------------------
uint32_t read_CM36686_ps(void)
{
	uint8_t lsb,msb;
	uint32_t ps_value;
	uint32_t data;
	
	data = Read_CM36686_A(PS_DATA);
	//QDT_DEBUG("read_CM36686_ps	data : %x	 \n",data);
	lsb = data>>8;
	msb = data & 0xff;
	ps_value = (uint32_t)msb;
	ps_value = ((ps_value<<8)|(uint32_t)lsb);
	return ps_value;
}

//-----------------------------------------------------
// FUNCTION NAME: read_CM36686_int_flag
//
// DESCRIPTION:
// int_flat[0] = PS1_IF_AWAY
// int_flat[1] = PS1_IF_CLOSE
// int_flat[2] = Reserved
// int_flat[3] = Reserved
// int_flat[4] = ALS_IF_H
// int_flat[5] = ALS_IF_L
// int_flat[6] = PS1_SPELAG
// int_flat[7] = Reserved
//-----------------------------------------------------
void read_CM36686_int_flag(void)
{
	uint8_t lsb;
	uint8_t msb;
	uint32_t data;
	int i;
	
	data = Read_CM36686_A(INT_FLAG);
	lsb = data>>8;
	msb = data & 0xff;
	for (i=0;i<8;i++){
	if((msb>>i)&0x01)
		g_Data.int_flat[i] = TRUE;
	else
		g_Data.int_flat[i] = FALSE;
	}
}

void CM36686Setup(void)
{
	uint32_t als_value;
	
	set_als_cmd_disable_int();
	als_value = read_CM36686_als();
	//QDT_DEBUG("CM36686Setup    als_value : %x  \n",als_value);
	set_als_int_thd((als_value+300), (als_value-300));
	set_ps_thdL(0x16,0x00);
	set_ps_thdH(0x18, 0x00);
	set_ps_cmd1();
	set_ps_cmd2();
	//set_als_cmd_enable_int();
}

uint8_t CM36683_INT_Level(void)
{
    uint8_t retValue;
    retValue = (uint8_t)DrvGPIO_GetBit(GPA, 13);
    //QDT_DEBUG("CM36683_INT_Level  value : %d\n",retValue);
    return retValue;
}


