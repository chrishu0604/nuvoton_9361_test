
#ifndef PMU_H
#define PMU_H

void PMU_SetAlarmTime(uint32_t u32TAR_Time);
void PMU_SetCurrentTime(uint32_t u32TLR_Time);
void PMU_RTCInit(void);
void PMU_TickISR(void);	
void PMU_Alarm_ISR(void);
void PMU_Init(void);
void PMU_RTCClose(void);
void PMU_EnterStandbyPowerDown(void);
void PMU_StandbyPowerDown(void);
void PMU_EnterSleepMode(void);
void PMU_ExitSleepMode(void);

#define STBRAM_IDX_STBCOUNTER		0x00
#define STBRAM_IDX_SYSSTATE			0x01
#define STBRAM_IDX_ALERTEVENT		0x02   //0:Audio alert,


#endif

