#ifdef BUILDTYPE_PARENTUINT_PROJECT

#include <stdio.h>
#include <string.h>
#include "ISD93xx.h"
#include "Driver\DrvUART.h"
#include "Driver\DrvGPIO.h"
#include "Driver\DrvSYS.h"
#include "Driver\DrvQSPI.h"
#include "Driver\DrvPDMA.h"
#include "Driver\DrvADC.h"
#include "Driver\DrvI2C.h"
#include "Driver\DrvTIMER.h"
#include "Driver\DrvRTC.h"
#include "NVTTypes.h"
#include "PMU.h"


#include "bsp.h"
#include "radio.h"
#include "qdt_ram.h"
#include "qdt_utils.h"
#include "qdt_fifo.h"
#include "qdt_flash_hal.h"
#include "qdt_sysState.h"
#include "qdt_parentunit.h"
#include "qdt_parentunit_func.h"
#include "qdt_common_func.h"
#include "qdt_uart_func.h"
#include "qdt_factory_func.h"

#include "qdt_human_voice.h"


//#include "si446x_api_lib.h"

/*---------------------------------------------------------------------------------------------------------*/
/* Extern Function Prototypes                                                                              */
/*---------------------------------------------------------------------------------------------------------*/
extern void InitialSPIPortMaster(void);
extern void PlayBufferSet(void);
extern void StartPclkCount(void);
extern void InitCalibrateOSC (void);
extern BOOL IsCountingStop(void);
extern void RecordStart(void);
extern void InitialADC(void);

/*---------------------------------------------------------------------------------------------------------*/
/* Define Function Prototypes                                                                              */
/*---------------------------------------------------------------------------------------------------------*/
void InitialUART(void);
void RecordStart(void);
void PlayStart(void);

void S7Init(void);
void S7EncDec(void);

void SysTimerDelay(uint32_t us);
void Delay(uint32_t delayCnt);


void RF446x_Pollhandler(void);
BIT vSampleCode_SendFixPacket(void);


/*------------------------------------------------------------------------*/
/*                            Local Macros                                */
/*------------------------------------------------------------------------*/
#define PACKET_SEND_INTERVAL  5000u
#define DUTY_LED_INTENSITY		1000
/*------------------------------------------------------------------------*/
/*                          Local variables                               */
/*------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------------------------------------*/
/* Define global variables                                                                                 */
/*---------------------------------------------------------------------------------------------------------*/
GlobalData g_Data;

extern volatile uint32_t CallBackCounter;	//in RecordPCM.c
extern qdt_bool bPdmaTxDone;
extern qdt_bool bPdmaRxDone;
extern qdt_bool bPdmaRxDone, bPdmaTxDone, bIsTestOver;

extern volatile __align(4) signed short s16Out_words[40];
extern const qdt_uint8 g_ucFWVersion[13];


uint8_t bMain_IT_Status;
uint16_t lPer_MsCnt;


uint8_t ucTransmitBufferIdx = 0;
//signed short AudioRXBuffer[20];
#ifdef SUPPORT_SLEEP_MODE
qdt_uint16 ucSleepLEDLevel = 0;
qdt_bool bSleepLEDDirection  = qdt_true;
qdt_uint16 ucSleepLEDBrightLevel[10] =  {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};	
#endif

//====================
// Functions & main

void UartInit(void)
{
	STR_UART_T sParam;
	//DrvGPIO_InitFunction(FUNC_UART0);
    //DrvGPIO_InitFunction(FUNC_UART0_FLOW);
    DrvGPIO_UART_TXRX_PA8_PA9();
	//DrvGPIO_UART_RTSCTS_PA10_PA11();
	
    sParam.u32BaudRate 		= 115200;
    sParam.u8cDataBits 		= DRVUART_DATABITS_8;
    sParam.u8cStopBits 		= DRVUART_STOPBITS_1;
    sParam.u8cParity 		= DRVUART_PARITY_NONE;
    sParam.u8cRxTriggerLevel= DRVUART_FIFO_4BYTES;
	//sParam.u8TimeOut 			= 0;

	if(DrvUART_Open(UART_PORT0,&sParam) == 0) 
	{
		//DrvUART_EnableInt(UART_PORT0, DRVUART_RDAINT,interrupt_callback_uart_function);
		UART0->IER.AUTO_CTS_EN = 1;
     	//UART0->IER.AUTO_RTS_EN = 1;

	}

	printf("\n\n\n\n\nPARENT UNIT MCU BUILD: %s %s\n", __TIME__, __DATE__);
	printf("Version: %x.%x.%x.%x \n", g_ucFWVersion[9], g_ucFWVersion[10], g_ucFWVersion[11], g_ucFWVersion[12]);
	printf("#################### UART_DEBUG_READY ####################\n");
}


void interrupt_callback_function(uint32_t u32GpaStatus, uint32_t u32GpbStatus)
{

	GPIOB->ISRC = GPIOB->ISRC;
	GPIOA->ISRC = GPIOA->ISRC;
	if (u32GpbStatus&0x80)
	gRadio_CheckTxRxStatus();
	if (u32GpbStatus&0x40)
		QDT_AddKeyCmdData(_USER_KEY_PARENTUNIT_KEY_ALERT, NULL);
}


void InitGPIOWakeupInSPD(void)
{
    // Clear interrupts.
	GPIOA->ISRC = GPIOA->ISRC;
	GPIOB->ISRC = GPIOB->ISRC;
	////INT from GPA pin1 as an example wake up
    //DrvGPIO_SetIntCallback(interrupt_callback_CheckRF_function);
    DrvGPIO_SetIntCallback(interrupt_callback_function);
    DrvGPIO_SetDebounceTime(3, DBCLKSRC_HCLK);
    DrvGPIO_EnableDebounce(GPB, 7);
	//DrvGPIO_EnableInt(GPB, 7, IO_FALLING, MODE_EDGE);  //enable RF-4461 interrupt Pin
	DrvGPIO_DisableInt(GPB, 7);  						    //disable RF-4461 interrupt Pin
    DrvGPIO_EnableInt(GPA, 4, IO_FALLING, MODE_EDGE);      //enable power key interrupt Pin
    DrvGPIO_EnableInt(GPB, 6, IO_FALLING, MODE_EDGE);      //enable PTT key interrupt Pin
}

void GpioInit(void)
{
	DrvGPIO_Open(GPA,15, IO_OUTPUT); //RF-Si4463 shoutdown PIN
	DrvGPIO_Open(GPB,1, IO_OUTPUT); //RF-Si4463 SPI SSB PIN
	DrvGPIO_Open(GPB,7, IO_INPUT); //RF-Si4463 nIRQ PIN
	DrvGPIO_Open(GPB,2, IO_OUTPUT); //PWR_CTRL PIN

	DrvGPIO_Open(GPA,4, IO_INPUT); //power key 
	DrvGPIO_Open(GPA,5, IO_INPUT); //volume up 
	DrvGPIO_Open(GPA,6, IO_INPUT); //volume down 
	DrvGPIO_Open(GPA,7, IO_INPUT); //low battery 
	//DrvGPIO_Open(GPA,10, IO_INPUT); //PTT(push to talk key)  revB
    //DrvGPIO_Open(GPA,11, IO_INPUT); //Alert key(play alert sound) revB

	DrvGPIO_Open(GPB,5, IO_INPUT); //PTT(push to talk key) 
    DrvGPIO_Open(GPB,6, IO_INPUT); //Alert key(play alert sound)	
    DrvGPIO_Open(GPA,10, IO_INPUT); //DC_DET

	DrvGPIO_Open(GPA,12, IO_OUTPUT); // Intensity LED_G1
	DrvGPIO_Open(GPB,13, IO_OUTPUT); // Intensity LED_R1
	DrvGPIO_Open(GPB,14, IO_OUTPUT); // Intensity LED_G2
	DrvGPIO_Open(GPB,15, IO_OUTPUT); // Intensity LED_R2
	
	DrvGPIO_Open(GPA,13, IO_OUTPUT); //Alert LED
	DrvGPIO_Open(GPA,14, IO_OUTPUT); //Power LED

	DrvGPIO_Open(GPB,0, IO_OUTPUT); //low battery LED
	DrvGPIO_Open(GPB,3, IO_OUTPUT); //Vibrator

	//DrvGPIO_SetBit(GPA,12);//Intensity LED tune on for CES		
	//DrvGPIO_SetBit(GPA,13);//Alert LED tune on for CES		
	//DrvGPIO_ClrBit(GPA,14);//Power Light tune on for CES		
	//DrvGPIO_ClrBit(GPB,0);//ow battery LED tune on for CES	

	SYS->GPA_ALT.GPA2              =0;  //Disable 9160 SPI CS funtion. it be controled at qdt_flash_hal.c
	DrvGPIO_Open(GPA,2, IO_OUTPUT); //SPI FLASH SSB0
	QDT_TRACE("GPIO Init successful\n");
	
	if (SYSCLK->PFLAGCON.PD_FLAG==0)
	{
	PARENTUNIT_LED_PWR_RED(qdt_ON);
	PARENTUNIT_LED_PWR_GREEN(qdt_OFF);	
	
	PARENTUNIT_LED_ALERT_ORANGE(qdt_OFF);
	PARENTUNIT_VIBRATOR(qdt_OFF);
	
	PARENTUNIT_LED_INTENSITY_G1(qdt_OFF);
	PARENTUNIT_LED_INTENSITY_G2(qdt_OFF);
	PARENTUNIT_LED_INTENSITY_R1(qdt_OFF);
	PARENTUNIT_LED_INTENSITY_R2(qdt_OFF);
	}
	
}

void LdoOn(void)
{
	SYSCLK->APBCLK.ANA_EN=1;
	ANA->LDOPD.PD=0;
	ANA->LDOSET=3;
}
/*---------------------------------------------------------------------------------------------------------*/
/* SysTimerDelay                                                                                           */
/*---------------------------------------------------------------------------------------------------------*/
void SysTimerDelay(uint32_t us)
{
#ifdef USE_SYSTEM_CLOCK_48MHZ	
    SysTick->LOAD = us * 48; /* Assume the internal 49MHz RC used */
#endif
#ifdef USE_SYSTEM_CLOCK_98MHZ	
    SysTick->LOAD = us * 98; /* Assume the internal 49MHz RC used */
#endif
    SysTick->VAL  =  (0x00);
    SysTick->CTRL = (1 << SYSTICK_CLKSOURCE) | (1<<SYSTICK_ENABLE);

    /* Waiting for down-count to zero */
    while((SysTick->CTRL & (1 << 16)) == 0);
}

void InitialSystemClock(void)
{
#if 0
    /* Unlock the protected registers */	
	UNLOCKREG();
	SYSCLK->PWRCON.OSC49M_EN = 1;
	SYSCLK->CLKSEL0.HCLK_S = 0; /* Select HCLK source as 48MHz */ 
	SYSCLK->CLKDIV.HCLK_N  = 0;	/* Select no division          */
	SYSCLK->CLKSEL0.OSCFSel = 0;	/* 1= 32MHz, 0=48MHz */
	//SYSCLK->CLKSEL0.STCLK_S = 3; /* Use internal HCLK */
	SYSCLK->PWRCON.XTL32K_EN = 1;
#endif
	DrvSYS_UnlockKeyAddr();
    DrvSYS_SetOscCtrl(E_SYS_OSC49M, 1);
    DrvSYS_SetOscCtrl(E_SYS_OSC10K, 1);
    DrvSYS_SetOscCtrl(E_SYS_XTL32K, 1);
#ifdef USE_SYSTEM_CLOCK_48MHZ	
	DrvSYS_SetHCLK(E_DRVSYS_48M, 1);
#endif
#ifdef USE_SYSTEM_CLOCK_98MHZ	
	DrvSYS_SetHCLK(E_DRVSYS_96M, 1);
#endif

	DrvSYS_LockKeyAddr();	
}
void InitialI2C(void)
{
	/*  GPIO initial and select operation mode for I2C*/
    //DrvGPIO_InitFunction(FUNC_I2C0); //Set I2C I/O
    DrvGPIO_I2C_PA10_PA11();
	//DrvI2C_Open(I2C_PORT0, (DrvSYS_GetHCLK() * 1000), 12000);  //clock = 24Kbps
	DrvI2C_Open(I2C_PORT0, (DrvSYS_GetHCLK() * 1000), 48000);  //clock = 48Kbps
	DrvI2C_EnableInt(I2C_PORT0); //Enable I2C0 interrupt and set corresponding NVIC bit
    DrvI2C_Ctrl(I2C_PORT0, 1, 0, 0, 0);
}



void TMR_CallBack10msTimer()
{
	static qdt_bool bRedLEDOnOff = 0;
	static qdt_bool bGreenLEDOnOff = 0;
	static qdt_bool bOrangeLEDOnOff = 0;
	static qdt_bool bVibrationOnOff = 0;
	static qdt_uint8 uCount = 0;
	static qdt_bool bNeedRecoveredLED = qdt_false;
	g_Data.uc10MSCount++;
	g_Data.uc500MSCount++;


	if (g_Data.uc10MSCount == 100)  //1000ms
	{
	    g_Data.uc10MSCount = 0;
	
		if (g_Data.bVibrationEnable)
		{
			if(uCount < 10)
			{
				bVibrationOnOff = ~ bVibrationOnOff;
				PARENTUNIT_VIBRATOR(bVibrationOnOff);
				uCount++;
			}
			else
			{	
				g_Data.bTwinkleOrangeLED = qdt_false; //when parent got audio alert, twinkle led 10 sec, then turn on led until user cancel it.
				PARENTUNIT_LED_ALERT_ORANGE(qdt_ON);
			}
		}
		else
		{
			uCount = 0;
			bVibrationOnOff = 0;
		}
	
	}

	if (g_Data.uc500MSCount == 50) //500ms
	{
		g_Data.uc500MSCount = 0;
		if (!QDT_PU_IsDCConnected())
		{
			g_Data.bIsBatteryMode = qdt_true;
			if (g_Data.bUserCancelQuietMode)
			{
				g_Data.bIsQuietMode = qdt_false;
			}
			else
			{
				g_Data.bIsQuietMode = qdt_true;
				QDT_Common_SPKPause();
			}
		}
		else
			g_Data.bIsBatteryMode = qdt_false;


		if (QDT_PU_IsGetLowPowerSignal())
		{		
			g_Data.bTwinkleGreenLED = qdt_true;
			g_Data.bTwinkleRedLED = qdt_true;
			g_Data.bIsLowBattery = qdt_true;
			bNeedRecoveredLED = qdt_true;
		}
		else
		{
			g_Data.bIsLowBattery = qdt_false;
			
			if (bNeedRecoveredLED)
			{
				g_Data.bTwinkleGreenLED = qdt_false;
				g_Data.bTwinkleRedLED = qdt_false;			
				if (g_Data.ucSysState == SYSSTATE_STANDBY)
				{
					PARENTUNIT_LED_PWR_RED(qdt_ON);
					PARENTUNIT_LED_PWR_GREEN(qdt_OFF);					
				}
				else if (g_Data.ucSysState == SYSSTATE_UNPAIRING)
				{
					g_Data.bTwinkleRedLED = qdt_true;
					PARENTUNIT_LED_PWR_RED(qdt_ON);
					PARENTUNIT_LED_PWR_GREEN(qdt_OFF);					
				}
				else if (g_Data.ucSysState == SYSSTATE_RUNNING || g_Data.ucSysState == SYSSTATE_WAKEUP)
				{
					PARENTUNIT_LED_PWR_RED(qdt_OFF);
					PARENTUNIT_LED_PWR_GREEN(qdt_ON);					
				}		
				else 
				{
					PARENTUNIT_LED_PWR_RED(qdt_OFF);
					PARENTUNIT_LED_PWR_GREEN(qdt_ON);					
				}	

			}
		}
		
		if (g_Data.bTwinkleGreenLED && g_Data.bTwinkleRedLED)
		{
			PARENTUNIT_LED_PWR_GREEN(bGreenLEDOnOff);
			PARENTUNIT_LED_PWR_RED(bRedLEDOnOff);
			bRedLEDOnOff = ~bRedLEDOnOff;
			bGreenLEDOnOff = ~bGreenLEDOnOff;
		}
		else if (g_Data.bTwinkleGreenLED)
		{
			PARENTUNIT_LED_PWR_GREEN(bGreenLEDOnOff);
			bGreenLEDOnOff = ~ bGreenLEDOnOff;
		}
		else if (g_Data.bTwinkleRedLED)
		{
			PARENTUNIT_LED_PWR_RED(bRedLEDOnOff);
			bRedLEDOnOff = ~ bRedLEDOnOff;
		}		

		if (g_Data.bTwinkleOrangeLED)
		{
			PARENTUNIT_LED_ALERT_ORANGE(bOrangeLEDOnOff);
			bOrangeLEDOnOff = ~ bOrangeLEDOnOff;
		}		
	}


	
}


void InitialTimer(void)
{
	DrvTIMER_Init();
	DrvSYS_SetIPClockSource(E_SYS_TMR0_CLKSRC,2);

    /* Using TIMER0 PERIODIC_MODE , 2 tick /sec */
	DrvTIMER_Open(TMR0, 1000, PERIODIC_MODE);  
                		    

	DrvTIMER_SetTimerEvent(TMR0,10, (TIMER_CALLBACK)TMR_CallBack10msTimer,0);		
	
}



BIT vSampleCode_SendFixPacket(void)
{
  	qdt_uint8 ucPayloadIndex = 0;
  	qdt_uint8 ucEncodeBufferIndex = 0;
	qdt_uint16 ucRFCmdDataLen = 0;

	/*Build QDT Header Info*/
	pRadioConfiguration->Radio_Custom_Long_Payload[0] = QDTUID_TYPE_AUDIO;                 	//ucQdtUIDType
	pRadioConfiguration->Radio_Custom_Long_Payload[1] = g_UserSaveData.ucSerialNumber[0];  	//ucSrcSerialNumber1
	pRadioConfiguration->Radio_Custom_Long_Payload[2] = g_UserSaveData.ucSerialNumber[1];	//ucSrcSerialNumber2
	pRadioConfiguration->Radio_Custom_Long_Payload[3] = g_UserSaveData.ucSerialNumber[2];	//ucSrcSerialNumber3
	pRadioConfiguration->Radio_Custom_Long_Payload[4] = g_UserSaveData.ucSerialNumber[3];	//ucSrcSerialNumber4
	pRadioConfiguration->Radio_Custom_Long_Payload[5] = g_UserSaveData.ucSerialNumber[4];	//ucSrcSerialNumber5	
	pRadioConfiguration->Radio_Custom_Long_Payload[6] = RFCMD_PARENTCOMMAND_PUSHTOTALKING; 	//ucRFSubCmd
	pRadioConfiguration->Radio_Custom_Long_Payload[7] = 0x00;  								//ucDestSerialNumber1
	pRadioConfiguration->Radio_Custom_Long_Payload[8] = 0x00;								//ucDestSerialNumber2
	pRadioConfiguration->Radio_Custom_Long_Payload[9] = 0x00; 								//ucDestSerialNumber3
	pRadioConfiguration->Radio_Custom_Long_Payload[10] = 0x00;								//ucDestSerialNumber4
	pRadioConfiguration->Radio_Custom_Long_Payload[11] = 0x00;								//ucDestSerialNumber5	
	pRadioConfiguration->Radio_Custom_Long_Payload[12] = 0x00;								//ucDatalength1
	pRadioConfiguration->Radio_Custom_Long_Payload[13] = 0x00;								//ucDatalength2
	pRadioConfiguration->Radio_Custom_Long_Payload[14] = 0x00;								//ucReserved
	pRadioConfiguration->Radio_Custom_Long_Payload[15] = QDT_Common_CheckSumCalc(pRadioConfiguration->Radio_Custom_Long_Payload, 15);		//ucCheckSum
  
	ucRFCmdDataLen = pRadioConfiguration->Radio_Custom_Long_Payload[QDT_RFPKT_DATALENGTH_START_IDX]<<8|pRadioConfiguration->Radio_Custom_Long_Payload[QDT_RFPKT_DATALENGTH_START_IDX+1];

  
 	for(ucPayloadIndex = (QDT_RFPKT_HEAD_LENGTH+ucRFCmdDataLen); ucPayloadIndex < (QDT_RFPKT_HEAD_LENGTH+ucRFCmdDataLen+40); ucPayloadIndex=ucPayloadIndex+2)
  	{
	  pRadioConfiguration->Radio_Custom_Long_Payload[ucPayloadIndex+1] = (s16Out_words[ucEncodeBufferIndex]>>8) & 0xFF;;
	  pRadioConfiguration->Radio_Custom_Long_Payload[ucPayloadIndex] = s16Out_words[ucEncodeBufferIndex] & 0xFF;;
	  ucEncodeBufferIndex++;
 	 }
 	#if 0
	QDT_Common_SPKResume();
	ucTransmitBufferIdx = 0;
 	for(ucPayloadIndex = (QDT_RFPKT_HEAD_LENGTH+ucRFCmdDataLen); ucPayloadIndex < (QDT_RFPKT_HEAD_LENGTH+ucRFCmdDataLen+40); ucPayloadIndex=ucPayloadIndex+2)
	{
  		AudioRXBuffer[ucTransmitBufferIdx] = pRadioConfiguration->Radio_Custom_Long_Payload[2*ucPayloadIndex] | (pRadioConfiguration->Radio_Custom_Long_Payload[2*ucPayloadIndex+1]<<8)/* | (pRadioConfiguration->Radio_Custom_Long_Payload[4*ucIndex+2]<<16) | (pRadioConfiguration->Radio_Custom_Long_Payload[4*ucIndex+3]<<24)*/;
		ucTransmitBufferIdx++;
	}
    playAudio(AudioRXBuffer);
	#endif
  	vRadio_StartTx(pRadioConfiguration->Radio_ChannelNumber, (U8 *) &pRadioConfiguration->Radio_Custom_Long_Payload[0u]);
     
 	 /* Packet sending initialized */
  	return TRUE;
}

/*---------------------------------------------------------------------------------------------------------*/
/*  Main Function									                                           			   */
/*---------------------------------------------------------------------------------------------------------*/
RETURN_TYPE ParentUnit_main (void)
{
	qdt_uint8 ucCmd;
	qdt_uint8 ucDumpRxData[32];
#ifdef SUPPORT_SLEEP_MODE			
	static qdt_uint32 ucNonKeyCmdCounter = 0;
	static qdt_uint32 u32LoopCounter = 0;
#endif
	/*Initial MCU 9160/9361 HW Funtion*/
	InitialSystemClock();						
	LdoOn();  									//Enable GPA0~GPA7 power if no external power providing
	DrvADC_AnaOpen();
	UartInit();	 								//Set UART Configuration 
	InitialSPIPortMaster(); 					//Initial SPI and Set SPI as master
	GpioInit();									//Init AudioDocking GPIO config
	DrvPDMA_Init();								//Set PDMI Configuration
	InitialTimer();								//Initial Timer 

	/* Initial RF-446x */
	//vRadio_Init();					
	
	/*Init RTC for StandBy/Sleep Mode*/
	
	//PMU_EnterStandbyPowerDown();
	/* initial qdt function */
	QDT_InitCmdBuffer();
	g_Data.ucPreSysState = SYSSTATE_COLDBOOT;
	g_Data.ucSysState = SYSSTATE_STANDBY;

	/*Calibrate OSC*/
	InitCalibrateOSC();
	IsCountingStop();
	
	/* Initial S7 Decode/Encode Function */
	InitialADC();	  		//ADC initialization
	
	S7Init();
	RecordStart();
	while(CallBackCounter == 0)	;
	PlayBufferSet();
	S7EncDec();
	while(CallBackCounter == 1)	;
	PlayStart();


	/* Set RF-466x into RX mode */
	//vRadio_StartRX(pRadioConfiguration->Radio_ChannelNumber);

	/* Initial GPIO interrupt for RF-NIRQ */
    InitGPIOWakeupInSPD();

	QDT_Common_MicPause();
	QDT_Common_SPKPause();
	
#ifdef SUPPORT_SLEEP_MODE
	//QDT_PU_CheckIsWakeUp();
	//QDT_PU_SysEnterSleep();
	//QDT_PU_SysPowerOff();
	PMU_EnterSleepMode();
#endif	

	while(1)
	{
	    QDT_Common_ScanUserKey();   					//scan User press key btn
		QDT_GetkeyCmdData(&ucCmd, ucDumpRxData); 					//get audio docking RF command or need handle command 
		QDT_Common_CheckComboKey(ucCmd);
		
#ifdef SUPPORT_SLEEP_MODE
		if (ucCmd == _USER_KEY_NONE && g_Data.ucSysState != SYSSTATE_SLEEP)
			ucNonKeyCmdCounter++;
		else
			ucNonKeyCmdCounter = 0;
#endif		
		switch (g_Data.ucSysState)
		{
		case SYSSTATE_STANDBY:
			QDT_PU_ProcessStandByKeyCmd(ucCmd, ucDumpRxData);				//handle stand by function
			break;
		case SYSSTATE_UNPAIRING:
			QDT_PU_ProcessPairingKeyCmd(ucCmd, ucDumpRxData);				//handle command main function
#ifdef USER_SELECT_FREQ
            QDT_PU_PingAudioDocking();
#endif			
			//QDT_Common_IsRFalive(); 						//checking RF is alive
			break;	
		case SYSSTATE_RUNNING:
		case SYSSTATE_WAKEUP:
			QDT_PU_PushToTalk();
			QDT_PU_ProcessKeyCmd(ucCmd, ucDumpRxData);				//handle command main function
				
			QDT_Common_IsRFalive(); 						//checking RF is alive
			break;	
#ifdef SUPPORT_SLEEP_MODE			
		case SYSSTATE_SLEEP:
			QDT_PU_ProcessSleepModeKeyCmd(ucCmd, ucDumpRxData);
			break;
#endif		
		case SYSSTATE_FACTORY:
			QDT_Factory_ProcessKeyCmd(ucCmd, ucDumpRxData);
			break;
		case SYSSTATE_UPGRADE:
			QDT_PU_ProcessUpgradeKeyCmd(ucCmd, ucDumpRxData);
			break;
		default:
			QDT_PU_ProcessKeyCmd(ucCmd, ucDumpRxData);				//handle command main function
			break;
		}
		
		if(IsCountingStop()==TRUE)				//Calibrate the OSC freq to 49.152MHz
			StartPclkCount();	
#if 0
		if  (ucNonKeyCmdCounter > 0x10000 && 		g_Data.bRFTransmitFinished == qdt_true &&
		 g_Data.bRFReceiveFinished == qdt_true)
		{
			si446x_get_modem_status(0);
			ucNonKeyCmdCounter = 0;
		}
#endif		
#ifdef SUPPORT_SLEEP_MODE
		
		if (g_Data.ucSysState != SYSSTATE_SLEEP  && ucNonKeyCmdCounter > 0x100000  && g_Data.bIsBatteryMode  && !g_Data.bDisableSleepMode)
		{
			ucNonKeyCmdCounter = 0;
			
			g_Data.bUserCancelQuietMode = qdt_false;
			g_Data.bIsQuietMode = qdt_true;	
			g_Data.bEnableSleepLED = qdt_true;
			
			ucSleepLEDLevel = 0;
			bSleepLEDDirection = qdt_true;
			PMU_Init();
			QDT_PU_SysEnterSleep();
			PMU_RTCInit();
			PMU_SetCurrentTime(0x00180000); 		//18:00:00
			PMU_SetAlarmTime(0x00180100);			//18:01:00	
			SysTimerDelay(1000);			
			PMU_EnterSleepMode();
			//PMU_EnterStandbyPowerDown();
		}

		u32LoopCounter++;
		if (g_Data.bEnableSleepLED && u32LoopCounter > 8000 && g_Data.bIsBatteryMode)
		{
			u32LoopCounter = 0;
			PWMA->CMR0=ucSleepLEDBrightLevel[ucSleepLEDLevel];
			PWMB->CMR0=ucSleepLEDBrightLevel[ucSleepLEDLevel];
			PWMA->CMR2=ucSleepLEDBrightLevel[ucSleepLEDLevel];
			PWMA->CMR3=ucSleepLEDBrightLevel[ucSleepLEDLevel];

			if (bSleepLEDDirection)
			{
				ucSleepLEDLevel+=1;
				if (ucSleepLEDLevel >= 10)
				{
					ucSleepLEDLevel = 10;
					bSleepLEDDirection = qdt_false;
				}
			}
			else
			{
		
				if (ucSleepLEDLevel == 0)
				{
					ucSleepLEDLevel = 0;
					bSleepLEDDirection = qdt_true;
				}
				else
				{
					ucSleepLEDLevel-=1;
			
				}	

			}

		}				
#endif
	}

}


#endif //BUILDTYPE_PARENTUINT_PROJECT
