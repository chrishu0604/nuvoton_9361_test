/*! @file radio.h
 * @brief This file is contains the public radio interface functions.
 *
 * @b COPYRIGHT
 * @n Silicon Laboratories Confidential
 * @n Copyright 2012 Silicon Laboratories, Inc.
 * @n http://www.silabs.com
 */

#ifndef RADIO_H_
#define RADIO_H_

/*****************************************************************************
 *  Global Macros & Definitions
 *****************************************************************************/
/*! Maximal packet length definition (FIFO size) */
#define RADIO_MAX_PACKET_LENGTH     64u

/*! Maximal long packet length */
//#define RADIO_MAX_LONG_PACKET_LENGTH (4u * RADIO_MAX_PACKET_LENGTH)
#define RADIO_MAX_LONG_PACKET_LENGTH (12u * RADIO_MAX_PACKET_LENGTH +2)


/*! Threshold for TX FIFO */
#define RADIO_TX_ALMOST_EMPTY_THRESHOLD 50 /*50u 500k*/ /*48u 320k*/ /*48u  304k*/ /*30u  192k~288k*/

/*! Threshold for TX FIFO */
#define RADIO_RX_ALMOST_FULL_THRESHOLD 58 /*55u 500k*/ /*48u  304k*/  /*48u  192k~288k*/

/*****************************************************************************
 *  Global Typedefs & Enums
 *****************************************************************************/
typedef struct
{
    U8   *Radio_ConfigurationArray;

    U8   Radio_ChannelNumber;
    U16   Radio_PacketLength;
    U8   Radio_State_After_Power_Up;

    U16  Radio_Delay_Cnt_After_Reset;

    U8   *Radio_Custom_Long_Payload;
} tRadioConfiguration;

/*****************************************************************************
 *  Global Variable Declarations
 *****************************************************************************/
extern  SEGMENT_VARIABLE_SEGMENT_POINTER(pRadioConfiguration, tRadioConfiguration, SEG_CODE, SEG_CODE);
extern  SEGMENT_VARIABLE(fixRadioPacket[RADIO_MAX_LONG_PACKET_LENGTH], U8, SEG_XDATA);


/*! Si446x configuration array */
extern  SEGMENT_VARIABLE(Radio_Configuration_Data_Array[], U8, SEG_CODE);

/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
void  vRadio_Init(void);
BIT   gRadio_CheckTransmitted(void);
void  vRadio_StartTx(U8, U8 *);
BIT   gRadio_CheckReceived(void);
void  vRadio_StartRX(U8);
BIT gRadio_CheckTxRxStatus(void);
void vRadio_DeInit(void);


#endif /* RADIO_H_ */
