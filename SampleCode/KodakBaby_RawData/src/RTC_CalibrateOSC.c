/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright(c) 2009 Nuvoton Technology Corp. All rights reserved.                                         */
/*                                                                                                         */
/* Siren7 (G.722) licensed from Polycom Technology                                                         */
/*---------------------------------------------------------------------------------------------------------*/
#include <stdio.h>
#include "ISD93xx.h"
#include "Driver\DrvSYS.h"
#include "Driver\DrvRTC.h"

#include "NVTTypes.h"


#define	MAX_OSCTRIM	0xCF			//Here is a sample, in real case this value may be too fast for operation.
#define	MIN_OSCTRIM	0x80
#define STEP	8 					//SUPERFINE step, should be even number
uint8_t	u8CurrentOSCTRIM, u8RtcCount;
int8_t	i8CurrentSUPERFINE;
uint16_t	u16Pcounter;
BOOL	bCalibrationStop;

void InitCalibrateOSC (void)
{
	UNLOCKREG();
	DrvRTC_Init();
	SYSCLK->APBCLK.ANA_EN=1;
	i8CurrentSUPERFINE=0;
	ANA->TRIM.SUPERFINE=0;
	u8CurrentOSCTRIM=SYS->OSCTRIM[0].TRIM;
	u16Pcounter=0;
	bCalibrationStop=TRUE;
	ANA->FREQ_CTRL.FM_SEL=1;
	ANA->FREQ_CTRL.FM_CYCLE=19;		// (19+1) RTC clocks, will get 30000 PCLK
	ANA->FREQ_CTRL.FM_GO = FALSE;	//Disable counting and reset counter
}

void StartPclkCount(void)
{
	ANA->FREQ_CTRL.FM_GO=TRUE;		//Start counting
	bCalibrationStop=FALSE;
}

void IncreaseOSC(void)
{
	UNLOCKREG();
	i8CurrentSUPERFINE= i8CurrentSUPERFINE+STEP;
	if(i8CurrentSUPERFINE>0)
		ANA->TRIM.SUPERFINE=i8CurrentSUPERFINE;	   	//Only let SUPERFINE work with the value 0~127
	else											//Handle for SUPERFINE value 128~255 (negative)
	{	
		if(u8CurrentOSCTRIM < MAX_OSCTRIM)
		{
			i8CurrentSUPERFINE=0;
			ANA->TRIM.SUPERFINE=i8CurrentSUPERFINE;
			u8CurrentOSCTRIM++;
			if((u8CurrentOSCTRIM % 0x20)==0)				//Sequential increasing during 0x20N * N ~ (0x20 * N + 0x1F)
				u8CurrentOSCTRIM=u8CurrentOSCTRIM+0x10;		//Frequency of 0x20 * N is close to 0x20 * (N-1) + 0x10
			SYS->OSCTRIM[0].TRIM= u8CurrentOSCTRIM;	
		}
		else
		{
			i8CurrentSUPERFINE= i8CurrentSUPERFINE-STEP; 	//keep original SUPERFINE and OSCTRIM
		}
	}
}


void DecreaseOSC(void)
{
	UNLOCKREG();
	i8CurrentSUPERFINE= i8CurrentSUPERFINE-STEP;
	if(i8CurrentSUPERFINE>=0)
		ANA->TRIM.SUPERFINE=i8CurrentSUPERFINE;	   		//Only let SUPERFINE work with the value 0~127
	else												//Handle for SUPERFINE value 128~255 (negative)
	{
		if(u8CurrentOSCTRIM > MIN_OSCTRIM)
		{
			i8CurrentSUPERFINE=0x80-STEP;
			ANA->TRIM.SUPERFINE=i8CurrentSUPERFINE;
			u8CurrentOSCTRIM--;
			if((u8CurrentOSCTRIM % 0x20)==0x1F)		   		//Sequential increasing during 0x20N * N ~ (0x20 * N + 0x1F)
				u8CurrentOSCTRIM=u8CurrentOSCTRIM-0x10;		//Frequency of 0x20 * N is close to 0x20 * (N-1) + 0x10
			SYS->OSCTRIM[0].TRIM= u8CurrentOSCTRIM;	
		}
		else
		{
			i8CurrentSUPERFINE= i8CurrentSUPERFINE+STEP; 	//keep original SUPERFINE and OSCTRIM
		}
	}
}

BOOL IsCountingStop(void)
{
	if( ANA->FREQ_CTRL.FM_DONE == 1)
	{
		u16Pcounter= ANA->FREQ_CNT;
		ANA->FREQ_CTRL.FM_GO= FALSE;
		bCalibrationStop= TRUE;
#ifdef 	USE_SYSTEM_CLOCK_48MHZ	
		if (u16Pcounter > 30030)
			DecreaseOSC();
		else if (u16Pcounter < 29970)
				IncreaseOSC();
#endif		
#ifdef 	USE_SYSTEM_CLOCK_98MHZ	
			if (u16Pcounter > 30030*2)
				DecreaseOSC();
			else if (u16Pcounter < 29970*2)
				IncreaseOSC();
#endif	
	}
	
	return(bCalibrationStop);
}




