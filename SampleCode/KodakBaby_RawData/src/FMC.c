#include <stdio.h>
#include "ISD93xx.h"
//#include "Platform.h"
#include "Driver/DrvGPIO.h"
#include "Driver/DrvFMC.h"
#include "FMC.h"
#include "qdt_utils.h"


int FMC_Write(unsigned int address, unsigned int data)
{
    unsigned int Reg;
    
    outp32(ISPCMD, ISP_Program);
    outp32(ISPADR, address);
    outp32(ISPDAT, data);
    outp32(ISPTRG, ISPGO); 
    
    __ISB();
    
    Reg = inp32(FISPCON);
    if (Reg & ISPFF)
    {
    	outp32(FISPCON, Reg);
    	return -1;
    }
    return 0;
}


int FMC_Read(unsigned int address, unsigned int * data)
{
    unsigned int Reg;

    outp32(ISPCMD, ISP_Read);
    outp32(ISPADR, address);
    outp32(ISPDAT, 0x00000000);
	outp32(ISPTRG, ISPGO); 
    
    __ISB();

    Reg = inp32(FISPCON);
    if (Reg & ISPFF)
    {
    	outp32(FISPCON, Reg);
    	return -1;
    }
 
	*data = inp32(ISPDAT);

    return 0;
}


int FMC_Erase(unsigned int address)
{
    unsigned int Reg;
    
    outp32(ISPCMD, ISP_PageErase);
    outp32(ISPADR, address);
    outp32(ISPTRG, ISPGO); 

	__ISB();
    
    Reg = inp32(FISPCON);
    if (Reg & ISPFF)
    {
    	outp32(FISPCON, Reg);
    	return -1;
    }
    return 0;
}


void ReadData(unsigned int addr_start, unsigned int addr_end, unsigned int* data)    // Read data from flash
{
    unsigned int rLoop;

    for ( rLoop = addr_start; rLoop < addr_end; rLoop += 4 ) 
    {     
		DrvFMC_Read(rLoop, data);
		data++;
    }
    return;
}

void WriteData(unsigned int addr_start, unsigned int addr_end, unsigned int *data)  // Write data into flash
{
    unsigned int wLoop;
    
    for ( wLoop = addr_start; wLoop < addr_end; wLoop+=4 ) 
    {
        DrvFMC_Write(wLoop, *data);
        data++;
    }
}
void UpdateConfig(unsigned int data1, unsigned int data2)
{
	unsigned int readData1, readData2;
	FMC->ISPCON.CFGUEN = 1;//enable config update
    FMC_Erase(Config0);
    
   	FMC_Write(Config0, data1);
   	FMC_Write(Config1, data2);
     
	FMC_Read(Config0, &readData1);
	FMC_Read(Config1, &readData2);

	QDT_DEBUG("Config0 = 0x%lx\n", readData1);
	QDT_DEBUG("Config1 = 0x%lx\n", readData2);
     
    FMC->ISPCON.CFGUEN = 0;//disable config update
	
	FMC->ISPCON.SWRST = 1;//enable config update
}




