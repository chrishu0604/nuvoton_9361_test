
/*
 * CM36686Setup.h
 *
 */

#ifndef _CM36686SETUP_H
#define	_CM36686SETUP_H


//-------------------------------------------------------
// Global definition for CM36686 Registers
//-------------------------------------------------------
#define CM36686_SLAVE_ADD 0x60
#define ALS_CONF 0x00
#define ALS_THDH 0x01
#define ALS_THDL 0x02
#define PS_CONF1_2 0x03
#define PS_CONF3 0x04
#define PS_CANC 0x05
#define PS1_THDL 0x06
#define PS2_THDH 0x07
#define PS_DATA 0x08
#define ALS_DATA 0x09
#define White_DATA 0x0A
#define INT_FLAG 0x0B
#define CM36686_ID 0x0C

void CM36686Setup(void);
uint8_t CM36683_INT_Level(void);
void read_CM36686_int_flag(void);

#endif 


