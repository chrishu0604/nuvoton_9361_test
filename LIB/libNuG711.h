
/*
 * libNuG711.h
 * Nuvoton G711 CODEC implementation
 * Header file for CCITT conversion routines.
 *
 */

#ifndef _G711_H
#define	_G711_H

extern int linear2alaw(int pcm_val);
extern int alaw2linear(int a_val);
extern int linear2ulaw(int pcm_val);
extern 	int ulaw2linear(int u_val);
extern int alaw2ulaw(int aval);
extern int ulaw2alaw(int uval);

#endif 


